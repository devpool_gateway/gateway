/**
 * WalletWebServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.rp.service;

public interface WalletWebServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getWalletWebServiceImplPortAddress();

    public com.nirvasoft.rp.service.WalletSoapService getWalletWebServiceImplPort() throws javax.xml.rpc.ServiceException;

    public com.nirvasoft.rp.service.WalletSoapService getWalletWebServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
