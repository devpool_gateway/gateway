package com.nirvasoft.rp.service;

public class WalletSoapServiceProxy implements com.nirvasoft.rp.service.WalletSoapService {
  private String _endpoint = null;
  private com.nirvasoft.rp.service.WalletSoapService walletSoapService = null;
  
  public WalletSoapServiceProxy() {
    _initWalletSoapServiceProxy();
  }
  
  public WalletSoapServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initWalletSoapServiceProxy();
  }
  
  private void _initWalletSoapServiceProxy() {
    try {
      walletSoapService = (new com.nirvasoft.rp.service.WalletWebServiceImplServiceLocator()).getWalletWebServiceImplPort();
      if (walletSoapService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)walletSoapService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)walletSoapService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (walletSoapService != null)
      ((javax.xml.rpc.Stub)walletSoapService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.nirvasoft.rp.service.WalletSoapService getWalletSoapService() {
    if (walletSoapService == null)
      _initWalletSoapServiceProxy();
    return walletSoapService;
  }
  
  public com.nirvasoft.rp.service.TransferResData transferIn(java.lang.String apiKey, java.lang.String transID, java.lang.String fromOperator, java.lang.String fromAccount, java.lang.String fromWallet, java.lang.String fromName, java.lang.String toOperator, java.lang.String toAccount, java.lang.String toWallet, java.lang.String toName, double amount, double commission, java.lang.String narration, java.lang.String t1, java.lang.String t2, java.lang.String t3) throws java.rmi.RemoteException{
    if (walletSoapService == null)
      _initWalletSoapServiceProxy();
    return walletSoapService.transferIn(apiKey, transID, fromOperator, fromAccount, fromWallet, fromName, toOperator, toAccount, toWallet, toName, amount, commission, narration, t1, t2, t3);
  }
  
  public com.nirvasoft.rp.service.TransferConfirmResData transferconfirm(java.lang.String apikey, java.lang.String transid, java.lang.String switchkey, java.lang.String refkey, java.lang.String t1, java.lang.String t2, java.lang.String t3) throws java.rmi.RemoteException{
    if (walletSoapService == null)
      _initWalletSoapServiceProxy();
    return walletSoapService.transferconfirm(apikey, transid, switchkey, refkey, t1, t2, t3);
  }
  
  
}