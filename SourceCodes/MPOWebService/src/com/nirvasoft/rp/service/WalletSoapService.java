/**
 * WalletSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.rp.service;

public interface WalletSoapService extends java.rmi.Remote {
    public com.nirvasoft.rp.service.TransferResData transferIn(java.lang.String apiKey, java.lang.String transID, java.lang.String fromOperator, java.lang.String fromAccount, java.lang.String fromWallet, java.lang.String fromName, java.lang.String toOperator, java.lang.String toAccount, java.lang.String toWallet, java.lang.String toName, double amount, double commission, java.lang.String narration, java.lang.String t1, java.lang.String t2, java.lang.String t3) throws java.rmi.RemoteException;
    public com.nirvasoft.rp.service.TransferConfirmResData transferconfirm(java.lang.String apikey, java.lang.String transid, java.lang.String switchkey, java.lang.String refkey, java.lang.String t1, java.lang.String t2, java.lang.String t3) throws java.rmi.RemoteException;
}
