/**
 * WalletWebServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.rp.service;

public class WalletWebServiceImplServiceLocator extends org.apache.axis.client.Service implements com.nirvasoft.rp.service.WalletWebServiceImplService {

    public WalletWebServiceImplServiceLocator() {
    }


    public WalletWebServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WalletWebServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WalletWebServiceImplPort
    private java.lang.String WalletWebServiceImplPort_address = "http://192.168.203.249:8080/WalletServiceOK/walletwebservice";

    public java.lang.String getWalletWebServiceImplPortAddress() {
        return WalletWebServiceImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WalletWebServiceImplPortWSDDServiceName = "WalletWebServiceImplPort";

    public java.lang.String getWalletWebServiceImplPortWSDDServiceName() {
        return WalletWebServiceImplPortWSDDServiceName;
    }

    public void setWalletWebServiceImplPortWSDDServiceName(java.lang.String name) {
        WalletWebServiceImplPortWSDDServiceName = name;
    }

    public com.nirvasoft.rp.service.WalletSoapService getWalletWebServiceImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WalletWebServiceImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWalletWebServiceImplPort(endpoint);
    }

    public com.nirvasoft.rp.service.WalletSoapService getWalletWebServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.nirvasoft.rp.service.WalletWebServiceImplPortBindingStub _stub = new com.nirvasoft.rp.service.WalletWebServiceImplPortBindingStub(portAddress, this);
            _stub.setPortName(getWalletWebServiceImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWalletWebServiceImplPortEndpointAddress(java.lang.String address) {
        WalletWebServiceImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.nirvasoft.rp.service.WalletSoapService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.nirvasoft.rp.service.WalletWebServiceImplPortBindingStub _stub = new com.nirvasoft.rp.service.WalletWebServiceImplPortBindingStub(new java.net.URL(WalletWebServiceImplPort_address), this);
                _stub.setPortName(getWalletWebServiceImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WalletWebServiceImplPort".equals(inputPortName)) {
            return getWalletWebServiceImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.rp.nirvasoft.com/", "WalletWebServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.rp.nirvasoft.com/", "WalletWebServiceImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WalletWebServiceImplPort".equals(portName)) {
            setWalletWebServiceImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
