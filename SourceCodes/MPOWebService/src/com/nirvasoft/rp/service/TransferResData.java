/**
 * TransferResData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.rp.service;

public class TransferResData  implements java.io.Serializable {
    private java.lang.String code;

    private java.lang.String desc;

    private java.lang.String refKey;

    private java.lang.String t1;

    private java.lang.String t2;

    private java.lang.String t3;

    private java.lang.String transID;

    public TransferResData() {
    }

    public TransferResData(
           java.lang.String code,
           java.lang.String desc,
           java.lang.String refKey,
           java.lang.String t1,
           java.lang.String t2,
           java.lang.String t3,
           java.lang.String transID) {
           this.code = code;
           this.desc = desc;
           this.refKey = refKey;
           this.t1 = t1;
           this.t2 = t2;
           this.t3 = t3;
           this.transID = transID;
    }


    /**
     * Gets the code value for this TransferResData.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this TransferResData.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this TransferResData.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this TransferResData.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the refKey value for this TransferResData.
     * 
     * @return refKey
     */
    public java.lang.String getRefKey() {
        return refKey;
    }


    /**
     * Sets the refKey value for this TransferResData.
     * 
     * @param refKey
     */
    public void setRefKey(java.lang.String refKey) {
        this.refKey = refKey;
    }


    /**
     * Gets the t1 value for this TransferResData.
     * 
     * @return t1
     */
    public java.lang.String getT1() {
        return t1;
    }


    /**
     * Sets the t1 value for this TransferResData.
     * 
     * @param t1
     */
    public void setT1(java.lang.String t1) {
        this.t1 = t1;
    }


    /**
     * Gets the t2 value for this TransferResData.
     * 
     * @return t2
     */
    public java.lang.String getT2() {
        return t2;
    }


    /**
     * Sets the t2 value for this TransferResData.
     * 
     * @param t2
     */
    public void setT2(java.lang.String t2) {
        this.t2 = t2;
    }


    /**
     * Gets the t3 value for this TransferResData.
     * 
     * @return t3
     */
    public java.lang.String getT3() {
        return t3;
    }


    /**
     * Sets the t3 value for this TransferResData.
     * 
     * @param t3
     */
    public void setT3(java.lang.String t3) {
        this.t3 = t3;
    }


    /**
     * Gets the transID value for this TransferResData.
     * 
     * @return transID
     */
    public java.lang.String getTransID() {
        return transID;
    }


    /**
     * Sets the transID value for this TransferResData.
     * 
     * @param transID
     */
    public void setTransID(java.lang.String transID) {
        this.transID = transID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferResData)) return false;
        TransferResData other = (TransferResData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.refKey==null && other.getRefKey()==null) || 
             (this.refKey!=null &&
              this.refKey.equals(other.getRefKey()))) &&
            ((this.t1==null && other.getT1()==null) || 
             (this.t1!=null &&
              this.t1.equals(other.getT1()))) &&
            ((this.t2==null && other.getT2()==null) || 
             (this.t2!=null &&
              this.t2.equals(other.getT2()))) &&
            ((this.t3==null && other.getT3()==null) || 
             (this.t3!=null &&
              this.t3.equals(other.getT3()))) &&
            ((this.transID==null && other.getTransID()==null) || 
             (this.transID!=null &&
              this.transID.equals(other.getTransID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getRefKey() != null) {
            _hashCode += getRefKey().hashCode();
        }
        if (getT1() != null) {
            _hashCode += getT1().hashCode();
        }
        if (getT2() != null) {
            _hashCode += getT2().hashCode();
        }
        if (getT3() != null) {
            _hashCode += getT3().hashCode();
        }
        if (getTransID() != null) {
            _hashCode += getTransID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferResData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.rp.nirvasoft.com/", "transferResData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("t1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "t1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("t2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "t2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("t3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "t3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
