package com.nirvasoft.mpo.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.nirvasoft.mpo.mgr.TransferMgr;
import com.nirvasoft.mpo.shared.TransferInResData;
import com.nirvasoft.mpo.shared.TransferReqData;
import com.nirvasoft.mpo.shared.TransferResData;
import com.nirvasoft.mpo.util.WriteXML;

@Path("/walletservice")
public class WalletService {

	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;
	
	
	@POST
	@Path("walletcredittransferin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferInResData walletcredittransferin(TransferReqData req) {
		TransferInResData res = new TransferInResData();
		TransferMgr dbmgr = new TransferMgr();
		
		//ValidateReqParameter
		res = validateMPOTransfer(req);
		
		if(!res.getCode().equals("0000")){
			return res;
		}
		
		//GenerateRefKey
		res = dbmgr.getRandomRefKey();
		req.setRefkey(res.getRefkey());
		
		//XML Write
		try {
			WriteXML.ModifyBalanceXmlFile(req.getAmount());
			try {
				WriteXML.WriteTransactionsXmlFile(req);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//res.setTransid(req.getTransid());
				res.setRefkey(res.getRefkey());
				res.setCode("0014");
				res.setDesc("Internal server error!Please contact to administrator");
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//res.setTransid(req.getTransid());
			res.setRefkey(res.getRefkey());
			res.setCode(res.getCode());
			res.setDesc("Internal server error!Please contact to administrator");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//res.setTransid(req.getTransid());
			res.setRefkey(res.getRefkey());
			res.setCode(res.getCode());
			res.setDesc("Internal server error!Please contact to administrator");
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//res.setTransid(req.getTransid());
			res.setRefkey(res.getRefkey());
			res.setCode(res.getCode());
			res.setDesc("Internal server error!Please contact to administrator");
		}
		
		if(res.getCode().equals("0000")){
			//res.setTransid(req.getTransid());
			res.setRefkey(res.getRefkey());
			res.setCode(res.getCode());
			res.setDesc("Credit Transferred successfully.");
		}else{
			//res.setTransid(req.getTransid());
			res.setRefkey(res.getRefkey());
			res.setCode(res.getCode());
			res.setDesc("Credit Transfer failed.");
		}
		return res;
	}
	
	@POST
	@Path("walletcredittransferconfirm")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferResData walletcredittransferconfirm(TransferReqData req) {
		TransferResData res = new TransferResData();
		
		//ValidateReqParameter
		res = validateConfirm(req);
		
		if(!res.getCode().equals("0000")){
			return res;
		}
		res.setTransid(req.getTransid());
		res.setRefkey(res.getRefkey());
		res.setCode(res.getCode());
		res.setDesc("Credit transfer confirmation.");
		return res;
	}
	
	public TransferInResData validateMPOTransfer(TransferReqData aData){
		TransferInResData res = new TransferInResData();
		res.setCode("0000");
		if(aData.getApikey().equals("") || aData.getApikey() == null || !aData.getApikey().equals("MPO123456")){
			res.setCode("0014");
			res.setDesc("Invalid Request Parameter");
			return res;
		}
		
		if(aData.getTransid().equals("") || aData.getTransid() == null){
			res.setCode("0014");
			res.setDesc("Transaction ID is required");
			return res;
		}
		
		if(aData.getFromoperator().equals("") || aData.getFromoperator() == null){
			res.setCode("0014");
			res.setDesc("Sender Operator is required");
			return res;
		}
		
		if(aData.getFromaccount().equals("") || aData.getFromaccount() == null){
			res.setCode("0014");
			res.setDesc("Sender Account is required");
			return res;
		}
		
		if(aData.getFromwallet().equals("") || aData.getFromwallet() == null){
			res.setCode("0014");
			res.setDesc("Sender Wallet is required");
			return res;
		}
		
		if(aData.getTooperator().equals("") || aData.getTooperator() == null){
			res.setCode("0014");
			res.setDesc("Beneficiary Operator is required");
			return res;
		}
		
		if(aData.getToaccount().equals("") || aData.getToaccount() == null){
			res.setCode("0014");
			res.setDesc("Beneficiary Account is required");
			return res;
		}
		
		if(aData.getTowallet().equals("") || aData.getTowallet() == null){
			res.setCode("0014");
			res.setDesc("Beneficiary Wallet is required");
			return res;
		}
		
		if(aData.getAmount() <= 0.00){
			res.setCode("0014");
			res.setDesc("Invalid Amount");
			return res;
		}
		
		return res;
	}
	
	public TransferInResData validateTransferIn(TransferReqData aData){
		TransferInResData res = new TransferInResData();
		res.setCode("0000");
		if(aData.getApikey().equals("") || aData.getApikey() == null || !aData.getApikey().equals("MPO123456")){
			res.setCode("0014");
			res.setDesc("Invalid Request Parameter");
			return res;
		}
		
		if(aData.getMpokey().equals("") || aData.getMpokey() == null){
			res.setCode("0014");
			res.setDesc("Transaction ID is required");
			return res;
		}
		
		if(aData.getFromoperator().equals("") || aData.getFromoperator() == null){
			res.setCode("0014");
			res.setDesc("Sender Operator is required");
			return res;
		}
		
		if(aData.getFromaccount().equals("") || aData.getFromaccount() == null){
			res.setCode("0014");
			res.setDesc("Sender Account is required");
			return res;
		}
		
		if(aData.getFromwallet().equals("") || aData.getFromwallet() == null){
			res.setCode("0014");
			res.setDesc("Sender Wallet is required");
			return res;
		}
		
		if(aData.getTooperator().equals("") || aData.getTooperator() == null){
			res.setCode("0014");
			res.setDesc("Beneficiary Operator is required");
			return res;
		}
		
		if(aData.getToaccount().equals("") || aData.getToaccount() == null){
			res.setCode("0014");
			res.setDesc("Beneficiary Account is required");
			return res;
		}
		
		if(aData.getTowallet().equals("") || aData.getTowallet() == null){
			res.setCode("0014");
			res.setDesc("Beneficiary Wallet is required");
			return res;
		}
		
		if(aData.getAmount() <= 0.00){
			res.setCode("0014");
			res.setDesc("Invalid Amount");
			return res;
		}
		
		return res;
	}
	
	private TransferResData validateConfirm(TransferReqData aData){
		TransferResData res = new TransferResData();
		res.setCode("0000");
		if(aData.getApikey().equals("") || aData.getApikey() == null || !aData.getApikey().equals("MPO123456")){
			res.setCode("0014");
			res.setDesc("Invalid Request Parameter");
			return res;
		}
		
		if(aData.getTransid().equals("") || aData.getTransid() == null){
			res.setCode("0014");
			res.setDesc("Transaction ID is required");
			return res;
		}
		
		if(aData.getRefkey().equals("") || aData.getRefkey() == null){
			res.setCode("0014");
			res.setDesc("Third Party Transaction ID is required");
			return res;
		}
		
		return res;
	}
}
