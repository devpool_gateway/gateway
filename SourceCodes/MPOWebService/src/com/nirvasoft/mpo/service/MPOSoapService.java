package com.nirvasoft.mpo.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceException;

import com.nirvasoft.mpo.shared.TransferResData;


@WebService
@SOAPBinding(style = Style.DOCUMENT)
public interface MPOSoapService {
	
	@WebMethod
	public TransferResData transfer(
			@WebParam(name = "apikey") @XmlElement(required = true) String apikey,
			@WebParam(name = "transid") @XmlElement(required = true) String transid,
			@WebParam(name = "fromoperator") @XmlElement(required = true) String fromoperator,
			@WebParam(name = "fromaccount") @XmlElement(required = true) String fromaccount,
			@WebParam(name = "fromwallet") @XmlElement(required = true) String fromwallet,
			@WebParam(name = "fromname") @XmlElement(required = false) String fromname,
			@WebParam(name = "tooperator") @XmlElement(required = true) String tooperator,
			@WebParam(name = "toaccount") @XmlElement(required = true) String toaccount,
			@WebParam(name = "towallet") @XmlElement(required = true) String towallet,
			@WebParam(name = "toname") @XmlElement(required = true) String toname,
			@WebParam(name = "amount") @XmlElement(required = true) double amount,
			@WebParam(name = "commission") @XmlElement(required = true) double commission,
			@WebParam(name = "narration") @XmlElement(required = false) String narration,
			@WebParam(name = "t1") @XmlElement(required = false) String t1,
			@WebParam(name = "t2") @XmlElement(required = false) String t2,
			@WebParam(name = "t3") @XmlElement(required = false) String t3)
			throws WebServiceException;
	
}
