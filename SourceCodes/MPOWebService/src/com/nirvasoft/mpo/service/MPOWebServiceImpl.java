package com.nirvasoft.mpo.service;

import java.rmi.RemoteException;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import com.nirvasoft.mpo.framework.ServerSession;
import com.nirvasoft.mpo.mgr.TransferMgr;
import com.nirvasoft.mpo.shared.ServerGlobal;
import com.nirvasoft.mpo.shared.TransferReqData;
import com.nirvasoft.mpo.shared.TransferResData;
import com.nirvasoft.mpo.util.FileUtil;
import com.nirvasoft.mpo.util.Geneal;
import com.nirvasoft.rp.service.TransferConfirmResData;
import com.nirvasoft.rp.service.WalletSoapServiceProxy;

@WebService(endpointInterface = "com.nirvasoft.mpo.service.MPOSoapService")
public class MPOWebServiceImpl implements MPOSoapService {
	
	@Resource
    private WebServiceContext context;
	
	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;
	
	private void getPath() {
		ServletContext servletContext = (ServletContext) context.getMessageContext()
                .get(MessageContext.SERVLET_CONTEXT);
        
        String path = servletContext.getRealPath("");
        System.out.println(path);
        ServerSession.serverPath = path;
	}
	
	 public java.util.ArrayList<String> getServiceSettings() throws IllegalArgumentException {
		    getPath();
			//Get Server Location Path
			ServerGlobal.setAppPath(ServerSession.serverPath );
			java.util.ArrayList<String> l_resultList = null;
			try {
				l_resultList = FileUtil.readFile(ServerSession.serverPath  + "data\\ServiceSetting.txt");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Get Service Path URL
			ServerGlobal.setMsettingsArr(l_resultList);
			
			return l_resultList;
		}

	@Override
	public TransferResData transfer(String apikey, String transid, String fromoperator, String fromaccount,
			String fromwallet, String fromname, String tooperator, String toaccount, String towallet, String toname, 
			double amount, double commission, String narration,
			String t1, String t2, String t3){
		
		
		TransferResData res = new TransferResData();
		TransferReqData req = new TransferReqData();
		MPOService mposervice = new MPOService();
		TransferMgr dbmgr = new TransferMgr();
		getPath();
		getServiceSettings();
		
		req.setApikey(apikey);
		req.setTransid(transid);
		req.setFromoperator(fromoperator);
		req.setFromaccount(fromaccount);
		req.setFromwallet(fromwallet);
		req.setFromname(fromname);
		req.setTooperator(tooperator);
		req.setToaccount(toaccount);
		req.setTowallet(towallet);
		req.setToname(toname);
		req.setAmount(amount);
		
		//ValidateReqParameter
		res = mposervice.validateMPOTransfer(req);
		
		if(!res.getCode().equals("0000")){
			res.setTransid(req.getTransid() != null ? req.getTransid() : "");
			res.setRefkey("");
			return res;
		}
		
		res = new TransferResData();
		res = dbmgr.checkTransferTrans(transid);
		//CheckTrans();
		if(res.getCode().equals("0003")){
			res.setTransid(req.getTransid());
			res.setRefkey("");
			res.setCode("0003");
			res.setDesc("Duplicate Transaction ID");
			return res;
		}else if(!res.getCode().equals("0000")){
			res.setTransid(req.getTransid());
			res.setRefkey("");
			res.setCode("0002");
			res.setDesc("Internal server error.Please contact to administrator");
			return res;
		}
		
		//SaveTrans();
		res = new TransferResData();
		res = dbmgr.saveTransferTrans(req);// Status ==> 1
		
		if(!res.getCode().equals("0000")){
			if(res.getCode().equals("0003")){
				res.setTransid(req.getTransid());
				res.setRefkey("");
				res.setCode("0003");
				res.setDesc("Duplicate Transaction ID");
			}else{
				res.setTransid(req.getTransid());
				res.setRefkey("");
				res.setCode("0002");
				res.setDesc("Internal server error.Please contact to administrator");
			}
			
			return res;
		}
		
		//Define URL for credit transfer in
		for (String string : ServerGlobal.getMsettingsArr()) {
			
			if(string.startsWith(req.getFromoperator())){
				ServerGlobal.setmDebitServiceURL(string.split(ServerGlobal.getSeparator())[1]);break;
			}
		}
		
		for (String string : ServerGlobal.getMsettingsArr()) {
		
			if(string.startsWith(req.getTooperator())){
				
				ServerGlobal.setmCreditServiceURL(string.split(ServerGlobal.getSeparator())[1]);break;
			}
		}
		
		if(ServerGlobal.getmCreditServiceURL().equals("")){
			res.setTransid(req.getTransid());
			res.setRefkey("");
			res.setCode("0002");
			res.setDesc("Internal server error.Please contact to administrator");
			return res;
		}
		
		if(ServerGlobal.getmDebitServiceURL().equals("")){
			res.setTransid(req.getTransid());
			res.setRefkey("");
			res.setCode("0002");
			res.setDesc("Internal server error.Please contact to administrator");
			return res;
		}
		
		req.setMpokey(res.getRefkey());
		
		//Create Thread
		System.out.println("Start Thread Creating" + Geneal.getTime());
		callCreditRunnable creditThread = new callCreditRunnable(req, ServerGlobal.getmCreditServiceURL(), ServerGlobal.getmDebitServiceURL());
        Thread thread = new Thread(creditThread);
        thread.start();
        
        dbmgr.updateRequestACK(transid);//Status ==> 2 
		
		res.setCode("0000");
		res.setTransid(req.getTransid());
		res.setDesc("Transfer received successfully.");
		res.setRefkey(res.getRefkey());
        System.out.println("ACK" + Geneal.getTime());
		return res;
	
	}
	
	private class callCreditRunnable implements Runnable {
        private TransferReqData req;
        private String crediturl;
        private String debiturl;
        TransferMgr dbmgr = new TransferMgr();
        com.nirvasoft.rp.service.TransferResData res = new com.nirvasoft.rp.service.TransferResData();
        TransferConfirmResData confirmres = new TransferConfirmResData();
        public callCreditRunnable(TransferReqData data, String creditURL,  String debitURL) {
            this.req = data;
            this.crediturl = creditURL;
            this.debiturl = debitURL;
        }
        @Override
        public void run() {
        	System.out.println("Before Transfer In" + Geneal.getTime());
        	
        	//Credit Service
        	WalletSoapServiceProxy creditproxy = new WalletSoapServiceProxy();
    		creditproxy.setEndpoint(crediturl);
    		
    		//Debit Service
    		WalletSoapServiceProxy debitproxy = new WalletSoapServiceProxy();
    		debitproxy.setEndpoint(debiturl);
	    	
    	    try {
    	    	dbmgr.updateBeneReqInfo(req.getTransid());
    	    	res = creditproxy.transferIn(req.getApikey(), req.getTransid(), req.getFromoperator(), req.getFromaccount()
    	    			, req.getFromwallet(), req.getFromname(), req.getTooperator(), req.getToaccount(), req.getTowallet(), req.getToname()
    	    			, req.getAmount(), req.getComm1()
    	    			, req.getNarration(), req.getT1(), req.getT2(), req.getT3());
    	    	//res.getT1 ==> To Name
    	    	dbmgr.updateCreditTransRes(req.getTransid(), res.getRefKey(), res.getCode(), res.getDesc(), res.getT1(), res.getT2(), res.getT3());
    	    	
    	    	System.out.println("Processed" + Geneal.getTime());
    	    	
    	    	if(res.getCode().equals("0000")){
    	    		confirmres = debitproxy.transferconfirm(req.getApikey(), req.getTransid(), req.getMpokey(), res.getRefKey(), res.getT1(), "", "");
        	    	dbmgr.updateCompleteConfirm(req.getTransid(), confirmres.getCode(), confirmres.getDesc());
    	    	}
    		} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
    }
}