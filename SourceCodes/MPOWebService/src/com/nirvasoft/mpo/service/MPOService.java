package com.nirvasoft.mpo.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.mpo.dao.DAOManager;
import com.nirvasoft.mpo.framework.ServerSession;
import com.nirvasoft.mpo.mgr.ViberMgr;
import com.nirvasoft.mpo.mgr.TransferMgr;
import com.nirvasoft.mpo.shared.ViberRegData;
import com.nirvasoft.mpo.shared.ViberTestData;
import com.nirvasoft.mpo.shared.TransferReqData;
import com.nirvasoft.mpo.shared.TransferResData;

@Path("/mposervice")
public class MPOService {

	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;
	
	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}
	
	@POST
	@Path("mpotransferservice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferResData mpotransferservice(TransferReqData req) {
		
		getPath();
		TransferResData res = new TransferResData();
		TransferMgr dbmgr = new TransferMgr();
		//ValidateReqParameter
		res = validateMPOTransfer(req);
		if(res.getCode().equals("0000")){
			//SaveTrans();
			//res = dbmgr.saveTransferTrans(req);
			if(res.getCode().equals("0000")){
				res.setTransid(req.getTransid());
				res.setCode("0000");
				res.setDesc("Transfer request received successfully.");
			}else{
				res.setTransid(req.getTransid());
				res.setCode("0011");
				res.setDesc("Internal server error.Please contact to administrator");
			}
		}else{
			res.setTransid("");
			return res;
		}
		
		
		return res;
	}
	
	public TransferResData validateMPOTransfer(TransferReqData aData){
		TransferResData res = new TransferResData();
		res.setCode("0000");
		if(aData.getApikey() == null || aData.getApikey().equals("") || !aData.getApikey().equals("MPO123456")){
			res.setCode("0001");
			res.setDesc("Invalid Request Parameter");
			return res;
		}
		
		if(aData.getTransid() == null || aData.getTransid().equals("")){
			res.setCode("0001");
			res.setDesc("Transaction ID is required");
			return res;
		}
		
		if(aData.getFromoperator() == null || aData.getFromoperator().equals("")){
			res.setCode("0001");
			res.setDesc("Sender Operator is required");
			return res;
		}
		
		if(aData.getFromaccount() == null || aData.getFromaccount().equals("")){
			res.setCode("0001");
			res.setDesc("Sender Account is required");
			return res;
		}
		
		if(aData.getFromwallet() == null || aData.getFromwallet().equals("")){
			res.setCode("0001");
			res.setDesc("Sender Wallet is required");
			return res;
		}
		
		if(aData.getTooperator() == null || aData.getTooperator().equals("")){
			res.setCode("0001");
			res.setDesc("Beneficiary Operator is required");
			return res;
		}
		
		if(aData.getToaccount() == null || aData.getToaccount().equals("")){
			res.setCode("0001");
			res.setDesc("Beneficiary Account is required");
			return res;
		}
		
		if(aData.getTowallet() == null || aData.getTowallet().equals("")){
			res.setCode("0001");
			res.setDesc("Beneficiary Wallet is required");
			return res;
		}
		
		if(aData.getAmount() <= 0.00){
			res.setCode("0001");
			res.setDesc("Invalid Amount");
			return res;
		}
		
		return res;
	}
	
	@GET
    @Path("saveId")
    @Produces(MediaType.APPLICATION_JSON)
    public ViberTestData saveId() {
		getPath();
        String id = request.getParameter("Id");
        ViberTestData res = new ViberTestData();
        ViberMgr dmgr = new ViberMgr();
        
        res = dmgr.saveId(id);
       
        
        return res;
    }
	
	@GET
    @Path("viberregister")
    @Produces(MediaType.APPLICATION_JSON)
    public ViberTestData viberregister() {
		getPath();
	    ViberTestData res = new ViberTestData();
        ViberMgr dmgr = new ViberMgr();
	        
		String key = request.getParameter("key");
        String vibername = request.getParameter("vibername");
        String viberuserid = null;
		try {
			viberuserid = new String(request.getParameter("viberuserid").getBytes("iso-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        String vibermsg = request.getParameter("vibermsg");
        int substatus = Integer.parseInt(request.getParameter("substatus"));
        try {
        	key = URLEncoder.encode(key, "UTF-8");
        	vibername = URLEncoder.encode(vibername, "UTF-8");
			vibermsg = URLEncoder.encode(vibermsg, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res.setCode("0014");//internal server error
			res.setDesc("Kindly contact to administrator!");
			return res;
		}
       
        res = dmgr.checkAldyReg(key);
        
        if(res.getCode().equals("0011") || res.getCode().equals("0012"))//New || Unscribe
        	res = dmgr.viberregister(vibername, viberuserid, vibermsg, substatus, key);
        
        return res;
    }
	
	@POST
	@Path("viberregister2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ViberTestData viberregister2(ViberRegData req) {
		
		getPath();
	    ViberTestData res = new ViberTestData();
        ViberMgr dmgr = new ViberMgr();
       
        res = dmgr.checkAldyReg(req.getKey());
        
        if(res.getCode().equals("0011") || res.getCode().equals("0012"))//New || Unscribe
        	res = dmgr.viberregister(req.getVibername(), req.getViberuserid(), req.getVibermsg(), req.getSubstatus(), req.getKey());
        
        return res;
	}
	
	@POST
	@Path("vibercheckbalance")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ViberTestData vibercheckbalance(ViberRegData req) {
		getPath();
        String viberuserid = req.getViberuserid();
       
        ViberTestData res = new ViberTestData();
        ViberMgr dmgr = new ViberMgr();
        
        res = dmgr.getBalanceByViber(viberuserid);
        
        return res;
    }
	
	@POST
	@Path("viberunscribe")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ViberTestData viberunscribe(ViberRegData req) {
		getPath();
        String viberuserid = req.getViberuserid();
       
        ViberTestData res = new ViberTestData();
        ViberMgr dmgr = new ViberMgr();
        
        res = dmgr.viberunscribe(viberuserid);
        
        return res;
    }
	
	
	
}
