package com.nirvasoft.mpo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.mpo.shared.TransferReqData;
import com.nirvasoft.mpo.util.Constant;
import com.nirvasoft.mpo.util.Geneal;

public class TransferDao {
	
	private String mtable = "mpotransfer";
	
	public boolean checkTransferTrans(String transid , Connection conn) throws SQLException{
		boolean res = false;
		String sql = "select top 1 * from " + mtable + " where 1=1 and transid = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, transid);
		ResultSet rst = ps.executeQuery();
		if (rst.next()) {
			res = true;
		}
		ps.close();
		rst.close();
		return res;
	}

	public boolean saveTransferTrans(TransferReqData aData , Connection conn) throws SQLException{
		boolean res = false;
		String sql = "insert into MPOTransfer (APIKey,MPOKey,TransID,RefKey,SenderOperator,SenderAccount,"
				+ "SenderWallet,SenderName,BeneOperator,BeneAccount,BeneWallet,BeneName,Amount,Commission1,Commission2,"
				+ "Commission3,Narration,Status,SenderResCode,SenderResDesc,BeneResCode,BeneResDesc,SenderReqDateTime,SenderResDateTime,BeneReqDateTime,BeneResDateTime,T1,T2,T3,T4,T5,"
				+ "N1,N2,N3,N4,N5) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updatePrepareStatement(ps, aData);
		int rst = ps.executeUpdate();
		if (rst > 0) {
			res = true;
		}
		ps.close();
		return res;
	}
	
	private void updatePrepareStatement(PreparedStatement ps, TransferReqData aData) throws SQLException {
		int index = 1;
		ps.setString(index++, aData.getApikey());
		ps.setString(index++, aData.getMpokey());
		ps.setString(index++, aData.getTransid());
		ps.setString(index++, aData.getRefkey());
		ps.setString(index++, aData.getFromoperator());
		ps.setString(index++, aData.getFromaccount());
		ps.setString(index++, aData.getFromwallet());
		ps.setString(index++, aData.getFromname());
		ps.setString(index++, aData.getTooperator());
		ps.setString(index++, aData.getToaccount());
		ps.setString(index++, aData.getTowallet());
		ps.setString(index++, aData.getToname());
		ps.setDouble(index++, aData.getAmount());
		ps.setDouble(index++, aData.getComm1());
		ps.setDouble(index++, aData.getComm2());
		ps.setDouble(index++, aData.getComm3());
		ps.setString(index++, aData.getNarration());
		ps.setInt(index++, aData.getStatus());
		ps.setString(index++, aData.getFromcode());
		ps.setString(index++, aData.getFromdesc());
		ps.setString(index++, aData.getTocode());
		ps.setString(index++, aData.getTodesc());
		ps.setString(index++, aData.getFromreqdatetime());
		ps.setString(index++, aData.getFromresdatetime());
		ps.setString(index++, aData.getToreqdatetime());
		ps.setString(index++, aData.getToresdatetime());
		ps.setString(index++, aData.getT1());
		ps.setString(index++, aData.getT2());
		ps.setString(index++, aData.getT3());
		ps.setString(index++, aData.getT4());
		ps.setString(index++, aData.getT5());
		ps.setInt(index++, aData.getN1());
		ps.setInt(index++, aData.getN2());
		ps.setInt(index++, aData.getN3());
		ps.setInt(index++, aData.getN4());
		ps.setInt(index++, aData.getN5());
	}
	
	public boolean updateMPOForCreditTransferResponse(String aTransID, String aRefKey, String aCode, String aDesc, String aT1, String aT2, String aT3, Connection conn) throws SQLException{
		boolean res = false;
		String sql = "update " + mtable + " set status = ?, refkey = ?, BeneResCode = ?, BeneResDesc =?, BeneResDateTime = ?, BeneName = ?  where transid = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int index = 1;
		ps.setInt(index++, Constant.creditresponse);
		ps.setString(index++, aRefKey);
		ps.setString(index++, aCode);
		ps.setString(index++, aDesc);
		ps.setString(index++, Geneal.getCurrentDateYYYYMMDDHHMMSS());
		ps.setString(index++, aT1);//Bene Name
		ps.setString(index++, aTransID);
		if(ps.executeUpdate() > 0){
			res = true;
		}
		return res;
	}
	
	public boolean updateRequestACK(String aTransID, Connection conn) throws SQLException{
		boolean res = false;
		String sql = "update " + mtable + " set status = ? where transid = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int index = 1;
		ps.setInt(index++, Constant.getrequestAck);
		ps.setString(index++, aTransID);
		if(ps.executeUpdate() > 0){
			res = true;
		}
		return res;
	}
	
	public boolean updateBeneReqInfo(String aTransID, Connection conn) throws SQLException{
		boolean res = false;
		String sql = "update " + mtable + " set status = ?, BeneReqDateTime = ? where transid = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int index = 1;
		ps.setInt(index++, Constant.creditrequest);
		ps.setString(index++, Geneal.getCurrentDateYYYYMMDDHHMMSS());
		ps.setString(index++, aTransID);
		if(ps.executeUpdate() > 0){
			res = true;
		}
		return res;
	}
	
	public boolean updateCompleteConfirm(String aTransID, String aCode, String aDesc, Connection conn) throws SQLException{
		boolean res = false;
		String sql = "update " + mtable + " set status = ?,senderrescode = ?, senderresdesc = ?, senderresdatetime = ? where transid = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int index = 1;
		ps.setInt(index++, Constant.confirmresponse);
		ps.setString(index++, aCode);
		ps.setString(index++, aDesc);
		ps.setString(index++, Geneal.getCurrentDateYYYYMMDDHHMMSS());
		ps.setString(index++, aTransID);
		if(ps.executeUpdate() > 0){
			res = true;
		}
		return res;
	}
}
