package com.nirvasoft.mpo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.mpo.shared.ViberTestData;
import com.nirvasoft.mpo.util.Constant;

public class ViberDao {
	
	public ViberTestData saveId(String id,Connection conn) throws SQLException{
		ViberTestData res = new ViberTestData();
		String query = "INSERT INTO Test(Id,Name) VALUES(?,?)";
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setString(1,"1234");
		pstmt.setString(2,id);
		int rs = pstmt.executeUpdate();
		if(rs > 0){
			res.setCode("0000");
		}
		else{
			res.setCode("0014");
		}
		return res;
	}
	
	public ViberTestData viberregister(String vibername, String viberuserid, String vibermsg, int substatus, String key,Connection conn) throws SQLException{
		ViberTestData res = new ViberTestData();
		String query = "update ViberRegister set ViberName = ? , ViberUserID = ? , ViberMessage = ?, SubscribeStatus = ? , [Status] = ? "
				+ " where [Key] = ?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int index = 1;
		pstmt.setString(index++,vibername);
		pstmt.setString(index++,viberuserid);
		pstmt.setString(index++,vibermsg);
		pstmt.setInt(index++,substatus);
		pstmt.setInt(index++,Constant.viberregistered);
		pstmt.setString(index++,key);
		int rs = pstmt.executeUpdate();
		if(rs > 0){
			res.setCode("0000");
			res.setDesc("Registered Successfully!");
		}
		else{
			res.setCode("0014");//internal server error
			res.setDesc("Kindly contact to administrator!");
		}
		return res;
	}
	
	public ViberTestData checkAldyReg(String key,Connection conn) throws SQLException{
		ViberTestData ret = new ViberTestData();
		String query = "select ViberUserID, SubscribeStatus from ViberRegister where [key] = ? ";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int index = 1;
		pstmt.setString(index++,key);
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()){
			if(rs.getString("ViberUserID").equals(""))
				ret.setCode("0011");//New
			else{
				if(rs.getInt("SubscribeStatus") == Constant.viberunscribe){
					ret.setCode("0012");
					ret.setDesc("Please subscribe to our public account!");
				}else if(rs.getInt("SubscribeStatus") == Constant.viberscribe){
					ret.setCode("0014");
					ret.setDesc("Already Registered");
				}
			}
		}
		else{
			ret.setCode("0014");
			ret.setDesc("Invalid Key!");
		}
		return ret;
	}
	
	public String getCusIDbyViberUserID(String viberuserid,Connection conn) throws SQLException{
		String cusid = "";
		String query = "select CusID from ViberRegister where ViberUserID = ? and Status = ?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int index = 1;
		pstmt.setString(index++,viberuserid);
		pstmt.setInt(index++,Constant.viberregistered);
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()){
			cusid = rs.getString("CusID");
		}
		return cusid;
	}
	
	public double getBalByViber(String cusid,Connection conn) throws SQLException{
		double amount = 0.00;
		String query = "select sum(acy_avl_bal) as acy_avl_bal from sttm_cust_account where record_stat = 'O' and cust_no = ? ";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int index = 1;
		pstmt.setString(index++,cusid);
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()){
			amount = rs.getDouble("acy_avl_bal");
		}
		return amount;
	}
	
	public ViberTestData viberunscribe(String viberuserid,Connection conn) throws SQLException{
		ViberTestData res = new ViberTestData();
		String query = "update ViberRegister set SubscribeStatus = ?, Status = 0 "
				+ " where [viberuserid] = ?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int index = 1;
		pstmt.setInt(index++,Constant.viberunscribe);
		pstmt.setString(index++,viberuserid);
		int rs = pstmt.executeUpdate();
		if(rs > 0){
			res.setCode("0000");
		}
		else{
			res.setCode("0014");
		}
		return res;
	}

}
