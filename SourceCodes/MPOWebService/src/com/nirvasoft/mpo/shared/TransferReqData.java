package com.nirvasoft.mpo.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferReqData {
	
	private String apikey;
	private String transid;
	private String mpokey;
	private String refkey;
	private String fromoperator;
	private String fromaccount;
	private String fromwallet;
	private String fromname;
	private String tooperator;
	private String toaccount;
	private String towallet;
	private String toname;
	private double amount;
	private double comm1;
	private double comm2;
	private double comm3;
	private String narration;
	private int status;
	private String fromrescode;
	private String fromresdesc;
	private String torescode;
	private String toresdesc;
	private String fromreqdatetime;
	private String fromresdatetime;
	private String toreqdatetime;
	private String toresdatetime;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;
	
	public TransferReqData(){
		clearproperty();
	}
	
	void clearproperty(){
		apikey = "";
		transid = "";
		mpokey = "";
		refkey = "";
		fromoperator = "";
		fromaccount = "";
		fromwallet = "";
		fromname = "";
		tooperator = "";
		toaccount = "";
		towallet = "";
		toname = "";
		amount = 0.00;
		comm1 = 0.00;
		comm2 = 0.00;
		comm3 = 0.00;
		narration = "";
		status = -9;
		fromrescode = "";
		fromresdesc = "";
		torescode = "";
		toresdesc = "";
		fromreqdatetime = "";
		fromresdatetime = "";
		toreqdatetime = "";
		toresdatetime = "";
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		t5 = "";
		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;
		n5 = 0;
	}

	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	public double getComm1() {
		return comm1;
	}

	public void setComm1(double comm1) {
		this.comm1 = comm1;
	}

	public double getComm2() {
		return comm2;
	}

	public void setComm2(double comm2) {
		this.comm2 = comm2;
	}

	public double getComm3() {
		return comm3;
	}

	public void setComm3(double comm3) {
		this.comm3 = comm3;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFromcode() {
		return fromrescode;
	}

	public void setFromcode(String fromcode) {
		this.fromrescode = fromcode;
	}

	public String getFromdesc() {
		return fromresdesc;
	}

	public void setFromdesc(String fromdesc) {
		this.fromresdesc = fromdesc;
	}

	public String getTocode() {
		return torescode;
	}

	public void setTocode(String tocode) {
		this.torescode = tocode;
	}

	public String getTodesc() {
		return toresdesc;
	}

	public void setTodesc(String todesc) {
		this.toresdesc = todesc;
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getT3() {
		return t3;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public String getT4() {
		return t4;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public String getT5() {
		return t5;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public int getN1() {
		return n1;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public int getN2() {
		return n2;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public int getN3() {
		return n3;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public int getN4() {
		return n4;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public int getN5() {
		return n5;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public String getTransid() {
		return transid;
	}

	public void setTransid(String transid) {
		this.transid = transid;
	}

	public String getFromoperator() {
		return fromoperator;
	}

	public void setFromoperator(String fromoperator) {
		this.fromoperator = fromoperator;
	}

	public String getFromaccount() {
		return fromaccount;
	}

	public void setFromaccount(String fromaccount) {
		this.fromaccount = fromaccount;
	}

	public String getFromwallet() {
		return fromwallet;
	}

	public void setFromwallet(String fromwallet) {
		this.fromwallet = fromwallet;
	}

	public String getTooperator() {
		return tooperator;
	}

	public void setTooperator(String tooperator) {
		this.tooperator = tooperator;
	}

	public String getToaccount() {
		return toaccount;
	}

	public void setToaccount(String toaccount) {
		this.toaccount = toaccount;
	}

	public String getTowallet() {
		return towallet;
	}

	public void setTowallet(String towallet) {
		this.towallet = towallet;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getFromrescode() {
		return fromrescode;
	}

	public void setFromrescode(String fromrescode) {
		this.fromrescode = fromrescode;
	}

	public String getFromresdesc() {
		return fromresdesc;
	}

	public void setFromresdesc(String fromresdesc) {
		this.fromresdesc = fromresdesc;
	}

	public String getTorescode() {
		return torescode;
	}

	public void setTorescode(String torescode) {
		this.torescode = torescode;
	}

	public String getToresdesc() {
		return toresdesc;
	}

	public void setToresdesc(String toresdesc) {
		this.toresdesc = toresdesc;
	}

	public String getFromreqdatetime() {
		return fromreqdatetime;
	}

	public void setFromreqdatetime(String fromreqdatetime) {
		this.fromreqdatetime = fromreqdatetime;
	}

	public String getFromresdatetime() {
		return fromresdatetime;
	}

	public void setFromresdatetime(String fromresdatetime) {
		this.fromresdatetime = fromresdatetime;
	}

	public String getToreqdatetime() {
		return toreqdatetime;
	}

	public void setToreqdatetime(String toreqdatetime) {
		this.toreqdatetime = toreqdatetime;
	}

	public String getToresdatetime() {
		return toresdatetime;
	}

	public void setToresdatetime(String toresdatetime) {
		this.toresdatetime = toresdatetime;
	}

	public String getMpokey() {
		return mpokey;
	}

	public void setMpokey(String mpokey) {
		this.mpokey = mpokey;
	}

	public String getFromname() {
		return fromname;
	}

	public void setFromname(String fromname) {
		this.fromname = fromname;
	}

	public String getToname() {
		return toname;
	}

	public void setToname(String toname) {
		this.toname = toname;
	}
	
	

}
