package com.nirvasoft.mpo.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ViberTestData {
private String text;
private String Id;
private String code;
private String desc;
private String amount;

public ViberTestData(){
	this.text = "";
	this.Id = "";
	this.code = "";
	this.desc = "";
	this.amount = "";
}

public String getId() {
	return Id;
}

public void setId(String id) {
	Id = id;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public String getText() {
	return text;
}
public void setText(String text) {
	this.text = text;
}

public String getDesc() {
	return desc;
}

public void setDesc(String desc) {
	this.desc = desc;
}

public String getAmount() {
	return amount;
}

public void setAmount(String amount) {
	this.amount = amount;
}



}
