package com.nirvasoft.mpo.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ViberRegData {
	
	private String key ;
	private String vibername;
	private String viberuserid;
	private String vibermsg;
	private int substatus;
	
	void clearProperty(){
		this.key = "";
		this.vibername = "";
		this.viberuserid = "";
		this.vibermsg = "";
		this.substatus = 0;
	}
	
	public ViberRegData(){
		clearProperty();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getVibername() {
		return vibername;
	}

	public void setVibername(String vibername) {
		this.vibername = vibername;
	}

	public String getViberuserid() {
		return viberuserid;
	}

	public void setViberuserid(String viberuserid) {
		this.viberuserid = viberuserid;
	}

	public String getVibermsg() {
		return vibermsg;
	}

	public void setVibermsg(String vibermsg) {
		this.vibermsg = vibermsg;
	}

	public int getSubstatus() {
		return substatus;
	}

	public void setSubstatus(int substatus) {
		this.substatus = substatus;
	}
	
	
	

}
