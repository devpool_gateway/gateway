package com.nirvasoft.mpo.shared;

import java.util.ArrayList;


public class ServerGlobal {
	
	private static String mAppPath = "";
	private static String mSeparator = "\\|\\|\\_\\_";
	private static String mFCSeparator = "\\_\\_\\|";
	private static boolean mSetWriteLog = false;
	private static String fortuneYgnMerchant;
	private static String fortuneMdyMerchant;
	private static String mFCRTServiceURL="";
	private static String mEPIXServiceURL="";
	private static String mCreditServiceURL="";
	public static String getmCreditServiceURL() {
		return mCreditServiceURL;
	}

	public static void setmCreditServiceURL(String mCreditServiceURL) {
		ServerGlobal.mCreditServiceURL = mCreditServiceURL;
	}

	public static String getmDebitServiceURL() {
		return mDebitServiceURL;
	}

	public static void setmDebitServiceURL(String mDebitServiceURL) {
		ServerGlobal.mDebitServiceURL = mDebitServiceURL;
	}

	private static String mDebitServiceURL="";
	private static ArrayList<String> msettingsArr = null;
	
	public static ArrayList<String> getMsettingsArr() {
		return msettingsArr;
	}

	public static void setMsettingsArr(ArrayList<String> msettingsArr) {
		ServerGlobal.msettingsArr = msettingsArr;
	}

	private static String mFCACServiceURL="";
	private static String mFCCUServiceURL="";
	private static int mBrCodeStart = 0;
	private static int mBrCodeEnd = 0;
	private static String mXREFPrefix = "";

	
	public static String getmFCACServiceURL() {
		return mFCACServiceURL;
	}

	public static void setmFCACServiceURL(String mFCACServiceURL) {
		ServerGlobal.mFCACServiceURL = mFCACServiceURL;
	}

	public static String getmFCCUServiceURL() {
		return mFCCUServiceURL;
	}

	public static void setmFCCUServiceURL(String mFCCUServiceURL) {
		ServerGlobal.mFCCUServiceURL = mFCCUServiceURL;
	}

	public static String getmEPIXServiceURL() {
		return mEPIXServiceURL;
	}

	public static void setmEPIXServiceURL(String mEPIXServiceURL) {
		ServerGlobal.mEPIXServiceURL = mEPIXServiceURL;
	}

	public static String getSeparator(){
		return mSeparator;
	}
	
	public static String getFCSeparator(){
		return mFCSeparator;
	}

	public static String getAppPath() {
		return mAppPath;
	}

	public static void setAppPath(String mAppPath) {
		ServerGlobal.mAppPath = mAppPath;
	}
	
	public static boolean isWriteLog() {
		return mSetWriteLog;
	}

	public static void setWriteLog(boolean mWLog) {
		ServerGlobal.mSetWriteLog = mWLog;
	}

	public static String getmAppPath() {
		return mAppPath;
	}

	public static void setmAppPath(String mAppPath) {
		ServerGlobal.mAppPath = mAppPath;
	}

	public static String getmSeparator() {
		return mSeparator;
	}

	public static void setmSeparator(String mSeparator) {
		ServerGlobal.mSeparator = mSeparator;
	}

	

	public static boolean ismSetWriteLog() {
		return mSetWriteLog;
	}

	public static void setmSetWriteLog(boolean mSetWriteLog) {
		ServerGlobal.mSetWriteLog = mSetWriteLog;
	}

	public static String getFortuneYgnMerchant() {
		return fortuneYgnMerchant;
	}

	public static void setFortuneYgnMerchant(String fortuneYgnMerchant) {
		ServerGlobal.fortuneYgnMerchant = fortuneYgnMerchant;
	}

	public static String getFortuneMdyMerchant() {
		return fortuneMdyMerchant;
	}

	public static void setFortuneMdyMerchant(String fortuneMdyMerchant) {
		ServerGlobal.fortuneMdyMerchant = fortuneMdyMerchant;
	}
	
	public static String getFCRTServiceURL() {
		return mFCRTServiceURL;
	}

	public static void setFCRTServiceURL(String mFCURL) {
		ServerGlobal.mFCRTServiceURL = mFCURL;
	}

	public static int getmBrCodeStart() {
		return mBrCodeStart;
	}

	public static void setmBrCodeStart(int mBrCodeStart) {
		ServerGlobal.mBrCodeStart = mBrCodeStart;
	}

	public static int getmBrCodeEnd() {
		return mBrCodeEnd;
	}

	public static void setmBrCodeEnd(int mBrCodeEnd) {
		ServerGlobal.mBrCodeEnd = mBrCodeEnd;
	}

	public static String getmXREFPrefix() {
		return mXREFPrefix;
	}

	public static void setmXREFPrefix(String mXREFPrefix) {
		ServerGlobal.mXREFPrefix = mXREFPrefix;
	}


	
	
}
