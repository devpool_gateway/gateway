package com.nirvasoft.mpo.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferResData {
	
	private String transid;
	private String refkey;
	private String code;
	private String desc;
	
	public TransferResData(){
		clearproperty();
	}
	
	void clearproperty(){
		transid = "";
		refkey = "";
		code = "";
		desc = "";
	}

	public String getTransid() {
		return transid;
	}

	public void setTransid(String transid) {
		this.transid = transid;
	}

	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	

}
