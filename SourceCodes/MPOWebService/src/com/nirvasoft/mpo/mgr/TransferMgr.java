package com.nirvasoft.mpo.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import com.nirvasoft.mpo.dao.DAOManager;
import com.nirvasoft.mpo.dao.TransferDao;
import com.nirvasoft.mpo.framework.ConnAdmin;
import com.nirvasoft.mpo.shared.TransferInResData;
import com.nirvasoft.mpo.shared.TransferReqData;
import com.nirvasoft.mpo.shared.TransferResData;
import com.nirvasoft.mpo.util.Constant;
import com.nirvasoft.mpo.util.Geneal;

public class TransferMgr {
	
	public TransferResData getRefkey(){
		TransferResData res = new TransferResData();
		Connection conn = null;
		DAOManager daomgr = new DAOManager();
		try {
			conn = ConnAdmin.getConn("001", "");
			String key = daomgr.getRefKey(conn);
			if(key.equals("0014")){
				res.setCode("0014");
				res.setDesc("Key cannot generated!");
			}else{
				res.setRefkey(key);
				res.setCode("0000");
			}
			res.setRefkey(key);
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}
		return res;
	}
	
	public TransferInResData getRandomRefKey(){
		TransferInResData res = new TransferInResData();
		try {
			Random rnd = new Random();
			int n = 0;
			n = 100000 + rnd.nextInt(900000);
			if(n == 0){
				res.setCode("0014");
				res.setDesc("Invalid Random Number!");
			}else{
				res.setRefkey(String.valueOf(n));
				res.setCode("0000");
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} 
		return res;
	}
	
	
	public TransferResData saveTransferTrans(TransferReqData data) {

		TransferResData res = new TransferResData();
		TransferDao dao = new TransferDao();
		Connection conn = null;
		TransferMgr dbmgr = new TransferMgr();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = dbmgr.getRefkey();
			if(res.getCode().equals("0000")){
				data.setMpokey(res.getRefkey());
			}else{
				res.setCode("0002");
				return res;
			}
			prepareTransferTrans(data);
			if(dao.saveTransferTrans(data, conn)){
				res.setCode("0000");
				res.setRefkey(data.getMpokey());
			}else{
				res.setCode("0002");
			}
		} catch (SQLException e) {
			//2627
			System.out.println(e.getSQLState());
			if(e.getSQLState().equals("23000")){
				res.setCode("0003");
			}else{
				res.setCode("0002");
			}
			e.printStackTrace();
			
		} catch (Exception e) {
			//2627
			e.printStackTrace();
			res.setCode("0002");
		} finally {

			try {
				if(conn != null){
					if (!conn.isClosed())
						conn.close();
				}

			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0002");
			}
		}

		return res;
	}
	
	public TransferResData checkTransferTrans(String transid) {

		TransferResData result = new TransferResData();
		boolean res = false;
		TransferDao dao = new TransferDao();
		Connection conn = null;
		TransferMgr dbmgr = new TransferMgr();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = dao.checkTransferTrans(transid, conn);
			if(!res){
				result.setCode("0000");
			}else{
				result.setCode("0003");
			}
		} catch (SQLException e) {
			//2627
			result.setCode("0014");
			e.printStackTrace();
			
		} catch (Exception e) {
			//2627
			e.printStackTrace();
			result.setCode("0014");
		} finally {

			try {
				if(conn != null){
					if (!conn.isClosed())
						conn.close();
				}

			} catch (SQLException e) {

				e.printStackTrace();
				result.setCode("0014");
			}
		}

		return result;
	}


	private void prepareTransferTrans(TransferReqData data) {
		
		data.setMpokey(data.getMpokey());
		data.setStatus(Constant.getrequest);
		data.setFromreqdatetime(Geneal.getCurrentDateYYYYMMDDHHMMSS());
	}
	
	public TransferResData updateCreditTransRes(String aTransID, String aRefKey, String aCode, String aDesc, String T1, String T2, String T3) {

		TransferResData res = new TransferResData();
		TransferDao dao = new TransferDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if(dao.updateMPOForCreditTransferResponse(aTransID, aRefKey, aCode, aDesc, T1, T2, T3, conn)){
				res.setCode("0000");
			}else{
				res.setCode("0014");
				res.setDesc("Update Fail!");
			}
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}

		return res;
	}
	
	public TransferResData updateRequestACK(String aTransID) {

		TransferResData res = new TransferResData();
		TransferDao dao = new TransferDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if(dao.updateRequestACK(aTransID, conn)){
				res.setCode("0000");
			}else{
				res.setCode("0014");
				res.setDesc("Update Fail!");
			}
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}

		return res;
	}
	
	public TransferResData updateBeneReqInfo(String aTransID) {

		TransferResData res = new TransferResData();
		TransferDao dao = new TransferDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if(dao.updateBeneReqInfo(aTransID, conn)){
				res.setCode("0000");
			}else{
				res.setCode("0014");
				res.setDesc("Update Fail!");
			}
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}

		return res;
	}
	
	public TransferResData updateCompleteConfirm(String aTransID, String aCode, String aDesc) {

		TransferResData res = new TransferResData();
		TransferDao dao = new TransferDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if(dao.updateCompleteConfirm(aTransID, aCode, aDesc, conn)){
				res.setCode("0000");
			}else{
				res.setCode("0014");
				res.setDesc("Update Fail!");
			}
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}

		return res;
	}

}
