package com.nirvasoft.mpo.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.mpo.dao.DAOManager;
import com.nirvasoft.mpo.dao.ViberDao;
import com.nirvasoft.mpo.framework.ConnAdmin;
import com.nirvasoft.mpo.shared.ViberTestData;
import com.nirvasoft.mpo.util.Geneal;

public class ViberMgr {
	
	public ViberTestData saveId(String id){
		ViberTestData res = new ViberTestData();
		ViberDao test_dao = new ViberDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
	
			res = test_dao.saveId(id, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	public ViberTestData viberregister(String vibername, String viberuserid, String vibermsg, int substatus, String key){
		ViberTestData res = new ViberTestData();
		ViberDao test_dao = new ViberDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
	
			res = test_dao.viberregister(vibername, viberuserid, vibermsg, substatus, key, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");//internal server error
			res.setDesc("Kindly contact to administrator!");
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				res.setCode("0014");//internal server error
				res.setDesc("Kindly contact to administrator!");
			}
		}
		return res;
	}


	public ViberTestData checkAldyReg(String key){
		ViberTestData res = new ViberTestData();
		ViberDao test_dao = new ViberDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			res = test_dao.checkAldyReg(key, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");//internal server error
			res.setDesc("Kindly contact to administrator!");
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				res.setCode("0014");//internal server error
				res.setDesc("Kindly contact to administrator!");
			}
		}
		return res;
	}
	
	public ViberTestData getBalanceByViber(String viberuserid){
		ViberTestData res = new ViberTestData();
		ViberDao test_dao = new ViberDao();
		Connection l_Conn = null;
		Connection l_OraConn = null;
		String cusid = "";
		double bal = 0.00;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			l_OraConn = DAOManager.openOracleConnection();
			cusid = test_dao.getCusIDbyViberUserID(viberuserid, l_Conn);
			if(cusid.equals("")){
				res.setCode("0014");
				res.setDesc("Invalid User ID!");
				return res;
			}
			bal = test_dao.getBalByViber(cusid, l_OraConn);
			
			res.setCode("0000");
			res.setAmount(Geneal.formatNumber2(bal));
			res.setDesc("Balance Inquiry Successfully.");
			
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");//internal server error
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_OraConn.isClosed())
					l_OraConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				res.setCode("0014");//internal server error
			}
		}
		return res;
	}
	
	public ViberTestData viberunscribe(String viberuserid){
		ViberTestData res = new ViberTestData();
		ViberDao test_dao = new ViberDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
	
			res = test_dao.viberunscribe(viberuserid, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				res.setCode("0014");
			}
		}
		return res;
	}
}
