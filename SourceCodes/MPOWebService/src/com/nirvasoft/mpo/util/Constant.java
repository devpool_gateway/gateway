package com.nirvasoft.mpo.util;

public class Constant {

	final public static int getrequest = 1;
	final public static int getrequestAck = 2;
	final public static int creditrequest = 3;
	final public static int creditresponse = 4;
	final public static int confirmrequest = 5;//This is never used
	final public static int confirmresponse = 6;
	
	final public static int viberregistered = 7;
	final public static int viberunscribe = 2;
	final public static int viberscribe = 1;
}
