package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class V_AssetData extends AssetData {

	public V_AssetData() {
		clearProperties();
	}

	private String brand;
	private String company;
	private String currency;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void clearProperties() {
		super.clearProperties();
		this.brand = "";
		this.company = "";
		this.currency = "";
	}

}
