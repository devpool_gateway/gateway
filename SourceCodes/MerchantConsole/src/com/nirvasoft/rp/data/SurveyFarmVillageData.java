package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SurveyFarmVillageData {
	
	private String refkey;
	private String refsvytype;
	private String hub;
	private String division;
	private String township;
	private String branch;
	private String branchno;
	private String villagename;
	private String gpscoordinate;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String t11;
	
	void clearProperty(){
		refkey = "REF";
		refsvytype = "";
		hub = "dddfdfsdf";
		division = "sfsd";
		township = "sfasdf";
		branch = "sdfsdfd";
		branchno = "sfasdf";
		villagename = "sfasd";
		gpscoordinate = "sdfsdfsdf";
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		t5 = "";
		t6 = "";
		t7 = "";
		t8 = "";
		t9 = "";
		t10 = "";
		t11 = "";
	}
	
	public SurveyFarmVillageData(){
		clearProperty();
	}

	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	public String getRefsvytype() {
		return refsvytype;
	}

	public void setRefsvytype(String refsvytype) {
		this.refsvytype = refsvytype;
	}

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getTownship() {
		return township;
	}

	public void setTownship(String township) {
		this.township = township;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBranchno() {
		return branchno;
	}

	public void setBranchno(String branchno) {
		this.branchno = branchno;
	}

	public String getVillagename() {
		return villagename;
	}

	public void setVillagename(String villagename) {
		this.villagename = villagename;
	}

	public String getGpscoordinate() {
		return gpscoordinate;
	}

	public void setGpscoordinate(String gpscoordinate) {
		this.gpscoordinate = gpscoordinate;
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getT3() {
		return t3;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public String getT4() {
		return t4;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public String getT5() {
		return t5;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public String getT6() {
		return t6;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public String getT7() {
		return t7;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public String getT8() {
		return t8;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public String getT9() {
		return t9;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public String getT10() {
		return t10;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public String getT11() {
		return t11;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}
	
	
	
	
}
