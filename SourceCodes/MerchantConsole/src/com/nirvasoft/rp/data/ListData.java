package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListData {
       private String refkey;
       private String type;
       private String hub;
       private String division;
       private String township;
       private String branch;
       private String period;
       
       public ListData(){
    	   clearData();
       }
       
       void clearData(){
    	   refkey = "123";
    	   type = "mmppm";
    	   hub = "hlaing hlaing hlaing";
    	   division = "";
    	   township = "";
    	   branch = "";
    	   period = "";
       }

	public String getrefkey() {
		return refkey;
	}

	public void setrefkey(String refkey) {
		this.refkey = refkey;
	}

	public String gettype() {
		return type;
	}

	public void settype(String type) {
		this.type = type;
	}

	public String gethub() {
		return hub;
	}

	public void sethub(String hub) {
		this.hub = hub;
	}

	public String getdivision() {
		return division;
	}

	public void setdivision(String division) {
		this.division = division;
	}

	public String gettownship() {
		return township;
	}

	public void settownship(String township) {
		this.township = township;
	}

	public String getbranch() {
		return branch;
	}

	public void setbranch(String branch) {
		this.branch = branch;
	}

	public String getperiod() {
		return period;
	}

	public void setperiod(String period) {
		this.period = period;
	}

       
       
}
