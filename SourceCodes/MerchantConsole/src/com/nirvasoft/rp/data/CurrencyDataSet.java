package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CurrencyDataSet {

	private CurrencyData[] data;

	public CurrencyDataSet() {
		super();
		data = new CurrencyData[0];

	}

	public CurrencyData[] getData() {
		return data;
	}

	public void setData(CurrencyData[] data) {
		this.data = data;
	}

}