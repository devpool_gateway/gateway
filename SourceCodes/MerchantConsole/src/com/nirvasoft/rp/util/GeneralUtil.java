package com.nirvasoft.rp.util;
import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.ServerGlobal;

public class GeneralUtil {
	
public static void readDataConfig() {
		
		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(DAOManager.AbsolutePath +"/data/DataConfig.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (String string : l_resultList) {
			if(string.startsWith("FortuneYgnMerchant")){
				ServerGlobal.setFortuneYgnMerchant(string.split(ServerGlobal.getSeparator())[1]);
			}
			if(string.startsWith("FortuneMdyMerchant")){
				ServerGlobal.setFortuneMdyMerchant(string.split(ServerGlobal.getSeparator())[1]);
			}
			
		}
		
	}

public static boolean writeLog(ArrayList<String> pErrorList) {
	boolean l_result = false;

	try {
		String l_Date = datetoString();
		String l_Path =DAOManager.AbsolutePath + "log\\sms";
		System.out.println(l_Path);
		File dir = new File(l_Path);
		dir.mkdirs();
		FileUtil.writeList(l_Path +"\\"+ l_Date +".txt", pErrorList, true);
		l_result = true;
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		l_result = false;
	}
	return l_result;
	
}

public static String datetoString()
{
	String l_date = "";
	java.util.Date l_Date = new java.util.Date();
	SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMdd");	
	l_date = l_sdf.format(l_Date);
	
	return l_date;
}

public static String getTime(){
	String l_Time = "";
	SimpleDateFormat l_DateFormat = new SimpleDateFormat("HH:mm:ss");
	Calendar l_Canlendar = Calendar.getInstance();
	l_Time = l_DateFormat.format(l_Canlendar.getTime());
	
	return l_Time;
}

public static String formatddMMYYYY2YYYYMMDD(String p){
	 String ret = "";
	SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	SimpleDateFormat f2 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	Date d;
	try {
		d = f.parse(p);
		ret = f2.format(d);				
	} catch (ParseException e) {				
		e.printStackTrace();
	}
	return ret;	
}

public static String formatddMMYYYY2YYYYMMdd(String p) {
   String ret = "";
   String dd="";
   String mm="";
   try {
       int d = Integer.parseInt(p.substring(0, 2));
       
       int m = Integer.parseInt(p.substring(3, 5));
       
       int y = Integer.parseInt(p.substring(6, 10));
       
       if(d<10){
       	 dd="0"+d;
       }else{
    	   dd = p.substring(0, 2);
       }
       if(m<10){
       	 mm="0"+m;
       }else{
    	   mm = p.substring(3, 5);
       }
       ret = y+""+mm+""+dd;
   } catch (Exception ex) {
       ex.printStackTrace();
   }
   return ret;
}
public static String formatNumber(double pAmount) {
	String l_result="0.00";
	DecimalFormat l_df=new DecimalFormat("###0.00");
	l_result=l_df.format(pAmount);
	return l_result;
}
	public static String toMonth(String pMonth){
		String ret = "";
		if("01".equals(pMonth)){
			ret = "Jan";
		}else if("02".equals(pMonth)){
			ret = "Feb";
		}else if("03".equals(pMonth)){
			ret = "Mar";
		}else if("04".equals(pMonth)){
			ret = "Apr";
		}else if("05".equals(pMonth)){
			ret = "May";
		}else if("06".equals(pMonth)){
			ret = "June";
		}else if("07".equals(pMonth)){
			ret = "July";
		}else if("08".equals(pMonth)){
			ret = "Aug";
		}else if("09".equals(pMonth)){
			ret = "Sept";
		}else if("10".equals(pMonth)){
			ret = "Oct";
		}else if("11".equals(pMonth)){
			ret = "Nov";
		}else if("12".equals(pMonth)){
			ret = "Dec";
		}
		return ret;
	}

	}
