package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserViewDataArr {
    private UserViewData[] data;
    
    private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
    	
    public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public UserViewDataArr(){
    	super();
    	data = new UserViewData[1];	
    	
    }

	public UserViewData[] getdata() {
		return data;
	}

	public void setdata(UserViewData[] data) {
		this.data = data;
	}
    
    
}
