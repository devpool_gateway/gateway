package com.nirvasoft.rp.users.data;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserViewDataset {
	
	private ArrayList<UserViewData> arlData;	

	
	public ArrayList<UserViewData> getArlData() {
		return arlData;
	}

	public void setArlData(ArrayList<UserViewData> ret) {
		this.arlData = ret;
	}


}
