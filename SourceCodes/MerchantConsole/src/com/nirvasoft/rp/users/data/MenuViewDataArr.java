package com.nirvasoft.rp.users.data;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MenuViewDataArr {
    private MenuViewData[] data;
    private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	
	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public MenuViewDataArr(){
    	super();
    	data = new MenuViewData[1];	
    	
    }

	public MenuViewData[] getdata() {
		return data;
	}

	public void setdata(MenuViewData[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "MenuViewDataArr [data=" + Arrays.toString(data) + ", searchText=" + searchText + ", totalCount="
				+ totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + "]";
	}    
    
}
