package com.nirvasoft.rp.users.data;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DepartmentDataset {

	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private ArrayList<DepartmentData> arlData;
	private DepartmentData[] departmentdata;

	
	public ArrayList<DepartmentData> getArlData() {
		return arlData;
	}

	public void setArlData(ArrayList<DepartmentData> arlData) {
		this.arlData = arlData;
	}

	public DepartmentData[] getDepartmentdata() {
		return departmentdata;
	}

	public void setDepartmentdata(DepartmentData[] departmentdata) {
		this.departmentdata = departmentdata;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	


}
