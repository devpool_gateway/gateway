package com.nirvasoft.rp.bl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.MUJunctionDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.MUJunctionData;

public class MUJunctionMgr {
	
	public Result saveMUJunction(String name,ArrayList<MUJunctionData> datalist, MrBean user) {

		Result ret = new Result();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        MUJunctionDao dao = new MUJunctionDao();
        
        try {
        	if(dao.checkMerchantIDAlreadyexist(name,conn)){
        		
        		if(dao.updateMUJunction(name,datalist, conn)){
        			ret.setMerchantID(name);
	           	     ret.setState(true);
	   	   			 ret.setMsgCode("0000");
	   	   			 ret.setMsgDesc("Updated Successfully.");
        		}else{
	           		ret.setState(false);
	           		ret.setMsgCode("0014");
	      			 	ret.setMsgDesc("Update Failed.");
        		}
        		
        	}else{
        		ret.setMerchantID(name);
        		if(dao.saveMUJunction(datalist, conn)){
        			
	        	     ret.setState(true);
		   			 ret.setMsgCode("0000");
		   			 ret.setMsgDesc("Saved Successfully.");
	        	}else{
	        		ret.setState(false);
	        		ret.setMsgCode("0014");
	   			 	ret.setMsgDesc("Save Failed.");
	        	}
        	}
        	
		        	

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return ret;

	}

	public Result deleteMerchantID(String merchantID, MrBean user) {
		Result ret=new Result();
		 Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		 MUJunctionDao dao = new MUJunctionDao();
		 try {
	        	
			 ret=dao.deleteMerchantID(merchantID, conn);

	        } catch (Exception e) {

	            e.printStackTrace();
	        } finally {
	            try {
	                if (!conn.isClosed())
	                    conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();

	            }
	        }
		 
	return ret;
		
	}

	

	
}
