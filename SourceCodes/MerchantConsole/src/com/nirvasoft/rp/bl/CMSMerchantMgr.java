package com.nirvasoft.rp.bl;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.CMSMerchantDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.LoginUserMerchantInfo;
import com.nirvasoft.rp.shared.MerchantListDataArr;

public class CMSMerchantMgr {
	
	
	public CMSMerchantData getMerchantInfo(String aMerchantID, Connection aConnection) throws SQLException {

		    CMSMerchantData datalist=new CMSMerchantData();

	        try {
	            datalist = CMSMerchantDao.read(aMerchantID, aConnection);

	        } catch (Exception e) {

	            e.printStackTrace();
	        } 

	     return datalist;

    }
	
	public Lov3 getMerchantIDListDetail(MrBean user) {

		Lov3 lov3 = new Lov3();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        String getUser = user.getUser().getUserId();
       
        
        CMSMerchantDao dao = new CMSMerchantDao();
        try {
        	lov3 = dao.getMerchantIDListDetail(conn, getUser);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return lov3;

	}
	
	public Lov3 getInitiatedBy(MrBean user) {

		Lov3 lov3 = new Lov3();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        String getUser = user.getUser().getUserId();
       
        
        CMSMerchantDao dao = new CMSMerchantDao();
        try {
        	lov3 = dao.getInitiatedBy(conn, getUser);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return lov3;

	}
	
	public Lov3 getStatus(MrBean user) {

		Lov3 lov3 = new Lov3();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        String getUser = user.getUser().getUserId();
       
        
        CMSMerchantDao dao = new CMSMerchantDao();
        try {
        	lov3 = dao.getStatus(conn, getUser);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return lov3;

	}
	
	public Lov3 getMerchantIDList(MrBean user) {

		Lov3 lov3 = new Lov3();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        CMSMerchantDao dao = new CMSMerchantDao();
        try {
        	lov3 = dao.getMerchantIDList(conn);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return lov3;

	}
	
	
	public MerchantListDataArr getMerchantList(MrBean user) {

		MerchantListDataArr lov3 = new MerchantListDataArr();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        CMSMerchantDao dao = new CMSMerchantDao();
        try {
        	System.out.println("Testing Mgr");
        	lov3 = dao.getMerchantList(conn);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return lov3;

	}

	public  LoginUserMerchantInfo LoginUserMerchantInfo(String loginUser, MrBean user) {
		LoginUserMerchantInfo data = new LoginUserMerchantInfo();
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
        CMSMerchantDao dao = new CMSMerchantDao();
        try {
        	data = dao.LoginUserMerchantInfo(loginUser,conn);
        	
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
		return data;
	}

}
