package com.nirvasoft.rp.bl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.DispaymentTransactionDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.shared.DetailsData;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.PayTemplateDetail;
import com.nirvasoft.rp.shared.PayTemplateHeader;

public class DBDispaymentMgr {

	public DispaymentList getMerchantList(int currentPage, int totalCount, int pageSize, String aMerchantID,String status,
			String aFromDate, String aToDate, String aFromAcc, String aToAcc,String did,String initiatedby,String transrefno, MrBean aUser,String processingCode,String proDisCode, PayTemplateDetail[] templateData) {

		DispaymentList ret = new DispaymentList();
		

		Connection l_Conn = null;
		DispaymentTransactionDao l_DAO = new DispaymentTransactionDao();
		try {
			l_Conn = ConnAdmin.getConn(aUser.getUser().getOrganizationID(), "");
			ret = l_DAO.getMerchantList(currentPage, totalCount, pageSize, aMerchantID,status, aFromDate, aToDate, aFromAcc, aToAcc,did,initiatedby,transrefno,processingCode,proDisCode, templateData, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	
	public DispaymentList getTransactionHisByCIFID(int currentPage, int totalCount, int pageSize, String aMerchantID, String aCustomerID, 
			String aFromDate, String aToDate, String aFromAcc, String aToAcc,String did,String initiatedby,String transrefno, MrBean aUser,String processingCode,String proDisCode, PayTemplateDetail[] templateData) {

		DispaymentList ret = new DispaymentList();

		Connection l_Conn = null;
		DispaymentTransactionDao l_DAO = new DispaymentTransactionDao();
        System.out.println("Merchant DAO.." + aMerchantID);
		try {
			l_Conn = ConnAdmin.getConn(aUser.getUser().getOrganizationID(), "");
			ret = l_DAO.getTransactionHisByCIFID(currentPage, totalCount, pageSize, aMerchantID, aCustomerID, aFromDate, aToDate, aFromAcc, aToAcc,did,initiatedby,transrefno,processingCode,proDisCode, templateData, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	public PayTemplateHeader getPaymentTemplateByID(String merchantID,String saveDirectory,MrBean aUser ) {
		PayTemplateHeader ret = new PayTemplateHeader();
		
		Connection l_Conn = null;
		DispaymentTransactionDao mdHeader = new DispaymentTransactionDao();
		try{
			l_Conn = ConnAdmin.getConn(aUser.getUser().getOrganizationID(), "");
			ret = mdHeader.getPaymentTemplateByID(merchantID,saveDirectory,l_Conn);
			
		}catch(Exception e){
			e.printStackTrace();
			
		}finally{
			try {
				if(!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		return ret;
	}
	
	public ArrayList<DetailsData> getshowdetails(String syskey,MrBean aUser) {
		ArrayList<DetailsData> ret = new ArrayList<DetailsData>();
		Connection l_Conn = null;
		DispaymentTransactionDao l_DAO = new DispaymentTransactionDao();
		try {
			l_Conn = ConnAdmin.getConn(aUser.getUser().getOrganizationID(), "");
			ret = l_DAO.getshowdetails(syskey, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	// CNP Transaction Report //
	
	public String getAllCustRefNo (String aFromDate, String aToDate , String batchNo) {

		 String custRef = "";
		 DispaymentTransactionDao dao = new DispaymentTransactionDao();

        Connection l_Conn = null;
        try {
       	 l_Conn = ConnAdmin.getConn("001", "");
       	 custRef = dao.getAllCustRefNo(aFromDate, aToDate, batchNo, l_Conn);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (!l_Conn.isClosed())
                    l_Conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return custRef;
    }

	
	 public DispaymentList getCNPTransactionList (int currentPage, int totalCount, int pageSize, String aFromDate, String aToDate, String accType) {

         DispaymentList ret = new DispaymentList();

         Connection l_Conn = null;
         DispaymentTransactionDao l_DAO = new DispaymentTransactionDao();
         try {
        	 l_Conn = ConnAdmin.getConn("001", "");
             ret = l_DAO.getCNPTransactionList(currentPage, totalCount, pageSize, aFromDate, aToDate, accType, l_Conn);
         } catch (Exception e) {
             e.printStackTrace();
         } finally {
             try {
                 if (!l_Conn.isClosed())
                     l_Conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }
         return ret;
     }

}
