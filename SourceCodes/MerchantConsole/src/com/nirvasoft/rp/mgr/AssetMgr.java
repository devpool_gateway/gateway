package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.AssetDao;
import com.nirvasoft.rp.dao.V_AssetDao;
import com.nirvasoft.rp.data.AssetData;
import com.nirvasoft.rp.data.V_AssetDataSet;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;


public class AssetMgr {

	public static Result saveAsset(AssetData data, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");

		try {
			res = save(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Result save(AssetData data, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		data = initData(data, user);
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			data.setT1(AssetDao.readMaxCount(conn));
			res = AssetDao.insert(data, conn);
		} else {
			res = AssetDao.update(data, conn);
		}
		if (res.isState()) {
			res.getLongResult().add(data.getSyskey());

			ArrayList<String> sr = res.getStringResult();
			sr.add(data.getT1());
			sr.add(data.getCreatedDate());
			sr.add(data.getT5());
		}
		return res;
	}
	
	public static AssetData initData(AssetData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
			data.setModifiedDate(date);
			data.setRecordStatus(1);
			data.setSyncBatch(0);
			data.setSyncStatus(1);
		} else {
			data.setModifiedDate(date);
		}

		return data;
	}

	public static V_AssetDataSet getAssetList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		V_AssetDataSet res = new V_AssetDataSet();
		try {
			res = V_AssetDao.getAssetList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
		
	}

	public static Result deleteAsset(long syskey, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = AssetDao.delete(syskey, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static AssetData readDataBySyskey(long pKey, MrBean user) {
		AssetData res = new AssetData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = AssetDao.read(pKey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	/*public static AssetData readDataBySyskey(long pKey, MrBean user) {
		AssetData res = new AssetData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = AssetDao.read(pKey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
*/
}
