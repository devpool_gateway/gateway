package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.MenuDao;
import com.nirvasoft.rp.dao.UserDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Menu;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.MenuData;
import com.nirvasoft.rp.users.data.MenuDataArr;
import com.nirvasoft.rp.users.data.MenuDataset;
import com.nirvasoft.rp.users.data.MenuRole;
import com.nirvasoft.rp.users.data.MenuViewData;
import com.nirvasoft.rp.users.data.MenuViewDataArr;
import com.nirvasoft.rp.users.data.MenuViewDataset;
import com.nirvasoft.rp.users.data.RoleMenuData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserRole;
import com.nirvasoft.rp.util.ServerUtil;

public class MenuDataMgr {
	
	public static MenuViewDataArr getAllMenuData(String searchText,int pageSize,int currentPage) {
		MenuViewDataArr res = new MenuViewDataArr();
		MenuViewDataset dataSet = new MenuViewDataset();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			dataSet = MenuDao.getAllMenuData(searchText, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<MenuViewData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));
		
		MenuViewData[] dataarry = new MenuViewData[dataSet.getArlData().size()];
		if(dataarry.length==1) 
		{ 
		dataarry = new MenuViewData[dataSet.getArlData().size()+1]; 
		dataarry[0] = dataSet.getArlData().get(0); 
		dataarry[1] = new MenuViewData(); 
		}
		for(int i=0;i<dataSet.getArlData().size();i++)
		{
			dataarry[i] = dataSet.getArlData().get(i);
		}
		
		res.setdata(dataarry);
		res.setSearchText(searchText);
		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);
		
		return res;
	}
	
	public static Result saveMenuData(MenuData data, MrBean user) {
		Result res = new Result();

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		
		
		try {
			long rolemenu= data.getSyskey();
			res = saveMenuData(data, user, conn);
			if(res.isState() &&  rolemenu==0)
			{
			      res= saveRoleMenuData(data,user,conn);	
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static MenuDataArr getmainmenulist(MrBean user) {
		MenuDataArr res = new MenuDataArr();
		MenuDataset dataSet = new MenuDataset();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			dataSet = MenuDao.getmainmenulist(user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		MenuData[] dataarry = new MenuData[dataSet.getArlData().size()];
		if(dataarry.length==1) { 
		dataarry = new MenuData[dataSet.getArlData().size()+1]; 
		dataarry[0] = dataSet.getArlData().get(0); 
		dataarry[1] = new MenuData(); 
		}
		if(dataarry.length==1)
		{
			dataarry = new MenuData[dataSet.getArlData().size()+1];
			dataarry[0] = dataSet.getArlData().get(0);
			dataarry[1] = new MenuData();
		}
		
		//RoleData[] dataarry = dataSet.getRoledata();
		
		for(int i=0;i<dataSet.getArlData().size();i++){
			dataarry[i] = dataSet.getArlData().get(i);
		}
		
		res.setdata(dataarry);
		
		return res;
	}
	
	
	public static Result saveRoleMenuData(MenuData data, MrBean user, Connection conn) throws SQLException {
	Result res=new Result();
	MenuRole mr=new MenuRole();
	mr = initMenuRoleData(mr,data,user,conn);
	if (mr.getSyskey() == 0) {
		
		mr.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
		res=MenuDao.insertMenuRole(mr, conn);
	} else {
		res = MenuDao.update(data, conn);	/// need to edit		
	}
	if (res.isState()) {
		
		
		res.getLongResult().add(data.getN2());
		res.getLongResult().add(data.getSyskey());
		//res.setKeyResult(data.getSyskey());
		long[] keyarry = new long[res.getLongResult().size()];

        for (int i = 0; i < res.getLongResult().size(); i++) {
            keyarry[i] = res.getLongResult().get(i); 
            System.out.println("HELLO KEY ARRAY"+keyarry[i]);
        }

        res.setKey(keyarry);

	}
	return res;

	}
	
	public static Result saveMenuData(MenuData data, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		
		data = initData(data, user, conn);
		System.out.println("SYSKEY+++"+data.getSyskey());
		if (data.getSyskey() == 0) {
			
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			res = MenuDao.insert(data, conn);
		
		} else {
			res = MenuDao.update(data, conn);			
		}
		if (res.isState()) {
			
			
		}
		return res;
	}
	
	
	public static MenuRole initMenuRoleData(MenuRole mr, MenuData data, MrBean user, Connection con) 
	{
		
		String date23 = new SimpleDateFormat("yyyyMMdd").format(new Date());
		mr.setUserId(user.getUser().getUserId());
		mr.setUserName(user.getUser().getUserName());
		mr.setModifiedDate(date23);	
		mr.setN1(1);
		mr.setN2(data.getSyskey());
		
		if (mr.getSyskey() == 0) {
			mr.setCreatedDate(date23);
			mr.setRecordStatus(1);
			mr.setSyncBatch(0);
			mr.setSyncStatus(1);	
		}

		
		return  mr;
	}
	
	public static MenuData initData(MenuData data, MrBean user, Connection con) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setModifiedDate(date);
		
		
		
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
			data.setRecordStatus(1);
			data.setSyncBatch(0);
			data.setSyncStatus(1);
			//int moduleId =(int) (Math.random()*48+52); // to get between 51 and 100
			//data.setT4(moduleId);	
		}

		return data;
	}
	
	/*public static MenuViewDataArr getAllMenuData(MrBean user) {
		MenuViewDataArr res = new MenuViewDataArr();
		MenuViewDataset dataSet = new MenuViewDataset();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			dataSet = MenuDao.getAllMenuData(user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		MenuViewData[] dataarry = new MenuViewData[dataSet.getArlData().size()];
		if(dataarry.length==1) 
		{ 
		dataarry = new MenuViewData[dataSet.getArlData().size()+1]; 
		dataarry[0] = dataSet.getArlData().get(0); 
		dataarry[1] = new MenuViewData(); 
		}
		for(int i=0;i<dataSet.getArlData().size();i++)
		{
			dataarry[i] = dataSet.getArlData().get(i);
		}
		
		res.setdata(dataarry);
		
		return res;
	}*/
	public static MenuData readDataBySyskey(long pKey, MrBean user) {
		MenuData res = new MenuData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = MenuDao.read(pKey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	public static Result deleteMenuData(long syskey,String isParent, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = MenuDao.delete(syskey,isParent, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	/*public static MenuViewDataArr getParentMenuData(MrBean user) {
		MenuViewDataArr res = new MenuViewDataArr();
		MenuViewDataset dataSet = new MenuViewDataset();
		
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			dataSet = MenuDao.getParentMenuData(user, conn);		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		MenuViewData[] dataarry = new MenuViewData[dataSet.getArlData().size()];
		
		if (dataSet.getArlData().size()==1)
			dataarry = new MenuViewData[dataSet.getArlData().size()+1];
		
		for(int i=0;i<dataSet.getArlData().size();i++){
			dataarry[i] = dataSet.getArlData().get(i);
		}
		
		res.setdata(dataarry);
		
		return res;
	}
	
	
	public static MenuViewDataArr getChildMenuData(long key, MrBean user) {
		MenuViewDataArr res = new MenuViewDataArr();
		MenuViewDataset dataSet = new MenuViewDataset();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			dataSet = MenuDao.getChildMenuData(key, user, conn);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		
		MenuViewData[] dataarry = new MenuViewData[dataSet.getArlData().size()];
		if(dataarry.length==1) { 
			dataarry = new MenuViewData[dataSet.getArlData().size()+1]; 
			dataarry[0] = dataSet.getArlData().get(0); 
			dataarry[1] = new MenuViewData(); }
		if (dataSet.getArlData().size()==1)
			dataarry = new MenuViewData[dataSet.getArlData().size()+1];
		
		for(int i=0;i<dataSet.getArlData().size();i++){
			dataarry[i] = dataSet.getArlData().get(i);
		}
		
		res.setdata(dataarry);
		
		return res;
	}
	*/
	
	/*
	public static void insertMenuRole (MrBean user, MenuData ur, Connection conn)
	{
		try
		{
			MenuRole obj = new MenuRole( );
			String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
			obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			obj.setUserId(user.getUser().getUserId());
			obj.setUserName(user.getUser().getUserName());
			obj.setModifiedDate(date);
			obj.setRecordStatus(1);
			obj.setN1(1);  //row syskey
			obj.setN2((int)ur.getSyskey());	//menu syskey
			MenuDao.insertMenuRole(obj,conn);
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		
	}
	*/
	

	
}
