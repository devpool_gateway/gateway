package com.nirvasoft.rp.mgr;

import java.sql.Connection;

import com.nirvasoft.rp.dao.UserRoleViewDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;

import com.nirvasoft.rp.users.data.UserRoleViewData;


public class UserRoleViewDataMgr {
	
	
	public static UserRoleViewData[] getUserRoleList(MrBean user) {
		
		UserRoleViewData[] dataarray;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		dataarray = UserRoleViewDao.getUserRoleList(conn);
		return dataarray ;
	}
	
	public static UserRoleViewData[] getMerchantUserList(MrBean user) {
		
		UserRoleViewData[] dataarray;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		dataarray = UserRoleViewDao.getMerchantUserList(conn);
		return dataarray ;
	}
}
