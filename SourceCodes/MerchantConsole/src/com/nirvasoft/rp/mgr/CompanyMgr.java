package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.CompanyDao;
import com.nirvasoft.rp.data.CompanyData;
import com.nirvasoft.rp.data.CompanyDataSet;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;

public class CompanyMgr {
	public static Result saveCompany(CompanyData data, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");

		try {
			res = save(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Result save(CompanyData data, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		data = initData(data, user);
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			res = CompanyDao.insert(data, conn);
		} else {
			res = CompanyDao.update(data, conn);
		}
		if (res.isState()) {
			res.getLongResult().add(data.getSyskey());

			ArrayList<String> sr = res.getStringResult();
			sr.add(data.getT1());
			sr.add(data.getCreatedDate());
			sr.add(data.getT5());
		}
		return res;
	}
	
	public static CompanyData initData(CompanyData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
			data.setModifiedDate(date);
			data.setRecordStatus(1);
			data.setSyncBatch(0);
			data.setSyncStatus(1);
		} else {
			data.setModifiedDate(date);
		}
		return data;
	}

	public static CompanyDataSet getCompanyList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		CompanyDataSet res = new CompanyDataSet();
		try {
			res = CompanyDao.getCompanyList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
