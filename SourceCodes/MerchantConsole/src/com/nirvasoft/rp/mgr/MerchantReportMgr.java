package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.management.modelmbean.RequiredModelMBean;

import com.nirvasoft.rp.shared.DBBarResponse;
import com.nirvasoft.rp.shared.DBGenrealRequest;
import com.nirvasoft.rp.shared.DBGenrealResponse;
import com.nirvasoft.rp.shared.DetailsData;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.LoginUserMerchantInfo;
import com.nirvasoft.rp.shared.PayTemplateDetail;
import com.nirvasoft.rp.shared.PayTemplateHeader;
import com.nirvasoft.rp.util.ServerUtil;
import com.nirvasoft.rp.dao.MerchantReportDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.MrBean;

public class MerchantReportMgr {

	public  LoginUserMerchantInfo LoginUserMerchantInfo(String loginUser) {
		LoginUserMerchantInfo data = new LoginUserMerchantInfo();
        Connection conn = null;
        MerchantReportDao dao = new MerchantReportDao();
        try {
        	conn = ConnAdmin.getConn("001", "");
        	data = dao.LoginUserMerchantInfo(loginUser,conn);
        	
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
		return data;
	}
	
	public String getFeatureName(int feature) {

		String ret = new String();
		Connection l_Conn = null;
		MerchantReportDao l_DAO = new MerchantReportDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = l_DAO.getFeatureName(feature, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	public ArrayList<DetailsData> getshowdetails(String syskey) {
		ArrayList<DetailsData> ret = new ArrayList<DetailsData>();
		Connection l_Conn = null;
		MerchantReportDao l_DAO = new MerchantReportDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = l_DAO.getshowdetails(syskey, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	public Lov3 getMerchantIDListDetail(String userId) {

		Lov3 lov3 = new Lov3();
        Connection conn = null;
        MerchantReportDao dao = new MerchantReportDao();
        
        try {
        	conn = ConnAdmin.getConn("001", "");
        	lov3 = dao.getMerchantIDListDetail(conn, userId);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

     return lov3;

	}
	
	public PayTemplateHeader getPaymentTemplateByID(String merchantID,String saveDirectory ) {
		PayTemplateHeader ret = new PayTemplateHeader();
		
		Connection l_Conn = null;
		MerchantReportDao mdHeader = new MerchantReportDao();
		try{
			l_Conn = ConnAdmin.getConn("001", "");
			ret = mdHeader.getPaymentTemplateByID(merchantID,saveDirectory,l_Conn);
			
		}catch(Exception e){
			e.printStackTrace();
			
		}finally{
			try {
				if(!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		return ret;
	}
	
	
	public DispaymentList getMerchantList(int currentPage, int totalCount, int pageSize, String aMerchantID,String aFeature,String status,
			String aFromDate, String aToDate, String aFromAcc, String aToAcc,String did,String initiatedby,String transrefno, String processingCode,String proDisCode, PayTemplateDetail[] templateData) {

		DispaymentList ret = new DispaymentList();
		

		Connection l_Conn = null;
		MerchantReportDao l_DAO = new MerchantReportDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = l_DAO.getMerchantList(currentPage, totalCount, pageSize, aMerchantID,aFeature,status, aFromDate, aToDate, aFromAcc, aToAcc,did,initiatedby,transrefno,processingCode,proDisCode, templateData, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	public ArrayList<DBGenrealResponse> getGeneralDashboard(DBGenrealRequest request ){
		ArrayList<DBGenrealResponse> ret = new ArrayList<DBGenrealResponse>();
		Connection l_Conn = null;
		MerchantReportDao l_Dao = new MerchantReportDao();
		try{
			l_Conn = ConnAdmin.getConn("001", "");
			
			if(request.getProcessingCode().subSequence(0, 2).equals("05")){
				ret = l_Dao.getGeneralDashboardCNP(request.getMerchantID(), l_Conn,request.getFeature());
				getPercentage(ret);
			}else if(request.getProcessingCode().subSequence(0, 2).equals("04")){
				ret = l_Dao.getGeneralDashboardSkyPay(request.getMerchantID(), l_Conn);
				getPercentage(ret);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}			
		return ret;					
	}
	private void getPercentage(ArrayList<DBGenrealResponse> list){
		
		double dbtotal = 0.00;
		for(int i=0; i<list.size(); i++){			
			dbtotal +=list.get(i).getY();
		}
		for(int k=0; k<list.size(); k++){
			double dbpercent = (list.get(k).getY()/dbtotal)*100;
			dbpercent = Double.parseDouble(ServerUtil.formatNumber(dbpercent).replace(",", ""));
			list.get(k).setY(dbpercent);
		}		
		
	}
	public DBBarResponse getBarMonthlyTxnCount(DBGenrealRequest request){
		DBBarResponse response = new DBBarResponse();
		Connection l_Conn = null;
		MerchantReportDao l_Dao = new MerchantReportDao();
		try{
			l_Conn = ConnAdmin.getConn("001", "");
			
			if(request.getProcessingCode().subSequence(0, 2).equals("05")){
				response = l_Dao.getBarMonthlyTxnCNP(l_Conn, request.getMerchantID(),"Count",request.getFeature());
			}else if(request.getProcessingCode().subSequence(0, 2).equals("04")){
				response = l_Dao.getBarMonthlyTxnSkypay(l_Conn, request.getMerchantID(), "Count");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}			
		return response;	
	}
	public DBBarResponse getBarMonthlyTxnAmount(DBGenrealRequest request){
		DBBarResponse response = new DBBarResponse();
		Connection l_Conn = null;
		MerchantReportDao l_Dao = new MerchantReportDao();
		try{
			l_Conn = ConnAdmin.getConn("001", "");
			
			if(request.getProcessingCode().subSequence(0, 2).equals("05")){
				response = l_Dao.getBarMonthlyTxnCNP(l_Conn, request.getMerchantID(),"Amount",request.getFeature());
			}else if(request.getProcessingCode().subSequence(0, 2).equals("04")){
				response = l_Dao.getBarMonthlyTxnSkypay(l_Conn, request.getMerchantID(), "Amount");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}			
		return response;	
	}//
	public DBBarResponse getBarTxnStatusofCurrentMonth(DBGenrealRequest request){
		DBBarResponse response = new DBBarResponse();
		Connection l_Conn = null;
		MerchantReportDao l_Dao = new MerchantReportDao();
		try{
			l_Conn = ConnAdmin.getConn("001", "");
			
			if(request.getProcessingCode().subSequence(0, 2).equals("05")){
				response = l_Dao.getBarTxnStatusofCurrentMonth(l_Conn, request.getMerchantID(),"Count",request.getFeature());
			}else if(request.getProcessingCode().subSequence(0, 2).equals("04")){
				response = l_Dao.getBarTxnStatusofCurrentMonthSkypay(l_Conn, request.getMerchantID(),"Count");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}			
		return response;	
	}
}
