package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.ReconciliationDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.shared.ReconciliationList;

public class ReconciliationMgr {
	
	public ReconciliationList getReconciliationList(int currentPage, int totalCount, int pageSize,
			String aFromDate, String aToDate,String tStatus, String status, String kbztransid,String transid, MrBean aUser) {

		ReconciliationList rec = new ReconciliationList();

		Connection l_Conn = null;
		ReconciliationDao l_DAO = new ReconciliationDao();
		try {
			l_Conn = ConnAdmin.getConn(aUser.getUser().getOrganizationID(), "");
			rec = l_DAO.getReconciliationList(currentPage, totalCount, pageSize, aFromDate, aToDate,tStatus, status, kbztransid, transid, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rec;
	}

}
