package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.bl.CMSMerchantMgr;
import com.nirvasoft.rp.bl.MUJunctionMgr;
import com.nirvasoft.rp.dao.CMSMerchantDao;
import com.nirvasoft.rp.dao.MUJunctionDao;
import com.nirvasoft.rp.dao.PersonDao;
import com.nirvasoft.rp.dao.UserDao;
import com.nirvasoft.rp.dao.UserRoleViewDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.CMSMerchantAccRefData;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.MUJunctionData;
import com.nirvasoft.rp.users.data.PersonData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserRoleViewData;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataArr;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.ServerUtil;

public class UserDataMgr {
	public static Result saveUserData(UserData data, MrBean user) {
		Result res = new Result();

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = saveUserData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	
	public ArrayList<Result> getAllFeatures(String code) {
		Connection conn = null;
		UserDao u_dao = new UserDao();
		ArrayList<Result> res = new ArrayList<Result>();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.getAllFeatures(code,conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static Result saveUserData(UserData data, MrBean user, Connection conn) throws SQLException {

		Result res = new Result();
		
		data.getPerson().setSyskey(UserDao.getPersonSyskey(data.getSyskey(),conn));

		if (data.getPerson().getSyskey() == 0) {
			data.getPerson()
					.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			data = initData(data, user);
			res = PersonDao.insert(getPersonData(data), conn);
		} else {
			data = initData(data, user);
			res = PersonDao.update(getPersonData(data), conn);
		}

		if (res.isState()) {
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));

				data.setN4(data.getPerson().getSyskey());
				res = UserDao.insert(data, conn);
			} else {
				data.setN4(data.getPerson().getSyskey());
				res = UserDao.update(data, conn);
			}

		}

		if (res.isState()) {

			res.setKeyResult(data.getSyskey());
			res.getLongResult().add(data.getSyskey());
			res.getLongResult().add(data.getPerson().getSyskey());
			
		}

		return res;

	}

	public static PersonData getPersonData(UserData bdata) {

		PersonData adata = new PersonData();
		adata.setSyskey(bdata.getPerson().getSyskey());
		adata.setT1(bdata.getT1());
		adata.setT2(bdata.getName());
		adata.setCreatedDate(bdata.getCreatedDate());
		adata.setModifiedDate(bdata.getModifiedDate());
		adata.setRecordStatus(bdata.getRecordStatus());
		adata.setSyncStatus(bdata.getSyncStatus());
		adata.setSyncBatch(bdata.getSyncBatch());
		adata.setUserId(bdata.getUserId());
		adata.setUserName(bdata.getUserName());
		return adata;
	}

	public static UserData initData(UserData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {

			data.setCreatedDate(date);
		}
		if(data.getCreatedDate().equals("")){
			data.setCreatedDate(date);
		}

		data.setT2(ServerUtil.encryptPIN(data.getT2()));
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	public static UserViewDataArr getAllUserData(String searchText,int pageSize,int currentPage,MrBean user) {
		UserViewDataArr res = new UserViewDataArr();
		UserViewDataset dataSet = new UserViewDataset();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			dataSet = UserDao.getAllUserData(searchText,user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<UserViewData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));
		UserViewData[] dataarry = new UserViewData[dataSet.getArlData().size()];
		if (dataarry.length == 1) {
			dataarry = new UserViewData[dataSet.getArlData().size() + 1];
			dataarry[0] = dataSet.getArlData().get(0);
			dataarry[1] = new UserViewData();
		}

		for (int i = 0; i < dataSet.getArlData().size(); i++) {
			dataarry[i] = dataSet.getArlData().get(i);

		}
		res.setdata(dataarry);
		res.setSearchText(searchText);

		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);

//		UserViewData[] dataarry = new UserViewData[dataSet.getArlData().size()];
//
//		if (dataarry.length == 1) {
//			dataarry = new UserViewData[dataSet.getArlData().size() + 1];
//			dataarry[0] = dataSet.getArlData().get(0);
//			dataarry[1] = new UserViewData();
//		}
//
//		for (int i = 0; i < dataSet.getArlData().size(); i++) {
//			dataarry[i] = dataSet.getArlData().get(i);
//
//		}
//
//		res.setdata(dataarry);

		return res;
	}
	
	public String getSessionID() {
		String sessionID = "";
		String prefix = "S";
		Long key = 0L;
		try {
			key= SysKeyMgr.getSysKey(1, "", ConnAdmin.getConn("001", ""));
			sessionID = prefix + key;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sessionID;
	}

	public static UserData readDataBySyskey(long pKey, MrBean user) {
		UserData res = new UserData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = UserDao.read(pKey, conn);
			
			UserRoleViewData[] dataarray;
			dataarray = UserRoleViewDao.getUserRoleList(pKey, conn);
			long pvalue[] = UserRoleViewDao.getRoleResult(pKey, conn);
			String name = PersonDao.getUserName(pKey, conn);
			res.setName(name);
			res.setUserrolelist(dataarray);
			res.setRolesyskey(pvalue);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	public static UserData readDataByID(String pID, MrBean user) {
		UserData res = new UserData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		CMSMerchantMgr dbmgr = new CMSMerchantMgr();
		CMSMerchantData data = new CMSMerchantData();
		try {
			data = dbmgr.getMerchantInfo(pID, conn);
			res.setName(data.getUserId());
			
			UserRoleViewData[] dataarray;
			dataarray = UserRoleViewDao.getMerchantUserList(pID, conn);
			long pvalue[] = UserRoleViewDao.getUsersResult(pID, conn);
			res.setUserrolelist(dataarray);
			res.setRolesyskey(pvalue);

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
            try {
                if (!conn.isClosed())
                	conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }

		return res;
	}

	public static Result deleteUserData(long syskey, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = UserDao.delete(syskey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

}
