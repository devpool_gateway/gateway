package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.BrandDao;
import com.nirvasoft.rp.data.BrandData;
import com.nirvasoft.rp.data.BrandDataSet;
import com.nirvasoft.rp.data.CompanyDataSet;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;


public class BrandMgr {

	public static Result saveBrand(BrandData data, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");

		try {
			res = save(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Result save(BrandData data, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		data = initData(data, user);
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			res = BrandDao.insert(data, conn);
		} else {
			res = BrandDao.update(data, conn);
		}
		if (res.isState()) {
			res.getLongResult().add(data.getSyskey());

			ArrayList<String> sr = res.getStringResult();
			sr.add(data.getT1());
			sr.add(data.getCreatedDate());
			sr.add(data.getT5());
		}
		return res;
	}
	
	public static BrandData initData(BrandData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
			data.setModifiedDate(date);
			data.setRecordStatus(1);
			data.setSyncBatch(0);
			data.setSyncStatus(1);
		} else {
			data.setModifiedDate(date);
		}

		return data;
	}

	public static BrandDataSet getBrandList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		BrandDataSet res = new BrandDataSet();
		try {
			res = BrandDao.getBrandList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}


}
