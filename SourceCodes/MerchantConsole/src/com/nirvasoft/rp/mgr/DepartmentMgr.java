package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.DepartmentDao;

import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.DepartmentData;
import com.nirvasoft.rp.users.data.DepartmentDataArr;
import com.nirvasoft.rp.users.data.DepartmentDataset;

import com.nirvasoft.rp.util.ServerUtil;


public class DepartmentMgr {
	
	public static DepartmentData readDataBySyskey(long pKey, MrBean user) {
		DepartmentData res = new DepartmentData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = DepartmentDao.read(pKey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	public static Result deleteDepartmentData(long syskey, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = DepartmentDao.deletedepartmentdata(syskey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static Result saveDepartmentData(DepartmentData data, MrBean user) {
		Result res = new Result();

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = saveDepartmentData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	
	public static Result saveDepartmentData(DepartmentData data, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		data = initData(data, user);
		
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			res = DepartmentDao.insert(data, conn);
		} else {
			res = DepartmentDao.update(data, conn);
		}
		if (res.isState()) {
			res.getLongResult().add(data.getSyskey());
		}
		return res;
	}
	
	public static DepartmentData initData(DepartmentData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setModifiedDate(date);
		
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
			data.setRecordStatus(1);
			data.setSyncBatch(0);
			data.setSyncStatus(1);
		}

		return data;
	}
	
	public static DepartmentDataArr getAllDepartmentData(MrBean user) {
		DepartmentDataArr res = new DepartmentDataArr();
		DepartmentDataset dataSet = new DepartmentDataset();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			dataSet = DepartmentDao.getAllDepartmentData(user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		DepartmentData[] dataarry = new DepartmentData[dataSet.getArlData().size()];
		
		
		
		for(int i=0;i<dataSet.getArlData().size();i++){
			dataarry[i] = dataSet.getArlData().get(i);
		}
		
		res.setData(dataarry);
		
		return res;
	}
}