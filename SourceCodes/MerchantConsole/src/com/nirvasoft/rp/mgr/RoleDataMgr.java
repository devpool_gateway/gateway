package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.RoleDao;
import com.nirvasoft.rp.dao.RoleMenuDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.MenuData;
import com.nirvasoft.rp.users.data.MenuRole;
import com.nirvasoft.rp.users.data.MenuViewData;
import com.nirvasoft.rp.users.data.MenuViewDataArr;
import com.nirvasoft.rp.users.data.RoleData;
import com.nirvasoft.rp.users.data.RoleDatas;
import com.nirvasoft.rp.users.data.RoleDataset;
import com.nirvasoft.rp.users.data.RoleMenuData;
import com.nirvasoft.rp.util.ServerUtil;


public class RoleDataMgr {
	
	public RoleDatas getAllRoleListData(String searchText,int pageSize,int currentPage) {
		RoleDatas res = new RoleDatas();
		RoleDataset dataSet = new RoleDataset();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			dataSet = RoleDao.getAllRoleListData(searchText,pageSize , currentPage,  conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<RoleData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));
		RoleData[] dataarray = new RoleData[dataSet.getArlData().size()];
		
		if(dataarray.length==1)
		{
			dataarray = new RoleData[dataSet.getArlData().size()+1];
			dataarray[0] = dataSet.getArlData().get(0);
			dataarray[1] = new RoleData();
		}
		
		for(int i=0;i<dataSet.getArlData().size();i++){
			dataarray[i] = dataSet.getArlData().get(i);
		}
		res.setdata(dataarray);
		res.setSearchText(searchText);
		
		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);
		
		
		return res;
	}
	
	public static Result saveRoleData(RoleData data, MrBean user) {
		Result res = new Result();

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			res = saveRoleData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static Result saveRoleData(RoleData data, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		Result res1 = new Result();
		
		data = initData(data, user);
		
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
			res = RoleDao.insert(data, conn);
			if(res.isState())
			{
				res1= insertRoleMenu(user, data, conn);
				
				if(res1.isState())
					res1.setMsgDesc("Saved successfully");
				else
					res1.setMsgDesc("Cannot save");
			}
			  
			else
				res1.setMsgDesc("Data already exist");
			
		} else {
			res = RoleDao.update(data, conn);
			
			if(res.isState())
			{
				res1 = updateRoleMenu(user, data, conn);
				if(res1.isState())
					res1.setMsgDesc("Updated successfully");
			}
			else
				res1.setMsgDesc("Data already exist");
			
			
		}
		if (res1.isState()) {
			res1.setKeyResult(data.getSyskey());
		}
		return res1;
	}
	
	public static RoleData initData(RoleData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);
		data.setT3(new String("51"));  //for Module
		
		if(data.getSyskey()==0)
		{
			data.setCreatedDate(date);
		}
		
		return data;
	}
	
	public static RoleDatas getAllRoleData(MrBean user) {
		RoleDatas res = new RoleDatas();
		RoleDataset dataSet = new RoleDataset();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			dataSet = RoleDao.getAllRoleData(user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		RoleData[] dataarray = new RoleData[dataSet.getArlData().size()];
		
		if(dataarray.length==1)
		{
			dataarray = new RoleData[dataSet.getArlData().size()+1];
			dataarray[0] = dataSet.getArlData().get(0);
			dataarray[1] = new RoleData();
		}
		
		for(int i=0;i<dataSet.getArlData().size();i++){
			dataarray[i] = dataSet.getArlData().get(i);
		}
		
		res.setdata(dataarray);
		
		return res;
	}
	
	public static RoleData readDataBySyskey(long pKey, MrBean user) {
		RoleData res = new RoleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		try {
			  res = RoleDao.read(pKey, conn);
			  RoleMenuData[] dataarray; 
			  dataarray= RoleMenuDao.getRoleMenuList(pKey, conn);
			  long pvalue[]=RoleMenuDao.getMenuResult(pKey,conn);
			  long cvalue[]=RoleMenuDao.getChildResult(pKey,conn);
			  res.setMenu(dataarray);
			  res.setParentsyskey(pvalue);
			  res.setChildsyskey(cvalue);
			  
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}

	public static Result deleteRoleData(long syskey, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		
		try {
		
			Result ret=RoleDao.deleteRoleMenu(syskey, conn);
			if(ret.isState())
			{
				res = RoleDao.delete(syskey, conn);
				res.setMsgDesc("Deleted successfully");
			}
			else
				res.setMsgDesc("Cannot Delete");
				
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static Result insertRoleMenu (MrBean user, RoleData ur, Connection conn)
	{
		Result res = new Result();
		try
		{
			if(ur.getParentsyskey()!=null && ur.getParentsyskey().length > 0)
			for(int i=0; i< ur.getParentsyskey().length; i++)
			{
				res = new Result();
				MenuRole obj = new MenuRole( );
				String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
				obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
				obj.setUserId(user.getUser().getUserId());
				obj.setUserName(user.getUser().getUserName());
				obj.setCreatedDate(date);
				obj.setModifiedDate(date);
				obj.setRecordStatus(1);
				obj.setN1(ur.getSyskey());  //row syskey
				obj.setN2(ur.getParentsyskey()[i]);	//menu syskey
				res=RoleDao.insertRoleMenu(obj,conn);
			   
				
			}
			
			if(ur.getChildsyskey()!=null && ur.getChildsyskey().length > 0)
			for(int i=0; i< ur.getChildsyskey().length; i++)
			{
			
			  if(RoleDao.checkParentContain(ur.getParentsyskey(),ur.getChildsyskey()[i],conn)){
				res = new Result();
				MenuRole obj = new MenuRole( );
				String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
				obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID(), "")));
				obj.setUserId(user.getUser().getUserId());
				obj.setUserName(user.getUser().getUserName());
				obj.setCreatedDate(date);
				obj.setModifiedDate(date);
				obj.setRecordStatus(1);
				obj.setN1(ur.getSyskey());  //row syskey
				obj.setN2(ur.getChildsyskey()[i]);	//menu syskey
				res=RoleDao.insertRoleMenu(obj,conn);
			
			  }
				
			}
			
			
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		return res;
	}
	
	public static Result updateRoleMenu (MrBean user, RoleData ur, Connection conn)
	{
		Result res = new Result();
		try
		{
			
			res = RoleDao.updateRoleMenu(ur,conn);
			if(res.isState())
			{
				res=insertRoleMenu(user, ur, conn);	
			}
			   
		
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		return res;
	}
	
}