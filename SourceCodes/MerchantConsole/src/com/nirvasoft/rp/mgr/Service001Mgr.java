package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.Service001Dao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Menu;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Profile;
import com.nirvasoft.rp.util.ServerUtil;


public class Service001Mgr {
	
	public static String login (MrBean user) {
		String st="";
		Connection con = ConnAdmin.getConn("001", "");
		try {
			st=Service001Dao.login(user, con);
			return st;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return st;
	}
	
	public Menu[] getProfileSubMenuItem (Profile p, String parent)
	{
		Menu[] ary = null;
		Service001Dao dao = new Service001Dao();
		Connection con = ConnAdmin.getConn("001", "");
		ary = dao.getProfileSubMenuItem(p, con, parent);		
		return ary;		
		
	}
	/*
	public static String[] getMenu (MrBean user, String userid) 
	{
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		ArrayList<String> arr = Service001Dao.getMenu(user, con, userid);
		String[ ] ary = new String[arr.size()];
		for(int i=0;i<arr.size();i++)
		{
			ary[i]=arr.get(i);
		}
		
		return ary;		
		
	}*/
	public static String[] getMainMenu (MrBean user, String userid) 
	{
		Connection con = ConnAdmin.getConn("001", "");
		ArrayList<String> arr = Service001Dao.getMainMenu(user, con, userid);
		String[ ] ary = new String[arr.size()];
		for(int i=0;i<arr.size();i++)
		{
			ary[i]=arr.get(i);
		}
		
		return ary;		
		
	}
	
	public static String[] getSubMenuItem (MrBean user, String userid, String parent)
	{
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		ArrayList<String> arr = Service001Dao.getSubMenuItem(user, con, userid, parent);
		String[ ] ary = new String[arr.size()];
		for(int i=0;i<arr.size();i++)
		{
			ary[i]=arr.get(i);
		}
		
		return ary;		
		
	}
	

	
}
