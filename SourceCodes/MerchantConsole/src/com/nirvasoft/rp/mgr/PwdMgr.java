package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.PasswordPolicyDao;
import com.nirvasoft.rp.dao.PwdDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.shared.ForceChangePwdReq;
import com.nirvasoft.rp.shared.ForceChangePwdResponse;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.shared.Result;
import com.nirvasoft.rp.util.ServerUtil;



public class PwdMgr {

	public Result changePwd(PwdData data, MrBean aUser) {
		// TODO Auto-generated method stub
		Result ret=new Result(); 
		PwdDao t_DAO=new PwdDao();
		DAOManager l_DAO = new DAOManager();
		Connection l_Conn = null;

		try{

			l_Conn = ConnAdmin.getConn(aUser.getUser().getOrganizationID(), "");
			
		    ret=t_DAO.updatePwdData(data, aUser.getUser().getUserId(), l_Conn);	    	
		  
			
		}catch(SQLException e){
			
			e.printStackTrace();
			
		}
		finally{

				try {
					if(!l_Conn.isClosed())
						l_Conn.close();
					//l_DAO.deregisterDriver();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		return ret;
	}
	public com.nirvasoft.rp.framework.Result checkPasswordPolicyPattern(String newPwd) throws ParseException {
		// TODO Auto-generated method stub
		PasswordPolicyDao t_DAO=new PasswordPolicyDao();
		com.nirvasoft.rp.framework.Result ret = new com.nirvasoft.rp.framework.Result();
		Connection l_Conn = null;

		try{
			l_Conn = ConnAdmin.getConn("001", "");
			ret=t_DAO.checkPasswordPolicyPattern(newPwd , l_Conn);	    	
		}catch(SQLException e){
			
			e.printStackTrace();
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.getMessage());
			
			
		}
		finally{
			ServerUtil.closeConnection(l_Conn);
			}			
		return ret;
	}
	public ForceChangePwdResponse forcechangePassword(ForceChangePwdReq data) throws ParseException {
		// TODO Auto-generated method stub
		ForceChangePwdResponse ret=new ForceChangePwdResponse(); 
		PwdDao t_DAO=new PwdDao();
		Connection l_Conn = null;

		try{
			l_Conn = ConnAdmin.getConn("001", "");
			ret=t_DAO.forcechangePassword(data, l_Conn);	    	
		}catch(SQLException e){
			
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
			
		}
		finally{

				try {
					if(!l_Conn.isClosed())
						l_Conn.close();
					//l_DAO.deregisterDriver();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
					ret.setCode("0014");
					ret.setDesc(e.getMessage());
				}
			}			
		return ret;
	}


}
