package com.nirvasoft.rp.mgr;

import java.sql.Connection;

import com.nirvasoft.rp.dao.RoleMenuDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.users.data.RoleMenuData;


public class RoleMenuDataMgr {
	
	
	public static RoleMenuData[] getRoleMenuList(MrBean user) {
		
		RoleMenuData[] dataarray;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
		dataarray = RoleMenuDao.getRoleMenuList(conn);
		return dataarray ;
	}
	

}
