package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.DepartmentData;
import com.nirvasoft.rp.users.data.DepartmentDataset;





public class DepartmentDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("GLM003");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 1));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		return ret;
	}

	public static DepartmentData getDBRecord(DBRecord adbr) {
		DepartmentData ret = new DepartmentData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));	
		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setN1(adbr.getInt("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));
		return ret;
	}

	public static DBRecord setDBRecord(DepartmentData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());	
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static DepartmentData read(long syskey, Connection conn)
			throws SQLException {
		DepartmentData ret = new DepartmentData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"WHERE RecordStatus<>4 AND Syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}
	
	public static Result deletedepartmentdata(long syskey, Connection conn)
			throws SQLException {
		Result res = new Result();
		String sql = "UPDATE GLM003 SET RecordStatus=4 WHERE Syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Delete Successfully");
		}
		return res;
	}
	
	public static Result insert(DepartmentData obj, Connection conn)
			throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.setMsgDesc("Save Successfully");
		}
		return res;
	}
	
	public static boolean isCodeExist(DepartmentData obj, Connection conn)
			throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND Syskey <> " + obj.getSyskey()
						+ " AND t1='" + obj.getT1() + "'"+ " OR t2='" + obj.getT2() + "'", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}	
	}
	
	public static Result update(DepartmentData obj, Connection conn)
			throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(
					" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(),
					define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.setMsgDesc("Update Successfully");
		}
		return res;
	}
	public static DepartmentDataset getAllDepartmentData(MrBean user, Connection conn)
			throws SQLException {

		ArrayList<DepartmentData> ret = new ArrayList<DepartmentData>();
		String whereClause = " WHERE RecordStatus<>4 ";

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause,
				" ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}

		DepartmentDataset dataSet = new DepartmentDataset();
		dataSet.setArlData(ret);

		return dataSet;
	}
	
	public static DepartmentDataset getDepartmentData(Connection conn)
			throws SQLException {

		ArrayList<DepartmentData> ret = new ArrayList<DepartmentData>();
		String whereClause = " WHERE RecordStatus<>4 ";

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause,
				" ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}

		DepartmentDataset dataSet = new DepartmentDataset();
		dataSet.setArlData(ret);

		return dataSet;
	}
	
}
