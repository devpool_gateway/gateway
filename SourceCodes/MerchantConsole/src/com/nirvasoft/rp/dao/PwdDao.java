package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.rp.shared.ForceChangePwdReq;
import com.nirvasoft.rp.shared.ForceChangePwdResponse;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.shared.Result;
import com.nirvasoft.rp.util.ServerUtil;

public class PwdDao {

	public Result updatePwdData(PwdData data, String userid, Connection l_Conn) throws SQLException {

		Result ret = new Result();

		if (checkCurrentPassword(data, userid, l_Conn)) {

			// String query = "UPDATE UVM005_W SET [t2]=? , [changePassword]=?
			// WHERE [t1]=?";
			String query = "UPDATE UVM005_W SET  [t2]=?  WHERE [t1]=?";
			PreparedStatement pstmt = l_Conn.prepareStatement(query);
			pstmt.setString(1, ServerUtil.encryptPIN(data.getNewpassword()));

			// pstmt.setInt(2,1);
			pstmt.setString(2, userid);
			System.out.println("Change Password Query" + query);

			if (pstmt.executeUpdate() > 0) {

				ret.setMsgDesc("Updated successfully");
				ret.setKeyst(Long.toString(data.getSyskey()));
				ret.setState(true);
			} else {
				ret.setMsgDesc("Updated fail");
			}
		} else {
			ret.setMsgDesc("Current Password Does Not Exist!");
		}
		return ret;
	}

	public boolean checkCurrentPassword(PwdData data, String userid, Connection l_Conn) throws SQLException {

		String l_Query = "SELECT * FROM UVM005_W WHERE [t1]=?";
		String psw = "";

		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setString(1, userid);

		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {

			psw = rs.getString("t2");
		}

		if (ServerUtil.encryptPIN(data.getPassword()).equals(psw))
			return true;
		else
			return false;
	}

	public ForceChangePwdResponse forcechangePassword(ForceChangePwdReq data, Connection l_Conn)
			throws SQLException, ParseException {
		String userid = data.getUserID();
		ForceChangePwdResponse ret = new ForceChangePwdResponse();
		//SessionDAO s_Dao = new SessionDAO();

		if (ForceChangePwdcheck(data.getOldPassword(), userid, l_Conn)) {

			if (data.getNewPassword().length() < 6 || data.getNewPassword().length() > 20) {
				ret.setCode("0014");
				ret.setDesc("Password length must be between 6 and 20 characters");
			} else {

				String query = "UPDATE UVM005_A SET  [t2]=?  WHERE RecordStatus <> 4 and ([t1]=? COLLATE SQL_Latin1_General_CP1_CS_AS )";
				PreparedStatement pstmt = l_Conn.prepareStatement(query);
				pstmt.setString(1, ServerUtil.encryptPIN(data.getNewPassword()));
				pstmt.setString(2, userid);

				if (pstmt.executeUpdate() > 0) {
					if (ChangePwd(userid, l_Conn) > 0) { // need to change
						//s_Dao.updateUserSession(userid, l_Conn);// Kill Session

						String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
						String logoutquery = "UPDATE tblSession SET Status = ?, LastActivityDateTime = ?, LogOutDateTime = ? WHERE SessionID = ?";
						PreparedStatement pstmt1 = l_Conn.prepareStatement(logoutquery);
						int j = 1;
						pstmt1.setInt(j++, 9);
						pstmt1.setString(j++, date);
						pstmt1.setString(j++, date);
						pstmt1.setString(j++, data.getSessionID());
						int rs = pstmt1.executeUpdate();
						if (rs > 0) {
							ret.setCode("0000");
							ret.setDesc("Updated successfully");
						}

					}

				} else {
					ret.setCode("0014");
					ret.setDesc("Updated Failed");
				}
			}

		} else {
			ret.setCode("0014");
			ret.setDesc("Current Password Doesn't Exist!");
		}

		return ret;
	}
	public boolean ForceChangePwdcheck(String oldPwd, String userid, Connection l_Conn) throws SQLException {
		boolean response = false;

		String l_Query = "SELECT Top(1) t2 FROM UVM005_A WHERE RecordStatus <> 4 And t2 = ? and ([t1]=? COLLATE SQL_Latin1_General_CP1_CS_AS)";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setString(1, ServerUtil.encryptPIN(oldPwd)
				);
		pstmt.setString(2, userid);

		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {

			rs.getString("t2");
			response = true;
		}

		return response;
	}
		public int ChangePwd(String userId, Connection l_Conn) throws SQLException {

		int rs = 0;
		String sql = "Update UVM012_A set n10=0 where t1='" + userId + "' and recordStatus <> 4  ";
		PreparedStatement stmt1 = l_Conn.prepareStatement(sql);

		rs = stmt1.executeUpdate();
		if (rs > 0) {
			return rs;

		}
		return rs;
	}

}
