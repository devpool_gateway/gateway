package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.MUJunctionData;

public class MUJunctionDao {
	
	private String mTable = "MUJunction";
	
	public boolean saveMUJunction(ArrayList<MUJunctionData> pDataList,Connection conn) throws SQLException{
		boolean ret = false;
		String query = " INSERT INTO " + mTable + " ( SysKey ,MerchantID , UserSysKey) VALUES  (?,?,?)";			
		
		PreparedStatement pstmt = conn.prepareStatement(query);
		for(int count = 0; count < pDataList.size(); count++){
			updateRecords(pstmt, pDataList.get(count));
			pstmt.addBatch();
		 }
		 if(pstmt.executeBatch() != null){
			 ret = true;
		 }else{
			 ret = false;
		 }
		 pstmt.close();
		return ret;
	}
	
	private boolean updateRecords(PreparedStatement pPS,MUJunctionData pdata) throws SQLException{
		boolean ret = false;
		
		int index = 1;
		pPS.setLong(index++, pdata.getSyskey());
		pPS.setString(index++, pdata.getMerchantid());
		pPS.setLong(index++, pdata.getUsersyskey());
		
		return ret;
		
	}
	
	
	public Result deleteMerchantID(String merchantID,Connection Conn) throws SQLException{
		Result ret =new Result();
		
		String query="delete from MUJunction where MerchantID = ?";
		PreparedStatement pstmt = Conn.prepareStatement(query);
		pstmt.setString(1, merchantID);
		if(pstmt.executeUpdate() > 0){
			ret.setMsgDesc("Deleted Successfully");
		}
	return ret;	
	}

	public boolean checkMerchantIDAlreadyexist(String name, Connection conn) throws SQLException {
		//Result ret=new Result();
		int count = 0;
		boolean ret = false;
		String query = "select count(*) c from MUJunction where MerchantID=?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setString(1, name);
		ResultSet rs1 = pstmt.executeQuery();

		if (rs1.next()) {
			count = rs1.getInt("c");
		}
		if (count > 0) {
			ret = true;
		}
	return ret;
	}

	public boolean updateMUJunction(String name,ArrayList<MUJunctionData> datalist, Connection conn) throws SQLException {
		boolean ret = false;
		
		String Query="delete from MUJunction where MerchantID = ?";
		PreparedStatement pstmt = conn.prepareStatement(Query);
		pstmt.setString(1, name);
		pstmt.executeUpdate();
		pstmt.close();
		
		String query = " INSERT INTO " + mTable + " ( SysKey ,MerchantID , UserSysKey) VALUES  (?,?,?)";			
		
		pstmt = conn.prepareStatement(query);
		for(int count = 0; count < datalist.size(); count++){
			updateRecords(pstmt, datalist.get(count));
			pstmt.addBatch();
		 }
		 if(pstmt.executeBatch() != null){
			 ret = true;
		 }else{
			 ret = false;
		 }
		
	return ret;

	}
}
