package com.nirvasoft.rp.dao;

import java.sql.Connection;
import com.nirvasoft.rp.framework.Ref;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.DBBarData;
import com.nirvasoft.rp.shared.DBBarDetail;
import com.nirvasoft.rp.shared.DBBarResponse;
import com.nirvasoft.rp.shared.DBGenrealResponse;
import com.nirvasoft.rp.shared.DetailsData;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.DispaymentTransactionData;
import com.nirvasoft.rp.shared.LOVSetupDetailData;
import com.nirvasoft.rp.shared.LoginUserMerchantInfo;
import com.nirvasoft.rp.shared.PayTemplateDetail;
import com.nirvasoft.rp.shared.PayTemplateDetailArr;
import com.nirvasoft.rp.shared.PayTemplateHeader;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerUtil;
import com.nirvasoft.rp.shared.ServerGlobal;

public class MerchantReportDao {

	public LoginUserMerchantInfo LoginUserMerchantInfo(String loginUser, Connection conn) throws SQLException {
		LoginUserMerchantInfo data = new LoginUserMerchantInfo();
		String query = "select * from CMSMerchant";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet res = pstmt.executeQuery();
		if (res.next()) {
			data.setLoginUser(loginUser);
			data.setMerchantId(res.getString("userid"));
		}
		return data;
	}
	
	public String getFeatureName(int feature, Connection conn) throws SQLException {
		String ret = "";
		String query = "select Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Feature') And code = '"+feature+"' ORDER BY Syskey";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet res = pstmt.executeQuery();
		if (res.next()) {
			ret = res.getString("Description");
		}
		res.close();
		pstmt.close();
		return ret;
	}

	public ArrayList<DetailsData> getshowdetails(String syskey, Connection pConn) throws SQLException {

		ArrayList<DetailsData> ret = new ArrayList<DetailsData>();
		String l_Query = "select  * from DisPaymentTransDetail where hkey = ?";

		PreparedStatement pstmt = pConn.prepareStatement(l_Query);

		pstmt.setString(1, syskey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			DetailsData data = new DetailsData();
			data.setSysKey(rs.getLong("Syskey"));
			data.setHkey(rs.getLong("Hkey"));
			data.setSrno(rs.getInt("SrNo"));
			data.setCode(rs.getString("Code"));
			data.setDescription(rs.getString("Description"));
			data.setPrice(rs.getDouble("Price"));
			data.setQuantity(rs.getInt("Qty"));
			data.setAmount(rs.getDouble("Amount"));
			ret.add(data);
		}
		return ret;
	}

	public Lov3 getMerchantIDListDetail(Connection conn, String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		System.out.println("user is; " + User);
		PreparedStatement stmt = conn.prepareStatement(
				"select p.MerchantID,p.MerchantName,p.ProcessingCode from CMSMerchant c, PayTemplateHeader p where c.UserId = p.MerchantID and c.RecordStatus <> 4 order by p.MerchantName");

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("MerchantID"));
			ref.setcaption(rs.getString("MerchantName"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		System.out.println("Dao getmerchant" + refarray.length);
		lov3.setRef015(refarray);
		System.out.println("Dao getmerchant lov" + lov3.getRef015().length);

		return lov3;

	}

	public String getMerchantNameById(String merchantId, Connection pConn) throws SQLException {
		String merName = "";
		PreparedStatement pstmt = null;
		;
		String query = "SELECT userName from CMSMerchant  where userId = ?";
		pstmt = pConn.prepareStatement(query);
		pstmt.setString(1, merchantId);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			merName = rs.getString("userName");
		}
		return merName;
	}

	public LOVSetupDetailData[] getLOVbyHKey(long sysKey, String processingCode, Connection Conn) throws SQLException {

		int srno = 1;
		ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();

		String query = "";
		PreparedStatement pstmt = null;
		;
		query = "SELECT Code, Description from LOVDetails where HKey = ?";
		pstmt = Conn.prepareStatement(query);
		pstmt.setLong(1, sysKey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			LOVSetupDetailData data = new LOVSetupDetailData();
			data.setSrno(srno++);
			data.setLovCde(rs.getString("Code"));
			data.setLovDesc1(rs.getString("Description"));

			if (processingCode.equalsIgnoreCase("02")) {
				data.setLovDesc2(getMerchantNameById(data.getLovCde(), Conn));
			}
			datalist.add(data);
		}
		LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		return dataarry;

	}

	public PayTemplateHeader getPaymentTemplateByID(String merchantID, String saveDirectory, Connection l_Conn)
			throws SQLException {
		PayTemplateHeader ret = new PayTemplateHeader();
		int count = 0;
		GeneralUtil.readDataConfig();

		String query = "select count(*) c from PayTemplateHeader H,PayTemplateDetail D where H.SysKey = D.HKey and FieldShow in (4,5,6) AND H.MerchantId = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(query);

		pstmt.setString(1, merchantID);
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			count = rs.getInt("c");
		}
		query = "select H.SysKey,H.MerchantID ,H.MerchantName,H.ProcessingCode,H.Logo,"
				+ "D.HKey,D.FieldID,D.FieldOrder,D.DataType,D.Caption,D.DisPayField,D.LovKey, D.RegExpKey ,D.IsMandatory, H.DetailOrder "
				+ "from PayTemplateHeader H inner join PayTemplateDetail D "
				+ "on H.SysKey = D.HKey and FieldShow in (4,5,6) AND H.MerchantID=? order by D.FieldOrder ";

		pstmt = l_Conn.prepareStatement(query);
		System.out.println("getPaymentTemplaate by id in dao: " + query);
		pstmt.setString(1, merchantID);
		rs = pstmt.executeQuery();

		PayTemplateDetail[] detailData = new PayTemplateDetail[count];
		int index = 0;

		while (rs.next()) {
			ret.setSysKey(rs.getLong("SysKey"));
			ret.setMerchantID(rs.getString("MerchantID"));
			ret.setName(rs.getString("MerchantName"));
			ret.setProcessingCode(rs.getString("ProcessingCode").substring(0, 2));
			ret.setProNotiCode(rs.getString("ProcessingCode").substring(2, 4));
			ret.setProDisCode(rs.getString("ProcessingCode").substring(4, 6));
			// ret.setLogo(FileUtil.getPhoto(saveDirectory,rs.getBytes("Logo"),l_Conn));
			ret.setDetailOrder(rs.getString("DetailOrder"));

			PayTemplateDetail detail = new PayTemplateDetail();
			detail.sethKey(rs.getLong("HKey")+"");
			detail.setFieldID(rs.getString("FieldID"));
			detail.setFieldOrder(rs.getInt("FieldOrder"));
			detail.setCaption(rs.getString("Caption"));
			detail.setDataType(rs.getString("DataType"));
			detail.setDisPayField(rs.getString("DisPayField"));
			String regExp = rs.getString("RegExpKey");
			detail.setRegExpKey(regExp.trim());
			detail.setIsMandatory(rs.getInt("IsMandatory"));

			long lovkey = rs.getLong("LovKey");
			detail.setLovKey(lovkey);
			if (lovkey != 0) {
				detail.setLovData(getLOVbyHKey(lovkey, ret.getProcessingCode(), l_Conn));
			}

			detailData[index] = detail;
			index++;
		}
		ret.setData(detailData);

		if (!ret.getDetailOrder().equals("")) {

			String sql = "";
			int srno = 1;
			long headerSyskey = (long) 0.0;
			ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();
			sql = "SELECT syskey from LOVHeader where Description = 'Items'";
			pstmt = l_Conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				headerSyskey = rs.getLong("syskey");
			}
			;
			sql = "SELECT Code, Description, Price from LOVDetails where HKey = ?";
			pstmt = l_Conn.prepareStatement(sql);
			pstmt.setLong(1, headerSyskey);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				LOVSetupDetailData data = new LOVSetupDetailData();
				data.setSrno(srno++);
				data.setLovCde(rs.getString("Code"));
				data.setLovDesc1(rs.getString("Description"));
				data.setPrice(rs.getDouble("Price"));

				datalist.add(data);
			}
			LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
			for (int i = 0; i < datalist.size(); i++) {
				dataarry[i] = datalist.get(i);
			}
			ret.setDetailLov(dataarry);
		}
		return ret;
	}

	public static String getLovDescriptionByLovKeyandLovCode(Long lovKey, String lovCode, Connection pConn) {

		String lovDesc = "";
		PreparedStatement pstmt = null;
		;
		String query = "select d.description from lovheader h , lovdetails d where h.syskey = d.hkey and h.syskey = ? and d.Code = ? ";
		try {
			pstmt = pConn.prepareStatement(query);
			pstmt.setLong(1, lovKey);
			pstmt.setString(2, lovCode);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				lovDesc = rs.getString("description");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lovDesc;
	}

	public DispaymentList getMerchantList(int currentPage, int totalCount, int pageSize, String aMerchantID,
			String aFeature, String status, String aFromDate, String aToDate, String aFromAcc, String aToAcc,
			String did, String initiatedby, String transrefno, String processingCode, String proDisCode,
			PayTemplateDetail[] templateData, Connection conn) throws SQLException {
		

		DispaymentList res = new DispaymentList();
		MerchantReportDao dao = new MerchantReportDao();
		ArrayList<DispaymentTransactionData> datalist = new ArrayList<DispaymentTransactionData>();

		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;

		PayTemplateDetailArr[] DetailArray = null;
		GeneralUtil.readDataConfig();
		String templateMerchant = "";

		if (processingCode.equalsIgnoreCase("02")) {
			// Just Fortune Ygn merchant is mapped in Paytemplate Setup
			templateMerchant = ServerGlobal.getFortuneYgnMerchant();
		} else {
			templateMerchant = aMerchantID;
		}
		String pCode = "";
		String query = "Select ProcessingCode From PayTemplateHeader Where MerchantID = '" + aMerchantID + "'";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet result = pstmt.executeQuery();
		while (result.next()) {
			pCode = result.getString("ProcessingCode"); // 080400
		}

		if (pCode.equalsIgnoreCase("080400")) {
			String aCriteria = "";
			if (!aFromDate.trim().equals("")) {
				aCriteria += " and d.TransDate >= '" + aFromDate.trim() + "'";
			}

			if (!aToDate.trim().equals("")) {
				aCriteria += " and d.TransDate <= '" + aToDate.trim() + "'";
			}
			if (!aFromAcc.trim().equals("")) {
				aCriteria += " and FromAccount = '" + aFromAcc.trim() + "'";
			}

			if (!aToAcc.trim().equals("")) {
				aCriteria += " and ToAccount = '" + aToAcc.trim() + "'";
			}

			if (!did.trim().equals("")) {
				aCriteria += " and CustomerCode = '" + did.trim() + "'";
			}

			if (!status.trim().equalsIgnoreCase("ALL")) {
				if (status.trim().equalsIgnoreCase("SUCCESS")) {
					aCriteria += " and d.t14 = '1'";
				} else if (status.trim().equalsIgnoreCase("Reversed")) {
					aCriteria += " and d.t14 <> '1' and d.n2 = '4'";
				} else if (status.trim().equalsIgnoreCase("FAIL")) {// FAIL
					aCriteria += " and d.t14 <> '1'";
				}else{
                    aCriteria += " and d.t14 =''";
                }
			}

			if (!aFeature.trim().equals("")) {
				if (aFeature.trim().equalsIgnoreCase("All")) {
					aCriteria += " ";
				} else {
					aCriteria += " and d.n20= " + aFeature + " ";
				}
			}

			if (!transrefno.trim().equals("")) {
				aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
			}

			String l_Query = "";
			String l_Query_Total = "";
			String GT = "", Tax = "", Service = "";
			String grandTotalFormat = "", serviceTotalFormat = "", taxTotalFormat = "";

			l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,"
					+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, PayTemplateHeader p where p.ProcessingCode = '"
					+ pCode + "' and p.MerchantID = '" + aMerchantID + "' and d.T20 = '" + aMerchantID + "' "
					+ " And d.n3=4 " + aCriteria + ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "
					+ l_endRecord;

			l_Query_Total = "select sum(GTA) as grandtotalAmount , Sum(Service) As ServiceCharges, Sum(Tax) As CommercialTax from (select Amount as GTA , Cast(T15 as float) As Service , Cast(T18 as float) As Tax"
					+ " from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,"
					+ "d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d, PayTemplateHeader p"
					+ " where p.ProcessingCode = '" + pCode + "' and p.MerchantID = '" + aMerchantID + "'And d.T20 ='"
					+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r) as g";

			PreparedStatement stmtTotal = conn.prepareStatement(l_Query_Total);
			System.out.println("Query1..." + l_Query);
			System.out.println("Query2... " + l_Query_Total);
			ResultSet rsTotal = stmtTotal.executeQuery();
			while (rsTotal.next()) {
				GT = rsTotal.getString("grandtotalAmount");
				Service = rsTotal.getString("ServiceCharges");
				Tax = rsTotal.getString("CommercialTax");
				if (GT != null) {
					grandTotalFormat = ServerUtil.formatNumber(Double.parseDouble(GT));
					serviceTotalFormat = ServerUtil.formatNumber(Double.parseDouble(Service));
					taxTotalFormat = ServerUtil.formatNumber(Double.parseDouble(Tax));

					res.setGrandTotal(grandTotalFormat);
					res.setIncomeAmt(taxTotalFormat);
					res.setServiceAmt(serviceTotalFormat);
				}
			}

			PreparedStatement stmt = conn.prepareStatement(l_Query);
			System.out.println("Query1..." + l_Query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				DispaymentTransactionData data = new DispaymentTransactionData();
				String aTransDate = "";
				aTransDate = rs.getString("TransDate");
				data.setTransactiondate(
						aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4));
				data.setSyskey(rs.getLong("Autokey"));
				data.setTranrefnumber(rs.getString("FlexRefNo"));
				data.setTrantaxrefnumber(rs.getString("t22"));
				data.setActivebranch(rs.getString("FromBranch"));
				data.setDebitaccount(rs.getString("FromAccount"));
				data.setDebitbranch(rs.getString("T1"));
				data.setXref(rs.getString("t5"));
				data.setCollectionaccount(rs.getString("ToAccount"));
				data.setaFeature(rs.getInt("n20"));
				data.setFeature(dao.getFeatureName(rs.getInt("n20"), conn));
				data.setBranch("Online");

				Double amount = rs.getDouble("Amount");
				data.setAmountst(String.format("%,.2f", amount));
				data.setAmount(rs.getDouble("Amount"));
				if (!(rs.getString("t15").equals(""))) {
					data.setT15(Double.valueOf(rs.getString("t15")));
				}
				if (!(rs.getString("t18").equals(""))) {
					data.setT18(Double.valueOf(rs.getString("t18")));
				}

				data.setCommchargest(String.format("%,.2f", data.getT15())); // service
				data.setCommuchargest(String.format("%,.2f", data.getT18())); // tax

				data.setDid(rs.getString("CustomerCode"));
				data.setCustomername(rs.getString("CustomerName"));
				data.setDescription(rs.getString("t16"));

				if (proDisCode.equalsIgnoreCase("01")) {
					data.setDistributorName(rs.getString("DistributorName"));
					data.setDistributorAddress(rs.getString("DistributorAddress"));
				}

				data.setMessageCode(rs.getString("t14"));// messagecode
				data.setMessageDesc(rs.getString("t12"));// messagedescription
				datalist.add(data);

			}
			res.setDetailData(DetailArray);
			res.setPageSize(pageSize);
			res.setCurrentPage(currentPage);
			String countSQL = "";
			countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  where 1=1 and d.t20 = '"
					+ aMerchantID + "' ";

			countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";

			PreparedStatement stat = conn.prepareStatement(countSQL);
			System.out.println("Count.." + countSQL);
			ResultSet result1 = stat.executeQuery();
			result1.next();
			res.setTotalCount(result1.getInt("recCount"));
			DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
			dataarry = datalist.toArray(dataarry);
			res.setData(dataarry);

//			return res;
		} else  { //if (pCode.equalsIgnoreCase("050200"))

			GeneralUtil.readDataConfig();

			if (processingCode.equalsIgnoreCase("02")) {
				// Just Fortune Ygn merchant is mapped in Paytemplate Setup
				templateMerchant = ServerGlobal.getFortuneYgnMerchant();
			} else {
				templateMerchant = aMerchantID;
			}
			long key = 0;
			String Query = "SELECT SysKey from PayTemplateHeader WHERE MerchantID = ?";
			pstmt = conn.prepareStatement(Query);
			pstmt.setString(1, templateMerchant);
			ResultSet rs1 = pstmt.executeQuery();
			if (rs1.next()) {
				key = rs1.getLong("SysKey");
			}
			pstmt.close();
			rs1.close();

			ArrayList<PayTemplateDetailArr> DetailList = new ArrayList<PayTemplateDetailArr>();
			query = "select Caption,DataType,DisPayField from PayTemplateDetail "
					+ " where HKey=? and FieldShow in (4,5,6)"
					+ " and DisPayField not in('TransDate','Amount','CustomerCode','T16')";

			pstmt = conn.prepareStatement(query);
			pstmt.setLong(1, key);
			ResultSet rs2 = pstmt.executeQuery();
			while (rs2.next()) {
				PayTemplateDetailArr Detail = new PayTemplateDetailArr();

				Detail.setPayCaption(rs2.getString("Caption"));
				Detail.setPaydatatype(rs2.getString("DataType"));
				Detail.setPaydispayfield(rs2.getString("DisPayField"));
				DetailList.add(Detail);
			}

			String aCriteria = "", bCriteria = "";
			if (!aFromDate.trim().equals("")) {
				aCriteria += " and c.TransDate >= '" + aFromDate.trim() + "'"; // d.TransDate
				bCriteria += " and c.TransDate >= '" + aFromDate.trim() + "'"; // d.TransDate
			}
			
			if (!aFeature.trim().equals("")) {
				if (aFeature.trim().equalsIgnoreCase("All")) {
					aCriteria += " ";
				} else {
					aCriteria += " and d.n20= " + aFeature + " ";
				}
			}


			if (!aToDate.trim().equals("")) {
				aCriteria += " and c.TransDate <= '" + aToDate.trim() + "'"; // d.TransDate
				bCriteria += " and c.TransDate <= '" + aToDate.trim() + "'"; // d.TransDate
			}
			if (!aFromAcc.trim().equals("")) {
				aCriteria += " and FromAccount = '" + aFromAcc.trim() + "'";
			}

			if (!aToAcc.trim().equals("")) {
				aCriteria += " and ToAccount = '" + aToAcc.trim() + "'";
			}

			if (!did.trim().equals("")) {
				aCriteria += " and CustomerCode = '" + did.trim() + "'";
			}

			if (!initiatedby.trim().equals("ALL")) {
				if (initiatedby.trim().equals("Online")) {
					aCriteria += " and d.MobileUserID = 'Online'";
				} else if (initiatedby.trim().equals("OTC")) {
					aCriteria += " and d.MobileUserID <> 'Online'";
				}
			}

			if (!transrefno.trim().equals("")) {
				aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
			}

			if ("01".equalsIgnoreCase(processingCode) || "03".equalsIgnoreCase(processingCode)) {
				for (int i = 0; i < templateData.length; i++) {
					System.out.println("templateData.length " + templateData.length);
					String fValue = "";
					if (!templateData[i].getFieldvalue().trim().equals("000")
							&& templateData[i].getFieldID().equals("R4")) {
						fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(),
								templateData[i].getFieldvalue(), conn);
						aCriteria += " and d." + templateData[i].getDisPayField() + " = '" + fValue + "' ";
						System.out.println("R4  " + templateData[i].getFieldvalue() + " "
								+ templateData[i].getDisPayField() + " " + fValue);
					} else if (!templateData[i].getFieldvalue().trim().equals("000")
							&& templateData[i].getFieldID().equals("R5")) {
						fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(),
								templateData[i].getFieldvalue(), conn);
						aCriteria += " and d." + templateData[i].getDisPayField() + " = '" + fValue + "' ";
						System.out.println("R5  " + templateData[i].getFieldvalue() + " "
								+ templateData[i].getDisPayField() + " " + fValue);
					}

				}
			}
			String l_Query = "";
			String l_Query_Total = "";
			String GT = "";
			String IA = "";
			String DA = "";

			if (proDisCode.equalsIgnoreCase("01")) {
				l_Query = "   select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,  d.*, "
						+ "d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress  from DispaymentTransaction d ,DistributorList dl "
						+ "where d.CustomerCode=dl.DistributorID and d.t20 = '" + aMerchantID + "' And d.n3=4 "
						+ aCriteria + " )As r " + "WHERE RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord
						+ " order by autokey desc";
			} else {

				if (pCode.equalsIgnoreCase("050200")) {// CNP
					if (status.equalsIgnoreCase("SUCCESS")) {
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Success'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1  "
								+ bCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Success'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 "
								+ bCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
								+ ") As r) as g";

					} else if (status.equalsIgnoreCase("FAIL")) {
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Failed'" + ") As r  WHERE 1=1 and RowNum > "
								+ l_startRecord + " and RowNum <= " + l_endRecord;
						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Failed'" + ") As r) as g";

					} else if (status.equalsIgnoreCase("PENDING")) {

						l_Query = "select * from ( select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
								+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
								+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
								+ ") As r) as g";

					} else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED")
							|| status.equalsIgnoreCase("Reversed")) {

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status in ('" + status + "')"
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status in ('" + status + "')" + ") As r) as g";

					} else {// ALL

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
								+ " from (SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
								+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
								+ aCriteria + " and d.t20 = "
								+ " (select merchantid from PayTemplateHeader where ProcessingCode = '050200') And d.n3=4 "
								+ " ) as t" + ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "
								+ l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
								+ " from ( SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
								+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
								+ aCriteria
								+ " and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
								+ " And d.n3=4 " + " ) as t" + ") As r) as g";
					}

				} else {
					if (status.equalsIgnoreCase("ALL")) {
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r  WHERE 1=1 and RowNum > "
								+ l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r) as g";

					} else if (status.equalsIgnoreCase("PENDING")) {
						String code = "217";
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.MessageCode='" + code + "'" + " And d.n3=4 " + aCriteria
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
								+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
								+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.MessageCode='" + code + "'" + " And d.n3=4 " + aCriteria
								+ ") As r) as g";

					} else if (status.equalsIgnoreCase("FAIL")) {

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '"
								+ 216 + "'" + " And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
								+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
								+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And  c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r) as g";

					} else {

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
								+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
								+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r) as g";

					}
				}
			}

			PreparedStatement stmtTotal = conn.prepareStatement(l_Query_Total);
			System.out.println("Query1..." + l_Query);
			System.out.println("Query2... " + l_Query_Total);
			ResultSet rsTotal = stmtTotal.executeQuery();
			while (rsTotal.next()) {
				GT = rsTotal.getString("grandtotalAmount");
				IA = rsTotal.getString("incomeAmt"); //servicecharges
				DA = rsTotal.getString("deduAmt"); //tax
				if (GT != null || IA != null || DA != null) {
					String formatted = ServerUtil.formatNumber(Double.parseDouble(GT));
					String formattedIA = ServerUtil.formatNumber(Double.parseDouble(IA));
					String formattedDA = ServerUtil.formatNumber(Double.parseDouble(DA));

					res.setGrandTotal(formatted);
					res.setIncomeAmt(formattedDA);
					res.setServiceAmt(formattedIA);
					res.setDeduAmt(formattedDA);
				}

			}

			PreparedStatement stmt = conn.prepareStatement(l_Query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				DispaymentTransactionData data = new DispaymentTransactionData();
				String aTransDate = "";
				aTransDate = rs.getString("TransDate");
				data.setTransactiondate(
						aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4));
				data.setSyskey(rs.getLong("Autokey"));
				data.setTranrefnumber(rs.getString("FlexRefNo"));
				data.setTrantaxrefnumber(rs.getString("t22"));
				data.setaFeature(rs.getInt("n20"));
				data.setFeature(dao.getFeatureName(rs.getInt("n20"), conn));
				data.setActivebranch(rs.getString("FromBranch"));
				data.setDebitaccount(rs.getString("FromAccount"));
				data.setDebitbranch(rs.getString("T1"));
				data.setXref(rs.getString("t5"));
				data.setCollectionaccount(rs.getString("ToAccount"));
				if (rs.getString("MobileUserID").equals("Online")) {
					data.setBranch("Online");
				} else {
					data.setBranch("OTC");
				}

				Double amount = rs.getDouble("Amount");
				data.setAmountst(String.format("%,.2f", amount));
				data.setAmount(rs.getDouble("Amount"));
				if (!(rs.getString("t15").equals(""))) {
					data.setT15(Double.valueOf(rs.getString("t15")));
				}
				if (!(rs.getString("t18").equals(""))) {
					data.setT18(Double.valueOf(rs.getString("t18")));
				}

				data.setCommchargest(String.format("%,.2f", data.getT15()));
				data.setCommuchargest(String.format("%,.2f", data.getT18()));
				data.setDid(rs.getString("CustomerCode"));
				data.setCustomername(rs.getString("CustomerName"));
				data.setDescription(rs.getString("t16"));
				if (proDisCode.equalsIgnoreCase("01")) {
					data.setDistributorName(rs.getString("DistributorName"));
					data.setDistributorAddress(rs.getString("DistributorAddress"));
				}
				DetailArray = new PayTemplateDetailArr[DetailList.size()];
				DetailArray = DetailList.toArray(DetailArray);
				for (int i = 0; i < DetailArray.length; i++) {
					if (DetailArray[i].getPaydatatype().equalsIgnoreCase("lov")
							|| DetailArray[i].getPaydatatype().equalsIgnoreCase("date")
							|| DetailArray[i].getPaydatatype().equalsIgnoreCase("text")) {

						switch (DetailArray[i].getPaydispayfield()) {

						case "FileUploadDateTime":
							data.setFileUploadDateTime(rs.getString("FileUploadDateTime"));
							break;// finished
						case "CustomerNumber":
							data.setCustomerNumber(rs.getString("CustomerNumber"));
							break;// finished

						case "T7":
							data.setT7(rs.getString("T7"));
							break;// finished
						case "T8":
							data.setT8(rs.getString("T8"));
							break;// finished
						case "T10":
							data.setT10(rs.getString("T10"));
							break;// finished
						case "T11":
							data.setT11(rs.getString("T11"));
							break;// finished
						case "T12":
							data.setT12(rs.getString("T12"));
							break;// finished
						case "T13":
							data.setT13(rs.getString("T13"));
							break;// finished
						case "T15":
							data.setT16(rs.getString("T15"));
							break;// finished
						case "T16":
							data.setT16(rs.getString("T16"));
							break;
						case "T17":
							data.setT17(rs.getString("T17"));
							break;// finished
						case "T19":
							data.setT19(rs.getString("T19"));
							break;// finished
						case "T20":
							data.setT20(rs.getString("T20"));
							break;// finished
						case "T21":
							data.setT21(rs.getString("T21"));
							break;// finished
						case "T22":
							data.setT22(rs.getString("T22"));
							break;// finished
						case "T23":
							data.setT23(rs.getString("T23"));
							break;// finished
						case "T24":
							data.setT24(rs.getString("T24"));
							break;// finished
						case "T25":
							data.setT25(rs.getString("T25"));
							break;// finished

						}

					}

					if (DetailArray[i].getPaydatatype().equalsIgnoreCase("number")
							|| (!(DetailArray[i].getPaydispayfield().equals("")))) {
						switch (DetailArray[i].getPaydispayfield()) {
						case "N1":
							data.setN1(rs.getInt("N1"));
							break;// finished
						case "N7":
							data.setN7(rs.getInt("N7"));
							break;// finished
						case "N8":
							data.setN8(rs.getInt("N8"));
							break;// finished
						case "N9":
							data.setN9(rs.getInt("N9"));
							break;// finished
						case "N17":
							data.setN17(rs.getInt("N17"));
							break;// finished
						case "N18":
							data.setN18(rs.getInt("N18"));
							break;// finished
						case "N19":
							data.setN19(rs.getInt("N19"));
							break;// finished
						case "N20":
							data.setN20(rs.getInt("N20"));
							break;// finished
						// case "T18" :
						// data.setT18(Double.parseDouble(rs.getString("T18")));break;
						}
					}

				}
				data.setMessageCode(rs.getString("messagecode"));
				data.setMessageDesc(rs.getString("messagedescription"));

				datalist.add(data);

			}
			res.setDetailData(DetailArray);
			res.setPageSize(pageSize);
			res.setCurrentPage(currentPage);
			String countSQL = "";
			if (proDisCode.equalsIgnoreCase("01")) {
				countSQL = "select COUNT(*) as recCount from  ( SELECT d.*, d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress "
						+ "from DispaymentTransaction d ,DistributorList dl where d.CustomerCode=dl.DistributorID and d.t20 = '"
						+ aMerchantID + "' " + " And d.n3=4   " + aCriteria + " )As r  ";
			} else {

				if (pCode.equalsIgnoreCase("050200")) {

					if (status.equalsIgnoreCase("SUCCESS")) {
						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 2 and c.Status = 'Success'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
								+ " ) As r ";
					} else if (status.equalsIgnoreCase("FAIL")) {

						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 2 and c.Status = 'Failed'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed'))"
								+ " ) As r ";

					} else if (status.equalsIgnoreCase("PENDING")) {

						countSQL = "select COUNT(*) as recCount from ( select * from ( SELECT ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num, d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria
								+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
								+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
								+ " ) As r ";

					} else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED")
							|| status.equalsIgnoreCase("REVERSED")) {

						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 3 and c.Status in ('" + status
								+ "')" + " ) As r ";

					} else if (status.equalsIgnoreCase("ALL")) {

						countSQL = "select COUNT(*) as recCount from ( SELECT  c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
								+ " and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
								+ "  And d.n3=4  " + aCriteria + " ) As r ";

					}

				} else if (pCode.equalsIgnoreCase("040300")){
					countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' ";
					if (status.equalsIgnoreCase("FAIL")) {
						countSQL += "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '" + 216 + "'"
								+ " And c.status='" + status + "'";
					} else if (status.equalsIgnoreCase("SUCCESS")) {
						countSQL += "And c.status='" + status + "'";
					} else if (status.equalsIgnoreCase("PENDING")) {
						countSQL += "And c.MessageCode='" + 217 + "'";
					}
					countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";
				}

			}

			System.out.println("Count.." + countSQL);
			PreparedStatement stat = conn.prepareStatement(countSQL);
			
			result = stat.executeQuery();
			result.next();
			res.setTotalCount(result.getInt("recCount"));
			DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
			dataarry = datalist.toArray(dataarry);
			res.setData(dataarry);
			return res;
		}		
		return res;
	
		/*DispaymentList res = new DispaymentList();
		ArrayList<DispaymentTransactionData> datalist = new ArrayList<DispaymentTransactionData>();

		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;

		PayTemplateDetailArr[] DetailArray = null;
		MerchantReportDao dao = new MerchantReportDao();
		GeneralUtil.readDataConfig();
		String templateMerchant = "";

		if (processingCode.equalsIgnoreCase("02")) {
			// Just Fortune Ygn merchant is mapped in Paytemplate Setup
			templateMerchant = ServerGlobal.getFortuneYgnMerchant();
		} else {
			templateMerchant = aMerchantID;
		}
		String pCode = "";
		String query = "Select ProcessingCode From PayTemplateHeader Where MerchantID = '" + aMerchantID + "'";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet result = pstmt.executeQuery();
		while (result.next()) {
			pCode = result.getString("ProcessingCode"); // 080400
		}

		if (pCode.equalsIgnoreCase("080400")) {
			String aCriteria = "";
			if (!aFromDate.trim().equals("")) {
				aCriteria += " and d.TransDate >= '" + aFromDate.trim() + "'";
			}

			if (!aToDate.trim().equals("")) {
				aCriteria += " and d.TransDate <= '" + aToDate.trim() + "'";
			}
			if (!aFromAcc.trim().equals("")) {
				aCriteria += " and FromAccount = '" + aFromAcc.trim() + "'";
			}

			if (!aToAcc.trim().equals("")) {
				aCriteria += " and ToAccount = '" + aToAcc.trim() + "'";
			}

			if (!did.trim().equals("")) {
				aCriteria += " and CustomerCode = '" + did.trim() + "'";
			}

			if (!status.trim().equalsIgnoreCase("ALL")) {
				if (status.trim().equalsIgnoreCase("SUCCESS")) {
					aCriteria += " and d.t14 = '1'";
				} else if (status.trim().equalsIgnoreCase("Reversed")) {
					aCriteria += " and d.t14 <> '1' and d.n2 = '4'";
				} else if (status.trim().equalsIgnoreCase("FAIL"))  {// FAIL
					aCriteria += " and d.t14 <> '1'";
				}
			}

			if (!aFeature.trim().equals("")) {
				if (aFeature.trim().equalsIgnoreCase("All")) {
					aCriteria += " ";
				} else {
					aCriteria += " and d.n20= " + aFeature + " ";
				}
			}

			if (!transrefno.trim().equals("")) {
				aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
			}

			String l_Query = "";
			String l_Query_Total = "";
			String GT = "", Tax = "", Service = "";
			String grandTotalFormat = "", serviceTotalFormat = "", taxTotalFormat = "";

			l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,"
					+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, PayTemplateHeader p where p.ProcessingCode = '"
					+ pCode + "' and p.MerchantID = '" + aMerchantID + "' and d.T20 = '" + aMerchantID + "' "
					+ " And d.n3=4 " + aCriteria + ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "
					+ l_endRecord;

			l_Query_Total = "select sum(GTA) as grandtotalAmount , Sum(Service) As ServiceCharges, Sum(Tax) As CommercialTax from (select Amount as GTA , Cast(T15 as float) As Service , Cast(T18 as float) As Tax"
					+ " from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,"
					+ "d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d, PayTemplateHeader p"
					+ " where p.ProcessingCode = '" + pCode + "' and p.MerchantID = '" + aMerchantID + "'And d.T20 ='"
					+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r) as g";

			PreparedStatement stmtTotal = conn.prepareStatement(l_Query_Total);
			System.out.println("Query1..." + l_Query);
			System.out.println("Query2... " + l_Query_Total);
			ResultSet rsTotal = stmtTotal.executeQuery();
			while (rsTotal.next()) {
				GT = rsTotal.getString("grandtotalAmount");
				Service = rsTotal.getString("ServiceCharges");
				Tax = rsTotal.getString("CommercialTax");
				if (GT != null) {

					DecimalFormat df = new DecimalFormat("###,000.00");
					grandTotalFormat = df.format(Double.parseDouble(GT));
					serviceTotalFormat = df.format(Double.parseDouble(Service));
					taxTotalFormat = df.format(Double.parseDouble(Tax));

					res.setGrandTotal(grandTotalFormat);
					res.setIncomeAmt(taxTotalFormat);
					res.setServiceAmt(serviceTotalFormat);
					System.out.println("GrandTotal" + grandTotalFormat + " ServiceTotal" + serviceTotalFormat
							+ " TaxTotal" + taxTotalFormat);
				}

			}

			PreparedStatement stmt = conn.prepareStatement(l_Query);
			System.out.println("Query1..." + l_Query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				DispaymentTransactionData data = new DispaymentTransactionData();
				String aTransDate = "";
				aTransDate = rs.getString("TransDate");
				data.setTransactiondate(
						aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4));
				data.setSyskey(rs.getLong("Autokey"));
				data.setTranrefnumber(rs.getString("FlexRefNo"));
				data.setTrantaxrefnumber(rs.getString("t22"));
				data.setActivebranch(rs.getString("FromBranch"));
				data.setDebitaccount(rs.getString("FromAccount"));
				data.setDebitbranch(rs.getString("T1"));
				data.setFeature(dao.getFeatureName(rs.getInt("n20"), conn));
				data.setCollectionaccount(rs.getString("ToAccount"));
				data.setBranch("Online");

				Double amount = rs.getDouble("Amount");
				data.setAmountst(String.format("%,.2f", amount));
				data.setAmount(rs.getDouble("Amount"));
				if (!(rs.getString("t15").equals(""))) {
					data.setT15(Double.valueOf(rs.getString("t15")));
				}
				if (!(rs.getString("t18").equals(""))) {
					data.setT18(Double.valueOf(rs.getString("t18")));
				}

				data.setCommchargest(String.format("%,.2f", data.getT15())); // service
				data.setCommuchargest(String.format("%,.2f", data.getT18())); // tax

				data.setDid(rs.getString("CustomerCode"));
				data.setCustomername(rs.getString("CustomerName"));
				data.setDescription(rs.getString("t16"));

				if (proDisCode.equalsIgnoreCase("01")) {
					data.setDistributorName(rs.getString("DistributorName"));
					data.setDistributorAddress(rs.getString("DistributorAddress"));
				}

				data.setMessageCode(rs.getString("t14"));// messagecode
				data.setMessageDesc(rs.getString("t12"));// messagedescription
				datalist.add(data);

			}
			res.setDetailData(DetailArray);
			res.setPageSize(pageSize);
			res.setCurrentPage(currentPage);
			String countSQL = "";
			countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  where 1=1 and d.t20 = '"
					+ aMerchantID + "' ";

			countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";

			PreparedStatement stat = conn.prepareStatement(countSQL);
			System.out.println("Count.." + countSQL);
			ResultSet result1 = stat.executeQuery();
			result1.next();
			res.setTotalCount(result1.getInt("recCount"));
			DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
			dataarry = datalist.toArray(dataarry);
			res.setData(dataarry);

//			return res;
		} else {

			GeneralUtil.readDataConfig();

			if (processingCode.equalsIgnoreCase("02")) {
				// Just Fortune Ygn merchant is mapped in Paytemplate Setup
				templateMerchant = ServerGlobal.getFortuneYgnMerchant();
			} else {
				templateMerchant = aMerchantID;
			}
			long key = 0;
			String Query = "SELECT SysKey from PayTemplateHeader WHERE MerchantID = ?";
			pstmt = conn.prepareStatement(Query);
			pstmt.setString(1, templateMerchant);
			ResultSet rs1 = pstmt.executeQuery();
			if (rs1.next()) {
				key = rs1.getLong("SysKey");
			}
			pstmt.close();
			rs1.close();

			ArrayList<PayTemplateDetailArr> DetailList = new ArrayList<PayTemplateDetailArr>();
			query = "select Caption,DataType,DisPayField from PayTemplateDetail "
					+ " where HKey=? and FieldShow in (4,5,6)"
					+ " and DisPayField not in('TransDate','Amount','CustomerCode','T16')";

			pstmt = conn.prepareStatement(query);
			pstmt.setLong(1, key);
			ResultSet rs2 = pstmt.executeQuery();
			while (rs2.next()) {
				PayTemplateDetailArr Detail = new PayTemplateDetailArr();

				Detail.setPayCaption(rs2.getString("Caption"));
				Detail.setPaydatatype(rs2.getString("DataType"));
				Detail.setPaydispayfield(rs2.getString("DisPayField"));
				DetailList.add(Detail);
			}

			String aCriteria = "", bCriteria = "";
			if (!aFromDate.trim().equals("")) {
				aCriteria += " and c.TransDate >= '" + aFromDate.trim() + "'"; // d.TransDate
				bCriteria += " and c.TransDate >= '" + aFromDate.trim() + "'"; // d.TransDate
			}
			
			if (!aFeature.trim().equals("")) {
				if (aFeature.trim().equalsIgnoreCase("All")) {
					aCriteria += " ";
				} else {
					aCriteria += " and d.n20= " + aFeature + " ";
				}
			}


			if (!aToDate.trim().equals("")) {
				aCriteria += " and c.TransDate <= '" + aToDate.trim() + "'"; // d.TransDate
				bCriteria += " and c.TransDate <= '" + aToDate.trim() + "'"; // d.TransDate
			}
			if (!aFromAcc.trim().equals("")) {
				aCriteria += " and FromAccount = '" + aFromAcc.trim() + "'";
			}

			if (!aToAcc.trim().equals("")) {
				aCriteria += " and ToAccount = '" + aToAcc.trim() + "'";
			}

			if (!did.trim().equals("")) {
				aCriteria += " and CustomerCode = '" + did.trim() + "'";
			}

			if (!initiatedby.trim().equals("ALL")) {
				if (initiatedby.trim().equals("Online")) {
					aCriteria += " and d.MobileUserID = 'Online'";
				} else if (initiatedby.trim().equals("OTC")) {
					aCriteria += " and d.MobileUserID <> 'Online'";
				}
			}

			if (!transrefno.trim().equals("")) {
				aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
			}

			if ("01".equalsIgnoreCase(processingCode) || "03".equalsIgnoreCase(processingCode)) {
				for (int i = 0; i < templateData.length; i++) {
					System.out.println("templateData.length " + templateData.length);
					String fValue = "";
					if (!templateData[i].getFieldvalue().trim().equals("000")
							&& templateData[i].getFieldID().equals("R4")) {
						fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(),
								templateData[i].getFieldvalue(), conn);
						aCriteria += " and d." + templateData[i].getDisPayField() + " = '" + fValue + "' ";
						System.out.println("R4  " + templateData[i].getFieldvalue() + " "
								+ templateData[i].getDisPayField() + " " + fValue);
					} else if (!templateData[i].getFieldvalue().trim().equals("000")
							&& templateData[i].getFieldID().equals("R5")) {
						fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(),
								templateData[i].getFieldvalue(), conn);
						aCriteria += " and d." + templateData[i].getDisPayField() + " = '" + fValue + "' ";
						System.out.println("R5  " + templateData[i].getFieldvalue() + " "
								+ templateData[i].getDisPayField() + " " + fValue);
					}

				}
			}
			String l_Query = "";
			String l_Query_Total = "";
			String GT = "";
			String IA = "";
			String DA = "";

			if (proDisCode.equalsIgnoreCase("01")) {
				l_Query = "   select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,  d.*, "
						+ "d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress  from DispaymentTransaction d ,DistributorList dl "
						+ "where d.CustomerCode=dl.DistributorID and d.t20 = '" + aMerchantID + "' And d.n3=4 "
						+ aCriteria + " )As r " + "WHERE RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord
						+ " order by autokey desc";
			} else {

				if (pCode.equalsIgnoreCase("050200")) {// CNP
					if (status.equalsIgnoreCase("SUCCESS")) {
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Success'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1  "
								+ bCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Success'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 "
								+ bCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
								+ ") As r) as g";

					} else if (status.equalsIgnoreCase("FAIL")) {
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Failed'" + ") As r  WHERE 1=1 and RowNum > "
								+ l_startRecord + " and RowNum <= " + l_endRecord;
						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 2 and c.Status = 'Failed'" + ") As r) as g";

					} else if (status.equalsIgnoreCase("PENDING")) {

						l_Query = "select * from ( select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
								+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
								+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
								+ ") As r) as g";

					} else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED")
							|| status.equalsIgnoreCase("Reversed")) {

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status in ('" + status + "')"
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + aCriteria + " And d.n3=4 "
								+ "and c.ApiStep = 3 and c.Status in ('" + status + "')" + ") As r) as g";

					} else {// ALL

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
								+ " from (SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
								+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
								+ aCriteria + " and d.t20 = "
								+ " (select merchantid from PayTemplateHeader where ProcessingCode = '050200') And d.n3=4 "
								+ " ) as t" + ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "
								+ l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
								+ "SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
								+ " from ( SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
								+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
								+ aCriteria
								+ " and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
								+ " And d.n3=4 " + " ) as t" + ") As r) as g";
					}

				} else {
					if (status.equalsIgnoreCase("ALL")) {
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r  WHERE 1=1 and RowNum > "
								+ l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r) as g";

					} else if (status.equalsIgnoreCase("PENDING")) {
						String code = "217";
						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.MessageCode='" + code + "'" + " And d.n3=4 " + aCriteria
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
								+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
								+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.MessageCode='" + code + "'" + " And d.n3=4 " + aCriteria
								+ ") As r) as g";

					} else if (status.equalsIgnoreCase("FAIL")) {

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '"
								+ 216 + "'" + " And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
								+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
								+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And  c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r) as g";

					} else {

						l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord;

						l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
								+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
								+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
								+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
								+ ") As r) as g";

					}
				}
			}

			PreparedStatement stmtTotal = conn.prepareStatement(l_Query_Total);
			System.out.println("Query1..." + l_Query);
			System.out.println("Query2... " + l_Query_Total);
			ResultSet rsTotal = stmtTotal.executeQuery();
			while (rsTotal.next()) {
				GT = rsTotal.getString("grandtotalAmount");
				IA = rsTotal.getString("incomeAmt"); //servicecharges
				DA = rsTotal.getString("deduAmt"); //tax
				if (GT != null || IA != null || DA != null) {
					DecimalFormat df = new DecimalFormat("###,000.00");
					String formatted = df.format(Double.parseDouble(GT));

					DecimalFormat dfI = new DecimalFormat("###,000.00");
					String formattedIA = dfI.format(Double.parseDouble(IA));

					DecimalFormat dfD = new DecimalFormat("###,000.00");
					String formattedDA = dfD.format(Double.parseDouble(DA));

					res.setGrandTotal(formatted);
//					res.setIncomeAmt(formattedIA);
					res.setIncomeAmt(formattedDA);
					res.setServiceAmt(formattedIA);
					res.setDeduAmt(formattedDA);
				}

			}

			PreparedStatement stmt = conn.prepareStatement(l_Query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				DispaymentTransactionData data = new DispaymentTransactionData();
				String aTransDate = "";
				aTransDate = rs.getString("TransDate");
				data.setTransactiondate(
						aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4));
				data.setSyskey(rs.getLong("Autokey"));
				data.setTranrefnumber(rs.getString("FlexRefNo"));
				data.setTrantaxrefnumber(rs.getString("t22"));
				data.setActivebranch(rs.getString("FromBranch"));
				data.setFeature(dao.getFeatureName(rs.getInt("n20"), conn));
				data.setDebitaccount(rs.getString("FromAccount"));
				data.setDebitbranch(rs.getString("T1"));
				data.setCollectionaccount(rs.getString("ToAccount"));
				if (rs.getString("MobileUserID").equals("Online")) {
					data.setBranch("Online");
				} else {
					data.setBranch("OTC");
				}

				Double amount = rs.getDouble("Amount");
				data.setAmountst(String.format("%,.2f", amount));
				data.setAmount(rs.getDouble("Amount"));
				if (!(rs.getString("t15").equals(""))) {
					data.setT15(Double.valueOf(rs.getString("t15")));
				}
				if (!(rs.getString("t18").equals(""))) {
					data.setT18(Double.valueOf(rs.getString("t18")));
				}

				data.setCommchargest(String.format("%,.2f", data.getT15()));
				data.setCommuchargest(String.format("%,.2f", data.getT18()));
				data.setDid(rs.getString("CustomerCode"));
				data.setCustomername(rs.getString("CustomerName"));
				data.setDescription(rs.getString("t16"));
				if (proDisCode.equalsIgnoreCase("01")) {
					data.setDistributorName(rs.getString("DistributorName"));
					data.setDistributorAddress(rs.getString("DistributorAddress"));
				}
				DetailArray = new PayTemplateDetailArr[DetailList.size()];
				DetailArray = DetailList.toArray(DetailArray);
				for (int i = 0; i < DetailArray.length; i++) {
					if (DetailArray[i].getPaydatatype().equalsIgnoreCase("lov")
							|| DetailArray[i].getPaydatatype().equalsIgnoreCase("date")
							|| DetailArray[i].getPaydatatype().equalsIgnoreCase("text")) {

						switch (DetailArray[i].getPaydispayfield()) {

						case "FileUploadDateTime":
							data.setFileUploadDateTime(rs.getString("FileUploadDateTime"));
							break;// finished
						case "CustomerNumber":
							data.setCustomerNumber(rs.getString("CustomerNumber"));
							break;// finished

						case "T7":
							data.setT7(rs.getString("T7"));
							break;// finished
						case "T8":
							data.setT8(rs.getString("T8"));
							break;// finished
						case "T10":
							data.setT10(rs.getString("T10"));
							break;// finished
						case "T11":
							data.setT11(rs.getString("T11"));
							break;// finished
						case "T12":
							data.setT12(rs.getString("T12"));
							break;// finished
						case "T13":
							data.setT13(rs.getString("T13"));
							break;// finished
						case "T15":
							data.setT16(rs.getString("T15"));
							break;// finished
						case "T16":
							data.setT16(rs.getString("T16"));
							break;
						case "T17":
							data.setT17(rs.getString("T17"));
							break;// finished
						case "T19":
							data.setT19(rs.getString("T19"));
							break;// finished
						case "T20":
							data.setT20(rs.getString("T20"));
							break;// finished
						case "T21":
							data.setT21(rs.getString("T21"));
							break;// finished
						case "T22":
							data.setT22(rs.getString("T22"));
							break;// finished
						case "T23":
							data.setT23(rs.getString("T23"));
							break;// finished
						case "T24":
							data.setT24(rs.getString("T24"));
							break;// finished
						case "T25":
							data.setT25(rs.getString("T25"));
							break;// finished

						}

					}

					if (DetailArray[i].getPaydatatype().equalsIgnoreCase("number")
							|| (!(DetailArray[i].getPaydispayfield().equals("")))) {
						switch (DetailArray[i].getPaydispayfield()) {
						case "N1":
							data.setN1(rs.getInt("N1"));
							break;// finished
						case "N7":
							data.setN7(rs.getInt("N7"));
							break;// finished
						case "N8":
							data.setN8(rs.getInt("N8"));
							break;// finished
						case "N9":
							data.setN9(rs.getInt("N9"));
							break;// finished
						case "N17":
							data.setN17(rs.getInt("N17"));
							break;// finished
						case "N18":
							data.setN18(rs.getInt("N18"));
							break;// finished
						case "N19":
							data.setN19(rs.getInt("N19"));
							break;// finished
						case "N20":
							data.setN20(rs.getInt("N20"));
							break;// finished
						// case "T18" :
						// data.setT18(Double.parseDouble(rs.getString("T18")));break;
						}
					}

				}
				data.setMessageCode(rs.getString("messagecode"));
				data.setMessageDesc(rs.getString("messagedescription"));

				datalist.add(data);

			}
			res.setDetailData(DetailArray);
			res.setPageSize(pageSize);
			res.setCurrentPage(currentPage);
			String countSQL = "";
			if (proDisCode.equalsIgnoreCase("01")) {
				countSQL = "select COUNT(*) as recCount from  ( SELECT d.*, d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress "
						+ "from DispaymentTransaction d ,DistributorList dl where d.CustomerCode=dl.DistributorID and d.t20 = '"
						+ aMerchantID + "' " + " And d.n3=4   " + aCriteria + " )As r  ";
			} else {

				if (pCode.equalsIgnoreCase("050200")) {

					if (status.equalsIgnoreCase("SUCCESS")) {
						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 2 and c.Status = 'Success'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
								+ " ) As r ";
					} else if (status.equalsIgnoreCase("FAIL")) {

						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 2 and c.Status = 'Failed'"
								+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed'))"
								+ " ) As r ";

					} else if (status.equalsIgnoreCase("PENDING")) {

						countSQL = "select COUNT(*) as recCount from ( select * from ( SELECT ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num, d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria
								+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
								+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
								+ " ) As r ";

					} else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED")
							|| status.equalsIgnoreCase("REVERSED")) {

						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ aMerchantID + "' ";
						countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 3 and c.Status in ('" + status
								+ "')" + " ) As r ";

					} else if (status.equalsIgnoreCase("ALL")) {

						countSQL = "select COUNT(*) as recCount from ( SELECT  c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
								+ " and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
								+ "  And d.n3=4  " + aCriteria + " ) As r ";

					}

				} else if(pCode.equalsIgnoreCase("040300")) {
					countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' ";
					if (status.equalsIgnoreCase("FAIL")) {
						countSQL += "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '" + 216 + "'"
								+ " And c.status='" + status + "'";
					} else if (status.equalsIgnoreCase("SUCCESS")) {
						countSQL += "And c.status='" + status + "'";
					} else if (status.equalsIgnoreCase("PENDING")) {
						countSQL += "And c.MessageCode='" + 217 + "'";
					}
					countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";
				}

			}

			PreparedStatement stat = conn.prepareStatement(countSQL);
			System.out.println("Count.." + countSQL);
			result = stat.executeQuery();
			result.next();
			res.setTotalCount(result.getInt("recCount"));
			DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
			dataarry = datalist.toArray(dataarry);
			res.setData(dataarry);

			return res;

		}
		
		return res;*/
	}
	public ArrayList<DBGenrealResponse> getGeneralDashboardCNP(String merID, Connection conn, int feature) throws SQLException{
		ArrayList<DBGenrealResponse> ret = new ArrayList<DBGenrealResponse>();
		String sql =" select replace (replace (t13,'B-',''),'T-','') as CNPType,count(d.Autokey) cnptypeCount from "
				+ " dispaymenttransaction d inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 "
				+ " where d.t20=? and d.n20= ? group by replace (replace (t13,'B-',''),'T-','')";
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setString(1, merID);
		stat.setInt(2, feature);
		ResultSet rs = stat.executeQuery();
		while (rs.next()){
			DBGenrealResponse data=new DBGenrealResponse();
			data.setName(rs.getString("CNPType"));
			data.setY(rs.getDouble("cnptypeCount"));
			ret.add(data);
		}
		rs.close();
		stat.close();
		
		return ret;
	}
	public ArrayList<DBGenrealResponse> getGeneralDashboardSkyPay(String merID, Connection conn) throws SQLException{
		ArrayList<DBGenrealResponse> ret = new ArrayList<DBGenrealResponse>();
		String sql ="select t8 as operatortype , count(1) as txnCount from dispaymenttransaction d "
				+ " inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? "
				+ " and d.n20=2 and t8<>'Select' group by t8";
		
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setString(1, merID);
		ResultSet rs = stat.executeQuery();
		while (rs.next()){
			DBGenrealResponse data=new DBGenrealResponse();
			data.setName(rs.getString("operatortype"));
			data.setY(rs.getDouble("txnCount"));
			ret.add(data);
		}
		rs.close();
		stat.close();
		
		return ret;
	}
	
	public DBBarResponse getBarMonthlyTxnCNP(Connection conn, String merchantId, String flag,int feature) throws SQLException{
		ArrayList<String> cnptype = new ArrayList<String>();
		//Get distinct CNP Type
		String sql ="select distinct replace (replace (t13,'B-',''),'T-','') as CNPType from dispaymenttransaction d "
				+ "inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n20=?";
		
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		stat.setInt(2, feature);
		ResultSet rs = stat.executeQuery();
		while (rs.next()){
			cnptype.add(rs.getString("CNPType"));
		}
		rs.close();
		stat.close();
		//Get distinct Months
		ArrayList<String> dbMonth = new ArrayList<String>();
		sql ="select distinct substring(d.TransDate,5,2) as dbmonth from dispaymenttransaction d "
				+ "inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? "
				+ "and d.n20=? order by substring(d.TransDate,5,2) ";
		
		stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		stat.setInt(2, feature);
		rs = stat.executeQuery();
		while (rs.next()){
			dbMonth.add(rs.getString("dbmonth"));
		}
		rs.close();
		stat.close(); 
		DBBarResponse barResp = new DBBarResponse();
		DBBarDetail[] detailArr = new DBBarDetail[cnptype.size()];
		String getColumn = "";
		if(flag.equals("Count")){
			getColumn = ",count(d.Autokey)as dbcount";
		}else if (flag.equals("Amount")){
			getColumn =",sum(d.Amount)as dbcount";
		}
		for(int i=0; i<cnptype.size();i++){
			
			sql= "select substring(d.TransDate,5,2) as dbmonth "+getColumn+" from dispaymenttransaction d inner join "
					+ "CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n20=?"
					+ " AND t13 like '%"+cnptype.get(i)+"%' group by substring(d.TransDate,5,2) ";
			DBBarDetail detail = new DBBarDetail();
			detail.setName(cnptype.get(i));
			stat = conn.prepareStatement(sql);
			stat.setString(1, merchantId);
			stat.setInt(2, feature);
			rs = stat.executeQuery();
			ArrayList<DBBarData> cnpData = new ArrayList<DBBarData>();
			while (rs.next()){
				DBBarData data = new DBBarData();
				data.setMonth(rs.getString("dbmonth"));
				data.setValue(rs.getDouble("dbcount"));
				cnpData.add(data);
			}
			rs.close();
			stat.close();
			double[] bardata = new double[dbMonth.size()];
			for(int k=0; k<dbMonth.size(); k++){
				bardata[k] = 0.00;
				for(int l=0; l< cnpData.size(); l++){
					if(dbMonth.get(k).equals(cnpData.get(l).getMonth())){
						bardata[k] = cnpData.get(l).getValue();
					}
				}
			}
			detail.setData(bardata);
			detailArr[i]=detail;
		}
		barResp.setDetaildata(detailArr);
		for(int k=0;k<dbMonth.size();k++){
			dbMonth.set(k, GeneralUtil.toMonth(dbMonth.get(k)));
		}
		String[] monthArr = new String[dbMonth.size()];
		monthArr = dbMonth.toArray(monthArr);		
		barResp.setMonth(monthArr);
		return barResp;				
		
	} 
	public DBBarResponse getBarMonthlyTxnSkypay(Connection conn, String merchantId, String flag) throws SQLException{
		ArrayList<String> operatortype = new ArrayList<String>();
		//Get distinct CNP Type
		String sql ="select distinct t8 as operatorType from dispaymenttransaction d inner join CNPMerchant c on "
				+ "d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n20=2 and t8<>'Select'";
		
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		ResultSet rs = stat.executeQuery();
		while (rs.next()){
			operatortype.add(rs.getString("operatorType"));
		}
		rs.close();
		stat.close();
		//Get distinct Months
		ArrayList<String> dbMonth = new ArrayList<String>();
		sql ="select distinct substring(d.TransDate,5,2) as dbmonth  from dispaymenttransaction d "
				+ "inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? "
				+ "and d.n20=2 and t8<>'Select' order by dbmonth";
		
		stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		rs = stat.executeQuery();
		while (rs.next()){
			dbMonth.add(rs.getString("dbmonth"));
		}
		rs.close();
		stat.close(); 
		DBBarResponse barResp = new DBBarResponse();
		DBBarDetail[] detailArr = new DBBarDetail[operatortype.size()];
		String getColumn = "";
		if(flag.equals("Count")){
			getColumn = ",count(1)as dbcount";
		}else if (flag.equals("Amount")){
			getColumn =",sum(d.Amount)as dbcount";
		}
		for(int i=0; i<operatortype.size();i++){
			
			sql = "select substring(d.TransDate,5,2) as dbmonth "+getColumn+" from dispaymenttransaction d "
					+ "inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? AND t8 =? "
					+ "group by substring(d.TransDate,5,2) ";
			
			
			DBBarDetail detail = new DBBarDetail();
			detail.setName(operatortype.get(i));
			stat = conn.prepareStatement(sql);
			stat.setString(1, merchantId);
			stat.setString(2, operatortype.get(i));
			rs = stat.executeQuery();
			ArrayList<DBBarData> cnpData = new ArrayList<DBBarData>();
			while (rs.next()){
				DBBarData data = new DBBarData();
				data.setMonth(rs.getString("dbmonth"));
				data.setValue(rs.getDouble("dbcount"));
				cnpData.add(data);
			}
			rs.close();
			stat.close();
			double[] bardata = new double[dbMonth.size()];
			for(int k=0; k<dbMonth.size(); k++){
				bardata[k] = 0.00;
				for(int l=0; l< cnpData.size(); l++){
					if(dbMonth.get(k).equals(cnpData.get(l).getMonth())){
						bardata[k] = cnpData.get(l).getValue();
					}
				}
			}
			detail.setData(bardata);
			detailArr[i]=detail;
		}
		barResp.setDetaildata(detailArr);
		for(int k=0;k<dbMonth.size();k++){
			dbMonth.set(k, GeneralUtil.toMonth(dbMonth.get(k)));
		}
		String[] monthArr = new String[dbMonth.size()];
		monthArr = dbMonth.toArray(monthArr);		
		barResp.setMonth(monthArr);
		return barResp;				
		
	}
	public DBBarResponse getBarTxnStatusofCurrentMonth(Connection conn, String merchantId, String flag, int feature) throws SQLException{
		ArrayList<String> cnptype = new ArrayList<String>();
		//Get distinct CNP Type
		String sql ="select distinct replace (replace (t13,'B-',''),'T-','') as CNPType from dispaymenttransaction d "
				+ " inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n20=? and "
				+ " cast( SUBSTRING(d.TransDate,5,2)as int)= MONTH(getdate())";
		
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		stat.setInt(2, feature);
		ResultSet rs = stat.executeQuery();
		while (rs.next()){
			cnptype.add(rs.getString("CNPType"));
		}
		rs.close();
		stat.close();
		//Get distinct Months
		ArrayList<String> dbStatus = new ArrayList<String>();
		sql ="select distinct  c.status as dbStatus from dispaymenttransaction d "
				+ " inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n20=? "
				+ " and cast( SUBSTRING(d.TransDate,5,2)as int)= MONTH(getdate()) order by c.status ";
		
		stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		stat.setInt(2, feature);
		rs = stat.executeQuery();
		while (rs.next()){
			dbStatus.add(rs.getString("dbStatus"));
		}
		rs.close();
		stat.close(); 
		DBBarResponse barResp = new DBBarResponse();
		DBBarDetail[] detailArr = new DBBarDetail[cnptype.size()];
		String getColumn = "";
		if(flag.equals("Count")){
			getColumn = ",count(d.Autokey)as dbcount";
		}else if (flag.equals("Amount")){
			getColumn =",sum(d.Amount)as dbcount";
		}
		for(int i=0; i<cnptype.size();i++){
			
			sql= "select c.status as dbStatus "+getColumn+" from dispaymenttransaction d inner join "
					+ " CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n20=? "
					+ " and cast( SUBSTRING(d.TransDate,5,2)as int)= MONTH(getdate()) AND t13 like '%"+cnptype.get(i)+"%' group by c.status";
			DBBarDetail detail = new DBBarDetail();
			detail.setName(cnptype.get(i));
			stat = conn.prepareStatement(sql);
			stat.setString(1, merchantId);
			stat.setInt(2, feature);
			rs = stat.executeQuery();
			ArrayList<DBBarData> cnpData = new ArrayList<DBBarData>();
			while (rs.next()){
				DBBarData data = new DBBarData();
				data.setMonth(rs.getString("dbStatus"));
				data.setValue(rs.getDouble("dbcount"));
				cnpData.add(data);
			}
			rs.close();
			stat.close();
			double[] bardata = new double[dbStatus.size()];
			for(int k=0; k<dbStatus.size(); k++){
				bardata[k] = 0.00;
				for(int l=0; l< cnpData.size(); l++){
					if(dbStatus.get(k).equals(cnpData.get(l).getMonth())){
						bardata[k] = cnpData.get(l).getValue();
					}
				}
			}
			detail.setData(bardata);
			detailArr[i]=detail;
		}
		barResp.setDetaildata(detailArr);
		String[] monthArr = new String[dbStatus.size()];
		monthArr = dbStatus.toArray(monthArr);		
		barResp.setMonth(monthArr);
		return barResp;				
		
	}
	public DBBarResponse getBarTxnStatusofCurrentMonthSkypay(Connection conn, String merchantId, String flag) throws SQLException{
		ArrayList<String> cnptype = new ArrayList<String>();
		//Get distinct CNP Type
		String sql ="select distinct T8 as CNPType from dispaymenttransaction d "
				+ " inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and "
				+ " cast( SUBSTRING(d.TransDate,5,2)as int)= MONTH(getdate())";
		
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		ResultSet rs = stat.executeQuery();
		while (rs.next()){
			cnptype.add(rs.getString("CNPType"));
		}
		rs.close();
		stat.close();
		//Get distinct Months
		ArrayList<String> dbStatus = new ArrayList<String>();
		sql ="select distinct  c.status as dbStatus from dispaymenttransaction d "
				+ " inner join CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? "
				+ " and cast( SUBSTRING(d.TransDate,5,2)as int)= MONTH(getdate()) order by c.status ";
		
		stat = conn.prepareStatement(sql);
		stat.setString(1, merchantId);
		rs = stat.executeQuery();
		while (rs.next()){
			dbStatus.add(rs.getString("dbStatus"));
		}
		rs.close();
		stat.close(); 
		DBBarResponse barResp = new DBBarResponse();
		DBBarDetail[] detailArr = new DBBarDetail[cnptype.size()];
		String getColumn = "";
		if(flag.equals("Count")){
			getColumn = ",count(d.Autokey)as dbcount";
		}else if (flag.equals("Amount")){
			getColumn =",sum(d.Amount)as dbcount";
		}
		for(int i=0; i<cnptype.size();i++){
			
			sql= "select c.status as dbStatus "+getColumn+" from dispaymenttransaction d inner join "
					+ " CNPMerchant c on d.FlexRefNo = c.TransferRefNo2 where d.t20=? and d.n2=9 "
					+ " and cast( SUBSTRING(d.TransDate,5,2)as int)= MONTH(getdate()) AND t8 = '"+cnptype.get(i)+"' group by c.status";
			DBBarDetail detail = new DBBarDetail();
			detail.setName(cnptype.get(i));
			stat = conn.prepareStatement(sql);
			stat.setString(1, merchantId);
			rs = stat.executeQuery();
			ArrayList<DBBarData> cnpData = new ArrayList<DBBarData>();
			while (rs.next()){
				DBBarData data = new DBBarData();
				data.setMonth(rs.getString("dbStatus"));
				data.setValue(rs.getDouble("dbcount"));
				cnpData.add(data);
			}
			rs.close();
			stat.close();
			double[] bardata = new double[dbStatus.size()];
			for(int k=0; k<dbStatus.size(); k++){
				bardata[k] = 0.00;
				for(int l=0; l< cnpData.size(); l++){
					if(dbStatus.get(k).equals(cnpData.get(l).getMonth())){
						bardata[k] = cnpData.get(l).getValue();
					}
				}
			}
			detail.setData(bardata);
			detailArr[i]=detail;
		}
		barResp.setDetaildata(detailArr);
		String[] monthArr = new String[dbStatus.size()];
		monthArr = dbStatus.toArray(monthArr);		
		barResp.setMonth(monthArr);
		return barResp;				
		
	}
	
	
}
