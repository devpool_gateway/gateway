package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.data.AssetData;
import com.nirvasoft.rp.data.BrandData;
import com.nirvasoft.rp.data.BrandDataSet;
import com.nirvasoft.rp.data.ListBrandData;
import com.nirvasoft.rp.data.V_AssetData;
import com.nirvasoft.rp.data.V_AssetDataSet;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;


public class BrandDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("HRM915");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));		
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	public static BrandData getDBRecord(DBRecord adbr) {
		BrandData ret = new BrandData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		return ret;
	}

	public static DBRecord setDBRecord(BrandData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getRecordStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		
		
		return ret;
	}

	public static BrandData read(long syskey, Connection conn) throws SQLException {
		BrandData ret = new BrandData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0) {
			String ddate = dbrs.get(0).getString("t5");
			dbrs.get(0).setValue("t5", ddate);
			ret = getDBRecord(dbrs.get(0));
		}
		return ret;
	}

	public static boolean isCodeExist(BrandData obj, Connection conn) throws SQLException {
		String sql = " SELECT COUNT (*) AS retCount FROM A00001 WHERE RecordStatus <> 4 AND syskey<>? AND T1 = ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setLong(1, obj.getSyskey());
		stat.setString(2, obj.getT1());
		ResultSet result = stat.executeQuery();
		result.next();
		int count = result.getInt("retCount");
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static Result insert(BrandData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
			}
		}
		return res;
	}

	public static Result update(BrandData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
			}
		}
		return res;
	}

	public static Result delete(String syskey, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = " UPDATE HRM915 SET RecordStatus=4,userid=?,username=? WHERE Syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, user.getUser().getUserId());
		stmt.setString(2, user.getUser().getUserName());
		stmt.setString(3, syskey);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
		}
		return res;
	}

	public static BrandDataSet getBrandList(Connection conn) throws SQLException {
		BrandDataSet res = new BrandDataSet();
		ArrayList<BrandData> datalist = new ArrayList<BrandData>();
		String whereClause = " WHERE RecordStatus<>4 ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause, " ORDER BY syskey", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		BrandData[] dataarry = new BrandData[datalist.size()];
		
		for(int i=0;i<datalist.size();i++){
			dataarry[i] = datalist.get(i);
		}
		
		res.setData(dataarry);		
		
		return res;
	}


	
}
