package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.ReconcilationListData;
import com.nirvasoft.rp.shared.ReconciliationList;

public class ReconciliationDao {
	
	
	public ReconciliationList getReconciliationList(int currentPage, int totalCount, int pageSize,
			String aFromDate, String aToDate,String tStatus, String status, String kbztransid,String transid, Connection conn) throws SQLException {		
		ReconciliationList rec = new ReconciliationList();		
		ArrayList<ReconcilationListData> datalist = new ArrayList<ReconcilationListData>();
		PreparedStatement stmt;
		ResultSet rs = null;
		ResultSet result = null;
		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;		
		String whereSql = "";
		String rspk = "";
		
		if(!transid.trim().equals("")){
			if(tStatus.trim().equals("reg")){
			whereSql = " and re.RSPInfoKey = '" + transid.trim() + "'";
			}
			if(tStatus.trim().equals("liq") ||  tStatus.trim().equals("refund")){
				rspk = transid.substring(0, transid.length() - 3);
				whereSql = " and re.t4 = '" + rspk.trim() + "'";
			}
		}		
		if(!kbztransid.trim().equals("")){
			whereSql += " and re.TransRef = '" + kbztransid.trim() + "'";
		}
		String aSValue = "";
		if(!tStatus.trim().equals("")){
			if(tStatus.trim().equals("reg")){
				aSValue = "1";
			}
			if(tStatus.trim().equals("liq")){
				aSValue = "2";
			}
			if(tStatus.trim().equals("refund")){
				aSValue = "3";
			}			
		}	
		whereSql +=" and re.Status = " + aSValue;
		if(!status.trim().equals(" ")){
			if(status.trim().equals("Success")){
				whereSql += " and re.t1 ='"+aSValue+"'";
			}else if(status.trim().equals("Fail")){
				whereSql +=" and re.t1<> '"+aSValue+"'";
			}
		}		
		String l_Query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY r.RSPDate DESC,r.RSPTime DESC) AS RowNum, ";
		if(tStatus.trim().equals("reg")){ 
			l_Query +=" r.RSPDate,r.RSPTime";					
		}
		if(tStatus.trim().equals("liq") ||  tStatus.trim().equals("refund")){
			l_Query += " re.SysDate,re.SysTime";				
		}
		l_Query	+= ",re.TransRef,re.RSPInfoKey,r.Status,r.ToCCY,Amount,re.t2"
			+ " from RemitTransfer r join RemitTransferExt re on r.t4 = re.t4 "
			+ " Where ";
		if(tStatus.trim().equals("reg")){
			l_Query += " r.RSPDate >='"+aFromDate+"' AND r.RSPDate <= '"+aToDate + "' " + whereSql;
		}
		if(tStatus.trim().equals("liq") ||  tStatus.trim().equals("refund")){
			l_Query += " re.SysDate >='"+aFromDate+"' AND re.SysDate <= '"+aToDate + "' "+ whereSql;
		}
		l_Query += " )a WHERE RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;
		l_Query += " order by RowNum";	
		stmt = conn.prepareStatement(l_Query);
		rs = stmt.executeQuery();		
		while (rs.next()) {
			ReconcilationListData data = new ReconcilationListData();
			String td = "";
			String ttime ="";			
			if(tStatus.trim().equals("reg")){
				td = rs.getString("RSPDate");
				ttime = rs.getString("RSPTime");
				
			}
			if(tStatus.trim().equals("liq") || tStatus.trim().equals("refund")){
				td = rs.getString("SysDate");
				ttime = rs.getString("SysTime");
			}
			
			data.setSrno(rs.getInt("RowNum"));
			data.setTransactionDateTime(td.substring(6)+ "/" + td.substring(4, 6)+ "/" + td.substring(0, 4) +" "+ttime);
			data.setKbzTransID(rs.getString("TransRef"));
			data.setTransID(rs.getString("RSPInfoKey"));			
			data.setTransStatus(status);
			double amount = 0.0;
			amount = rs.getDouble("Amount");		
			data.setAmount(rs.getString("ToCCY").concat(String.format("%,.2f", amount)));
			data.setRemark(rs.getString("t2"));
			datalist.add(data);
		}				
		rec.setPageSize(pageSize);
		rec.setCurrentPage(currentPage);		
		String countSQL = "SELECT COUNT(*) As recCount FROM (SELECT ROW_NUMBER() OVER (ORDER BY r.RSPDate DESC,r.RSPTime DESC) AS RowNum, ";
		if(tStatus.trim().equals("reg")){ 
			countSQL +=" r.RSPDate,r.RSPTime";					
		}
		if(tStatus.trim().equals("liq") ||  tStatus.trim().equals("refund")){
			countSQL += " re.SysDate,re.SysTime";				
		}
	    countSQL += ",re.TransRef,re.RSPInfoKey,re.t2,r.ToCCY,Amount "
			+ "from RemitTransfer r join RemitTransferExt re on r.t4 = re.t4 "
			+ " Where ";
		if(tStatus.trim().equals("reg")){
			countSQL += " r.RSPDate >='"+aFromDate+"' AND r.RSPDate <= '"+aToDate+ "' " + whereSql;
		}
		if(tStatus.trim().equals("liq") ||  tStatus.trim().equals("refund")){
			countSQL += " re.sysDate >='"+aFromDate+"' AND re.sysDate <= '"+aToDate+ "' " + whereSql;
		}
		countSQL += " ) a";
		PreparedStatement stat = conn.prepareStatement(countSQL);
		result = stat.executeQuery();
		if(result.next()){
			rec.setTotalCount(result.getInt("recCount"));
			ReconcilationListData[] dataarry = new ReconcilationListData[datalist.size()];
			dataarry = datalist.toArray(dataarry);
			rec.setData(dataarry);
		}
		stmt.close();
		rs.close();
		stat.close();
		result.close();		
		return rec;
	}

}
