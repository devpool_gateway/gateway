package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserDataset;
import com.nirvasoft.rp.users.data.UserRole;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.ServerUtil;

public class UserDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 2));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("V_U001_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		// ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		// ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("UN", (byte) 5));

		return ret;
	}

	public static UserViewData getDBViewRecord(DBRecord adbr) {
		UserViewData ret = new UserViewData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setUsername(adbr.getString("UN"));

		return ret;
	}

	public static UserData getDBRecord(DBRecord adbr) {
		UserData ret = new UserData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(ServerUtil.decryptPIN(adbr.getString("t2")));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getInt("n5"));
		ret.setN6(adbr.getInt("n6"));
		ret.setN7(adbr.getInt("n7"));
		ret.setN8(adbr.getLong("n8"));

		return ret;
	}

	public static DBRecord setDBRecord(UserData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t3", data.getT3());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		return ret;
	}

	public static DBRecord setDBRecord(UserRole data) {
		DBRecord ret = define("JUN002_A");
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static UserRole getDBRecords(DBRecord adbr) {
		UserRole ret = new UserRole();
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));

		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));

		return ret;
	}

	public static UserData read(long syskey, Connection conn) throws SQLException {

		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	public static boolean isCodeExist(UserData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND syskey <> " + obj.getSyskey() + " AND T1=\'" + obj.getT1() + "\'", "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<Result> getAllFeatures(String code ,Connection conn) throws SQLException {

		ArrayList<Result> datalist = new ArrayList<Result>();
		String whereclause = "";
		
		if(code.equalsIgnoreCase("null") || code.equalsIgnoreCase("")){
			 whereclause = "";
		}else{
			 whereclause = " And code in (Select Feature From MerchantFeature Where MerchantID = '"+code+"') ";
		}

		String sql = "select Code,Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Feature') " + whereclause +" ORDER BY Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			Result combo = new Result();
			combo.setKeyst(res.getString("Code"));
			combo.setKeyString(res.getString("Description"));
			datalist.add(combo);
		}

		return datalist;
	}
	
	public static Result insert(UserData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int rs = stmt.executeUpdate();

			if (rs > 0) {

				UserRole jun = new UserRole();
				for (long l : obj.getRolesyskey()) {

					if (l != 0) {

						jun.setRecordStatus(obj.getRecordStatus());
						jun.setSyncBatch(obj.getSyncBatch());
						jun.setSyncStatus(obj.getSyncStatus());
						jun.setUsersyskey(obj.getSyskey());
						jun.setN1(obj.getSyskey());
						jun.setN2(l);
						res = insertUserRole(jun, conn);

					}

				}

				if (res.isState()) {

					res.setState(true);
					res.setMsgDesc("Saved Successfully");

				} else
					res.setMsgDesc("Cannot Save");

			}
		} else
			res.setMsgDesc("Data already exist");

		return res;
	}

	public static Result insertUserRole(UserRole obj, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = DBMgr.insertString(define("JUN002_A"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int rs = stmt.executeUpdate();

		if (rs > 0) {

			res.setState(true);
		}

		return res;
	}

	public static Result update(UserData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int rs = stmt.executeUpdate();

			if (rs > 0) {

				UserRole jun = new UserRole();

				for (long l : obj.getRolesyskey()) {

					jun.setN1(obj.getSyskey());
					jun.setN2(l);

					sql = "DELETE FROM JUN002_A WHERE n1=?";
					stmt = conn.prepareStatement(sql);
					stmt.setLong(1, jun.getN1());
					stmt.executeUpdate();

				}

				for (long l : obj.getRolesyskey()) {

					if (l != 0) {

						jun.setRecordStatus(obj.getRecordStatus());
						jun.setSyncBatch(obj.getSyncBatch());
						jun.setSyncStatus(obj.getSyncStatus());
						jun.setUsersyskey(obj.getUsersyskey());

						jun.setN1(obj.getSyskey());
						jun.setN2(l);
						res = insertUserRole(jun, conn);

					}

				}

				if (res.isState()) {

					res.setState(true);
					res.setMsgDesc("Updated Successfully");

				} else
					res.setMsgDesc("Cannot Update");

			}
		} else
			res.setMsgDesc("Data already exist");
		return res;
	}

	public static Result delete(long syskey, Connection conn) throws SQLException {
		Result res = new Result();
		String userId = "";

		res = PersonDao.deletePerson(syskey, conn);

		if (res.isState()) {

			String s_sql = "SELECT T1 FROM UVM005_A a WHERE Syskey='" + syskey + "' and a.recordstatus<>4";
			PreparedStatement sttmt = conn.prepareStatement(s_sql);
			ResultSet result = sttmt.executeQuery();
			while (result.next()) {
				res.setUserId(result.getString("T1"));
				userId = result.getString("T1");
			}

			String sql = "UPDATE JUN002_A SET RecordStatus=4 WHERE n1=?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {

				sql = "UPDATE UVM005_A SET RecordStatus=4 WHERE Syskey=?";
				stmt = conn.prepareStatement(sql);
				stmt.setLong(1, syskey);
				rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);
					res.setMsgDesc("Deleted Successfully");
				}
			}

		}

		return res;
	}

	public static boolean canDelete(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From JUN002_A Where n2=" + key;
		PreparedStatement stat = conn.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));

		}
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static UserViewDataset getAllUserData(String searchtext, MrBean user, Connection conn) throws SQLException {

		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		String searchText = searchtext.replace("'", "''");
		// String whereClause = " WHERE RecordStatus<>4 ";
		String whereClause = "";
		if (searchText.equals("")) {
			whereClause = " WHERE RecordStatus<>4 ";

		} else {

			whereClause = " WHERE RecordStatus<>4  and (t1 like '%" + searchText + "%' or UN like '%" + searchText
					+ "%')";
		}

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), whereClause, " ORDER BY t1,UN", conn); // syskey

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBViewRecord(dbrs.get(i)));
		}

		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);
		return dataSet;
	}

	public static long getPersonSyskey(long usys, Connection con) throws SQLException {
		String sql = "SELECT * FROM UVM005_A WHERE syskey=" + usys;
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		long u = 0;
		while (result.next()) {
			u = result.getLong("n4");

		}
		return u;

	}

}