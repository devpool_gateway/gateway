package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.framework.Menu;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Profile;
import com.nirvasoft.rp.util.ServerUtil;

public class Service001Dao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		return ret;
	}
	
	public Menu[] getProfileSubMenuItem(Profile p, Connection con, String parent) {
		ArrayList<String> arrdesp = new ArrayList<String>();
		ArrayList<String> arrmenu = new ArrayList<String>();
		String[] arydesp = null;
		String[] arymenu = null;

		Menu[] items = null;

		try {

			String sql = "SELECT t1,t2 FROM UVM022_A " + " WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE  "
					+ " n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN " + "(SELECT syskey FROM UVM005_A WHERE (t1=\'"
					+ p.getUserID() + "\'))))"
					+ " AND RecordStatus<> 4 AND n2 IN (SELECT DISTINCT syskey from UVM022_A WHERE t2=" + "\'" + parent
					+ "\')";
			sql += " order by n6 ";

			System.out.println("Sub Menu query : " + sql);
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				arrdesp.add(result.getString("t2"));
				arrmenu.add(result.getString("t1"));

			}

			arydesp = new String[arrdesp.size()];
			for (int i = 0; i < arrdesp.size(); i++) {
				arydesp[i] = arrdesp.get(i);
			}

			arymenu = new String[arrmenu.size()];
			for (int i = 0; i < arrmenu.size(); i++) {
				arymenu[i] = arrmenu.get(i);
			}

			items = new Menu[arymenu.length];
			Menu subobj = new Menu();
			if (arymenu.length == 1)
				items = new Menu[arymenu.length + 1];

			for (int j = 0; j < arymenu.length; j++) {
				subobj = new Menu();
				subobj.setMenuItem(arymenu[j]);
				subobj.setCaption(arydesp[j]);
				items[j] = subobj;

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return items;

	}
	
	/*public static String login (MrBean user, Connection con) throws SQLException{
	
	  	String name = user.getUser().getUserId();
		String psw = user.getUser().getPassword();
		psw=ServerUtil.encryptPIN(psw);
		String u="";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), 
					              "WHERE n2 = 3 and  (t1=\'"+name+"\' COLLATE SQL_Latin1_General_CP1_CS_AS AND t2=\'"+psw+"\' COLLATE SQL_Latin1_General_CP1_CS_AS)"
					              +"OR (t3=\'"+name+"\' COLLATE SQL_Latin1_General_CP1_CS_AS AND t2=\'"+psw+"\' COLLATE SQL_Latin1_General_CP1_CS_AS)",
					              "", con);
			if (dbrs.size() > 0){
					name= dbrs.get(0).getString("t1");
					u= getUserData(name,con);
			}
				
			return u;	
	
		}*/

	public static String login(MrBean user, Connection con) throws SQLException {

		String userid = user.getUser().getUserId();
		String psw =  user.getUser().getPassword();

		String sql = "";
		psw = ServerUtil.encryptPIN(psw);
		String u = "";
		PreparedStatement stmt2 = null;

		sql = "Select t1,t2,RecordStatus,n7,n2 From UVM005_A Where n2 = 3 and recordStatus <> 4 and ((t1=? COLLATE SQL_Latin1_General_CP1_CS_AS and t2=?))";
		stmt2 = con.prepareStatement(sql);
		int k = 1;
		stmt2.setString(k++, userid);
		stmt2.setString(k++, psw);

		ResultSet result = stmt2.executeQuery();
		while (result.next()) {
			userid = result.getString("t1");
			u = getUserData(userid, con);

		}
		return u;

	}
	
	public static String getUserData(String userid, Connection con) throws SQLException
	{
		String sql = " SELECT *  FROM View_User_A WHERE userid=\'"+userid+"\'";
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		String u="";
		if(result.next())
		{
			u=result.getString("username");
			return u;
		}
		return u;
		
	}
	
	public static ArrayList<String> getMainMenu (MrBean user, Connection con, String userid) 
	{
		ArrayList<String> ret = new ArrayList<String>();
		try
		{
			//long skey = getUserKey(con,userid);
			//long userSyskey = user.getUser().getUserSK();
			String sql = "SELECT DISTINCT t2 FROM UVM022_A "
					+ "WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE n3 = 3 And  "
					+ "n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN (SELECT syskey FROM UVM005_A "				
					+ "WHERE (t1=\'"+user.getUser().getUserId()+"\' OR t3=\'"+user.getUser().getUserId()+"\'))))"
					+ " AND n2=0 AND RecordStatus<> 4 ";//AND t4 BETWEEN 50 AND 100";
			System.out.println("SQL Query : " + sql);

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();
			while(result.next())
			{
				ret.add(result.getString("t2"));
				
			}			
			
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		return ret;
		
	}
	
	public static ArrayList<String> getSubMenuItem (MrBean user, Connection con, String userid, String parent) 
	{
		ArrayList<String> ret = new ArrayList<String>();
	
	    try
			{
				
				String sql = "SELECT DISTINCT t2 FROM UVM022_A "
							+" WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE " 
							+" n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN "
							+"(SELECT syskey FROM UVM005_A WHERE (t1=\'"+user.getUser().getUserId()+"\' OR t3=\'"+user.getUser().getUserId()+"\'))))"
							+" AND RecordStatus<> 4 AND n2 IN (SELECT DISTINCT syskey from UVM022_A WHERE t2="+ "\'"+parent+"\')";		

				
				PreparedStatement stat = con.prepareStatement(sql);
				ResultSet result = stat.executeQuery();
				
				
				while(result.next())
				{
					ret.add(result.getString("t2"));
					
				}			
				
			}
			catch(SQLException sqle)
			{
				sqle.printStackTrace();
			}
	    	return ret;
		
	}
	

	public static long[] getParentKey (MrBean user, Connection con, String userid) 
	{
		ArrayList<Long> key = new ArrayList<Long>();
		
		try
		{
					
			String sql = "SELECT DISTINCT syskey FROM UVM022_A "
					+ "WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE n3 = 3 and"
					+ "n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN (SELECT syskey FROM UVM005_A "				
					+ "WHERE t1=\'"+user.getUser().getUserId()+"\')))"
					+ " AND n2=0 AND RecordStatus<> 4 ORDER BY syskey ";		

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();
			
			
			while(result.next())
			{
				key.add(result.getLong("syskey"));
				
			}			
			
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		
		
		long parentkey[] = new long[key.size()];
		
		for (int i=0; i<key.size(); i++)
		{
			parentkey[i]= key.get(i);
		}
		
		return parentkey;
		
	}

	
	
}
