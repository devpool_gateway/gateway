package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.CNPTransData;
import com.nirvasoft.rp.shared.DetailsData;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.DispaymentTransactionData;
import com.nirvasoft.rp.shared.LOVSetupDetailData;
import com.nirvasoft.rp.shared.PayTemplateDetail;
import com.nirvasoft.rp.shared.PayTemplateDetailArr;
import com.nirvasoft.rp.shared.PayTemplateHeader;
import com.nirvasoft.rp.shared.ServerGlobal;
import com.nirvasoft.rp.util.GeneralUtil;

public class DispaymentTransactionDao {
	public DispaymentList getMerchantList(int currentPage, int totalCount, int pageSize, String aMerchantID,
			String status, String aFromDate, String aToDate, String aFromAcc, String aToAcc, String did,
			String initiatedby, String transrefno, String processingCode, String proDisCode,
			PayTemplateDetail[] templateData, Connection conn) throws SQLException {
		DispaymentList res = new DispaymentList();
		ArrayList<DispaymentTransactionData> datalist = new ArrayList<DispaymentTransactionData>();

		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;

		PayTemplateDetailArr[] DetailArray = null;
		GeneralUtil.readDataConfig();
		String templateMerchant = "";

		if (processingCode.equalsIgnoreCase("02")) {
			// Just Fortune Ygn merchant is mapped in Paytemplate Setup
			templateMerchant = ServerGlobal.getFortuneYgnMerchant();
		} else {
			templateMerchant = aMerchantID;
		}
		long key = 0;
		String Query = "SELECT SysKey from PayTemplateHeader WHERE MerchantID = ?";
		PreparedStatement pstmt = conn.prepareStatement(Query);
		pstmt.setString(1, templateMerchant);
		ResultSet rs1 = pstmt.executeQuery();
		if (rs1.next()) {
			key = rs1.getLong("SysKey");
		}
		pstmt.close();
		rs1.close();

		ArrayList<PayTemplateDetailArr> DetailList = new ArrayList<PayTemplateDetailArr>();
		String query = "select Caption,DataType,DisPayField from PayTemplateDetail "
				+ " where HKey=? and FieldShow in (4,5,6)"
				+ " and DisPayField not in('TransDate','Amount','CustomerCode','T16')";

		pstmt = conn.prepareStatement(query);
		pstmt.setLong(1, key);
		ResultSet rs2 = pstmt.executeQuery();
		while (rs2.next()) {
			PayTemplateDetailArr Detail = new PayTemplateDetailArr();

			Detail.setPayCaption(rs2.getString("Caption"));
			Detail.setPaydatatype(rs2.getString("DataType"));
			Detail.setPaydispayfield(rs2.getString("DisPayField"));
			DetailList.add(Detail);
		}
		
		
		String aCriteria = "" , bCriteria = "" ;
		if (!aFromDate.trim().equals("")) {
			aCriteria += " and c.TransDate >= '" + aFromDate.trim() + "'"; //d.TransDate
			bCriteria += " and c.TransDate >= '" + aFromDate.trim() + "'"; //d.TransDate
		}

		if (!aToDate.trim().equals("")) {
			aCriteria += " and c.TransDate <= '" + aToDate.trim() + "'"; //d.TransDate
			bCriteria += " and c.TransDate <= '" + aToDate.trim() + "'"; //d.TransDate
		}
		if (!aFromAcc.trim().equals("")) {
			aCriteria += " and FromAccount = '" + aFromAcc.trim() + "'";
		}

		if (!aToAcc.trim().equals("")) {
			aCriteria += " and ToAccount = '" + aToAcc.trim() + "'";
		}

		if (!did.trim().equals("")) {
			aCriteria += " and CustomerCode = '" + did.trim() + "'";
		}

		if (!initiatedby.trim().equals("ALL")) {
			if (initiatedby.trim().equals("Online")) {
				aCriteria += " and d.MobileUserID = 'Online'";
			} else if (initiatedby.trim().equals("OTC")) {
				aCriteria += " and d.MobileUserID <> 'Online'";
			}
		}

		if (!transrefno.trim().equals("")) {
			aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
		}

		if ("01".equalsIgnoreCase(processingCode) || "03".equalsIgnoreCase(processingCode)) {
			for (int i = 0; i < templateData.length; i++) {
				System.out.println("templateData.length " + templateData.length);
				String fValue = "";
				if (!templateData[i].getFieldvalue().trim().equals("000")
						&& templateData[i].getFieldID().equals("R4")) {
					fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(),
							templateData[i].getFieldvalue(), conn);
					aCriteria += " and d." + templateData[i].getDisPayField() + " = '" + fValue + "' ";
					System.out.println("R4  " + templateData[i].getFieldvalue() + " " + templateData[i].getDisPayField()
							+ " " + fValue);
				} else if (!templateData[i].getFieldvalue().trim().equals("000")
						&& templateData[i].getFieldID().equals("R5")) {
					fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(),
							templateData[i].getFieldvalue(), conn);
					aCriteria += " and d." + templateData[i].getDisPayField() + " = '" + fValue + "' ";
					System.out.println("R5  " + templateData[i].getFieldvalue() + " " + templateData[i].getDisPayField()
							+ " " + fValue);
				}

			}
		}
		String l_Query = "";
		String l_Query_Total = "";
		String GT = "";
		String IA = "";
		String DA = "";

		if (proDisCode.equalsIgnoreCase("01")) {
			l_Query = "   select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,  d.*, "
					+ "d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress  from DispaymentTransaction d ,DistributorList dl "
					+ "where d.CustomerCode=dl.DistributorID and d.t20 = '" + aMerchantID + "' And d.n3=4 "
					+ aCriteria + " )As r " + "WHERE RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord
					+ " order by autokey desc";
		} else {
			
			if (processingCode.equalsIgnoreCase("05")) {//CNP
				if (status.equalsIgnoreCase("SUCCESS")) {
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria + " And d.n3=4 " 
							+ "and c.ApiStep = 2 and c.Status = 'Success'"
							+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1  " + bCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;
					
					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
							+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria  + " And d.n3=4 " 
							+ "and c.ApiStep = 2 and c.Status = 'Success'"
							+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 " + bCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
							+") As r) as g";
					
				}else if (status.equalsIgnoreCase("FAIL")) {
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria + " And d.n3=4 " 
							+ "and c.ApiStep = 2 and c.Status = 'Failed'"
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;
					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
							+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria  + " And d.n3=4 " 
							+ "and c.ApiStep = 2 and c.Status = 'Failed'"
							+") As r) as g";
					
				}else if (status.equalsIgnoreCase("PENDING")) {
					
					l_Query = "select * from ( select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria + " And d.n3=4 " 
							+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
							+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;
					
					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
							+ "select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria  + " And d.n3=4 " 
							+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
							+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
							+") As r) as g";
					
				}else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED") || status.equalsIgnoreCase("Reversed")) {
					
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria  + " And d.n3=4 " 
							+ "and c.ApiStep = 3 and c.Status in ('" + status + "')"
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;
					
					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
							+ "SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + aCriteria + " And d.n3=4 " 
							+ "and c.ApiStep = 3 and c.Status in ('" + status + "')"
							+") As r) as g";
					
				}else{//ALL
					
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
							+ " from (SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
							+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 " + aCriteria +" and d.t20 = "
							+ " (select merchantid from PayTemplateHeader where ProcessingCode = '050200') And d.n3=4 " 
							+ " ) as t"
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;
					
					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( "
							+ "SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
							+ " from ( SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
							+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 " + aCriteria +" and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
							+ " And d.n3=4 "
							+ " ) as t"						
							+") As r) as g";
				}
				
			}else{
				if (status.equalsIgnoreCase("ALL")) {
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + " And d.n3=4 " + aCriteria
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;

					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA,  Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + " And d.n3=4 " + aCriteria + ") As r) as g";

				}
				else if (status.equalsIgnoreCase("PENDING")) {
					String code = "217";
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + "And c.MessageCode='" + code + "'" + " And d.n3=4 " + aCriteria
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;

					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
							+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
							+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + "And c.MessageCode='" + code + "'" + " And d.n3=4 " + aCriteria
							+ ") As r) as g";

				}else if (status.equalsIgnoreCase("FAIL")) {
					
					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '" + 216 + "'" + " And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;

					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
							+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
							+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + "And  c.status='" + status + "'" + " And d.n3=4 " + aCriteria
							+ ") As r) as g";

				}else {

					l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
							+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord ;

					l_Query_Total = "select sum(T15T) as incomeAmt, sum(GTA) as grandtotalAmount ,sum(T18T) as deduAmt from (select Amount as GTA, "
							+ " Cast(T15 as float) as  T15T ,CAST (T18 as float) as T18T from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc)"
							+ " AS RowNum,c.messagecode, c.messagedescription, d.*,"
							+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
							+ ") As r) as g";

				}
			}
		}

		PreparedStatement stmtTotal = conn.prepareStatement(l_Query_Total);
		System.out.println("Query1..." + l_Query);
		System.out.println("Query2... " + l_Query_Total);
		ResultSet rsTotal = stmtTotal.executeQuery();
		while (rsTotal.next()) {
			GT = rsTotal.getString("grandtotalAmount");
			IA = rsTotal.getString("incomeAmt");
			DA = rsTotal.getString("deduAmt");
			if (GT != null || IA != null || DA != null) {
				DecimalFormat df = new DecimalFormat("###,000.00");
				String formatted = df.format(Double.parseDouble(GT));

				DecimalFormat dfI = new DecimalFormat("###,000.00");
				String formattedIA = dfI.format(Double.parseDouble(IA));

				DecimalFormat dfD = new DecimalFormat("###,000.00");
				String formattedDA = dfD.format(Double.parseDouble(DA));

				res.setGrandTotal(formatted);
				res.setIncomeAmt(formattedIA);
				res.setDeduAmt(formattedDA);
			}

		}

		PreparedStatement stmt = conn.prepareStatement(l_Query);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			DispaymentTransactionData data = new DispaymentTransactionData();
			String aTransDate = "";
			aTransDate = rs.getString("TransDate");
			data.setTransactiondate(
					aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4));
			data.setSyskey(rs.getLong("Autokey"));
			data.setTranrefnumber(rs.getString("FlexRefNo"));
			data.setActivebranch(rs.getString("FromBranch"));
			data.setDebitaccount(rs.getString("FromAccount"));
			data.setDebitbranch(rs.getString("T1"));
			data.setCollectionaccount(rs.getString("ToAccount"));
			if (rs.getString("MobileUserID").equals("Online")) {
				data.setBranch("Online");
			} else {
				data.setBranch("OTC");
			}

			Double amount = rs.getDouble("Amount");
			data.setAmountst(String.format("%,.2f", amount));
			data.setAmount(rs.getDouble("Amount"));
			if (!(rs.getString("t15").equals(""))) {
				data.setT15(Double.valueOf(rs.getString("t15")));
			}
			if (!(rs.getString("t18").equals(""))) {
				data.setT18(Double.valueOf(rs.getString("t18")));
			}

			
			data.setCommchargest(String.format("%,.2f", data.getT15()));
			data.setCommuchargest(String.format("%,.2f", data.getT18()));
			data.setDid(rs.getString("CustomerCode"));
			data.setCustomername(rs.getString("CustomerName"));
			data.setDescription(rs.getString("t16"));
			if (proDisCode.equalsIgnoreCase("01")) {
				data.setDistributorName(rs.getString("DistributorName"));
				data.setDistributorAddress(rs.getString("DistributorAddress"));
			}
			DetailArray = new PayTemplateDetailArr[DetailList.size()];
			DetailArray = DetailList.toArray(DetailArray);
			for (int i = 0; i < DetailArray.length; i++) {
				if (DetailArray[i].getPaydatatype().equalsIgnoreCase("lov")
						|| DetailArray[i].getPaydatatype().equalsIgnoreCase("date")
						|| DetailArray[i].getPaydatatype().equalsIgnoreCase("text")) {

					switch (DetailArray[i].getPaydispayfield()) {

					case "FileUploadDateTime":
						data.setFileUploadDateTime(rs.getString("FileUploadDateTime"));
						break;// finished
					case "CustomerNumber":
						data.setCustomerNumber(rs.getString("CustomerNumber"));
						break;// finished

					case "T7":
						data.setT7(rs.getString("T7"));
						break;// finished
					case "T8":
						data.setT8(rs.getString("T8"));
						break;// finished
					case "T10":
						data.setT10(rs.getString("T10"));
						break;// finished
					case "T11":
						data.setT11(rs.getString("T11"));
						break;// finished
					case "T12":
						data.setT12(rs.getString("T12"));
						break;// finished
					case "T13":
						data.setT13(rs.getString("T13"));
						break;// finished
					case "T15":
						data.setT16(rs.getString("T15"));
						break;// finished
					case "T16":
						data.setT16(rs.getString("T16"));
						break;
					case "T17":
						data.setT17(rs.getString("T17"));
						break;// finished
					case "T19":
						data.setT19(rs.getString("T19"));
						break;// finished
					case "T20":
						data.setT20(rs.getString("T20"));
						break;// finished
					case "T21":
						data.setT21(rs.getString("T21"));
						break;// finished
					case "T22":
						data.setT22(rs.getString("T22"));
						break;// finished
					case "T23":
						data.setT23(rs.getString("T23"));
						break;// finished
					case "T24":
						data.setT24(rs.getString("T24"));
						break;// finished
					case "T25":
						data.setT25(rs.getString("T25"));
						break;// finished

					}

				}

				if (DetailArray[i].getPaydatatype().equalsIgnoreCase("number")
						|| (!(DetailArray[i].getPaydispayfield().equals("")))) {
					switch (DetailArray[i].getPaydispayfield()) {
					case "N1":
						data.setN1(rs.getInt("N1"));
						break;// finished
					case "N7":
						data.setN7(rs.getInt("N7"));
						break;// finished
					case "N8":
						data.setN8(rs.getInt("N8"));
						break;// finished
					case "N9":
						data.setN9(rs.getInt("N9"));
						break;// finished
					case "N17":
						data.setN17(rs.getInt("N17"));
						break;// finished
					case "N18":
						data.setN18(rs.getInt("N18"));
						break;// finished
					case "N19":
						data.setN19(rs.getInt("N19"));
						break;// finished
					case "N20":
						data.setN20(rs.getInt("N20"));
						break;// finished
					// case "T18" :
					// data.setT18(Double.parseDouble(rs.getString("T18")));break;
					}
				}

			}
			data.setMessageCode(rs.getString("messagecode"));
			data.setMessageDesc(rs.getString("messagedescription"));

			datalist.add(data);

		}
		res.setDetailData(DetailArray);
		res.setPageSize(pageSize);
		res.setCurrentPage(currentPage);
		String countSQL = "";
		if (proDisCode.equalsIgnoreCase("01")) {
			countSQL = "select COUNT(*) as recCount from  ( SELECT d.*, d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress "
					+ "from DispaymentTransaction d ,DistributorList dl where d.CustomerCode=dl.DistributorID and d.t20 = '"
					+ aMerchantID + "' " + " And d.n3=4   " + aCriteria + " )As r  ";
		} else {
			
			if(processingCode.equalsIgnoreCase("05")){
				
				if(status.equalsIgnoreCase("SUCCESS")){
					countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' ";
					countSQL += "  And d.n3=4  " + aCriteria 
						     + "and c.ApiStep = 2 and c.Status = 'Success'"
							 + "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
							 + " ) As r ";
				}else if(status.equalsIgnoreCase("FAIL")){
					
					countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' ";
					countSQL += "  And d.n3=4  " + aCriteria 
							 + "and c.ApiStep = 2 and c.Status = 'Failed'"
							 + "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed'))"
							 + " ) As r ";
					
				}else if(status.equalsIgnoreCase("PENDING")){
					
					countSQL = "select COUNT(*) as recCount from ( select * from ( SELECT ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num, d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' ";
					countSQL += "  And d.n3=4  " + aCriteria 
							 + "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
							 + "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
							 + " ) As r ";
					
				}else if(status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED") || status.equalsIgnoreCase("REVERSED") ){
					
					countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
							+ aMerchantID + "' ";
					countSQL += "  And d.n3=4  " + aCriteria 
							 + "and c.ApiStep = 3 and c.Status in ('" + status + "')"
							 + " ) As r ";
					
				}else if(status.equalsIgnoreCase("ALL")){
					
					countSQL = "select COUNT(*) as recCount from ( SELECT  c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
							 + " and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
							 + "  And d.n3=4  "  + aCriteria 							
							 + " ) As r ";					
					
				}
				
			}else{
				countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
						+ aMerchantID + "' ";
				if(status.equalsIgnoreCase("FAIL")){
					countSQL += "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '" + 216 + "'" + " And c.status='" + status + "'";
				}else if(status.equalsIgnoreCase("SUCCESS")){
					countSQL += "And c.status='" + status + "'";
				}else if(status.equalsIgnoreCase("PENDING")){
					countSQL += "And c.MessageCode='" + 217 + "'";
				}
				countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";
			}
			
			
		}

		PreparedStatement stat = conn.prepareStatement(countSQL);
		System.out.println("Count.." + countSQL);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);

		return res;
	}
	
	public DispaymentList getTransactionHisByCIFID(int currentPage, int totalCount, int pageSize, String aMerchantID, String aCustomerID,
			String aFromDate, String aToDate, String aFromAcc, String aToAcc,String did,String initiatedby,String transrefno,String processingCode,String proDisCode, PayTemplateDetail[] templateData, Connection conn)
					throws SQLException {
		DispaymentList res = new DispaymentList();
		ArrayList<DispaymentTransactionData> datalist = new ArrayList<DispaymentTransactionData>();
		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;		
		PayTemplateDetailArr[] DetailArray = null;
		GeneralUtil.readDataConfig();
		String templateMerchant = "";		
		if(processingCode.equalsIgnoreCase("02")){
			//Just Fortune Ygn merchant is mapped in Paytemplate Setup 
			templateMerchant = ServerGlobal.getFortuneYgnMerchant();
		}else{
			templateMerchant = aMerchantID;
		}
		long key = 0;
		String Query="SELECT SysKey from PayTemplateHeader WHERE MerchantID = ?";
		PreparedStatement pstmt= conn.prepareStatement(Query);
		pstmt.setString(1, templateMerchant);
		ResultSet rs1 = pstmt.executeQuery();
		if(rs1.next()){
			key = rs1.getLong("SysKey");
		}
		pstmt.close();
		rs1.close();
		
		ArrayList<PayTemplateDetailArr> DetailList=new ArrayList<PayTemplateDetailArr>();
		String query="select Caption,DataType,DisPayField from PayTemplateDetail "
				+ " where HKey=? and FieldShow in (4,5,6)"
				+ " and DisPayField not in('TransDate','Amount','CustomerCode','T16')";
		
		pstmt= conn.prepareStatement(query);
		pstmt.setLong(1, key);
		ResultSet rs2 = pstmt.executeQuery();
		while (rs2.next()) {
			PayTemplateDetailArr Detail= new PayTemplateDetailArr();
			
			Detail.setPayCaption(rs2.getString("Caption"));
			Detail.setPaydatatype(rs2.getString("DataType"));
			Detail.setPaydispayfield(rs2.getString("DisPayField"));
			
			DetailList.add(Detail);
		}
		
		String aCriteria = "";
		if(!aFromDate.trim().equals("")){
			aCriteria += " and d.TransDate >= '" + aFromDate.trim() + "'";
		}
		
		if(!aToDate.trim().equals("")){
			aCriteria += " and d.TransDate <= '" + aToDate.trim() + "'";
		}
		if(!aFromAcc.trim().equals("")){
			aCriteria += " and FromAccount = '" + aFromAcc.trim() + "'";
		}
		
		if(!aToAcc.trim().equals("")){
			aCriteria += " and ToAccount = '" + aToAcc.trim() + "'";
		}
		
		if(!did.trim().equals("")){
			aCriteria += " and CustomerCode = '" + did.trim() + "'";
		}
		
			if(!initiatedby.trim().equals("ALL")){
				   if(initiatedby.trim().equals("Online")){
				    aCriteria += " and d.MobileUserID = 'Online'";
				   }else if(initiatedby.trim().equals("OTC")){
				    aCriteria += " and d.MobileUserID <> 'Online'";
				   }
				  }
		
		
			  
			  if(!transrefno.trim().equals("")){
			   aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
			}
			   
			  if("01".equalsIgnoreCase(processingCode)||"03".equalsIgnoreCase(processingCode)){
				for(int i = 0; i< templateData.length; i++){
					System.out.println("templateData.length "+templateData.length);
					String fValue = "";
					if(!templateData[i].getFieldvalue().trim().equals("000")&& templateData[i].getFieldID().equals("R4")){
						fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(), templateData[i].getFieldvalue(), conn);
						 aCriteria += " and d."+templateData[i].getDisPayField()+" = '"+fValue+"' ";
						 System.out.println("R4  "+templateData[i].getFieldvalue()+" "+templateData[i].getDisPayField()+" "+fValue);
					}else if(!templateData[i].getFieldvalue().trim().equals("000")&& templateData[i].getFieldID().equals("R5")){
						fValue = getLovDescriptionByLovKeyandLovCode(templateData[i].getLovKey(), templateData[i].getFieldvalue(), conn);
						 aCriteria += " and d."+templateData[i].getDisPayField()+" = '"+fValue+"' ";
						 System.out.println("R5  "+templateData[i].getFieldvalue()+" "+templateData[i].getDisPayField()+" "+fValue);
					}
					
				}
			}

			  String l_Query = "";  
		if(proDisCode.equalsIgnoreCase("01")){
			l_Query = "   select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,  d.*, "
					+ "d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress  from DispaymentTransaction d ,DistributorList dl "
					+ "where d.CustomerCode=dl.DistributorID ";
			if(!(aMerchantID.equalsIgnoreCase("ALL"))){
				l_Query += " and d.t20 = '" + aMerchantID + "' ";
			}
			// filter customer
			l_Query += " and d.t24 = '" + aCustomerID + "' ";
			l_Query += " And d.n3=4 "+aCriteria+" )As r "
					+ "WHERE RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord + " order by autokey desc";
		}else{
			l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription  , d.*,"
					+ "d.CustomerCode AS DID FROM  DispaymentTransaction d , CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 " ;
			if(!(aMerchantID.equalsIgnoreCase("ALL"))){
				l_Query += " and d.t20 = '" + aMerchantID + "' ";
			} 
			// filter customer
			l_Query += " and d.t24 = '" + aCustomerID + "' ";
			l_Query	+= " And d.n3=4 "+aCriteria+") As r  WHERE RowNum > " + l_startRecord + " and RowNum <= "+ l_endRecord + " order by autokey desc";
		}
		
		PreparedStatement stmt = conn.prepareStatement(l_Query);
		System.out.println("Query1..."+ l_Query);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			DispaymentTransactionData data = new DispaymentTransactionData();
			String aTransDate = "";
			aTransDate = rs.getString("TransDate");
			data.setTransactiondate(aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
					+ aTransDate.substring(0, 4));
			data.setSyskey(rs.getLong("Autokey"));
			data.setTranrefnumber(rs.getString("FlexRefNo"));
			data.setActivebranch(rs.getString("FromBranch"));
			data.setDebitaccount(rs.getString("FromAccount"));
			data.setDebitbranch(rs.getString("T1"));
			data.setCollectionaccount(rs.getString("ToAccount"));
			if (rs.getString("MobileUserID").equals("Online")) {
				data.setBranch("Online");
			} else {
				data.setBranch("OTC");
			}

			Double amount = rs.getDouble("Amount");
			data.setAmountst(String.format("%,.2f", amount));
			data.setAmount(rs.getDouble("Amount"));
			if (!(rs.getString("t15").equals(""))) {
				data.setT15(Double.valueOf(rs.getString("t15")));
			}
			if (!(rs.getString("t18").equals(""))) {
				data.setT18(Double.valueOf(rs.getString("t18")));
			}
			Double commcharge = 0.0;
			commcharge = data.getT15(); 
			data.setCommchargest(String.format("%,.2f", commcharge));
			data.setCommuchargest(String.format("%,.2f", data.getT18()));
			Double totalamount = 0.0;
			totalamount = data.getAmount();
			data.setTotalamountst(String.format("%,.2f", totalamount));
			//data.setDid(rs.getString("DID"));
			data.setDid(rs.getString("CustomerCode"));
			data.setCustomername(rs.getString("CustomerName"));
			data.setDescription(rs.getString("t16"));
			
			if(proDisCode.equalsIgnoreCase("01")){
				data.setDistributorName(rs.getString("DistributorName"));
				data.setDistributorAddress(rs.getString("DistributorAddress"));
			}
			DetailArray = new PayTemplateDetailArr[DetailList.size()];
			DetailArray =DetailList.toArray(DetailArray);
			
			for(int i=0;i<DetailArray.length;i++){
				
				if(DetailArray[i].getPaydatatype().equalsIgnoreCase("lov")||DetailArray[i].getPaydatatype().equalsIgnoreCase("date")||
						DetailArray[i].getPaydatatype().equalsIgnoreCase("text")){
					
					switch(DetailArray[i].getPaydispayfield()){
					
					case "FileUploadDateTime":data.setFileUploadDateTime(rs.getString("FileUploadDateTime"));break;//finished
					case "CustomerNumber":data.setCustomerNumber(rs.getString("CustomerNumber"));break;//finished
					case "T7"	: data.setT7(rs.getString("T7"));break;//finished
					case"T8":data.setT8(rs.getString("T8"));break;//finished
					case"T10":data.setT10(rs.getString("T10"));break;//finished
					case"T11":data.setT11(rs.getString("T11"));break;//finished
					case "T12"	: data.setT12(rs.getString("T12"));break;//finished
					case "T13"	: data.setT13(rs.getString("T13"));break;//finished
					case"T15":data.setT16(rs.getString("T15"));break;//finished
					case"T16":data.setT16(rs.getString("T16"));break;
					case "T17"	: data.setT17(rs.getString("T17"));break;//finished
					case"T19":data.setT19(rs.getString("T19"));break;//finished
					case"T20":data.setT20(rs.getString("T20"));break;//finished
					case"T21":data.setT21(rs.getString("T21"));break;//finished
					case"T22":data.setT22(rs.getString("T22"));break;//finished
					case"T23":data.setT23(rs.getString("T23"));break;//finished
					case"T24":data.setT24(rs.getString("T24"));break;//finished
					case"T25":data.setT25(rs.getString("T25"));break;//finished
					
					}
					
				}
				if(DetailArray[i].getPaydatatype().equalsIgnoreCase("number") ||(!(DetailArray[i].getPaydispayfield().equals("")))){
					switch(DetailArray[i].getPaydispayfield()){
					case "N1"	: data.setN1(rs.getInt("N1"));break;//finished
					case "N7"	: data.setN7(rs.getInt("N7"));break;//finished
					case "N8"	: data.setN8(rs.getInt("N8"));break;//finished
					case "N9"	: data.setN9(rs.getInt("N9"));break;//finished
					case "N17"	: data.setN17(rs.getInt("N17"));break;//finished
					case "N18"	: data.setN18(rs.getInt("N18"));break;//finished
					case "N19"	: data.setN19(rs.getInt("N19"));break;//finished
					case "N20"	: data.setN20(rs.getInt("N20"));break;//finished
					//case "T18"	: data.setT18(Double.parseDouble(rs.getString("T18")));break;
					}
				}
				
			}
			data.setMessageCode(rs.getString("messagecode"));
			data.setMessageDesc(rs.getString("messagedescription"));
			datalist.add(data);

		}
		res.setDetailData(DetailArray);
		res.setPageSize(pageSize);
		res.setCurrentPage(currentPage);

		String countSQL = "";
		if(proDisCode.equalsIgnoreCase("01")){
			countSQL = "select COUNT(*) as recCount from  ( SELECT d.*, d.CustomerCode AS DID  ,dl.DistributorName, dl.DistributorAddress "
					+ "from DispaymentTransaction d ,DistributorList dl where d.CustomerCode=dl.DistributorID ";
			if(!(aMerchantID.equalsIgnoreCase("ALL"))){
				countSQL += " and d.t20 = '" + aMerchantID + "' ";
			}
			// filter customer
			l_Query += " and d.t24 = '" + aCustomerID + "' ";
			countSQL += " And d.n3=4   " + aCriteria + " )As r  ";
		}else {
			countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d   , CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 ";
            if(!(aMerchantID.equalsIgnoreCase("ALL"))){
                countSQL += " and d.t20 = '" + aMerchantID + "' ";
            }
            // filter customer
            	countSQL += " and d.t24 = '" + aCustomerID + "' ";
                countSQL += " And d.n3=4  "+aCriteria+" ) As r ";
		}
	
		PreparedStatement stat = conn.prepareStatement(countSQL);
		System.out.println("Count.." + countSQL);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);

		return res;
	}
	
	public PayTemplateHeader getPaymentTemplateByID(String merchantID,String saveDirectory, Connection l_Conn) throws SQLException {
		PayTemplateHeader ret = new PayTemplateHeader();
		int count = 0;
		GeneralUtil.readDataConfig();
		
		if(ServerGlobal.getFortuneMdyMerchant().equalsIgnoreCase(merchantID)){
			merchantID = ServerGlobal.getFortuneYgnMerchant();
		}
		//(4,5,6) or (2,3)
		String query = "select count(*) c from PayTemplateHeader H,PayTemplateDetail D where H.SysKey = D.HKey and FieldShow in (4,5,6) AND H.MerchantId = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(query);

		pstmt.setString(1, merchantID);
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			count = rs.getInt("c");
		}
		query = "select H.SysKey,H.MerchantID ,H.MerchantName,H.ProcessingCode,H.Logo,"
				+ "D.HKey,D.FieldID,D.FieldOrder,D.DataType,D.Caption,D.DisPayField,D.LovKey, D.RegExpKey ,D.IsMandatory, H.DetailOrder "
				+ "from PayTemplateHeader H inner join PayTemplateDetail D "
				+ "on H.SysKey = D.HKey and FieldShow in (4,5,6) AND H.MerchantID=? order by D.FieldOrder ";
		pstmt = l_Conn.prepareStatement(query);
		pstmt.setString(1, merchantID);
		rs = pstmt.executeQuery();

		PayTemplateDetail[] detailData = new PayTemplateDetail[count];
		int index = 0;

		while (rs.next()) {
			ret.setSysKey(rs.getLong("SysKey"));
			ret.setMerchantID(rs.getString("MerchantID"));
			ret.setName(rs.getString("MerchantName"));
			ret.setProcessingCode(rs.getString("ProcessingCode").substring(0,2));
			ret.setProNotiCode(rs.getString("ProcessingCode").substring(2,4));
			ret.setProDisCode(rs.getString("ProcessingCode").substring(4,6));
		//	ret.setLogo(FileUtil.getPhoto(saveDirectory,rs.getBytes("Logo"),l_Conn));
			ret.setDetailOrder(rs.getString("DetailOrder"));

			PayTemplateDetail detail = new PayTemplateDetail();
			detail.sethKey(rs.getLong("HKey")+"");
			detail.setFieldID(rs.getString("FieldID"));
			detail.setFieldOrder(rs.getInt("FieldOrder"));
			detail.setCaption(rs.getString("Caption"));
			detail.setDataType(rs.getString("DataType"));
			detail.setDisPayField(rs.getString("DisPayField"));
			String regExp = rs.getString("RegExpKey");
			detail.setRegExpKey(regExp.trim());
			detail.setIsMandatory(rs.getInt("IsMandatory"));
			
			long lovkey = rs.getLong("LovKey");
			detail.setLovKey(lovkey);
			if (lovkey != 0) {
				detail.setLovData(getLOVbyHKey(lovkey, ret.getProcessingCode(), l_Conn));
			}

			detailData[index] = detail;
			index++;
		}
		ret.setData(detailData);
		
		if(!ret.getDetailOrder().equals("")){
			// DetailOrder 1 == detail for Myanmar Beer .
			//get  Items Lov for lov of detail list of Myanmar Beer .
			String sql = "";
			int srno = 1;
			long headerSyskey = (long) 0.0;
			ArrayList<LOVSetupDetailData> datalist= new ArrayList<LOVSetupDetailData>();
			sql = "SELECT syskey from LOVHeader where Description = 'Items'";
			pstmt = l_Conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				headerSyskey = rs.getLong("syskey");
			}
			;
			sql = "SELECT Code, Description, Price from LOVDetails where HKey = ?";
			pstmt = l_Conn.prepareStatement(sql);
			pstmt.setLong(1, headerSyskey);
			
			rs = pstmt.executeQuery();
			while (rs.next()) {
				LOVSetupDetailData data = new LOVSetupDetailData();
				data.setSrno(srno++);
				data.setLovCde(rs.getString("Code"));
				data.setLovDesc1(rs.getString("Description"));
				data.setPrice(rs.getDouble("Price"));

				datalist.add(data);
			}
			LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
			for (int i = 0; i < datalist.size(); i++) {
				dataarry[i] = datalist.get(i);
			}
			ret.setDetailLov(dataarry);
		}
		return ret;
	}
	public LOVSetupDetailData[] getLOVbyHKey(long sysKey, String processingCode, Connection Conn) throws SQLException {

		int srno = 1;
		ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();

		String query = "";
		PreparedStatement pstmt = null;
		;
		query = "SELECT Code, Description from LOVDetails where HKey = ?";
		pstmt = Conn.prepareStatement(query);
		pstmt.setLong(1, sysKey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			LOVSetupDetailData data = new LOVSetupDetailData();
			data.setSrno(srno++);
			data.setLovCde(rs.getString("Code"));
			data.setLovDesc1(rs.getString("Description"));

			if (processingCode.equalsIgnoreCase("02")) {
				data.setLovDesc2(getMerchantNameById(data.getLovCde(), Conn));
			}
			datalist.add(data);
		}
		LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		return dataarry;

	}
	public String getMerchantNameById(String merchantId, Connection pConn) throws SQLException {
		String merName = "";
		PreparedStatement pstmt = null;
		;
		String query = "SELECT userName from CMSMerchant  where userId = ?";
		pstmt = pConn.prepareStatement(query);
		pstmt.setString(1, merchantId);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			merName = rs.getString("userName");
		}
		return merName;
	}
	public String getLovDescriptionByLovKeyandLovCode(Long lovKey, String lovCode , Connection pConn){
		
		String lovDesc= "";
		PreparedStatement pstmt = null;
		;
		String query = "select d.description from lovheader h , lovdetails d where h.syskey = d.hkey and h.syskey = ? and d.Code = ? ";
		try {
			pstmt = pConn.prepareStatement(query);
			pstmt.setLong(1, lovKey);
			pstmt.setString(2, lovCode);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				lovDesc = rs.getString("description");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return  lovDesc;
	}
	
	public ArrayList<DetailsData> getshowdetails(String syskey, Connection pConn)
			throws SQLException {

		ArrayList<DetailsData> ret = new ArrayList<DetailsData>();
		String l_Query = "select  * from DisPaymentTransDetail where hkey = ?";

		PreparedStatement pstmt = pConn.prepareStatement(l_Query);

		pstmt.setString(1, syskey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			DetailsData data= new DetailsData();
			data.setSysKey(rs.getLong("Syskey"));
			data.setHkey(rs.getLong("Hkey"));
			data.setSrno(rs.getInt("SrNo"));
			data.setCode(rs.getString("Code"));
			data.setDescription(rs.getString("Description"));
			data.setPrice(rs.getDouble("Price"));
			data.setQuantity(rs.getInt("Qty"));
			data.setAmount(rs.getDouble("Amount"));
			ret.add(data);
		}
		return ret;
	}

	// CNP Transaction Report //
	/*public DispaymentList getCNPTransactionList(int currentPage, int totalCount, int pageSize, String aFromDate, String aToDate,String accType, Connection conn) throws SQLException {
        DispaymentList ret = new DispaymentList();
        String l_Query = "" , query = "" , cur_gt_query = "" , sav_gt_query = "" , aCriteria="";
        String GT = "";
        Double amount = 0.00 , curtotal = 0.00 , savTotal = 0.00;
        ArrayList<CNPTransData> datalist = new ArrayList<CNPTransData>();
        int l_startRecord = (currentPage - 1) * pageSize;
        int l_endRecord = l_startRecord + pageSize;  
        DecimalFormat df = new DecimalFormat("###,000.00");
        
		if (accType.equalsIgnoreCase("CA") || accType.equalsIgnoreCase("SA") ) {
			aCriteria += " and AccType = '" + accType.trim() + "'"; 			
		}else{
			aCriteria += " "; 
		}

		l_Query = "SELECT sum(Amount) As 'TotalAmt',Count(*) 'Count' FROM CNPTransaction Where 1=1  " + aCriteria + " And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "' ";
          
        PreparedStatement stmtTotal = conn.prepareStatement(l_Query);
        ResultSet rsTotal = stmtTotal.executeQuery();
        while (rsTotal.next()) {
            amount = rsTotal.getDouble("TotalAmt");
            ret.setTotalCount(rsTotal.getInt("Count"));
        }
        
        if (amount != 0.00 ) {            
            String formatted = df.format(amount);
            ret.setGrandTotal(formatted);
            
            if(accType.equalsIgnoreCase("CA")){
            	ret.setCurtotal(formatted);
            	ret.setSavTotal("0.00");
            }else if ( accType.equalsIgnoreCase("SA") ){
            	ret.setSavTotal(formatted);
            	ret.setCurtotal("0.00");
            }else{
                
                cur_gt_query = "SELECT sum(Amount) As 'CurTotalAmt' FROM CNPTransaction Where 1=1 And AccType = 'CA' And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "' ";
                        
                PreparedStatement pscur = conn.prepareStatement(cur_gt_query);
                ResultSet rscur = pscur.executeQuery();
                while (rscur.next()) {
                	curtotal = rscur.getDouble("CurTotalAmt");
                	String curtotformat = df.format(curtotal);
                    ret.setCurtotal(curtotformat);
                }
                    
               sav_gt_query = "SELECT sum(Amount) As 'SavTotalAmt' FROM CNPTransaction Where 1=1 And AccType = 'SA'  And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "' ";            
                    PreparedStatement pssav = conn.prepareStatement(sav_gt_query);
                    ResultSet rssav = pssav.executeQuery();
                    while (rssav.next()) {
                    	savTotal = rssav.getDouble("SavTotalAmt");
                    	  String savtotformated = df.format(savTotal);
                          ret.setSavTotal(savtotformated);
                    }
            }
        }
        
        
        
        query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey DESC) AS RowNum, "
                + " drAcc,crAcc,amount,commission,flexcubeId,xref,merchantId,AccType from CNPTransaction Where 1=1 " + aCriteria + " And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "') AS RowConstrainedResult "
                + " WHERE ( RowNum > " + l_startRecord + " AND RowNum <= " + l_endRecord+")";

        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            CNPTransData data = new CNPTransData();
            data.setDrAcc(rs.getString("drAcc"));
            data.setCrAcc(rs.getString("crAcc"));
            data.setAmount(rs.getDouble("Amount"));
            data.setFlexcubeId(rs.getString("flexcubeId"));
            data.setXref(rs.getString("xref"));
            data.setMerchantId(rs.getString("merchantId"));
            data.setAccType(rs.getString("accType"));
            datalist.add(data);
        }
        
        CNPTransData[] dataarray = new CNPTransData[datalist.size()];
        for(int i=0;i<datalist.size();i++){
            dataarray[i]=datalist.get(i);
        }
        
        ret.setCnpdata(dataarray);
        ret.setPageSize(pageSize);
        ret.setCurrentPage(currentPage);
        
        rs.close();
        ps.close();
        return ret;
    }*/
	

	public String getAllCustRefNo(String aFromDate, String aToDate , String batchNo,Connection conn) throws SQLException {

		String custRef = "";
		ArrayList<CNPTransData> datalist = new ArrayList<CNPTransData>();

		String sql = "Select CustRefNo From CNPMerchant Where T3 = '" +batchNo+ "' And TransDate <= '" + aToDate.trim() + "' And TransDate >= '" + aFromDate.trim() + "' ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			CNPTransData data = new CNPTransData();
			data.setCustRefNo(res.getString("CustRefNo"));
			datalist.add(data);
		}
		
		for(int i=0;i<datalist.size();i++){
			if(i==datalist.size()-1){
				custRef+=""+datalist.get(i).getCustRefNo()+"";
				
			}else{
				custRef +=""+datalist.get(i).getCustRefNo()+""+" , ";
			}
		};
		
		return custRef;
	}
	
	public DispaymentList getCNPTransactionList(int currentPage, int totalCount, int pageSize, String aFromDate, String aToDate,String accType, Connection conn) throws SQLException {
        DispaymentList ret = new DispaymentList();
        String l_Query = "" , query = "" , cur_gt_query = "" , sav_gt_query = "" , aCriteria="";
        String GT = "";
        Double amount = 0.00 , curtotal = 0.00 , savTotal = 0.00;
        ArrayList<CNPTransData> datalist = new ArrayList<CNPTransData>();
        DispaymentTransactionDao dao = new DispaymentTransactionDao();
        int l_startRecord = (currentPage - 1) * pageSize;
        int l_endRecord = l_startRecord + pageSize;  
        DecimalFormat df = new DecimalFormat("###,000.00");
        
		if (accType.equalsIgnoreCase("CA") || accType.equalsIgnoreCase("SA") ) {
			aCriteria += " and AccType = '" + accType.trim() + "'"; 			
		}else{
			aCriteria += " "; 
		}

		l_Query = "SELECT sum(Amount) As 'TotalAmt',Count(*) 'Count' FROM CNPTransaction Where 1=1  " + aCriteria + " And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "' ";
          
        PreparedStatement stmtTotal = conn.prepareStatement(l_Query);
        ResultSet rsTotal = stmtTotal.executeQuery();
        while (rsTotal.next()) {
            amount = rsTotal.getDouble("TotalAmt");
            ret.setTotalCount(rsTotal.getInt("Count"));
        }
        
        if (amount != 0.00 ) {            
            String formatted = df.format(amount);
            ret.setGrandTotal(formatted);
            
            if(accType.equalsIgnoreCase("CA")){
            	ret.setCurtotal(formatted);
            	ret.setSavTotal("0.00");
            }else if ( accType.equalsIgnoreCase("SA") ){
            	ret.setSavTotal(formatted);
            	ret.setCurtotal("0.00");
            }else{
                
                cur_gt_query = "SELECT sum(Amount) As 'CurTotalAmt' FROM CNPTransaction Where 1=1 And AccType = 'CA' And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "' ";
                        
                PreparedStatement pscur = conn.prepareStatement(cur_gt_query);
                ResultSet rscur = pscur.executeQuery();
                while (rscur.next()) {
                	curtotal = rscur.getDouble("CurTotalAmt");
                	String curtotformat = df.format(curtotal);
                    ret.setCurtotal(curtotformat);
                }
                    
               sav_gt_query = "SELECT sum(Amount) As 'SavTotalAmt' FROM CNPTransaction Where 1=1 And AccType = 'SA'  And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "' ";            
                    PreparedStatement pssav = conn.prepareStatement(sav_gt_query);
                    ResultSet rssav = pssav.executeQuery();
                    while (rssav.next()) {
                    	savTotal = rssav.getDouble("SavTotalAmt");
                    	  String savtotformated = df.format(savTotal);
                          ret.setSavTotal(savtotformated);
                    }
            }
        }
        
        
        
        query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey DESC) AS RowNum, "
                + " drAcc,crAcc,amount,commission,flexcubeId,xref,merchantId,AccType,t1 from CNPTransaction Where 1=1 " + aCriteria + " And CreatedDate <= '" + aToDate.trim() + "' And CreatedDate >= '" + aFromDate.trim() + "') AS RowConstrainedResult "
                + " WHERE ( RowNum > " + l_startRecord + " AND RowNum <= " + l_endRecord+")";

        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            CNPTransData data = new CNPTransData();
            data.setDrAcc(rs.getString("drAcc"));
            data.setCrAcc(rs.getString("crAcc"));
            data.setAmount(rs.getDouble("Amount"));
            data.setFlexcubeId(rs.getString("flexcubeId"));
            data.setXref(rs.getString("xref"));
            data.setBatchNo(rs.getString("t1"));
            data.setMerchantId(rs.getString("merchantId"));
            data.setAccType(rs.getString("accType"));
            String custrefno = dao.getAllCustRefNo(aFromDate, aToDate, data.getBatchNo(), conn);
            data.setCustRefNo(custrefno);
            datalist.add(data);
        }
        
        CNPTransData[] dataarray = new CNPTransData[datalist.size()];
        for(int i=0;i<datalist.size();i++){
            dataarray[i]=datalist.get(i);
        }
        
        ret.setCnpdata(dataarray);
        ret.setPageSize(pageSize);
        ret.setCurrentPage(currentPage);
        
        rs.close();
        ps.close();
        return ret;
    }
}
