package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.data.BrandData;
import com.nirvasoft.rp.data.BrandDataSet;
import com.nirvasoft.rp.data.CompanyData;
import com.nirvasoft.rp.data.CompanyDataSet;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Result;


public class CompanyDao {
	
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("PYM007");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("parentId", (byte) 2));
		ret.getFields().add(new DBField("userSyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));		
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));		
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));		
		ret.getFields().add(new DBField("t14", (byte) 5));
		ret.getFields().add(new DBField("t15", (byte) 5));
		ret.getFields().add(new DBField("t16", (byte) 5));
		ret.getFields().add(new DBField("t17", (byte) 5));
		ret.getFields().add(new DBField("t18", (byte) 5));
		ret.getFields().add(new DBField("t19", (byte) 5));		
		ret.getFields().add(new DBField("t20", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("t22", (byte) 5));
		ret.getFields().add(new DBField("t23", (byte) 5));
		ret.getFields().add(new DBField("t24", (byte) 5));		
		ret.getFields().add(new DBField("t25", (byte) 5));
		ret.getFields().add(new DBField("O1", (byte) 5));
		
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 3));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		ret.getFields().add(new DBField("n14", (byte) 2));
		ret.getFields().add(new DBField("n15", (byte) 3));
		ret.getFields().add(new DBField("n16", (byte) 2));
		ret.getFields().add(new DBField("n17", (byte) 2));
		ret.getFields().add(new DBField("n18", (byte) 2));
		ret.getFields().add(new DBField("n19", (byte) 2));

		return ret;
	}

	public static CompanyData getDBRecord(DBRecord adbr) {
		CompanyData ret = new CompanyData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setSyncBatch(adbr.getLong("parentId"));
		ret.setSyncBatch(adbr.getLong("userSyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));		
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setT14(adbr.getString("t14"));
		ret.setT15(adbr.getString("t15"));
		ret.setT16(adbr.getString("t16"));
		ret.setT17(adbr.getString("t17"));
		ret.setT18(adbr.getString("t18"));
		ret.setT19(adbr.getString("t19"));
		ret.setT20(adbr.getString("t20"));		
		ret.setT21(adbr.getString("t21"));
		ret.setT22(adbr.getString("t22"));
		ret.setT23(adbr.getString("t23"));
		ret.setT24(adbr.getString("t24"));
		ret.setT25(adbr.getString("t25"));
		ret.setO1(adbr.getString("O1"));
		
		ret.setN1(adbr.getInt("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getInt("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		ret.setN14(adbr.getLong("n14"));
		ret.setN15(adbr.getInt("n15"));
		ret.setN16(adbr.getLong("n16"));
		ret.setN17(adbr.getLong("n17"));
		ret.setN18(adbr.getLong("n18"));
		ret.setN19(adbr.getLong("n19"));
		return ret;
	}

	public static DBRecord setDBRecord(CompanyData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getRecordStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("parentId", data.getSyncBatch());
		ret.setValue("userSyskey", data.getSyncBatch());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("t14", data.getT14());
		ret.setValue("t15", data.getT15());
		ret.setValue("t16", data.getT16());
		ret.setValue("t17", data.getT17());
		ret.setValue("t18", data.getT18());
		ret.setValue("t19", data.getT19());
		ret.setValue("t20", data.getT20());
		ret.setValue("t21", data.getT21());
		ret.setValue("t22", data.getT22());
		ret.setValue("t23", data.getT23());
		ret.setValue("t24", data.getT24());
		ret.setValue("t25", data.getT25());
		ret.setValue("O1", data.getO1());
		
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		ret.setValue("n14", data.getN14());
		ret.setValue("n15", data.getN15());
		ret.setValue("n16", data.getN16());
		ret.setValue("n17", data.getN17());
		ret.setValue("n18", data.getN18());
		ret.setValue("n19", data.getN19());
		
		
		return ret;
	}

	public static CompanyData read(long syskey, Connection conn) throws SQLException {
		CompanyData ret = new CompanyData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0) {
			String ddate = dbrs.get(0).getString("t5");
			dbrs.get(0).setValue("t5", ddate);
			ret = getDBRecord(dbrs.get(0));
		}
		return ret;
	}

	public static boolean isCodeExist(CompanyData obj, Connection conn) throws SQLException {
		String sql = " SELECT COUNT (*) AS retCount FROM PYM007 WHERE RecordStatus <> 4 AND syskey<>? AND T1 = ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setLong(1, obj.getSyskey());
		stat.setString(2, obj.getT1());
		ResultSet result = stat.executeQuery();
		result.next();
		int count = result.getInt("retCount");
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static Result insert(CompanyData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
			}
		}
		return res;
	}

	public static Result update(CompanyData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
			}
		}
		return res;
	}

	public static Result delete(String syskey, MrBean user, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = " UPDATE PYM007 SET RecordStatus=4,userid=?,username=? WHERE Syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, user.getUser().getUserId());
		stmt.setString(2, user.getUser().getUserName());
		stmt.setString(3, syskey);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
		}
		return res;
	}

	public static CompanyDataSet getCompanyList(Connection conn) throws SQLException {
		CompanyDataSet res = new CompanyDataSet();
		ArrayList<CompanyData> datalist = new ArrayList<CompanyData>();
		String whereClause = " WHERE RecordStatus<>4 ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause, " ORDER BY syskey", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		CompanyData[] dataarry = new CompanyData[datalist.size()];
		
		for(int i=0;i<datalist.size();i++){
			dataarry[i] = datalist.get(i);
		}
		
		res.setData(dataarry);		
		
		return res;
	}


}
