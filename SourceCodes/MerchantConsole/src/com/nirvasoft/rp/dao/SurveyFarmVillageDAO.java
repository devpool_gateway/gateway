package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.nirvasoft.rp.data.SurveyFarmVillageData;

public class SurveyFarmVillageDAO {
	
	public SurveyFarmVillageData getSurveyFarmVillageData(Connection pConn){
		SurveyFarmVillageData ret = new SurveyFarmVillageData();
		try {
			String l_Query = "select * from SurveyFarmVillage";
			PreparedStatement pstmt = pConn.prepareStatement(l_Query);
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public boolean saveSurveyFarmVillageData (SurveyFarmVillageData pData, Connection pConn){
		boolean ret = false;
		try {
			String l_Query = "insert into SurveyFarmVillage (RefKey,RefSVYType,Hub,Division,Township,Branch,BranchNo,VillageName,GPSCoordinate,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = pConn.prepareStatement(l_Query);
			
			pstmt.setString(1, pData.getRefkey());
			pstmt.setString(2, pData.getRefsvytype());
			pstmt.setString(3, pData.getHub());
			pstmt.setString(4, pData.getDivision());
			pstmt.setString(5, pData.getTownship());
			pstmt.setString(6, pData.getBranch());
			pstmt.setString(7, pData.getBranchno());
			pstmt.setString(8, pData.getVillagename());
			pstmt.setString(9, pData.getGpscoordinate());
			pstmt.setString(10, pData.getT1());
			pstmt.setString(11, pData.getT2());
			pstmt.setString(12, pData.getT3());
			pstmt.setString(13, pData.getT4());
			pstmt.setString(14, pData.getT5());
			pstmt.setString(15, pData.getT6());
			pstmt.setString(16, pData.getT7());
			pstmt.setString(17, pData.getT8());
			pstmt.setString(18, pData.getT9());
			pstmt.setString(19, pData.getT10());
			pstmt.setString(20, pData.getT11());
			
			if(pstmt.executeUpdate() > 1){
				ret = true;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
