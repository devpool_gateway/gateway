package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Bean001;
import com.nirvasoft.rp.framework.Profile;

public class CommonDao {
		
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		Connection conn=null;		
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		conn=DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/CleanRp","sa","123");		
		return conn;
	}
	
	public static Profile signin(Profile p) throws SQLException, ClassNotFoundException{
		Profile pp = new Profile();		
		Connection conn = getConnection();		
			String sql = " SELECT COUNT (*) AS ccccc FROM UVM005 WHERE  T1 = ? AND T2 = ?";
			PreparedStatement stat = conn.prepareStatement(sql);
			stat.setString(1, p.getUserID());
			stat.setString(2, p.getPassword());
			ResultSet result = stat.executeQuery();
			result.next();
			int count = result.getInt("ccccc");		
			if( count > 0 ){				
				pp.setPassword("*");
			
			}	else{
				pp.setRole(0); 
			}
		return pp;
	}

	public static Profile save(Profile p) throws SQLException, ClassNotFoundException {
		Profile b = new Profile();
		Connection conn = getConnection();	
		
		String sql = "INSERT INTO UVM005 VALUES(?,'','','','',1,0,0,?,?,'','','','','',0,0,0,0,0,0,0,0,0)";		
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setLong(1, Long.parseLong(getAutoGenSyskey(conn)));
		stat.setString(2, p.getT1());
		stat.setString(3, p.getT2());		
		int count = stat.executeUpdate();
		
		if(count >0){
			b.setT1(p.getT1());
		}
		
		return b;
		
	}
	
	public static ArrayList<Profile> list() throws SQLException, ClassNotFoundException {
		ArrayList<Profile> b = new ArrayList<Profile>();
		Connection conn = getConnection();	
		
		String sql = "SELECT * FROM UVM005 WHERE RecordStatus<>0";		
		PreparedStatement stat = conn.prepareStatement(sql);
			
		ResultSet result = stat.executeQuery();
		while(result.next()){
			Profile p = new Profile();
			p.setT1(result.getString("T1"));
			p.setT2(result.getString("T2"));
			b.add(p);			
		}		
		return b;
		
	}
	public static String getAutoGenSyskey(Connection conn) throws SQLException {
		String ret;
		String sql = "SELECT ISNULL(MAX(syskey),0) AS ret FROM UVM005";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		int result = rs.getInt("ret") + 1;
		DecimalFormat df = new DecimalFormat("00000");
		ret =df.format(result).toString();

		return ret;
	

	}
	

}
