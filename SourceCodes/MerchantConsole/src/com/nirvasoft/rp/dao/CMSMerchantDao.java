package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.MerchantIDListData;
import com.nirvasoft.rp.shared.MerchantListDataArr;
import com.nirvasoft.rp.shared.LoginUserMerchantInfo;

public class CMSMerchantDao {

	public static CMSMerchantData read(String aMerchantID, Connection conn) throws SQLException {

		CMSMerchantData l_data = new CMSMerchantData();

		PreparedStatement stmt = conn
				.prepareStatement("select * from CMSMerchant where recordstatus<>4 and userid = ?");
		stmt.setString(1, aMerchantID);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {

			l_data.setUserId(rs.getString("userid"));
			l_data.setUserName(rs.getString("username"));
			l_data.setN1(rs.getInt("n1"));
			l_data.setN2(rs.getInt("n2"));
			l_data.setN3(rs.getInt("n3"));
			l_data.setSamecity(rs.getDouble("sameCity"));
			l_data.setDiffcity(rs.getDouble("diffCity"));
			l_data.setRate(rs.getDouble("rate"));
			l_data.setMinimumamount(rs.getDouble("minimumAmount"));
			l_data.setEmailtype(rs.getInt("emailType"));
			l_data.setN4(rs.getInt("n4"));
			l_data.setN5(rs.getInt("n5"));
			l_data.setT13(rs.getString("T13"));
			l_data.setT14(rs.getString("T14"));
			l_data.setT15(rs.getString("T15"));
			l_data.setT16(rs.getString("T16"));
			l_data.setT17(rs.getString("T17"));
			l_data.setT18(rs.getString("T18"));
			l_data.setT19(rs.getString("T19"));

		}
		stmt.close();
		rs.close();
		return l_data;
	}

	public MerchantListDataArr getMerchantList(Connection conn) throws SQLException {
		
		MerchantListDataArr lov3 = new MerchantListDataArr();
		MerchantIDListData[] refarray = null;
		ArrayList<MerchantIDListData> reflist = new ArrayList<MerchantIDListData>();
        
        PreparedStatement stmt = conn.prepareStatement("select distinct j.MerchantID,m.UserName as MerchantName"
        		+ " from MUJunction j, CMSMerchant m "
        		+ "where j.MerchantID = m.UserId order by j.MerchantID");
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
        	MerchantIDListData ref = new MerchantIDListData();
        	ref.setId(rs.getString("MerchantID"));
        	ref.setName(rs.getString("MerchantName"));
        	reflist.add(ref);
        }
        
        stmt.close();
        rs.close();
        
        refarray = new MerchantIDListData[reflist.size()];
        for(int i=0 ; i<reflist.size(); i++){
        	refarray[i] = reflist.get(i);
        }
        
        lov3.setDatalist(refarray);
        System.out.println("MUJunction LIst"+ refarray.length);
       
        return lov3;
	}
	
	public Lov3 getInitiatedBy(Connection conn,String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String query ="select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description like '%Initiated By%')";
		PreparedStatement stmt = conn.prepareStatement(query);	
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setRef012(refarray);

		return lov3;
	}
	
	public Lov3 getStatus(Connection conn,String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String query ="select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description like 'Status%')";
		PreparedStatement stmt = conn.prepareStatement(query);	
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setRef013(refarray);

		return lov3;
	}
	
	public Lov3 getMerchantIDListDetail(Connection conn,String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		System.out.println("user is; "+User);
		//PreparedStatement stmt = conn
		//.prepareStatement("Select c.UserId,c.UserName from UVM005_W A join MUJunction j On  j.UserSysKey = A.syskey Join CMSMerchant  c On j.MerchantID = c.UserId where 1=1 and A.t1 = ? and A.RecordStatus = 1 order by c.UserName ");
		String query ="select p.MerchantID,p.MerchantName,p.ProcessingCode from CMSMerchant c, PayTemplateHeader p where c.UserId = p.MerchantID and c.RecordStatus <> 4 "
					+ " And p.MerchantId In (Select t5 From UVM005_A Where t1 = '"+ User + "') order by p.MerchantName";
		PreparedStatement stmt = conn.prepareStatement(query);		
		//stmt.setString(1, User);
		System.out.println("Query Testing " + stmt);
		System.out.println("User...  " + User);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("MerchantID"));
			ref.setcaption(rs.getString("MerchantName"));
			ref.setProcessingCode(rs.getString("ProcessingCode"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
        System.out.println("Dao getmerchant"+ refarray.length);
		lov3.setRef015(refarray);
		System.out.println("Dao getmerchant lov"+ lov3.getRef015().length);

		return lov3;
	}

	/*public Lov3 getMerchantIDListDetail(Connection conn,String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		System.out.println("user is; "+User);
		//PreparedStatement stmt = conn
		//.prepareStatement("Select c.UserId,c.UserName from UVM005_W A join MUJunction j On  j.UserSysKey = A.syskey Join CMSMerchant  c On j.MerchantID = c.UserId where 1=1 and A.t1 = ? and A.RecordStatus = 1 order by c.UserName ");
		PreparedStatement stmt = conn.prepareStatement("select p.MerchantID,p.MerchantName,p.ProcessingCode from CMSMerchant c, PayTemplateHeader p where c.UserId = p.MerchantID and c.RecordStatus <> 4 order by p.MerchantName");		
		//stmt.setString(1, User);
		System.out.println("Query Testing " + stmt);
		System.out.println("User...  " + User);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("MerchantID"));
			ref.setcaption(rs.getString("MerchantName"));
			ref.setvalue(rs.getString("userid"));
			ref.setcaption(rs.getString("username"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
        System.out.println("Dao getmerchant"+ refarray.length);
		lov3.setRef015(refarray);
		System.out.println("Dao getmerchant lov"+ lov3.getRef015().length);

		return lov3;
	}*/
	
	public Lov3 getMerchantIDList(Connection conn) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();

		PreparedStatement stmt = conn
				.prepareStatement("select UserId, UserName from CMSMerchant where recordstatus<>4 ");
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("userid"));
			ref.setcaption(rs.getString("username"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}

		lov3.setRef011(refarray);

		return lov3;
	}

	public LoginUserMerchantInfo LoginUserMerchantInfo(String loginUser, Connection conn) throws SQLException {
		LoginUserMerchantInfo data = new LoginUserMerchantInfo();
		//String query = "select u.t1 as loginUsername,uj.merchantId from V_U001_W u ,mujunction uj where u.syskey = uj.UserSysKey and u.t1 = ?";
		//String query = "select u.t1 as loginUsername,cms.userid from V_U001 u ,CMSMerchant cms where u.t1 = ? and u.t1=cms.userid";
		String query = "select * from CMSMerchant";
		System.out.println("loginUser is: "+loginUser);
		PreparedStatement pstmt = conn.prepareStatement(query);
		//pstmt.setString(1, loginUser);
		ResultSet res = pstmt.executeQuery();
		if (res.next()) {
			/*data.setLoginUser(res.getString("loginUsername"));
			data.setMerchantId(res.getString("merchantId"));*/
			data.setLoginUser(loginUser);
			data.setMerchantId(res.getString("userid"));
		}
		return data;
	}

}
