package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PwdData {
private long syskey;	
private String password;
private String newpassword;
private String confirmnewpassword;



public PwdData() {
	clearProperties();
	// TODO Auto-generated constructor stub
}
private void clearProperties() {
	// TODO Auto-generated method stub
	syskey=0;
	password="";
	newpassword="";
	confirmnewpassword="";
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getNewpassword() {
	return newpassword;
}
public void setNewpassword(String newpassword) {
	this.newpassword = newpassword;
}
public String getConfirmnewpassword() {
	return confirmnewpassword;
}
public void setConfirmnewpassword(String confirmnewpassword) {
	this.confirmnewpassword = confirmnewpassword;
}
public long getSyskey() {
	return syskey;
}
public void setSyskey(long syskey) {
	this.syskey = syskey;
}


}
