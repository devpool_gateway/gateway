package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PayTemplateDetail {

	private long sysKey;
	private String hKey;
	private String fieldID;
	private int fieldOrder;
	private String caption;
	private String dataType;
	private String disPayField;
	private String regExpKey;
	private Long lovKey;
	private int fieldShow;
	private int IsMandatory;
	private String fieldvalue;
	private LOVSetupDetailData[] lovData;

	public PayTemplateDetail() {
		clearProperty();
	}

	private void clearProperty() {
		sysKey = 0;
		hKey = "";
		fieldID = "";
		fieldOrder = 0;
		caption = "";
		dataType = "";
		disPayField = "";
		regExpKey = "";
		lovKey = (long) 0.0;
		fieldShow = 0;
		IsMandatory = 0;
		fieldvalue = "";
		lovData = null;

	}


	public long getSysKey() {
		return sysKey;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public LOVSetupDetailData[] getLovData() {
		return lovData;
	}

	public void setLovData(LOVSetupDetailData[] lovData) {
		this.lovData = lovData;
	}

	public String getFieldID() {
		return fieldID;
	}

	public void setFieldID(String fieldID) {
		this.fieldID = fieldID;
	}

	public int getFieldOrder() {
		return fieldOrder;
	}

	public void setFieldOrder(int fieldOrder) {
		this.fieldOrder = fieldOrder;
	}

	public String getDisPayField() {
		return disPayField;
	}

	public void setDisPayField(String disPayField) {
		this.disPayField = disPayField;
	}

	public String getRegExpKey() {
		return regExpKey;
	}

	public void setRegExpKey(String regExpKey) {
		this.regExpKey = regExpKey;
	}

	public Long getLovKey() {
		return lovKey;
	}

	public void setLovKey(Long lovKey) {
		this.lovKey = lovKey;
	}

	public int getFieldShow() {
		return fieldShow;
	}

	public void setFieldShow(int fieldShow) {
		this.fieldShow = fieldShow;
	}

	public String getFieldvalue() {
		return fieldvalue;
	}

	public void setFieldvalue(String fieldvalue) {
		this.fieldvalue = fieldvalue;
	}

	public int getIsMandatory() {
		return IsMandatory;
	}

	public void setIsMandatory(int isMandatory) {
		IsMandatory = isMandatory;
	}

	public String gethKey() {
		return hKey;
	}

	public void sethKey(String hKey) {
		this.hKey = hKey;
	}
}
