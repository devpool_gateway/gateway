package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

	@XmlRootElement
	public class CNPTransData {
	    private String autoKey;
	    private String drAcc;
	    private String crAcc;
	    private String flexcubeId;
	    private String custRefNo;
	    private String batchNo;
	    private String xref;
	    private String accType;
	    private String merchantId;
	    private double amount;
	    private double commission;

	    private String aFromDate;
	    private String aToDate;
	    
	    private int totalCount;
	    private int currentPage;
	    private int pageSize;
	    private double grandTotal;
	    
	    public CNPTransData(){
	        clearProperty();
	    }
	    
	    void clearProperty(){
	        aFromDate = "";
	        aToDate = "";
	        accType = "";
	        drAcc = "";
	        crAcc = "";
	        flexcubeId = "";
	        xref = "";
	        merchantId = "";
	        grandTotal = 0.00;
	        amount = 0.00 ;
	        commission = 0.00;    
	        totalCount = 0;
	        currentPage = 0;
	        pageSize=0;
	        autoKey="";
	        batchNo = "";
	        custRefNo = "";
	    }
	    
	    public double getGrandTotal() {
	        return grandTotal;
	    }

	    public void setGrandTotal(double grandTotal) {
	        this.grandTotal = grandTotal;
	    }

	    public String getaFromDate() {
	        return aFromDate;
	    }

	    public void setaFromDate(String aFromDate) {
	        this.aFromDate = aFromDate;
	    }

	    public String getaToDate() {
	        return aToDate;
	    }

	    public void setaToDate(String aToDate) {
	        this.aToDate = aToDate;
	    }

	    public String getAccType() {
	        return accType;
	    }

	    public void setAccType(String accType) {
	        this.accType = accType;
	    }

	    public String getDrAcc() {
	        return drAcc;
	    }

	    public void setDrAcc(String drAcc) {
	        this.drAcc = drAcc;
	    }

	    public String getCrAcc() {
	        return crAcc;
	    }

	    public void setCrAcc(String crAcc) {
	        this.crAcc = crAcc;
	    }

	    public String getFlexcubeId() {
	        return flexcubeId;
	    }

	    public void setFlexcubeId(String flexcubeId) {
	        this.flexcubeId = flexcubeId;
	    }

	    public String getXref() {
	        return xref;
	    }

	    public void setXref(String xref) {
	        this.xref = xref;
	    }

	    public String getMerchantId() {
	        return merchantId;
	    }

	    public void setMerchantId(String merchantId) {
	        this.merchantId = merchantId;
	    }

	    public double getAmount() {
	        return amount;
	    }

	    public void setAmount(double amount) {
	        this.amount = amount;
	    }

	    public double getCommission() {
	        return commission;
	    }

	    public void setCommission(double commission) {
	        this.commission = commission;
	    }    
	    
	    public int getTotalCount() {
	        return totalCount;
	    }
	    public void setTotalCount(int totalCount) {
	        this.totalCount = totalCount;
	    }
	    public int getCurrentPage() {
	        return currentPage;
	    }
	    public void setCurrentPage(int currentPage) {
	        this.currentPage = currentPage;
	    }
	    public int getPageSize() {
	        return pageSize;
	    }
	    public void setPageSize(int pageSize) {
	        this.pageSize = pageSize;
	    }

		public String getAutoKey() {
			return autoKey;
		}

		public void setAutoKey(String autoKey) {
			this.autoKey = autoKey;
		}

		public String getCustRefNo() {
			return custRefNo;
		}

		public void setCustRefNo(String custRefNo) {
			this.custRefNo = custRefNo;
		}

		public String getBatchNo() {
			return batchNo;
		}

		public void setBatchNo(String batchNo) {
			this.batchNo = batchNo;
		}
		
		
	}


