package com.nirvasoft.rp.shared;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReconciliationData {
	
	private String aFromDate;
	private String aToDate;
	private String kbzTransID;
	private String transID;
	private String status;
	private String select;
	private String tStatus;
	
	private int totalCount;
	private int currentPage;
	private int pageSize;
	
	public ReconciliationData(){
		clearProperty();
	}
	
	public void clearProperty(){
		this.aFromDate="";
		this.aToDate="";
		this.kbzTransID="";
		this.transID="";
		this.status="";
		this.select="";
		this.tStatus="";
	}

	public String getaFromDate() {
		return aFromDate;
	}

	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}

	public String getaToDate() {
		return aToDate;
	}

	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}

	public String getKbzTransID() {
		return kbzTransID;
	}

	public void setKbzTransID(String kbzTransID) {
		this.kbzTransID = kbzTransID;
	}

	public String getTransID() {
		return transID;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String gettStatus() {
		return tStatus;
	}

	public void settStatus(String tStatus) {
		this.tStatus = tStatus;
	}
	
	

}
