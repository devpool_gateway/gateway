package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class CMSMerchantAccRefData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private long syskey;
	private String CreatedDate="";
	private String ModifiedDate="";
	private String CreatedUserID="";
	private String ModifiedUserID="";
	private String AccountNumber="";
	private String T1="";
	private String T2="";
	private String T3="";
	private String T4="";
	private String T5="";
	private String T6="";
	private String T7="";
	private String T8="";
	private String T9="";
	private String T10="";
	
	private int n1=0;
	private int n2=0;
	private int n3=0;
	private int n4=0;
	private int n5=0;
	private int n6=0;
	private int n7=0;
	private int n8=0;
	private int n9=0;
	private int n10=0;
	
	
	public String getT4() {
		return T4;
	}
	public void setT4(String t4) {
		T4 = t4;
	}
	public String getT5() {
		return T5;
	}
	public void setT5(String t5) {
		T5 = t5;
	}
	public String getT6() {
		return T6;
	}
	public void setT6(String t6) {
		T6 = t6;
	}
	public String getT7() {
		return T7;
	}
	public void setT7(String t7) {
		T7 = t7;
	}
	public String getT8() {
		return T8;
	}
	public void setT8(String t8) {
		T8 = t8;
	}
	public String getT9() {
		return T9;
	}
	public void setT9(String t9) {
		T9 = t9;
	}
	public String getT10() {
		return T10;
	}
	public void setT10(String t10) {
		T10 = t10;
	}
	public int getN4() {
		return n4;
	}
	public void setN4(int n4) {
		this.n4 = n4;
	}
	public int getN5() {
		return n5;
	}
	public void setN5(int n5) {
		this.n5 = n5;
	}
	public int getN6() {
		return n6;
	}
	public void setN6(int n6) {
		this.n6 = n6;
	}
	public int getN7() {
		return n7;
	}
	public void setN7(int n7) {
		this.n7 = n7;
	}
	public int getN8() {
		return n8;
	}
	public void setN8(int n8) {
		this.n8 = n8;
	}
	public int getN9() {
		return n9;
	}
	public void setN9(int n9) {
		this.n9 = n9;
	}
	public int getN10() {
		return n10;
	}
	public void setN10(int n10) {
		this.n10 = n10;
	}
	private String branchname="";
	private String branchCode="";
	private int recordstatus=0;
	public long getSyskey() {
		return syskey;
	}
	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getModifiedDate() {
		return ModifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		ModifiedDate = modifiedDate;
	}
	public String getCreatedUserID() {
		return CreatedUserID;
	}
	public void setCreatedUserID(String createdUserID) {
		CreatedUserID = createdUserID;
	}
	public String getModifiedUserID() {
		return ModifiedUserID;
	}
	public void setModifiedUserID(String modifiedUserID) {
		ModifiedUserID = modifiedUserID;
	}
	public String getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}
	public String getT1() {
		return T1;
	}
	public void setT1(String t1) {
		T1 = t1;
	}
	public String getT2() {
		return T2;
	}
	public void setT2(String t2) {
		T2 = t2;
	}
	public String getT3() {
		return T3;
	}
	public void setT3(String t3) {
		T3 = t3;
	}
	public int getN1() {
		return n1;
	}
	public void setN1(int n1) {
		this.n1 = n1;
	}
	public int getN2() {
		return n2;
	}
	public void setN2(int n2) {
		this.n2 = n2;
	}
	public int getN3() {
		return n3;
	}
	public void setN3(int n3) {
		this.n3 = n3;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public int getRecordstatus() {
		return recordstatus;
	}
	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}
	
}