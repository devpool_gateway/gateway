package com.nirvasoft.rp.shared;

import java.util.ArrayList;

public class DispaymentTransactionDataSet {
	private String m_searchText;
	private int m_totalCount;
	private int m_currentPage;
	private int m_pageSize;
	private boolean state;
	private DispaymentTransactionData[] data;
	private ArrayList<DispaymentTransactionData> branchDatalist;
	
	

	public String getSearchText() {

		return m_searchText;

	}

	public void setSearchText(String m_searchText) {
		this.m_searchText = m_searchText;
	}

	public int getTotalCount() {
		return m_totalCount;
	}

	public void setTotalCount(int m_totalCount) {
		this.m_totalCount = m_totalCount;
	}

	public int getCurrentPage() {
		return m_currentPage;
	}

	public void setCurrentPage(int m_currentPage) {
		this.m_currentPage = m_currentPage;
	}

	public int getPageSize() {
		return m_pageSize;
	}

	public boolean isState() {
		return state;
	}
	public void setState(boolean state) {
		this.state = state;
	}

	public void setPageSize(int m_pageSize) {
		this.m_pageSize = m_pageSize;
	}
	
	public ArrayList<DispaymentTransactionData> getBranchDatalist() {
		return branchDatalist;
	}

	public void setBranchDatalist(ArrayList<DispaymentTransactionData> branchDatalist) {
		this.branchDatalist = branchDatalist;
	}

	public DispaymentTransactionData[] getData() {
		return data;
	}

	public void setData(DispaymentTransactionData[] data) {
		this.data = data;
	}
}
