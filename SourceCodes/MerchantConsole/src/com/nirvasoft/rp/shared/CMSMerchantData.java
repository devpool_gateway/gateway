package com.nirvasoft.rp.shared;

import java.io.Serializable;
import java.util.ArrayList;


public class CMSMerchantData implements Serializable{
	private static final long serialVersionUID = 1L;
	private long sysKey;
	private String UserName = "";
    private String UserId = "";

    
    private String CreatedDate = "";
    private String ModifiedDate = "";
    private String ModifiedUserId="";
    private int recordStatus=0;
    
	private String T1 = "";
    private String T2 = "";
    private String T3 = "";
	private String T4 = "";
    private String T5= "";
    private String T6 = "";
    private String T7 = "";
	private String T8 = "";
    private String T9 = "";
    private String T10 = "";
    private String T11= "";
    private String T12= "";
	private String T13= "";
    private String T14= "";
    private String T15 = "";
	private int N1 = 0;
    private int N2= 0;
    private int N3 = 0;
    private int N4 = 0;
    private int N5 = 0;
    private int N6 = 0;
    private int N7 = 0;
    private int N8 = 0;
    private int N9 = 0;
    private int N10 = 0;
    private double samecity = 0;
    private double diffcity = 0;
    private double rate = 0;
    private double minimumamount = 0;
    private int emailtype = 0;
    
    ////////Suyi Adding/////////
    private String T16="";
    private String T17="";
    private String T18="";
    private String T19="";
    private String T20="";
    private String T21="";
    private String T22="";
    private String T23="";
    private String T24="";
    private String T25="";
    private int N11 = 0;
    private int N12= 0;
    private int N13 = 0;
    private int N14 = 0;
    private int N15 = 0;
    
    
    
    
    
    public int getN11() {
		return N11;
	}
	public void setN11(int n11) {
		N11 = n11;
	}
	public int getN12() {
		return N12;
	}
	public void setN12(int n12) {
		N12 = n12;
	}
	public int getN13() {
		return N13;
	}
	public void setN13(int n13) {
		N13 = n13;
	}
	public int getN14() {
		return N14;
	}
	public void setN14(int n14) {
		N14 = n14;
	}
	public int getN15() {
		return N15;
	}
	public void setN15(int n15) {
		N15 = n15;
	}
	public String getT16() {
		return T16;
	}
	public void setT16(String t16) {
		T16 = t16;
	}
	public String getT17() {
		return T17;
	}
	public void setT17(String t17) {
		T17 = t17;
	}
	public String getT18() {
		return T18;
	}
	public void setT18(String t18) {
		T18 = t18;
	}
	public String getT19() {
		return T19;
	}
	public void setT19(String t19) {
		T19 = t19;
	}
	public String getT20() {
		return T20;
	}
	public void setT20(String t20) {
		T20 = t20;
	}
	public String getT21() {
		return T21;
	}
	public void setT21(String t21) {
		T21 = t21;
	}
	public String getT22() {
		return T22;
	}
	public void setT22(String t22) {
		T22 = t22;
	}
	public String getT23() {
		return T23;
	}
	public void setT23(String t23) {
		T23 = t23;
	}
	public String getT24() {
		return T24;
	}
	public void setT24(String t24) {
		T24 = t24;
	}
	public String getT25() {
		return T25;
	}
	public void setT25(String t25) {
		T25 = t25;
	}
	public double getSamecity() {
		return samecity;
	}
	public void setSamecity(double samecity) {
		this.samecity = samecity;
	}
	public double getDiffcity() {
		return diffcity;
	}
	public void setDiffcity(double diffcity) {
		this.diffcity = diffcity;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getMinimumamount() {
		return minimumamount;
	}
	public void setMinimumamount(double minimumamount) {
		this.minimumamount = minimumamount;
	}
	public int getEmailtype() {
		return emailtype;
	}
	public void setEmailtype(int emailtype) {
		this.emailtype = emailtype;
	}
	public int getN2() {
		return N2;
	}
	public void setN2(int n2) {
		N2 = n2;
	}
	public int getN3() {
		return N3;
	}
	public void setN3(int n3) {
		N3 = n3;
	}
	public int getN4() {
		return N4;
	}
	public void setN4(int n4) {
		N4 = n4;
	}
	public int getN5() {
		return N5;
	}
	public void setN5(int n5) {
		N5 = n5;
	}
	public void setN1(int n1) {
		N1 = n1;
	}
	private ArrayList<CMSMerchantAccRefData> mMerchantAccData=new ArrayList<CMSMerchantAccRefData>();
    
    
	public String getModifiedUserId() {
		return ModifiedUserId;
	}
	public void setModifiedUserId(String modifiedUserId) {
		ModifiedUserId = modifiedUserId;
	}
	public ArrayList<CMSMerchantAccRefData> getmMerchantAccData() {
		return mMerchantAccData;
	}
	public void setmMerchantAccData(
			ArrayList<CMSMerchantAccRefData> mMerchantAccData) {
		this.mMerchantAccData = mMerchantAccData;
	}
	public long getSysKey() {
		return sysKey;
	}
	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getModifiedDate() {
		return ModifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		ModifiedDate = modifiedDate;
	}
	public int getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getT1() {
		return T1;
	}
	public void setT1(String t1) {
		T1 = t1;
	}
	public String getT2() {
		return T2;
	}
	public void setT2(String t2) {
		T2 = t2;
	}
	public String getT3() {
		return T3;
	}
	public void setT3(String t3) {
		T3 = t3;
	}
	public String getT4() {
		return T4;
	}
	public void setT4(String t4) {
		T4 = t4;
	}
	public String getT5() {
		return T5;
	}
	public void setT5(String t5) {
		T5 = t5;
	}
	public String getT6() {
		return T6;
	}
	public void setT6(String t6) {
		T6 = t6;
	}
	public String getT7() {
		return T7;
	}
	public void setT7(String t7) {
		T7 = t7;
	}
	public String getT8() {
		return T8;
	}
	public void setT8(String t8) {
		T8 = t8;
	}
	public String getT9() {
		return T9;
	}
	public void setT9(String t9) {
		T9 = t9;
	}
	public String getT10() {
		return T10;
	}
	public void setT10(String t10) {
		T10 = t10;
	}
	public String getT11() {
		return T11;
	}
	public void setT11(String t11) {
		T11 = t11;
	}
	public String getT12() {
		return T12;
	}
	public void setT12(String t12) {
		T12 = t12;
	}
	public String getT13() {
		return T13;
	}
	public void setT13(String t13) {
		T13 = t13;
	}
	public String getT14() {
		return T14;
	}
	public void setT14(String t14) {
		T14 = t14;
	}
	public String getT15() {
		return T15;
	}
	public void setT15(String t15) {
		T15 = t15;
	}
	public int getN1() {
		return N1;
	}
	
	public int getN6() {
		return N6;
	}
	public void setN6(int n6) {
		N6 = n6;
	}
	public int getN7() {
		return N7;
	}
	public void setN7(int n7) {
		N7 = n7;
	}
	public int getN8() {
		return N8;
	}
	public void setN8(int n8) {
		N8 = n8;
	}
	public int getN9() {
		return N9;
	}
	public void setN9(int n9) {
		N9 = n9;
	}
	public int getN10() {
		return N10;
	}
	public void setN10(int n10) {
		N10 = n10;
	}
    
   
}
