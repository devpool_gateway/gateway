package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DBBarData {
	
	String month;
	double value;
	
	public DBBarData(){
	clearProperty();	
	}
	public DBBarData(String pName, double value){
		this.month = pName;
		this.value = value;
	}
	private void clearProperty(){
		month= "";
		value = 0.00;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	
	
}
