package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DBBarDetail {
	
	String name;
	double[] data;
	
	public DBBarDetail(){
	clearProperty();	
	}
	public DBBarDetail(String pName, double[] pY){
		this.name = pName;
		this.data = pY;
	}
	private void clearProperty(){
		name= "";
		data = null;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double[] getData() {
		return data;
	}
	public void setData(double[] data) {
		this.data = data;
	}
	
	
}
