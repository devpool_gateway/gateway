package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantData {
	private String sessionID;
	private String select;
	private String aMerchantID;
	private String aFeature;
	private String aCustomerID;
	private String aFromDate;
	private String aToDate;
	private String debitaccount;
	private String collectionaccount;
	private String did;
	private String customername;
	private String description;
	private String initiatedby;
	private String transrefno;
	private String processingCode;
	private String proNotiCode;// for Notification
	private String proDisCode;// for Distributor use/ unuse
	
	private String status;
	private String msgstatus;
	
	private PayTemplateDetail[] templatedata;
	
	private int totalCount;
	private int currentPage;
	private int pageSize;
	
	public MerchantData(){
		clearProperty();
	}
	private void clearProperty(){
		sessionID = "";
		select = "";
		 aMerchantID="";
		 aFeature = "";
		 aCustomerID = "";
		 aFromDate="";
		 aToDate="";
		 debitaccount="";
		 collectionaccount="";
		 did="";
		 customername="";
		 description="";
		 initiatedby="";
		 transrefno="";
		 processingCode="";
		 proNotiCode="";// for Notification
		 proDisCode="";
		 templatedata = null;
		 totalCount = 0;
		 currentPage = 0;
		 pageSize=0;
		 status = "";
		 msgstatus = "";
	}
	
	
	
	 public String getaFeature() {
		return aFeature;
	}
	public void setaFeature(String aFeature) {
		this.aFeature = aFeature;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInitiatedby() {
	  return initiatedby;
	 }
	 public void setInitiatedby(String initiatedby) {
	  this.initiatedby = initiatedby;
	 }
	 public String getTransrefno() {
	  return transrefno;
	 }
	 public void setTransrefno(String transrefno) {
	  this.transrefno = transrefno;
	 }
	
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getaMerchantID() {
		return aMerchantID;
	}
	public void setaMerchantID(String aMerchantID) {
		this.aMerchantID = aMerchantID;
	}
	
	public String getaCustomerID() {
		return aCustomerID;
	}
	public void setaCustomerID(String aCustomerID) {
		this.aCustomerID = aCustomerID;
	}
	public String getaFromDate() {
		return aFromDate;
	}
	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}
	public String getaToDate() {
		return aToDate;
	}
	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}
//	public String getdebitaccount() {
//		return debitaccount;
//	}
//	public void setdebitaccount(String aDebitAccount) {
//		this.debitaccount = aDebitAccount;
//	}
	/*public String getcollectionaccount() {
		return collectionaccount;
	}
	public void setcollectionaccount(String aCollectionAccount) {
		this.collectionaccount = aCollectionAccount;
	}*/
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public PayTemplateDetail[] getTemplatedata() {
		return templatedata;
	}
	public void setTemplatedata(PayTemplateDetail[] templatedata) {
		this.templatedata = templatedata;
	}
	public String getProNotiCode() {
		return proNotiCode;
	}
	public void setProNotiCode(String proNotiCode) {
		this.proNotiCode = proNotiCode;
	}
	public String getProDisCode() {
		return proDisCode;
	}
	public void setProDisCode(String proDisCode) {
		this.proDisCode = proDisCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getDebitaccount() {
		return debitaccount;
	}
	public void setDebitaccount(String debitaccount) {
		this.debitaccount = debitaccount;
	}
	public String getCollectionaccount() {
		return collectionaccount;
	}
	public void setCollectionaccount(String collectionaccount) {
		this.collectionaccount = collectionaccount;
	}
	public String getMsgstatus() {
		return msgstatus;
	}
	public void setMsgstatus(String msgstatus) {
		this.msgstatus = msgstatus;
	}

}
