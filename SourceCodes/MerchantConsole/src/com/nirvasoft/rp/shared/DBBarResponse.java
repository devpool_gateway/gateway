package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DBBarResponse {
	
	String[] month;
	DBBarDetail[] detaildata;
	
	public DBBarResponse(){
	clearProperty();	
	}
	public DBBarResponse(String pName, double[] pY){
		
	}
	private void clearProperty(){
		month = null;
		detaildata = null;
				
	}
	public String[] getMonth() {
		return month;
	}
	public void setMonth(String[] month) {
		this.month = month;
	}
	public DBBarDetail[] getDetaildata() {
		return detaildata;
	}
	public void setDetaildata(DBBarDetail[] detaildata) {
		this.detaildata = detaildata;
	}
	
	
	
	
}
