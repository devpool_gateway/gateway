package com.nirvasoft.rp.shared;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReconciliationList {

	private ReconcilationListData[] data;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	
	public ReconcilationListData[] getData() {
		return data;
	}
	public void setData(ReconcilationListData[] data) {
		this.data = data;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
