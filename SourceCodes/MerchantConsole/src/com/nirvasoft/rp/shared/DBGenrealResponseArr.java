package com.nirvasoft.rp.shared;


import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.DBGenrealResponse;
@XmlRootElement
public class DBGenrealResponseArr {
	private DBGenrealResponse[] data;
	
	
	public DBGenrealResponse[] getData() {
		return data;
	}

	public void setData(DBGenrealResponse[] data) {
		this.data = data;
	}
}
