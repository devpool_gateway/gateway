package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DispaymentList {

	private DispaymentTransactionData[] data;
	private PayTemplateDetailArr[] detailData;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String grandTotal;
	private String incomeAmt;
	private String deduAmt;
	private String serviceAmt;
	
	private String curtotal;
	private String savTotal;
	private CNPTransData[] cnpdata;
	
	public DispaymentList(){
		clearProperty();
	}
	private void clearProperty(){
		data = null;
		detailData = null;
		totalCount = 0;
		currentPage = 0 ;
		pageSize = 0;
		grandTotal = "";
		incomeAmt = "";
		deduAmt = "";
		serviceAmt = "";
		curtotal = "";
		savTotal = "";
		cnpdata = null;
	}
	
	
	public String getCurtotal() {
		return curtotal;
	}
	public void setCurtotal(String curtotal) {
		this.curtotal = curtotal;
	}
	public String getSavTotal() {
		return savTotal;
	}
	public void setSavTotal(String savTotal) {
		this.savTotal = savTotal;
	}
	public CNPTransData[] getCnpdata() {
		return cnpdata;
	}
	public void setCnpdata(CNPTransData[] cnpdata) {
		this.cnpdata = cnpdata;
	}
	public String getServiceAmt() {
		return serviceAmt;
	}
	public void setServiceAmt(String serviceAmt) {
		this.serviceAmt = serviceAmt;
	}
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public DispaymentTransactionData[] getData() {
		return data;
	}

	public void setData(DispaymentTransactionData[] data) {
		this.data = data;
	}

	public PayTemplateDetailArr[] getDetailData() {
		return detailData;
	}

	public void setDetailData(PayTemplateDetailArr[] detailData) {
		this.detailData = detailData;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getIncomeAmt() {
		return incomeAmt;
	}
	public void setIncomeAmt(String incomeAmt) {
		this.incomeAmt = incomeAmt;
	}
	public String getDeduAmt() {
		return deduAmt;
	}
	public void setDeduAmt(String deduAmt) {
		this.deduAmt = deduAmt;
	}


	
    
}
