package com.nirvasoft.rp.shared;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReconcilationListData {
	
	private int srno;
	private String transactionDateTime;
	private String kbzTransID;
	private String transID;
	private String transStatus;
	private String amount;
	private String remark;
	
	public ReconcilationListData(){
		clearProperty();
	}
	
	public void clearProperty(){
		this.srno=0;
		this.transactionDateTime="";
		this.kbzTransID="";
		this.transID="";
		this.transStatus="";
		this.amount="";
		this.remark="";
	}
	
	

	public int getSrno() {
		return srno;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public String getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getKbzTransID() {
		return kbzTransID;
	}

	public void setKbzTransID(String kbzTransID) {
		this.kbzTransID = kbzTransID;
	}

	public String getTransID() {
		return transID;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	

}
