package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MUJunctionData {

	private long syskey;
	private String merchantid;
	private long usersyskey;
	
	void clearProperty(){
		syskey = 0;
		merchantid = "";
		usersyskey = 0;
	}
	
	public MUJunctionData(){
		clearProperty();
	}

	public long getSyskey() {
		return syskey;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public String getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}
	
	
}
