package com.nirvasoft.rp.shared;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PayTemplateHeader {
	
	private long sysKey;
	private String merchantID;
	private String name;
	private String processingCode;
	private String proNotiCode;// for Notification
	private String proDisCode;// for Distributor use/ unuse
	
//	private String logo;
	private String detailOrder;
	
	private PayTemplateDetail[] data;
	private LOVSetupDetailData[] detailLov;
	private DisPaymentTransDetail[] detailData;
	
	public PayTemplateHeader(){
		clearProperty();
	}
	private void clearProperty() {
		sysKey = 0;
		merchantID = "";
		name = "";
		processingCode = "";
		proNotiCode = "";
		proDisCode = "";
		data = null;
		detailOrder = "";
		detailLov = null;
		detailData = null;
	}



	public long getSysKey() {
		return sysKey;
	}



	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}



	public String getMerchantID() {
		return merchantID;
	}



	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getProcessingCode() {
		return processingCode;
	}



	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}



	/*public String getLogo() {
		return logo;
	}



	public void setLogo(String logo) {
		this.logo = logo;
	}
*/


	public String getDetailOrder() {
		return detailOrder;
	}



	public void setDetailOrder(String detailOrder) {
		this.detailOrder = detailOrder;
	}



	public PayTemplateDetail[] getData() {
		return data;
	}



	public void setData(PayTemplateDetail[] data) {
		this.data = data;
	}



	public LOVSetupDetailData[] getDetailLov() {
		return detailLov;
	}



	public void setDetailLov(LOVSetupDetailData[] detailLov) {
		this.detailLov = detailLov;
	}



	public DisPaymentTransDetail[] getDetailData() {
		return detailData;
	}



	public void setDetailData(DisPaymentTransDetail[] detailData) {
		this.detailData = detailData;
	}
	public String getProNotiCode() {
		return proNotiCode;
	}
	public void setProNotiCode(String proNotiCode) {
		this.proNotiCode = proNotiCode;
	}
	public String getProDisCode() {
		return proDisCode;
	}
	public void setProDisCode(String procDisCode) {
		this.proDisCode = procDisCode;
	}






	
}
