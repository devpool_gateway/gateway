package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantListDataArr {
	
	private MerchantIDListData[] datalist;
	
	void clearProperty(){
		datalist = null;
	}
	
	public MerchantListDataArr(){
		clearProperty();
	}

	public MerchantIDListData[] getDatalist() {
		return datalist;
	}

	public void setDatalist(MerchantIDListData[] datalist) {
		this.datalist = datalist;
	}
	
	

}
