package com.nirvasoft.rp.shared;


public class ServerGlobal {
	
	private static String mAppPath = "";
	private static String mSeparator = "\\|\\|\\_\\_";
	private static boolean mSetWriteLog = false;
	private static String fortuneYgnMerchant;
	private static String fortuneMdyMerchant;

	public static String getSeparator(){
		return mSeparator;
	}

	public static String getAppPath() {
		return mAppPath;
	}

	public static void setAppPath(String mAppPath) {
		ServerGlobal.mAppPath = mAppPath;
	}
	
	public static boolean isWriteLog() {
		return mSetWriteLog;
	}

	public static void setWriteLog(boolean mWLog) {
		ServerGlobal.mSetWriteLog = mWLog;
	}

	public static String getmAppPath() {
		return mAppPath;
	}

	public static void setmAppPath(String mAppPath) {
		ServerGlobal.mAppPath = mAppPath;
	}

	public static String getmSeparator() {
		return mSeparator;
	}

	public static void setmSeparator(String mSeparator) {
		ServerGlobal.mSeparator = mSeparator;
	}

	

	public static boolean ismSetWriteLog() {
		return mSetWriteLog;
	}

	public static void setmSetWriteLog(boolean mSetWriteLog) {
		ServerGlobal.mSetWriteLog = mSetWriteLog;
	}

	public static String getFortuneYgnMerchant() {
		return fortuneYgnMerchant;
	}

	public static void setFortuneYgnMerchant(String fortuneYgnMerchant) {
		ServerGlobal.fortuneYgnMerchant = fortuneYgnMerchant;
	}

	public static String getFortuneMdyMerchant() {
		return fortuneMdyMerchant;
	}

	public static void setFortuneMdyMerchant(String fortuneMdyMerchant) {
		ServerGlobal.fortuneMdyMerchant = fortuneMdyMerchant;
	}

	
	
}
