package com.nirvasoft.rp.framework;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Result {
	
	private boolean state = false;
	private String merchantID="";
	private String userId=""; 
	private String msgCode = "";
	private String msgDesc = "";
	private long keyResult = 0;
	private String Keyst = "";	
	private String keyString = "";
	private ArrayList<Long> longResult = new ArrayList<Long>();
	private ArrayList<String> stringResult = new ArrayList<String>();


    public String getKeyst() {
		return Keyst;
	}

	public void setKeyst(String keyst) {
		Keyst = keyst;
	}

	public String getKeyString() {
		return keyString;
	}

	public void setKeyString(String keyString) {
		this.keyString = keyString;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	private long[] key;
	
	public long[] getKey() {
		return key;
	}

	public void setKey(long[] key) {
		this.key = key;
	}


	public Result() {
		clearProperties();
	}

	

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	
	public long getKeyResult() {
		return keyResult;
	}

	public void setKeyResult(long keyResult) {
		this.keyResult = keyResult;
	}

	public ArrayList<Long> getLongResult() {
		return longResult;
	}

	public void setLongResult(ArrayList<Long> longResult) {
		this.longResult = longResult;
	}

	public ArrayList<String> getStringResult() {
		return stringResult;
	}

	public void setStringResult(ArrayList<String> stringResult) {
		this.stringResult = stringResult;
	}

	
	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	private void clearProperties() {
		state = false;
		merchantID ="";
		msgCode = "";
		msgDesc = "";
		keyResult=0;
	
		longResult = new ArrayList<Long>();
		stringResult = new ArrayList<String>();
	}
}
