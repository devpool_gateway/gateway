package com.nirvasoft.rp.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.bl.CMSMerchantMgr;
import com.nirvasoft.rp.bl.DBDispaymentMgr;
import com.nirvasoft.rp.bl.MUJunctionMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.CNPTransData;
import com.nirvasoft.rp.shared.DBBarResponse;
import com.nirvasoft.rp.shared.DBGenrealRequest;
import com.nirvasoft.rp.shared.DBGenrealResponse;
import com.nirvasoft.rp.shared.DBGenrealResponseArr;
import com.nirvasoft.rp.shared.DetailsData;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.ForceChangePwdReq;
import com.nirvasoft.rp.shared.ForceChangePwdResponse;
import com.nirvasoft.rp.shared.LoginUserMerchantInfo;
import com.nirvasoft.rp.shared.MUJunctionData;
import com.nirvasoft.rp.shared.MerchantData;
import com.nirvasoft.rp.shared.MerchantListDataArr;
import com.nirvasoft.rp.shared.PayTemplateHeader;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.shared.ReconciliationData;
import com.nirvasoft.rp.shared.ReconciliationList;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.users.data.*;
import com.nirvasoft.rp.mgr.*;
import com.nirvasoft.rp.framework.*;
import com.nirvasoft.rp.framework.Result;

@Path("/service001")
public class Service001 {
	@Context
	HttpServletRequest request;

	@javax.ws.rs.core.Context
	ServletContext context;
	public static String userid = "";
	public static String username = "";
	public static String userpsw = "";

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
	}
	
	private MrBean getUser(Profile p) {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(p.getUserID());
		user.getUser().setPassword(p.getPassword());
		userid = p.getUserID();

		return user;
	}

	private MrBean getUser() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(userid);
		user.getUser().setUserName(username);
		user.getUser().setPassword(userpsw);
		return user;
	}

	@GET
	@Path("getMainList")
	@Produces(MediaType.APPLICATION_JSON)
	public MenuDataArr getmainlist() {
		MenuDataArr res = new MenuDataArr();
		res = MenuDataMgr.getmainmenulist(getUser());
		return res;
	}

	@POST
	@Path("signin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Profile signin(Profile p) {
		if (p != null && p.getUserID() != null && p.getPassword() != null) {
			System.out.println("Sign In Path: " + context.getRealPath("/"));
			System.out.println(p.getUserID() + p.getPassword());
			userpsw = p.getPassword();
			/*
			 * if (p.getUserID().equalsIgnoreCase(p.getPassword()) ){
			 * p.setPassword("*"); //reset } else {p.setRole(0);}
			 */
			p.setUserID(p.getUserID());
			String u1 = Service001Mgr.login(getUser(p));
			System.out.println("PLEASE HELP ME===" + u1);
			if (!u1.equals("")) {
				p.setPassword("*");
				p.setUserName(u1);
				String[] a2 = Service001Mgr.getMainMenu(getUser(p), p.getUserID());
				if (a2.length > 0)
					p.setMenus(getProfileMainMenu(a2, getUser(p), p.getUserID()  , p));
				p.setRightMenus(getProfileRightMenu());
				username = u1;
			} else {
				p.setRole(0);
			}
		} else {
			p.setRole(0);
		}
		return p;
	}
	
	@GET
	@Path("getAllFeatures")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getAllFeatures() {
		UserDataMgr u_mgr = new UserDataMgr();
		ArrayList<Result> res = new ArrayList<Result>();
		Lov3 lov = new Lov3();
		getPath();
		String code = request.getParameter("Code");
		res = u_mgr.getAllFeatures(code);

		Ref arr[] = new Ref[res.size()];
		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getKeyst());
			ref.setcaption(res.get(i).getKeyString());
			arr[i] = ref;
		}
		
		lov.setRefFeature(arr);
		return lov;
	}

	@GET
	@Path("method001")
	@Produces(MediaType.APPLICATION_JSON)
	public Bean001 test1() {
		System.out.print("Path: " + context.getRealPath("/"));
		Bean001 res = new Bean001();
		return res;
	}

	@POST
	@Path("method002")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bean001 test2(Bean001 p) {
		System.out.print("Path: " + context.getRealPath("/"));
		p.setT1(p.getT1() + " *");
		System.out.println(p.getT1() + p.getT2() + p.getT3());
		return p;
	}

	/*
	 * @POST
	 * 
	 * @Path("signin")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Profile signin(Profile p){
	 * if (p!=null && p.getUserID()!=null && p.getPassword()!=null ) {
	 * System.out.println("Sign In Path: " + context.getRealPath("/"));
	 * System.out.println(p.getUserID() + p.getPassword()); if
	 * (p.getUserID().equalsIgnoreCase(p.getPassword()) ){ p.setPassword("*");
	 * //reset } else {p.setRole(0);} }else {p.setRole(0);} return p; }
	 */

	@GET
	@Path("gete001")
	@Produces(MediaType.APPLICATION_JSON)
	public E001 getE001() {
		System.out.print("Path: " + context.getRealPath("/"));
		E001 res = new E001();
		return res;
	}

	@POST
	@Path("poste001")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public E001 postE001(E001 p) {
		System.out.print("Path: " + context.getRealPath("/"));
		System.out.println("POST E001 > " + p.getT1() + p.getT2() + p.getUdf()[0].getValue());
		p.setT1("T1POST");
		p.getUdf()[0].setValue("UDF1POST");
		return p;
	}

	/*@POST
	@Path("getMerchantList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DispaymentList getMerchantList(MerchantData p) {
		DispaymentList res = new DispaymentList();
		DBDispaymentMgr dbCusMgr = new DBDispaymentMgr();
		res = dbCusMgr.getMerchantList(p.getCurrentPage(), p.getTotalCount(), p.getPageSize(), p.getaMerchantID(),p.getStatus(),
				p.getaFromDate().toString().replaceAll("[-+.^:,]", ""),
				p.getaToDate().toString().replaceAll("[-+.^:,]", ""), p.getdebitaccount(), p.getcollectionaccount(),
				p.getDid(), p.getInitiatedby(), p.getTransrefno(), getUser(), p.getProcessingCode(), p.getProDisCode(),
				p.getTemplatedata());
		return res;
	}*/
	
	// CNP Transaction Report //
    @POST
    @Path("getCNPTransactionList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public DispaymentList getCNPTransactionList(CNPTransData p) {
        DispaymentList res = new DispaymentList();
        DBDispaymentMgr dbCusMgr = new DBDispaymentMgr();
        res = dbCusMgr.getCNPTransactionList(p.getCurrentPage(), p.getTotalCount(), p.getPageSize(), p.getaFromDate().toString().replaceAll("[-+.^:,]", ""),
                p.getaToDate().toString().replaceAll("[-+.^:,]", ""), p.getAccType());
        
        return res;
    }
    
	
	@POST
	@Path("getMerchantList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DispaymentList getMerchantList(MerchantData p) {
		DispaymentList res = new DispaymentList();
		MerchantReportMgr dbCusMgr = new MerchantReportMgr();
		res = dbCusMgr.getMerchantList(p.getCurrentPage(), p.getTotalCount(), p.getPageSize(), p.getaMerchantID(),p.getaFeature(),
				p.getStatus(), p.getaFromDate().toString().replaceAll("[-+.^:,]", ""),
				p.getaToDate().toString().replaceAll("[-+.^:,]", ""), p.getDebitaccount(), p.getCollectionaccount(),
				p.getDid(), p.getInitiatedby(), p.getTransrefno(), p.getProcessingCode(), p.getProDisCode(),
				p.getTemplatedata());
		return res;
	}
	
	
	@POST
	@Path("getTransactionHisByCIFID")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DispaymentList getTransactionHisByCIFID(MerchantData p) {
		DispaymentList res = new DispaymentList();
		DBDispaymentMgr dbCusMgr = new DBDispaymentMgr();
		System.out.println("MerchantID.." + p.getaMerchantID());
		System.out.println("getaCustomerID.eem......" + p.getaCustomerID());
		res = dbCusMgr.getTransactionHisByCIFID(p.getCurrentPage(), p.getTotalCount(), p.getPageSize(), p.getaMerchantID(), p.getaCustomerID(), p
				.getaFromDate().toString().replaceAll("[-+.^:,]", ""),
				p.getaToDate().toString().replaceAll("[-+.^:,]", ""), p.getDebitaccount(), p.getCollectionaccount(),
				p.getDid(), p.getInitiatedby(), p.getTransrefno(), getUser(), p.getProcessingCode(), p.getProDisCode(),
				p.getTemplatedata());
		
		return res;
	}

	// Department

	@POST
	@Path("getDepartmentData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DepartmentData readDepartmentDataBySyskey(String skey) {
		System.out.print("Reach Department Read" + skey);
		DepartmentData res = new DepartmentData();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		res = DepartmentMgr.readDataBySyskey(syskey, getUser());
		return res;
	}

	@POST
	@Path("deleteDepartment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result deleteDepartmentData(String skey) {
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		Result res = DepartmentMgr.deleteDepartmentData(syskey, getUser());
		return res;
	}

	@POST
	@Path("saveDepartment")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveDepartment(DepartmentData data) {
		Result res = DepartmentMgr.saveDepartmentData(data, getUser());
		return res;
	}

	@GET
	@Path("getdepartmentlist")
	@Produces(MediaType.APPLICATION_JSON)
	public DepartmentDataArr getDepartmentlist() {

		System.out.println("Path: " + context.getRealPath("/"));
		DepartmentDataArr res = new DepartmentDataArr();
		res = DepartmentMgr.getAllDepartmentData(getUser());
		return res;
	}

	/*@GET
	@Path("getmenulist")
	@Produces(MediaType.APPLICATION_JSON)
	public MenuViewDataArr getMenulist() {

		// System.out.println("Path: " + context.getRealPath("/"));
		MenuViewDataArr res = new MenuViewDataArr();
		res = MenuDataMgr.getAllMenuData(getUser());
		return res;
	}*/
	
	
	@GET
	@Path("getmenulist")
	@Produces(MediaType.APPLICATION_JSON)
	public MenuViewDataArr getMenulist(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage) {
		getPath();
		MenuViewDataArr res = new MenuViewDataArr();
		getPath();
		res = MenuDataMgr.getAllMenuData(searchText, pageSize, currentPage);
		System.out.println("Result : "+ res.toString());
		return res;
	}
	

	@POST
	@Path("getMenuData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MenuData readMenuDataBySyskey(String skey) {

		MenuData res = new MenuData();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		res = MenuDataMgr.readDataBySyskey(syskey, getUser());
		return res;
	}

	@POST
	@Path("saveMenu")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveMenu(MenuData data) {
		Result res = MenuDataMgr.saveMenuData(data, getUser());
		System.out.println("MESSAGE" + res.getMsgDesc());
		return res;
	}

	@POST
	@Path("deleteMenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result deleteMenuData(MenuData meu) {

		String skey = meu.get_key();
		String isParent = meu.getIsParent();
		// skey = skey.substring(1,skey.length()-1);
		long syskey = Long.parseLong(skey);
		Result res = MenuDataMgr.deleteMenuData(syskey, isParent, getUser());
		System.out.println(res.getMsgDesc() + "AND" + res.isState());
		return res;
	}

	@GET
	@Path("getRoleMenuList")
	@Produces(MediaType.APPLICATION_JSON)
	public RoleData getRoleMenuList() {
		RoleMenuData[] dataarray;
		RoleData res = new RoleData();
		dataarray = RoleMenuDataMgr.getRoleMenuList(getUser());
		res.setMenu(dataarray);

		return res;
	}

	@POST
	@Path("getSearchRole")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RoleDatas getSerachRole() {
		String search = request.getParameter("searchVal");
		System.out.println("Search=" + search);
		RoleDatas res = RoleDataMgr.getAllRoleData(getUser());
		return res;
	}

	public Menu[] getProfileMainMenu(String[] promenu, MrBean m, String s1 , Profile p ) {
		Menu[] menus = new Menu[promenu.length];
		Menu obj = new Menu();
		Service001Mgr mgr = new Service001Mgr();

		if (promenu.length == 1)
			menus = new Menu[promenu.length + 1];

		for (int i = 0; i < promenu.length; i++) {
			obj = new Menu();
			obj.setMenuItem("");
			obj.setCaption(promenu[i]);
/*			String[] arr = Service001Mgr.getSubMenuItem(m, s1, promenu[i]);
			Menu[] menuitems = new Menu[arr.length];
			menuitems = getProfileSubMenuItem(arr);
			obj.setMenuItems(menuitems);
			menus[i] = obj;*/
			
			Menu[] menuitems = null;
			menuitems = mgr.getProfileSubMenuItem(p, promenu[i]);
			obj.setMenuItems(menuitems);
			menus[i] = obj;
		}

		return menus;

	}

	public Menu[] getProfileSubMenuItem(String[] promenu) {
		Menu[] items = new Menu[promenu.length];
		Menu subobj = new Menu();

		if (promenu.length == 1)
			items = new Menu[promenu.length + 1];

		for (int j = 0; j < promenu.length; j++) {
			subobj = new Menu();
			subobj.setMenuItem(promenu[j]);
			subobj.setCaption(promenu[j]);
			items[j] = subobj;

		}
		return items;

	}

	public Menu[] getProfileRightMenu() {

		Menu[] right = new Menu[2];
		Menu obj = new Menu();
		obj.setMenuItem("login");
		obj.setCaption("Sign Out");
		right[0] = obj;
		return right;
	}

	@POST
	@Path("saveUser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveUser(UserData data) {

		Result res = new Result();

		if (data.getRolesyskey() != null && data.getRolesyskey().length != 0) {

			res = UserDataMgr.saveUserData(data, getUser());

		} else
			res.setMsgDesc("Please select Role");

		return res;

	}

	//MMPPM
		@POST
		@Path("saveMerchantUsersJunction")
		@Consumes({ MediaType.APPLICATION_JSON })
		@Produces(MediaType.APPLICATION_JSON)
		public Result saveMerchantUsersJunction(UserData data) {
			
			Result res = new Result();
			MUJunctionMgr dbmgr = new MUJunctionMgr();
			ArrayList<MUJunctionData> junctionlist = new ArrayList<MUJunctionData>();
			
			if(data.getRolesyskey() != null && data.getRolesyskey().length != 0){
				
				for(int i=0; i<data.getUserrolelist().length; i++){
					
					if(data.getUserrolelist()[i].isFlag()){
						MUJunctionData jun = new MUJunctionData();
						
						try {
							
							jun.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(getUser().getUser().getOrganizationID(),"")));
							jun.setMerchantid(data.getT2());
							jun.setUsersyskey(data.getUserrolelist()[i].getSyskey());
							
							junctionlist.add(jun);
						} catch (SQLException e) {
						
							e.printStackTrace();
						}
					}
					
				}
				
				
				res = dbmgr.saveMUJunction(data.getName(),junctionlist, getUser());
					
			}
			else
				res.setMsgDesc("Please select Role");
		
			return res;
			
		}

	@POST
	@Path("deleteUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result deleteUserData(String skey) {

		Result res = new Result();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = 0;
		try {
			syskey = Long.parseLong(skey);
			res = UserDataMgr.deleteUserData(syskey, getUser());
		} catch (Exception e) {
			res.setMsgDesc("Cannot Delete");
		}

		System.out.println(skey);
		return res;

	}

	@GET
	@Path("getMerchantIDList")
	@Produces(MediaType.APPLICATION_JSON)
	public MerchantListDataArr getMerchantIDList() {

		MerchantListDataArr res = new MerchantListDataArr();
		CMSMerchantMgr dbmgr = new CMSMerchantMgr();
		res = dbmgr.getMerchantList(getUser());
		return res;
	}

	@GET
	@Path("getUserList")
	@Produces(MediaType.APPLICATION_JSON)
	public UserViewDataArr getUserlist(@QueryParam("searchVal") String searchText, @QueryParam("pagesize") int pageSize,
			@QueryParam("currentpage") int currentPage) {

		UserViewDataArr res = new UserViewDataArr();
		res = UserDataMgr.getAllUserData(searchText, pageSize, currentPage, getUser());
		return res;
	}

	@POST
	@Path("getUserData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData readUserDataBySyskey(String skey) {

		UserData res = new UserData();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		res = UserDataMgr.readDataBySyskey(syskey, getUser());

		return res;
	}

	@POST
	@Path("getMerchantIDData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData readMerchantDataByID(String id) {

		UserData res = new UserData();
		id = id.substring(1, id.length() - 1);
		res = UserDataMgr.readDataByID(id, getUser());

		return res;
	}

	@GET
	@Path("getUserRolelist")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData getRolelist() {

		UserRoleViewData[] dataarray;
		UserData res = new UserData();
		dataarray = UserRoleViewDataMgr.getUserRoleList(getUser());
		res.setUserrolelist(dataarray);
		return res;

	}

	@GET
	@Path("getMerchantUserlist")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData getMerchantUserlist() {

		UserRoleViewData[] dataarray;
		UserData res = new UserData();
		dataarray = UserRoleViewDataMgr.getMerchantUserList(getUser());
		res.setUserrolelist(dataarray);
		return res;

	}

	// role

	@GET
	@Path("getRoleList")
	@Produces(MediaType.APPLICATION_JSON)
	public RoleDatas getRoleList(@QueryParam("searchVal") String searchText, @QueryParam("pagesize") int pageSize,
			@QueryParam("currentpage") int currentPage) {
		RoleDatas res = new RoleDatas();
		RoleDataMgr r_mgr = new RoleDataMgr();
		getPath();
		res = r_mgr.getAllRoleListData(searchText, pageSize, currentPage);
		return res;
	}

	@POST
	@Path("saveRole")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveRole(RoleData data) {
		Result res = new Result();
		if (data.getParentsyskey() != null && data.getParentsyskey().length > 0) {

			res = RoleDataMgr.saveRoleData(data, getUser());

		}

		else
			res.setMsgDesc("Please select menu");

		return res;
	}

	@POST
	@Path("deleteRole")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result deleteRoleData(String skey) {

		Result res = new Result();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = 0;
		try {
			syskey = Long.parseLong(skey);
			res = RoleDataMgr.deleteRoleData(syskey, getUser());
		} catch (Exception e) {
			res.setMsgDesc("Cannot Delete");
		}

		System.out.println(skey);
		return res;
	}

	@GET
	@Path("readBySKRole")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RoleData readDataBySyskeyr() {
		RoleData ret;

		long pKey = Long.parseLong(request.getParameter("syskey"));

		ret = RoleDataMgr.readDataBySyskey(pKey, getUser());
		return ret;
	}

	@POST
	@Path("getRoleData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RoleData readRoleDataBySyskey(String skey) {
		RoleData res = new RoleData();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		res = RoleDataMgr.readDataBySyskey(syskey, getUser());
		return res;
	}

	@GET
	@Path("getmerchantidlistdetail")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getLoadReferences1() {

		Lov3 res = new Lov3();		
		CMSMerchantMgr dbmgr = new CMSMerchantMgr();
		
		res = dbmgr.getMerchantIDListDetail(getUser());
		System.out.println("getmerchantidLIst"+ res.getRef015().length);

		return res;
	}
	
	@GET
	@Path("getInitiatedBy")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getInitiatedBy() {

		Lov3 res = new Lov3();		
		CMSMerchantMgr dbmgr = new CMSMerchantMgr();
		
		res = dbmgr.getInitiatedBy(getUser());

		return res;
	}
	
	@GET
	@Path("getStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getStatus() {

		Lov3 res = new Lov3();		
		CMSMerchantMgr dbmgr = new CMSMerchantMgr();
		
		res = dbmgr.getStatus(getUser());

		return res;
	}

	@GET
	@Path("getmerchantidlist")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getLoadReferences() {

		Lov3 res = new Lov3();

		CMSMerchantMgr dbmgr = new CMSMerchantMgr();
		res = dbmgr.getMerchantIDList(getUser());

		return res;
	}
	@POST
	@Path("getMerchantIdByloginUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoginUserMerchantInfo getMerchantIdByloginUser(LoginUserMerchantInfo data) {
		LoginUserMerchantInfo res = new LoginUserMerchantInfo();
		CMSMerchantMgr mgr = new CMSMerchantMgr();
		res = mgr.LoginUserMerchantInfo(data.getLoginUser(), getUser());
		return res;
	}

	@POST
	@Path("getPortalFormControlByID")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PayTemplateHeader getPaymentTemplateByID(PayTemplateHeader p) {
		PayTemplateHeader res = new PayTemplateHeader();

		String merchantID = p.getMerchantID();
		DAOManager.AbsolutePath = context.getRealPath("");
		String saveDirectory = context.getRealPath("/");
		DBDispaymentMgr payment_trans = new DBDispaymentMgr();
		res = payment_trans.getPaymentTemplateByID(merchantID, saveDirectory, getUser());

		return res;
	}

	// detaillist
	@GET
	@Path("getshowdetails")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<DetailsData> getshowdetails() {
		String syskey = request.getParameter("syskey");
		ArrayList<DetailsData> res = new ArrayList<DetailsData>();
		DAOManager.AbsolutePath = context.getRealPath("");
		DBDispaymentMgr dbMgr = new DBDispaymentMgr();
		res = dbMgr.getshowdetails(syskey, getUser());
		return res;
	}

	@POST
	@Path("changePassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public com.nirvasoft.rp.shared.Result changePassword(PwdData pData) {
		com.nirvasoft.rp.shared.Result ret = new com.nirvasoft.rp.shared.Result();
		PwdMgr mgr = new PwdMgr();

		ret = mgr.changePwd(pData, getUser());

		return ret;
	}
	
	@POST
    @Path("deleteMerchantID")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public Result deleteMerchantID(UserData data) {
		Result res = new Result();
		String merchantID=data.getName();
		MUJunctionMgr mgr = new MUJunctionMgr();
		res = mgr.deleteMerchantID(merchantID, getUser());
	return res;
	}

	// Reconciliation
	@POST
	@Path("getReconciliationList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ReconciliationList getReconciliationList(ReconciliationData p) {
		ReconciliationList rec = new ReconciliationList();
		ReconciliationMgr recMgr = new ReconciliationMgr();
		rec = recMgr.getReconciliationList(p.getCurrentPage(), p.getTotalCount(), p.getPageSize(), p.getaFromDate()
				.toString().replaceAll("[-+.^:,]", ""), p.getaToDate().toString().replaceAll("[-+.^:,]", ""),
				p.gettStatus(), p.getStatus(), p.getKbzTransID(), p.getTransID(), getUser());
		return rec;
	}
	@POST
	@Path("getGeneralDashboard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DBGenrealResponseArr getGeneralDashboard(DBGenrealRequest p) {
		
		ArrayList<DBGenrealResponse> retlist = new ArrayList<DBGenrealResponse>();
		MerchantReportMgr mrMgr = new MerchantReportMgr();
		retlist = mrMgr.getGeneralDashboard(p);
		DBGenrealResponseArr data = new DBGenrealResponseArr();
		DBGenrealResponse[] dataarr = new DBGenrealResponse[retlist.size()];
		dataarr = retlist.toArray(dataarr);
		data.setData(dataarr);
		
		return data;
	}
	@POST
	@Path("getBarMonthlyTxnCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DBBarResponse getBarMonthlyTxnCount(DBGenrealRequest p) {
		
		DBBarResponse ret = new DBBarResponse();
		MerchantReportMgr mrMgr = new MerchantReportMgr();
		ret = mrMgr.getBarMonthlyTxnCount(p);
		
		return ret;
	}//
	@POST
	@Path("getBarMonthlyTxnAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DBBarResponse getBarMonthlyTxnAmount(DBGenrealRequest p) {
		
		DBBarResponse ret = new DBBarResponse();
		MerchantReportMgr mrMgr = new MerchantReportMgr();
		ret = mrMgr.getBarMonthlyTxnAmount(p);
		
		return ret;
	}//
	@POST
	@Path("getBarTxnStatusofCurrentMonth")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DBBarResponse getBarTxnStatusofCurrentMonth(DBGenrealRequest p) {
		
		DBBarResponse ret = new DBBarResponse();
		MerchantReportMgr mrMgr = new MerchantReportMgr();
		ret = mrMgr.getBarTxnStatusofCurrentMonth(p);
		
		return ret;
	}
	
	@POST
	@Path("forceChangePassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ForceChangePwdResponse forcechangePassword(ForceChangePwdReq pData) {
		getPath();
		ForceChangePwdResponse ret = new ForceChangePwdResponse();
		PwdMgr mgr = new PwdMgr();
		//SessionMgr s_mgr = new SessionMgr();
		Result chk_res = new Result();
		ResponseData res = new ResponseData();
		//GeneralUtil.readDebugLogStatus();
	/*	if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Id:" + pData.getUserID());
			l_err.add("OLd password:" + ServerUtil.encryptPIN(pData.getOldPassword()));
			l_err.add("New password:" + ServerUtil.encryptPIN(pData.getNewPassword()));
			l_err.add("forcechange Request Message : ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}*/
		try {
			res = validateID(pData.getUserID());
			if (res.getCode().equalsIgnoreCase("0000")) {
				chk_res = mgr.checkPasswordPolicyPattern(pData.getNewPassword());
				if (chk_res.getMsgCode().equals("0000")) {
					//res = s_mgr.updateActivityTime(pData.getSessionID());
						//s_mgr.saveActivity(pData.getSessionID(), pData.getUserID(), "forcechangePassword", "false", "");
						ret = mgr.forcechangePassword(pData);
								
				}else {
					ret.setCode(chk_res.getMsgCode());
					ret.setDesc(chk_res.getMsgDesc());
				}
			} else {
				ret.setCode(res.getCode());
				ret.setDesc(res.getDesc());
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		/*	if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("user Id:" + pData.getUserID());
				l_err.add("forcechange Response Message :");
				l_err.add("Key : " + ret.getCode());
				l_err.add("Message : " + ret.getDesc());
				l_err.add("New password : " + ServerUtil.encryptPIN(pData.getNewPassword()));
				l_err.add("=============================================================================");

				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}*/
			return ret;
		}
	/*	if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("user Id:" + pData.getUserID());
			l_err.add("forcechange Response Message :");
			l_err.add("Key : " + ret.getCode());
			l_err.add("Message : " + ret.getDesc());
			l_err.add("New password : " + ServerUtil.encryptPIN(pData.getNewPassword()));
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}*/
		return ret;
	}
	public ResponseData validateID(String userId){
		ResponseData response = new ResponseData();
	/*	if(sessionId.equalsIgnoreCase("null") || sessionId.equalsIgnoreCase("")){
			response.setCode("0014");
			response.setDesc("Session ID is mandatory");
		}else */			
		if(userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")){
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		}else{
			response.setCode("0000");
			response.setDesc("Success");
		}
			return response;
	}
}
