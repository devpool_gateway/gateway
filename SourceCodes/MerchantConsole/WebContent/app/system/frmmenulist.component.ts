import { Component,Input,OnDestroy,Output,EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router,ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import {Subscription}   from 'rxjs/Subscription'; 
// RP Framework
import {RpIntercomService} from '../framework/rp-intercom.service'; 
import {RpInputComponent} from '../framework/rp-input.component'; 
import {RpHttpService} from '../framework/rp-http.service'; 

declare var jQuery: any; 
// Application Specific
import {Pager} from '../util/pager.component';
import {ClientUtil} from '../util/rp-client.util';

@Component({
    selector: 'frmmenu-list',
    template: ` 
    <div class="container">
    <div class="row clearfix"> 
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
    <form class="form-inline"> 
    <!-- Form Name -->
    <legend>Menu List</legend>   
      <div class="input-group">
      <span class="input-group-btn input-md">
       <button class="btn btn-primary" type="button" (click)="goNew();">New</button>
       </span>
       <input id="textinput" name="textinput" type="text" placeholder="Search" [(ngModel)]="_searchVal" (keyup.enter)= "searchVal()"  class="form-control input-md">
       <span class="input-group-btn input-md">
	     <button class="btn btn-primary input-md" type="button" (click)="searchVal()" >
       <span class="glyphicon glyphicon-search"></span>Search
       </button>
	      </span>
	    </div> 
    </form>
     
    <div class="row col-md-12">&nbsp;</div> 
   <pager id="pgmenu" [(rpModel)]="_totalcount" [(rpCurrent)]="_pgobj.current" (rpChanged)="changedPager($event)"></pager> 
   <table class="table table-striped table-condensed table-hover" style="font-size:14px;">   
    <thead>
      <tr>
        <th>Code</th>
        <th>Description</th>
        <th>Link</th>
        <th>Width</th>
        <th>Height</th>
        <th>Parent Menu </th>     
      </tr>
    </thead>
    <tbody>
       <tr *ngFor="let obj of _array">
        <td><a (click)="goto(obj.syskey)">{{obj.syskey}}</a></td>
        <td>{{obj.t2}}</td>
        <td>{{obj.t1}}</td>
        <td>{{obj.t5}}</td>
        <td>{{obj.t6}}</td> 
        <td>{{obj.parentMenu}}</td>      
      </tr> 
    </tbody>
  </table>
  </div>
  </div>
  </div>
   ` ,  
}) 

export class FrmMenuList {  
  // RP Framework 
  subscription:Subscription; 
  // Application Specific
   _searchVal = ""; 
  _totalcount = 1;
	_pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
  _util:ClientUtil = new ClientUtil();
  _filter1 =""; 
  _output1=""; 
  _obj={"syskey":"","t1":"","t2":"","t4":"","t5":"","t6":"","n2":0,"n5":0};
  
  _objs=[
  {"t1":"00-9-81","t2":"Mr ABC"},
  {"t1":"00-9-82","t2":"Mrs EFG"},
  {"t1":"00-9-83","t2":"MS XYZ"},
  {"t1":"00-9-81","t2":"Dr ABC"},
  {"t1":"00-9-82","t2":"EFG"},
  {"t1":"00-9-83","t2":"XYZ"}
  ];
  
  _array=[{"syskey":0,"t1":"","t2":"","t5":"","t6":"","parentMenu":""}];
  
  _testone = "";
  constructor(private ics: RpIntercomService,private _router: Router, private http: RpHttpService ) { 
    if  (!ics.getRole() || ics.getRole()==0 )  this._router.navigate(['/login']);
    this.search(this._pgobj);
    
  }  
  searchVal() {
    this.http.doGet(this.ics._apiurl + 'service001/getmenulist?searchVal=' + this._searchVal).subscribe(
      response => {
        let k = response;
        this._totalcount = k.count;
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 0 };
        this.search(this._pgobj);
      },
      error => //alert(error),
      () => { }
    );
  }
  changedPager(event){
    		this._pgobj = event.obj;
        let k = event.flag;
    		if(k){this.search(this._pgobj);}
  	}
	search(p){      
        if(p.end == 0){p.end = this.ics._profile.n1;}
        if(p.size == 0){p.size = this.ics._profile.n1;}
    		let url: string = this.ics._apiurl + 'service001/browseAllMenu?searchVal='+this._searchVal;
      		let json: any = p;
      		this.http.doPost(url, json).subscribe(
        	response => {
          let k = response;
          if(k.state == true){                      
            this._totalcount = k.totalCount;
            if (response != null || response != undefined) {
              this._array = response.data;
            }
          } 
          else{
            this._array = [];
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
          }   
        },
        	error => //alert(error),
        	() => { }
      		);
  	}	
  goto(p) {
    this._router.navigate(['/menu', 'read', p]);
  }
  goNew() {
    this._router.navigate(['/menu', 'new']);
  } 
}
