import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from './framework/rp-http.service';
import { RpIntercomService } from './framework/rp-intercom.service';
import { RpBean } from './framework/rp-bean';
enableProdMode();
@Component({
  selector: 'rp-login',
  template: `
    <div class="container">
  <div class="row" style="height:25px">
  </div>
 </div>
<div class="container"> 
<div class="col-lg-4 col-sm-3 merchants-block">
<div>
<!-- Here -->
      <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 form-box" style="background:#d3d3d3;border-style:solid;border-color:#E0E6F8;border-radius: 10px; padding: 20px;  " >
                  <div class="form-top">
                    <div id="over" style="width:100%" align="center">
                    <img src="image/applogo.png" style="width:130px;">
                    </div>
                    <div class="form-top-left">
                       <h1 style="color:#67310B;text-align:center;font-family:serif;margin-bottom: 30px;">{{ics._appname}}</h1>
                    </div>
                    
                    <div class="form-top-right">
                      <i class="fa fa-key"></i>
                    </div>
                    </div>
                    <div class="form-bottom">
                  
                  <form class="login-form" (ngSubmit)="goPost()" >
                      <div style="margin-bottom: 30px" class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input type="text" id="inputUserID" class="form-control" placeholder="User ID" required autofocus [(ngModel)]=_user [ngModelOptions]="{standalone: true}" >
                      </div> 
                      <div style="margin-bottom: 30px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                         <input type="password" id="inputPassword" class="form-control" placeholder="Password" required [(ngModel)]=_pw [ngModelOptions]="{standalone: true}" >
                      </div> 
                     <div class="col-md-offset-0 col-md-10"> 
                      <div class="checkbox">
                        <label style="font-size: 15px;">
                        <input type="checkbox" value="remember-me" [(ngModel)]="remembercheck"  (change)="setremember(cname,_user,pname,_pw,$event)" [ngModelOptions]="{standalone: true}" > Remember me
                   </label>
                   </div>
                  </div>
                      <div align="center">                            
                        <button class="btn btn-md btn-primary col-sm-12" type="submit">Sign in</button>
                       </div>
                       <br/><br/><br/>
                    <div>
                    <br/>
                    <p style="color:red;">{{_result}} </p> 
                  </div> 
                  </form>
                                 
                </div>
                </div>
            </div>

        </div>

<!-- End -->
</div>

</div>
</div> 
  `
})
export class RpLoginComponent implements OnDestroy {
  _user: string;
  _pw: string;
  url: string = "url";
  cname: string = "username";
  pname: string = "password";
  remembercheck: boolean = true;
  _result: string;
  _remember: boolean;
  subscription: Subscription;
  _rpbean: RpBean;
  apiUrl: string;
  constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
    this.subscription = ics.rpbean$.subscribe(x => { });
    this.ics._profile.role = 0;
    this.checkCookie();
    this.ics.sendBean(new RpBean());
    ics.confirmUpload(false);
  }
  checkCookie() {
    let value = this.readCookieValue('username');
    //let urls = this.readUrl('url');
    if (value != "" && value != null) {
      this._user = value;
    }
    else {
      this._user = "";
      this.remembercheck = false;
    }
  }
  goPost() {
    if (this.apiUrl == null && this.apiUrl == undefined) {
      this.ics.sendBean({ t1: "rp-wait", t2: "Signing in ..." });
    }
    let url: string = this.ics._apiurl + 'service001/signin';
    let profile: any = { "userID": this._user, "password": this._pw, "commandCenter": this._remember };
    this.http.doPost(url, profile).subscribe(
      data => {
        if (this.apiUrl == null && this.apiUrl == undefined) {
          this.ics.sendBean({ t1: "rp-msg-off" });
        }
        if (data.role > 0) {
          this.authorize(data);
        } else {
          this._result = "Invalid User ID or Password";
        }
      },
      error => {
        this.ics.sendBean({ t1: "rp-error", t2: "HTTP Error Type " + error.type });
      },
      () => { }
    );
  }
  authorize(data: any) {
    this.ics._profile = data;
    this.ics._profile.userid = this._user;
    let value = this.readCookieValue('pager');
    if (value != "" && value != null) {
      this.ics._profile.n1 = Number(value);
    }
    else {
      this.ics._profile.n1 = 10;
    }
    this.ics.sendBean(new RpBean());
    this._router.navigate(['/mdashboard']);
  }
  anonymous() {
    this.ics._profile.role = 9;
    this.ics._profile = {
      "userID": ".",
      "userName": ".",
      "role": 9,
      "logoText": "AG2",
      "logoLink": "/frm000",
      "commandCenter": "true",
      "btndata": [],
      "userid": "",
      "t1": "",
      "n1": 10,
      "menus": [{
        "menuItem": "", "caption": "Form",
        "menuItems":
        [

          { "menuItem": "/merchant", "caption": "Transaction Report" },
          { "menuItem": "/frm000", "caption": "Dash Board" },
          { "menuItem": "/transcode", "caption": "Transaction Code" },
          { "menuItem": "/stock", "caption": "Stock" },
          { "menuItem": "/changepwd", "caption": "Change Password" }
        ]
      }
      ],
      "rightMenus": [
        { "menuItem": "/login", "caption": "Sign In" }
      ]
    };;
    this.ics.sendBean(new RpBean());
    this._router.navigate(['/mdashboard']);
  }
  setremember(cname, _user, pname, _pw, event) {
    if (event.target.checked) {
      if (cname == "" && _pw == "") {
        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      }
      else {
        let d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toDateString();
        document.cookie = cname + "=" + _user + ";" + expires;
        document.cookie = pname + "=" + _pw + ";" + expires;
        document.cookie = this.url + "=" + this.ics._apiurl + ";" + expires;
      }
    }
    else {
      document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    }
  }
  readCookieValue(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ')
        c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}