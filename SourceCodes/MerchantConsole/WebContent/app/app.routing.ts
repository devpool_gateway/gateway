import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RpLoginComponent } from './rp-login.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }, {
    path: 'login',
    component: RpLoginComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
