export class ClientUtil {
  constructor() {
  }
  //20160526 2:09pm YMK...
  changeDatetoString(dt) {
    if (dt != null) {
      var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
      return dt.replace(datepattern, '$1$2$3');
    } else {
      return "";
    }
  }

  //for YYYY-MM-DD fomat
  changeDatetoStringYMD(dt) {
    if (dt != null) {
      dt = dt.substring(0, 10);
      var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
      return dt.replace(datepattern, '$3/$2/$1');
    } else {
      return "";
    }
  }
  changeStringtoDate(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      return dt.replace(pattern, '$1-$2-$3');
    } else {
      return "";
    }
  }

  changeStringtoDateTime(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      dt = dt.substring(0, 10);
      return dt.replace(pattern, '$1-$2-$3');
    } else {
      return "";
    }
  }
  changeStringTimetoDate(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      return dt.replace(pattern, '$1/$2/$3');
    } else {
      return "";
    }
  }
  changeStringtoDateDDMMYYYY(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      return dt.replace(pattern, '$3/$2/$1');
    } else {
      return "";
    }
  }
  getTodayDate() {
    var d = new Date();
    var datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    return datestring;
  }

  getCurrentYear() {
    var d = new Date();
    var datestring = d.getFullYear();
    return datestring;
  }
  validateEmail(d) {
    var pattern = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
    return pattern.test(d);
  };
  validateIR(d) {
    var pattern = /IR(\d{2})(\d{2})/;
    return pattern.test(d);
  };
  compareStringLength(str, strln) {
    if (str.length <= strln) {
      return true;
    } else {
      return false;
    }
  }
  checkNumber(num) {
    return isNaN(num);
  }
  changeArray(data, obj, num) {
    let arr = [];
    if (data instanceof Array) {
      arr = data;
      return arr;
    } else {
      if (num == 0) {
        arr[0] = obj;
        arr[1] = data;
        return arr;
      }
      if (num == 1) {
        arr[0] = data;
        arr[1] = obj;
        return arr;
      }
    }
  }
  currencyFormat(p) {
    p.toFixed(2);
  }

  changeDatefromat(dt) {
    if (dt != undefined) { return this.changeStringtoDate(this.changeDatetoString(dt)).substring(0, 10); }
  }
  //money format #,###.## - ymk 20160908
  formatMoney(n) {
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  validateUrl(d) {
    var pattern = /(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$/;
    return pattern.test(d);
  };
}