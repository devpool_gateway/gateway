"use strict";
var ClientUtil = (function () {
    function ClientUtil() {
    }
    //20160526 2:09pm YMK...
    ClientUtil.prototype.changeDatetoString = function (dt) {
        if (dt != null) {
            var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
            return dt.replace(datepattern, '$1$2$3');
        }
        else {
            return "";
        }
    };
    //for YYYY-MM-DD fomat
    ClientUtil.prototype.changeDatetoStringYMD = function (dt) {
        if (dt != null) {
            dt = dt.substring(0, 10);
            var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
            return dt.replace(datepattern, '$3/$2/$1');
        }
        else {
            return "";
        }
    };
    ClientUtil.prototype.changeStringtoDate = function (dt) {
        if (dt != null) {
            var pattern = /(\d{4})(\d{2})(\d{2})/;
            return dt.replace(pattern, '$1-$2-$3');
        }
        else {
            return "";
        }
    };
    ClientUtil.prototype.changeStringtoDateTime = function (dt) {
        if (dt != null) {
            var pattern = /(\d{4})(\d{2})(\d{2})/;
            dt = dt.substring(0, 10);
            return dt.replace(pattern, '$1-$2-$3');
        }
        else {
            return "";
        }
    };
    ClientUtil.prototype.changeStringTimetoDate = function (dt) {
        if (dt != null) {
            var pattern = /(\d{4})(\d{2})(\d{2})/;
            return dt.replace(pattern, '$1/$2/$3');
        }
        else {
            return "";
        }
    };
    ClientUtil.prototype.changeStringtoDateDDMMYYYY = function (dt) {
        if (dt != null) {
            var pattern = /(\d{4})(\d{2})(\d{2})/;
            return dt.replace(pattern, '$3/$2/$1');
        }
        else {
            return "";
        }
    };
    ClientUtil.prototype.getTodayDate = function () {
        var d = new Date();
        var datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
        return datestring;
    };
    ClientUtil.prototype.getCurrentYear = function () {
        var d = new Date();
        var datestring = d.getFullYear();
        return datestring;
    };
    ClientUtil.prototype.validateEmail = function (d) {
        var pattern = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
        return pattern.test(d);
    };
    ;
    ClientUtil.prototype.validateIR = function (d) {
        var pattern = /IR(\d{2})(\d{2})/;
        return pattern.test(d);
    };
    ;
    ClientUtil.prototype.compareStringLength = function (str, strln) {
        if (str.length <= strln) {
            return true;
        }
        else {
            return false;
        }
    };
    ClientUtil.prototype.checkNumber = function (num) {
        return isNaN(num);
    };
    ClientUtil.prototype.changeArray = function (data, obj, num) {
        var arr = [];
        if (data instanceof Array) {
            arr = data;
            return arr;
        }
        else {
            if (num == 0) {
                arr[0] = obj;
                arr[1] = data;
                return arr;
            }
            if (num == 1) {
                arr[0] = data;
                arr[1] = obj;
                return arr;
            }
        }
    };
    ClientUtil.prototype.currencyFormat = function (p) {
        p.toFixed(2);
    };
    ClientUtil.prototype.changeDatefromat = function (dt) {
        if (dt != undefined) {
            return this.changeStringtoDate(this.changeDatetoString(dt)).substring(0, 10);
        }
    };
    //money format #,###.## - ymk 20160908
    ClientUtil.prototype.formatMoney = function (n) {
        return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    };
    ClientUtil.prototype.validateUrl = function (d) {
        var pattern = /(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$/;
        return pattern.test(d);
    };
    ;
    return ClientUtil;
}());
exports.ClientUtil = ClientUtil;
//# sourceMappingURL=rp-client.util.js.map