import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { RpRootComponent } from './rp-root.component';
import { routing } from './app.routing';

import { SystemModule } from './system/rp-system.module';
import { Frm000Module } from './frm000/frm000.module';
import { Frm001Module } from './frm001/frm001.module';
import { RpInputModule } from './framework/rp-input.module';
import { PagerModule } from './util/pager.module';
import { AdvancedSearchModule } from './util/advancedsearch.module';

import { RpLoginComponent } from './rp-login.component';

import { RpHttpService } from './framework/rp-http.service';
import { RpIntercomService } from './framework/rp-intercom.service';
import { RpMenuComponent } from './framework/rp-menu.component';
import { RpInputComponent } from './framework/rp-input.component';
import { RpReferences } from './framework/rp-references';

import {Pager} from './util/pager.component';
import {ClientUtil} from './util/rp-client.util';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    Frm000Module,
    Frm001Module,
    RpInputModule,
    SystemModule,
    PagerModule,
    AdvancedSearchModule
  ],
  declarations: [
    RpRootComponent,
    RpLoginComponent,
    RpMenuComponent
  ],
  providers: [
    RpHttpService,
    RpIntercomService,
    RpReferences,
    ClientUtil,
    { provide: APP_BASE_HREF, useValue : '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [RpRootComponent]
})
export class AppModule {
}
