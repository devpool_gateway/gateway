"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
core_2.enableProdMode();
var FrmTransCodeComponent = (function () {
    function FrmTransCodeComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._obj = this.getDefaultObj();
        this._btn_flag = { "_delete": false };
        this._rights = { "_new": false, "_save": false, "_delete": false, "_list": false };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._obj = this.getDefaultObj();
            this._btn_flag._delete = true;
            this.setBtns();
        }
        console.log("int");
        setTimeout(function () { console.log("2000"); }, 2000);
        setTimeout(function () { console.log("1000"); }, 1000);
        console.log("end");
    }
    FrmTransCodeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmTransCodeComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmTransCodeComponent.prototype.getDefaultObj = function () {
        return { "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "" };
    };
    FrmTransCodeComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceTransCode/readBySyskey?key=' + p).subscribe(function (data) {
            _this._obj = data;
            _this._btn_flag._delete = false;
        }, function (error) { }, function () { });
    };
    FrmTransCodeComponent.prototype.goNew = function () {
        this._btn_flag._delete = true;
        this._obj = this.getDefaultObj();
    };
    FrmTransCodeComponent.prototype.goSave = function () {
        var _this = this;
        var url = this.ics._apiurl + 'serviceTransCode/save';
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            _this._obj.syskey = data.longResult[0];
            _this._obj.createdDate = data.stringResult[0];
            _this._btn_flag._delete = false;
            _this.showMessage(data.msgDesc, data.state);
        }, function (error) {
            _this.showMessage("Can't Save This Record!!!", undefined);
        }, function () { });
    };
    FrmTransCodeComponent.prototype.goDelete = function () {
        var _this = this;
        if (this._obj.syskey != 0) {
            var url = this.ics._apiurl + 'serviceTransCode/delete';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this.showMessage("No Record to Delete!", undefined);
        }
    };
    FrmTransCodeComponent.prototype.goList = function () {
        this._router.navigate(['/transcodelist']);
    };
    FrmTransCodeComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmTransCodeComponent.prototype.setBtns = function () {
        var k = this.ics.getBtns("/transcode");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this._rights._new = true;
                }
                else if (strs[i] == "2") {
                    this._rights._save = true;
                }
                else if (strs[i] == "3") {
                    this._rights._delete = true;
                }
                else if (strs[i] == "4") {
                    this._rights._list = true;
                }
            }
        }
    };
    FrmTransCodeComponent = __decorate([
        core_1.Component({
            selector: 'pr-transcode',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" > \n    <!-- Form Name -->\n    <legend>Transaction Code</legend>\n    <div class=\"row  col-md-12\"> \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._new\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._save\" id=\"save\" type=\"button\" (click) = \"goSave();\" >Save</button> \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._delete\" [disabled]=\"_btn_flag._delete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button> \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._list\" type=\"button\"   (click)=\"goList()\">List</button>  \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.t1\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Code\" autofocus></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.t2\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Description\" autofocus></rp-input>\n    </div>\n\n    </form>\n    </div>\n    </div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmTransCodeComponent);
    return FrmTransCodeComponent;
}());
exports.FrmTransCodeComponent = FrmTransCodeComponent;
//# sourceMappingURL=frmTransCode.component.js.map