"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
var FrmChangePwd = (function () {
    function FrmChangePwd(ics, _router, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.ref = ref;
        this._pwdobj = { "password": "", "newpassword": "", "confirmnewpassword": "" };
        this._resultobj = { "msgCode": "", "msgDesc": "", "keyst": "", state: false };
        this._passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&()_+/':;?,.<>[]|\{}]).{6,20}$/;
        this._changePwdReq = { "userID": "", "sessionID": "", "oldPassword": "", "newPassword": "" };
        this._changePwdRes = { "code": "", "desc": "" };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['Login', , { p1: '*' }]);
    }
    FrmChangePwd.prototype.goChange = function () {
        var _this = this;
        if (this._pwdobj.newpassword == this._pwdobj.confirmnewpassword) {
            var url = this.ics._apiurl + 'service001/forceChangePassword';
            this._changePwdReq = { "userID": this.ics._profile.userID, "sessionID": "", "oldPassword": this._pwdobj.password, "newPassword": this._pwdobj.newpassword };
            var json = this._changePwdReq;
            this.http.doPost(url, json).subscribe(function (result) {
                _this._changePwdRes = result;
                _this.popupMessage(_this._changePwdRes.desc);
                if (_this._changePwdRes.code == '0000') {
                    _this._router.navigate(['/login']);
                }
            }, function (error) { return alert(error); }, function () { });
        }
        else {
            this.popupMessage("New and confirm passwords must be same");
        }
    };
    FrmChangePwd.prototype.goCancel = function () {
        this._pwdobj = { "password": "", "newpassword": "", "confirmnewpassword": "" };
    };
    FrmChangePwd.prototype.popupMessage = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "Password Information", t3: msg });
    };
    FrmChangePwd.prototype.passwordValidation = function (newPass) {
        if (newPass.match(this._passPattern)) {
            return true;
        }
        else {
            return false;
        }
    };
    FrmChangePwd.prototype.confirmLogout = function () {
        var _this = this;
        jQuery("#confirm").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogin();
        });
    };
    FrmChangePwd.prototype.goLogin = function () {
        jQuery("#confirm").modal('hide');
        this._router.navigate(['Login', , { p1: '*' }]);
    };
    FrmChangePwd = __decorate([
        core_1.Component({
            selector: 'frmpwd',
            template: "\n\n<div class=\"container\">\n            <div class=\"row clearfix\"> \n            <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n                <form class=\"form-horizontal\" (ngSubmit)=\"goChange()\">\n                <legend>Change Password</legend>\n\n                 <div class=\"form-group\">\n                    <div class=\"col-md-3\">\n                        <button type=\"submit\" class=\"btn btn-primary\"> Change </button> \n                        <button type=\"button\" class=\"btn btn-primary\" (click)=\"goCancel()\">Cancel</button> \n                    </div> \n                 </div>\n\n            <!--    <div class=\"form-group\">\n                    <div class=\"col-md-10\">\n                        <p>* Passwords are case-sensitive and must be at least 6 characters and no more than 20 characters.</p>\n                        <p>* A good password must contain a mix of capital and lower-case letters,numbers and special characters.</p>\n                    </div>\n                </div> -->                  \n               \n                 <div class=\"form-group\">\n                     <rp-input [(rpModel)]=\"_pwdobj.password\" rpRequired =\"true\" rpLabelRequired=\"true\" rpType=\"password\" rpLabel=\"Current Password\" autofocus ></rp-input>\n                </div>\n\n             \n                      <div class=\"form-group\">\n                     <rp-input [(rpModel)]=\"_pwdobj.newpassword\" rpRequired =\"true\" rpLabelRequired=\"true\" rpType=\"password\" rpLabel=\"New Password\" autofocus ></rp-input>\n                </div>\n        \n                      <div class=\"form-group\">\n                     <rp-input [(rpModel)]=\"_pwdobj.confirmnewpassword\" rpRequired =\"true\" rpLabelRequired=\"true\" rpType=\"password\" rpLabel=\"Confirm Password\" autofocus ></rp-input>\n                </div>\n                </form>\n             </div>\n          </div>\n      </div>\n      \n <div id=\"confirm\" class=\"modal fade\">\n<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n<div class=\"modal-body\">\n<p>&nbsp;<br></p>\n<p>Update successfully.Please login again.</p>\n</div>\n<div class=\"modal-footer\">\n\n</div>\n</div>\n</div>\n</div>\n\n \n <!-- End -->\n",
            //directives: [RpInputComponent],
            providers: [rp_http_service_1.RpHttpService]
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmChangePwd);
    return FrmChangePwd;
}());
exports.FrmChangePwd = FrmChangePwd;
//# sourceMappingURL=frmchangepwd.component.js.map