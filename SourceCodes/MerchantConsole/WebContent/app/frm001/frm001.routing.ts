import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';
import {FrmTransCodeComponent} from './frmTransCode.component';
import {FrmTransCodeListComponent} from './frmTransList.component';
import {FrmStockComponent} from './frmStock.component';
import {FrmStockListComponent} from './frmStockList.component';
import {FrmmerchantreportComponent} from './frmmerchantreport.component'; 
import {FrmChangePwd} from './frmchangepwd.component'

const frm001Routes: Routes = [
  {
    path: 'transcode',
    component: FrmTransCodeComponent
  }, {
    path: 'transcode/:cmd',
    component: FrmTransCodeComponent
  }, {
    path: 'transcode/:cmd/:id',
    component: FrmTransCodeComponent
  }, {
    path: 'transcodelist',
    component: FrmTransCodeListComponent
  }, {
    path: 'stock',
    component: FrmStockComponent
  }, {
    path: 'stock/:cmd',
    component: FrmStockComponent
  }, {
    path: 'stock/:cmd/:id',
    component: FrmStockComponent
  }, {
    path: 'stocklist',
    component: FrmStockListComponent
  },{
    path:'merchant',
    component : FrmmerchantreportComponent
  },{
    path:'changepwd',
    component : FrmChangePwd
  }
];

export const frm001Routing: ModuleWithProviders = RouterModule.forChild(frm001Routes);