import { Component, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router,ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import {Subscription}   from 'rxjs/Subscription'; 
// RP Framework
import {RpIntercomService} from '../framework/rp-intercom.service';
import {RpInputComponent} from '../framework/rp-input.component';
import {RpHttpService} from '../framework/rp-http.service';
import {RpBean} from '../framework/rp-bean';
declare var jQuery: any;
// Application Specific
import {Pager} from '../util/pager.component';
import {ClientUtil} from '../util/rp-client.util';
enableProdMode(); 
@Component({
  selector: 'pr-stocklist',
  template: ` 
  <div class="container col-md-12">
    <form class="form-inline"> 
    
    <!-- Form Name -->
    <legend>Stock List</legend>

      <div *ngIf="_flagas" class="input-group">
        <span class="input-group-btn input-md">
	        <button class="btn btn-primary" type="button" (click)="goNew();">New</button>
	      </span>  
        <input id="textinput" name="textinput" type="text" placeholder="Search" [(ngModel)]="_searchVal" (keyup.enter)= "searchVal()"  class="form-control input-md">
        <span class="input-group-btn input-md">
	        <button class="btn btn-primary input-md" type="button" (click)="searchVal()" >
	          <span class="glyphicon glyphicon-search"></span>Search
	        </button>
	      </span>        
	    </div>      
    </form>
    <div style = "margin-top : 10px">
   
   <ad-search-stocklist [(rpModel)]="_adsearch" (rpHidden)="hideShowAS($event)" (rpChanged)="changeAS($event)"></ad-search-stocklist>

   <pager id="pgcarrier" [(rpModel)]="_srchobj.pager.totalcount" [(rpCurrent)]="_srchobj.pager.current" (rpChanged)="changedPager($event)"></pager> 
    
  <table class="table table-striped table-condensed table-hover tblborder" style="font-size:14px;">
    <thead>
     <tr>
      <th>Code  <img src="image/sortAsc.png" [hidden]="_col_list[0].flag" alt="sortAsc.png" height="12" width="15" (click)="addSort(1)"/>
                        <img src="image/sortDesc.png" [hidden]="!_col_list[0].flag" alt="sortDesc.png" height="12" width="15" (click)="addSort(1)"/></th>
      <th>Description  <img src="image/sortAsc.png" [hidden]="_col_list[1].flag" alt="sortAsc.png" height="12" width="15" (click)="addSort(2)"/>
                        <img src="image/sortDesc.png" [hidden]="!_col_list[1].flag" alt="sortDesc.png" height="12" width="15" (click)="addSort(2)"/></th>      
      <th>Price</th>
      <th>Effective Date</th>
      <th>Trans. Code</th>
    </tr>
  </thead>
  <tbody>
    <tr *ngFor="let obj of _array">
      <td><a (click)="goto(obj.syskey)">{{obj.t1}}</a></td>
      <td>{{obj.t2}}</td>
      <td style="text-align:right;">{{formatNumber(obj.n2)}}</td>
      <td>{{changeStringtoDate(obj.t3)}} - {{changeStringtoDate(obj.t4)}}</td>
      <td>{{obj.transCode}}</td>       
    </tr> 
    </tbody>
  </table>
  </div> 
   `
})

export class FrmStockListComponent {
    // RP Framework 
    subscription: Subscription;
    // Application Specific
    _searchVal = ""; // simple search
    _adsearch = []; // advance search list
    _flagas = true; // flag advance search


    _col_list = [{"col_no":"1","flag":false},{"col_no":"2","flag":false}]
    _sorting = {_sort_type : "asc",_sort_col : "1"};

    _util: ClientUtil = new ClientUtil();
    _array = []; // datalist to show on grid

    _srchobj = { "pager": this.getDefaultPager(), "search": [] };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this._srchobj.pager = this.getDefaultPager();
            this.search(this._srchobj);
        }
    }
    
    search(p) {
        if(p.pager.end == 0){p.pager.end = this.ics._profile.n1;}
        if(p.pager.size == 0){p.pager.size = this.ics._profile.n1;}
        let url: string = this.ics._apiurl + 'serviceStock/search?searchVal=' + this._searchVal + "&sort=" + this._sorting._sort_type + "&type=" + this._sorting._sort_col;
        let json: any = p;
        this.http.doPost(url, json).subscribe(
            data => {
                if (data != null && data != undefined && data.state) {
                    this._srchobj.pager.totalcount = data.totalCount;
                    this._array = data.dataList;
                } else {
                    this._array = [];
                    this._srchobj.pager.totalcount = 1;
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
                }
            },
            error => //alert(error),
                () => { }
        );
    }
    
    goto(p) {
        this._router.navigate(['/stock', 'read', p]);
    }
    goNew() {
        this._router.navigate(['/stock', 'new']);
    }

    changeStringtoDate(k) {
        return this._util.changeStringtoDateDDMMYYYY(k);
    }

    formatNumber(n){
        return this._util.formatMoney(n);
    }

    getDefaultPager(){
        return { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    }

    searchVal() {
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = [];
        this.search(this._srchobj);
    }
    changeAS(event) {
        this._adsearch = event;
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = this._adsearch;
        this.search(this._srchobj);
    }

    hideShowAS(event) {
        this._flagas = event;
    }
    changedPager(event) {
        let k = event.flag;
        this._srchobj.pager = event.obj;
        this._srchobj.search = this._adsearch;
        if (k) { this.search(this._srchobj); }
    }

    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }
    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if(this._col_list[i].col_no==e){
                let _tmp_flag  = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag?"desc":"asc";
                this._sorting._sort_col = e;
                this.search(this._srchobj);
                break;
            }
        }
    }
}