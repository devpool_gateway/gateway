"use strict";
var router_1 = require('@angular/router');
var frmTransCode_component_1 = require('./frmTransCode.component');
var frmTransList_component_1 = require('./frmTransList.component');
var frmStock_component_1 = require('./frmStock.component');
var frmStockList_component_1 = require('./frmStockList.component');
var frmmerchantreport_component_1 = require('./frmmerchantreport.component');
var frmchangepwd_component_1 = require('./frmchangepwd.component');
var frm001Routes = [
    {
        path: 'transcode',
        component: frmTransCode_component_1.FrmTransCodeComponent
    }, {
        path: 'transcode/:cmd',
        component: frmTransCode_component_1.FrmTransCodeComponent
    }, {
        path: 'transcode/:cmd/:id',
        component: frmTransCode_component_1.FrmTransCodeComponent
    }, {
        path: 'transcodelist',
        component: frmTransList_component_1.FrmTransCodeListComponent
    }, {
        path: 'stock',
        component: frmStock_component_1.FrmStockComponent
    }, {
        path: 'stock/:cmd',
        component: frmStock_component_1.FrmStockComponent
    }, {
        path: 'stock/:cmd/:id',
        component: frmStock_component_1.FrmStockComponent
    }, {
        path: 'stocklist',
        component: frmStockList_component_1.FrmStockListComponent
    }, {
        path: 'merchant',
        component: frmmerchantreport_component_1.FrmmerchantreportComponent
    }, {
        path: 'changepwd',
        component: frmchangepwd_component_1.FrmChangePwd
    }
];
exports.frm001Routing = router_1.RouterModule.forChild(frm001Routes);
//# sourceMappingURL=frm001.routing.js.map