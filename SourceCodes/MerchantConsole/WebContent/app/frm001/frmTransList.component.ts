import { Component, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
declare var jQuery: any;
// Application Specific
import { Pager } from '../util/pager.component';
import { ClientUtil } from '../util/rp-client.util';
enableProdMode();
@Component({
    selector: 'pr-transcodelist',
    template: ` 
    <div *ngIf="_showGrid">
        <div class="container col-md-12">
            <form class="form-inline">
                <legend>Trans. Code List</legend>

                <div *ngIf="_flagas" class="input-group">
                    <span class="input-group-btn input-md">
                        <button class="btn btn-primary" type="button" (click)="goNew();">New</button>
                    </span>  
                    <input id="textinput" name="textinput" type="text" placeholder="Search" [(ngModel)]="_searchVal" (keyup.enter)= "searchVal()"  class="form-control input-md">
                    <span class="input-group-btn input-md">
                        <button class="btn btn-primary input-md" type="button" style="border-top-left-radius: 0px;border-bottom-left-radius: 0px;" (click)="searchVal()" >
                        <span class="glyphicon glyphicon-search"></span>Search
                        </button>
                    </span>  

                    <div style="float: left; padding:4px;">
                        <a (click)="alterShowGrid()"> Import/Export</a>
                    </div> 
                </div> 
               
            </form>
            <div style = "margin-top : 10px">
                <ad-search-transcode [(rpModel)]="_adsearch" (rpHidden)="hideShowAS($event)" (rpChanged)="changeAS($event)"></ad-search-transcode>
            </div>
            <pager id="pgcarrier" [(rpModel)]="_srchobj.pager.totalcount" [(rpCurrent)]="_srchobj.pager.current" (rpChanged)="changedPager($event)"></pager> 
        
            <table class="table table-striped table-condensed table-hover tblborder" style="font-size:14px;">
                <thead>
                    <tr>
                        <th>Code  <img src="image/sortAsc.png" [hidden]="_col_list[0].flag" alt="sortAsc.png" height="12" width="15" (click)="addSort(1)"/>
                                    <img src="image/sortDesc.png" [hidden]="!_col_list[0].flag" alt="sortDesc.png" height="12" width="15" (click)="addSort(1)"/></th>
                        <th>Description  <img src="image/sortAsc.png" [hidden]="_col_list[1].flag" alt="sortAsc.png" height="12" width="15" (click)="addSort(2)"/>
                                    <img src="image/sortDesc.png" [hidden]="!_col_list[1].flag" alt="sortDesc.png" height="12" width="15" (click)="addSort(2)"/></th>      
                    </tr>
                </thead>
                <tbody>
                    <tr *ngFor="let obj of _array">
                    <td><a (click)="goto(obj.syskey)">{{obj.t1}}</a></td>
                    <td>{{obj.t2}}</td>       
                    </tr> 
                </tbody>
            </table>
        </div> 
    </div>

    <!-- Import/Export -->
    <div *ngIf="!(_showGrid)" style="padding:4px;margin:5px;">
            <button class="btn btn-primary input-md" type="button" (click)="alterShowGrid()" >Back</button>
            <button class="btn btn-primary input-md" type="button" (click)="export()" >Export</button>
            <button class="btn btn-primary input-md" type="button" (click)="import()" >Import</button>
            <custom-browse (cuBrowse)="transUploadEvent($event)"></custom-browse>
        
            <div class="input-group stylish-input-group" style="width: 300px;margin-top : 10px">
                <select class="form-control input-sm" [(ngModel)]="_sheetNo" style="width: 230px;height:34px">
                    <option *ngFor="let sheet of _sheets | keyValuePipe" value="{{sheet.key}}">{{sheet.value}}</option>
                </select> 
                <span class="input-group-btn input-md"> 
                    <button class="btn btn-primary input-md" type="button" (click)="previewData()">Preview </button>
                </span>
            </div>
            
            <div>
                <!-- Tab -->
                <ul class="nav nav-tabs" style = "margin-top : 10px">
                <li class="active"><a data-toggle="tab" href="#tab1"><i class="glyphicon glyphicon-list"></i> New Data</a></li>
                <li><a data-toggle="tab" href="#tab2"><i class="glyphicon glyphicon-list"></i> Existing Data</a></li> 
                <li><a data-toggle="tab" href="#tab3"><i class="glyphicon glyphicon-list"></i> Error Data</a></li> 
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab-pane fade in active">
                        <div>
                            <pager id="pgcarrier" [(rpModel)]="_newList.length" [(rpCurrent)]="_new_pager.current" (rpChanged)="changedNewPager($event)"></pager> 	
                            <table class="table table-striped table-condensed table-hover table-bordered"
                                style="width: 70%">
                                <thead>
                                    <tr>
                                        <th style="width: 20%">Code</th>
                                        <th style="width: 80%">Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr *ngFor="let newData of _newList | slice:_new_pager.start-1:_new_pager.end ">
                                        <td>{{newData.t1}}</td>
                                        <td>{{newData.t2}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tab2" class="tab-pane fade">  
                        <div>
                            <pager id="pgcarrier" [(rpModel)]="_existingList.length" [(rpCurrent)]="_existing_pager.current" (rpChanged)="changedExistingPager($event)"></pager> 	
                            <table class="table table-striped table-condensed table-hover table-bordered"
                                style="width: 70%">
                                <thead>
                                    <tr>
                                        <th style="width: 20%">Code</th>
                                        <th style="width: 80%">Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr *ngFor="let exData of _existingList | slice:_existing_pager.start-1:_existing_pager.end " >
                                        <td>{{exData.t1}}</td>
                                        <td>{{exData.t2}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    <div id="tab3" class="tab-pane fade">  
                        <div>		
                            <pager id="pgcarrier" [(rpModel)]="_errorList.length" [(rpCurrent)]="_error_pager.current" (rpChanged)="changedErrorPager($event)"></pager> 	
                            
                            <table class="table table-striped table-condensed table-hover table-bordered"
                                style="width: 70%">
                                <thead>
                                    <tr>
                                        <th style="width: 20%">Code</th>
                                        <th style="width: 80%">Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr *ngFor="let erData of _errorList | slice:_error_pager.start-1:_error_pager.end ">
                                        <td>{{erData.t1}}</td>
                                        <td>{{erData.t2}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>
            </div>

    	<div style="display: none;width: 0px;height: 0px;">
            <iframe #iframe ></iframe>
        </div>
	</div>    
   `
})

export class FrmTransCodeListComponent {
    // RP Framework 
    subscription: Subscription;
    // Application Specific
    _searchVal = ""; // simple search
    _adsearch = []; // advance search list
    _flagas = true; // flag advance search

    _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
    _sorting = { _sort_type: "asc", _sort_col: "1" };

    _util: ClientUtil = new ClientUtil();
    _array = []; // datalist to show on grid

    _srchobj = { "pager": this.getDefaultPager(), "search": [] };

    @ViewChild('iframe') iframe: ElementRef;
    _new_pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    _existing_pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    _error_pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    _newList = [];
    _existingList = [];
    _errorList = [];
    _sheets: Object;
    _showGrid = true;
    _sheetNo: number;
    _fileName: string;


    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this._srchobj.pager = this.getDefaultPager();
            this.search(this._srchobj);
        }
    }

    search(p) {
        if (p.pager.end == 0) { p.pager.end = this.ics._profile.n1; }
        if (p.pager.size == 0) { p.pager.size = this.ics._profile.n1; }
        let url: string = this.ics._apiurl + 'serviceTransCode/search?searchVal=' + this._searchVal + "&sort=" + this._sorting._sort_type + "&type=" + this._sorting._sort_col;
        let json: any = p;
        this.http.doPost(url, json).subscribe(
            data => {
                if (data != null && data != undefined && data.state) {
                    this._srchobj.pager.totalcount = data.totalCount;
                    this._array = data.dataList;
                } else {
                    this._array = [];
                    this._srchobj.pager.totalcount = 1;
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
                }
            },
            error => //alert(error),
                () => { }
        );
    }

    goto(p) {
        this._router.navigate(['/transcode', 'read', p]);
    }
    goNew() {
        this._router.navigate(['/transcode', 'new']);
    }

    alterShowGrid() {
        this._showGrid = !this._showGrid;
    }

    transUploadEvent(event) {
        this.clearpreviewData();
        this._sheetNo = -1;
        this._sheets = "";
        let url: string;
        let json: any;
        this._fileName = event.fileName;

        if (this._fileName.endsWith("xlsx") || this._fileName.endsWith('xls')) {
            url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName;
            this.http.upload(url, event.file).subscribe(
                data => {
                    if (data.code == "SUCCESS") {
                        this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": "Upload Successfully!" });
                        url = this.ics._apiurl + 'file/getSheets';
                        json = { "fn": this._fileName, "fp": "upload" };
                        this.http.doPost(url, json).subscribe(
                            data => {
                                this._sheets = data;
                            },
                            error => {

                            }
                        );
                    } else {
                        this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Upload Unsuccessfully!" });

                    }
                },
                error => {
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Transaction" });
                }
            );
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Invalid File" });
            this.clearpreviewData();
        }

    }

    clearpreviewData() {
        this._newList = [];
        this._errorList = [];
        this._existingList = [];
    }

    previewData() {
        if (this._sheetNo >= 0) {
            let url: string;
            let json: any;
            url = this.ics._apiurl + 'serviceTransCode/isCorrectExcel';
            json = { "fn": this._fileName, "fp": "upload", "sn": this._sheetNo };
            this.http.doPost(url, json).subscribe(
                data => {
                    if (data.state == true) {
                        url = this.ics._apiurl + 'serviceTransCode/getExcelData';
                        this.http.doPost(url, json).subscribe(
                            data => {
                                if (this._new_pager.end == 0) { this._new_pager.end = this.ics._profile.n1; }
                                if (this._new_pager.size == 0) { this._new_pager.size = this.ics._profile.n1; }
                                if (this._existing_pager.end == 0) { this._existing_pager.end = this.ics._profile.n1; }
                                if (this._existing_pager.size == 0) { this._existing_pager.size = this.ics._profile.n1; }
                                if (this._error_pager.end == 0) { this._error_pager.end = this.ics._profile.n1; }
                                if (this._error_pager.size == 0) { this._error_pager.size = this.ics._profile.n1; }
                                this._newList = data.newd;
                                this._existingList = data.existing;
                                this._errorList = data.error;
                            });
                    }
                    if (data.state == false) {
                        this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Invalid Sheet" });
                        this.clearpreviewData();
                    }
                },
                error => {
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Sheets" });

                }
            );
        }
    }

    import() {
        let url: string;
        let json: any;
        if (this._newList.length > 0 || this._existingList.length > 0) {
            url = this.ics._apiurl + 'serviceTransCode/import';
            json = { "newd": this._newList, "existing": this._existingList }
            this.http.doPost(url, json).subscribe(
                data => {
                    if (data.state == true) {
                        this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": "Imported Successfully." });
                    } else {
                        this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Import Fail." });
                    }
                },
                error => {
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Import Fail." });
                }
            );
        } else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "No data to import." });
        }
    }

    export() {
        let url: string;
        url = this.ics._apiurl + 'serviceTransCode/checkData';
        this.http.doGet(url).subscribe(
            data => {
                if (data.state == true) {
                    this.iframe.nativeElement.src = this.ics._apiurl + 'serviceTransCode/exportData';
                } else {
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "No data." });
                }
            },
            error => {

            }

        );


    }

    changedNewPager(event) {
        let k = event.flag;
        if (k) {
            this._new_pager = event.obj;
        }
    }

    changedExistingPager(event) {
        let k = event.flag;
        if (k) {
            this._existing_pager = event.obj;
        }
    }

    changedErrorPager(event) {
        let k = event.flag;
        if (k) {
            this._error_pager = event.obj;
        }
    }

    getDefaultPager() {
        return { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    }

    searchVal() {
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = [];
        this.search(this._srchobj);
    }
    changeAS(event) {
        this._adsearch = event;
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = this._adsearch;
        this.search(this._srchobj);
    }

    hideShowAS(event) {
        this._flagas = event;
    }
    changedPager(event) {
        let k = event.flag;
        this._srchobj.pager = event.obj;
        this._srchobj.search = this._adsearch;
        if (k) { this.search(this._srchobj); }
    }

    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }
    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                let _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                this.search(this._srchobj);
                break;
            }
        }
    }
}