"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmStockComponent = (function () {
    function FrmStockComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._obj = this.getDefaultObj();
        this._btn_flag = { "_delete": false };
        this._rights = { "_new": false, "_save": false, "_delete": false, "_list": false };
        this._dates = { "date1": "", "date2": "" };
        this._util = new rp_client_util_1.ClientUtil();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._obj = this.getDefaultObj();
            this._btn_flag._delete = true;
            this.setBtns();
            this.getTransCode();
        }
    }
    FrmStockComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmStockComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmStockComponent.prototype.getDefaultObj = function () {
        this._dates = { "date1": "", "date2": "" };
        return { "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "n1": 0, "n2": 0.0, "transCode": "" };
    };
    FrmStockComponent.prototype.getTransCode = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceTransCode/getAllData').subscribe(function (response) {
            if (response != null && response != undefined) {
                _this.ref._lov3.ref003 = response.data;
            }
            else {
                _this.ref._lov3.ref003 = [{ "value": "", "caption": "" }];
            }
        }, function (error) { }, function () { });
    };
    FrmStockComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceStock/readBySyskey?key=' + p).subscribe(function (data) {
            _this._dates.date1 = _this._util.changeStringtoDateTime(data.t3);
            _this._dates.date2 = _this._util.changeStringtoDateTime(data.t4);
            _this._obj = data;
            _this._btn_flag._delete = false;
        }, function (error) { }, function () { });
    };
    FrmStockComponent.prototype.goNew = function () {
        this._btn_flag._delete = true;
        this._obj = this.getDefaultObj();
    };
    FrmStockComponent.prototype.goSave = function () {
        var _this = this;
        var url = this.ics._apiurl + 'serviceStock/save';
        this._obj.t3 = this._util.changeDatetoString(this._dates.date1);
        this._obj.t4 = this._util.changeDatetoString(this._dates.date2);
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            _this._obj.syskey = data.longResult[0];
            _this._obj.createdDate = data.stringResult[0];
            _this._btn_flag._delete = false;
            _this.showMessage(data.msgDesc, data.state);
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmStockComponent.prototype.goDelete = function () {
        var _this = this;
        if (this._obj.syskey != 0) {
            var url = this.ics._apiurl + 'serviceStock/delete';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this.showMessage("No Record to Delete!", undefined);
        }
    };
    FrmStockComponent.prototype.goList = function () {
        this._router.navigate(['/stocklist']);
    };
    FrmStockComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmStockComponent.prototype.setBtns = function () {
        var k = this.ics.getBtns("/transcode");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this._rights._new = true;
                }
                else if (strs[i] == "2") {
                    this._rights._save = true;
                }
                else if (strs[i] == "3") {
                    this._rights._delete = true;
                }
                else if (strs[i] == "4") {
                    this._rights._list = true;
                }
            }
        }
    };
    FrmStockComponent = __decorate([
        core_1.Component({
            selector: 'pr-stock',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\"> \n    <!-- Form Name -->\n    <legend>Stock</legend>\n    <div class=\"row  col-md-12\"> \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._new\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._save\" id=\"save\" type=\"button\" (click) = \"goSave();\" >Save</button> \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._delete\" [disabled]=\"_btn_flag._delete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button> \n      <button class=\"btn btn-primary\" *ngIf=\"_rights._list\" type=\"button\"   (click)=\"goList()\">List</button>  \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.t1\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Code\" autofocus></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.t2\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Description\" autofocus></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.n2\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Price\" autofocus></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <label class=\"col-md-2\">Efective Date</label>\n        <div class=\"col-md-2\"> \n            <input [(ngModel)]=\"_dates.date1\" required type=\"date\" autofocus class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n        </div>\n        <div class=\"col-md-2\"> \n            <input [(ngModel)]=\"_dates.date2\" required type=\"date\" autofocus class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n        </div>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.n1\" rpRequired =\"true\" rpType=\"ref003\" rpLabel=\"Trans. Code\" autofocus></rp-input>\n    </div>\n    </form>\n    </div>\n    </div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmStockComponent);
    return FrmStockComponent;
}());
exports.FrmStockComponent = FrmStockComponent;
//# sourceMappingURL=frmStock.component.js.map