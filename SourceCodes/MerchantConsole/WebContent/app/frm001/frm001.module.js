"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var pager_module_1 = require('../util/pager.module');
var advancedsearch_module_1 = require('../util/advancedsearch.module');
var frmTransCode_component_1 = require('./frmTransCode.component');
var frmTransList_component_1 = require('./frmTransList.component');
var frmStock_component_1 = require('./frmStock.component');
var frmStockList_component_1 = require('./frmStockList.component');
var frm001_routing_1 = require('./frm001.routing');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
var pipe_1 = require('../util/pipe');
var custom_browse_component_1 = require('../util/custom-browse.component');
var frmmerchantreport_component_1 = require('./frmmerchantreport.component');
var frmchangepwd_component_1 = require('./frmchangepwd.component');
var Frm001Module = (function () {
    function Frm001Module() {
    }
    Frm001Module = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                advancedsearch_module_1.AdvancedSearchModule,
                pager_module_1.PagerModule,
                frm001_routing_1.frm001Routing
            ],
            exports: [],
            declarations: [
                frmTransCode_component_1.FrmTransCodeComponent,
                frmTransList_component_1.FrmTransCodeListComponent,
                frmStock_component_1.FrmStockComponent,
                frmStockList_component_1.FrmStockListComponent,
                frmmerchantreport_component_1.FrmmerchantreportComponent,
                frmchangepwd_component_1.FrmChangePwd,
                pipe_1.KeyValuePipe,
                custom_browse_component_1.CustomBrowse
            ],
            providers: [
                rp_http_service_1.RpHttpService,
                rp_client_util_1.ClientUtil
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], Frm001Module);
    return Frm001Module;
}());
exports.Frm001Module = Frm001Module;
//# sourceMappingURL=frm001.module.js.map