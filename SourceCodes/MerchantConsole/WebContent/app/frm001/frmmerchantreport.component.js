"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// RP Framework
var router_1 = require('@angular/router');
var core_1 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
// Application Specific
var FrmmerchantreportComponent = (function () {
    function FrmmerchantreportComponent(ics, ref, _router, http) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        // Application Specific
        this.id = "";
        this._totalcount = 1;
        this._entryhide = true;
        this._util = new rp_client_util_1.ClientUtil();
        this._hideDetailTran = true;
        this._hideDetailDownload = true;
        this._grandAmtTotal = "";
        this._incomeAmt = "";
        this._serviceAmt = "";
        this._deduAmt = "";
        this._checkmerchant = "";
        this._sessionMsg = "";
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        //_deliveryOrderHidden: boolean;
        //_obj={"aMerchantID":"","aFromDate":"","aToDate":"","select":"EXCEL","totalCount":0,"currentPage":1,"pageSize":10,"msgstatus":"","debitaccount":"","collectionaccount":"","did":""};
        this._obj = {
            "sessionID": "", "aFeature": "",
            "aMerchantID": "", "aCustomerID": "", "aFromDate": "", "aToDate": "", "status": "ALL", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "debitaccount": "", "collectionaccount": "", "did": "", "customername": "", "description": "", "initiatedby": "ALL", "transrefno": "", "processingCode": "", "proNotiCode": "", "proDisCode": "",
            "templatedata": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }]
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "syskey": 0, "feature": "", "xref": "",
                    "transactiondate": "", "tranrefnumber": "", "trantaxrefnumber": "", "activebranch": "", "debitaccount": "", "debitbranch": "", "collectionaccount": "",
                    "branch": "", "amountst": "", "commchargest": "", "commuchargest": "", "customerCode": "", "customerNumber": "", "fileUploadDateTime": "",
                    "chequeNo": "", "currencyCode": "", "t7": "", "t8": "", "t10": "", "t11": "", "t12": "", "t13": "", "t15": "", "t16": "", "t17": "", "t19": "", "t20": "", "t21": "", "t22": "",
                    "t23": "", "t24": "", "t25": "", "n1": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n17": 0, "n18": 0, "n19": 0, "n20": 0, "distributorName": "", "distributorAddress": "", "messageCode": "", "messageDesc": ""
                }],
            "grandTotal": "", "incomeAmt": "", "serviceAmt": "", "deduAmt": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "detailData": [{ "payCaption": "", "paydatatype": "", "paydispayfield": "" }]
        };
        this._mInfo = { "loginUser": "", "merchantId": "", "merchantName": "" };
        this._mObject = {
            "sysKey": 0, "merchantID": "", "name": "", "processingCode": "", "proNotiCode": "", "proDisCode": "", "detailOrder": "", "detailLov": [{ "srno": 0, "lovCde": "", "lovDesc1": "", "price": 0 }],
            "detailData": [{ "sysKey": 0, "hKey": 0, "srno": 1, "code": "", "description": "", "price": 0, "qty": 0, "amount": 0 }], "data": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }],
        };
        this.loginUser = "";
        this._listpopup = { "arr": [{ "syskey": 0, "hkey": 0, "srno": 0, "code": "", "description": "", "price": "", "quantity": 0, "amount": "" }] };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['Login', , { p1: '*' }]);
        // Application Specific 
        this.ics.confirmUpload(false);
        this.loginUser = this.ics._profile.userID;
        // this.getMerchantIdByloginUser(this.loginUser);
        this.getmerchantidlist();
        this.getStatus();
        this.getInitiatedBy();
        this.getAllFeature("");
        this.messagehide = true;
        this._mInfo = { "loginUser": "", "merchantId": "", "merchantName": "" };
        console.log("login user " + this.loginUser);
        //this._obj.aMerchantID = 
        this._obj.aFromDate = this._util.getTodayDate();
        this._obj.aToDate = this._util.getTodayDate();
        var merchantID = this._mInfo.merchantId;
        this._mObject = {
            "sysKey": 0, "merchantID": "", "name": "", "processingCode": "", "proNotiCode": "", "proDisCode": "", "detailOrder": "", "detailLov": [{ "srno": 0, "lovCde": "", "lovDesc1": "", "price": 0 }],
            "detailData": [{ "sysKey": 0, "hKey": 0, "srno": 1, "code": "", "description": "", "price": 0, "qty": 0, "amount": 0 }], "data": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }]
        };
    }
    FrmmerchantreportComponent.prototype.getMerchantIdByloginUser = function (loginUser) {
        var _this = this;
        this._mInfo.loginUser = loginUser;
        var json = this._mInfo;
        var url = this.ics._apiurl + 'service001/getMerchantIdByloginUser';
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this._mInfo = data;
                _this._obj.aMerchantID = _this._mInfo.merchantId;
                console.log("getMerchantIdByloginUser " + _this._obj.aMerchantID);
                _this.getPortalFormControl(_this._obj.aMerchantID);
            }
        });
    };
    FrmmerchantreportComponent.prototype.showdetailspopup = function (syskey) {
        var _this = this;
        this._listpopup.arr = [];
        if (this.ics._apiurl != "") {
            console.log("Syskey" + syskey);
            this.http.doGet(this.ics._apiurl + 'service001/getshowdetails?syskey=' + syskey).subscribe(function (data) {
                _this._listpopup.arr = data.detailsData;
                if (data != null) {
                    if (data.detailsData != null) {
                        if (!(data.detailsData instanceof Array)) {
                            var m = [];
                            m[0] = data.detailsData;
                            data.detailsData = m;
                        }
                        _this._listpopup.arr = data.detailsData;
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        jQuery("#lu001popup").modal();
    };
    FrmmerchantreportComponent.prototype.changedPager = function (event) {
        this._pgobj = event.obj;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmmerchantreportComponent.prototype.goList = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getMerchantList';
        var capValue = "";
        this._grandAmtTotal = "0.00";
        this._incomeAmt = "0.00";
        this._deduAmt = "0.00";
        this._serviceAmt = "0.00";
        var json = this._obj;
        console.log("Param for List" + JSON.stringify(json));
        console.log("Processing code :: " + this._obj.processingCode);
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == "0016") {
                _this._sessionMsg = data.msgDesc;
                _this.showMessage();
            }
            else {
                console.log("List DATA " + JSON.stringify(data));
                _this._entryhide = false;
                _this._totalcount = data.totalCount;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.aFromDate > _this._obj.aToDate) {
                    _this._obj.msgstatus = "From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                }
                else {
                    if (_this._obj.totalCount == 0) {
                        _this._obj.msgstatus = "Data not Found!";
                        _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                        var fromdate = _this._obj.aFromDate;
                        var todate = _this._obj.aToDate;
                        var feature = _this._obj.aFeature;
                        var status_1 = _this._obj.status;
                        var intiatedby = _this._obj.initiatedby;
                        _this.goClear();
                        _this._obj.aFromDate = fromdate;
                        _this._obj.aToDate = todate;
                        _this._obj.aFeature = feature;
                        _this._obj.initiatedby = intiatedby;
                        _this._obj.status = status_1;
                    }
                }
                if (data != null) {
                    _this._list = data;
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            data.data = m;
                        }
                        _this._grandAmtTotal = _this._list.grandTotal;
                        _this._incomeAmt = _this._list.incomeAmt;
                        _this._serviceAmt = _this._list.serviceAmt;
                        _this._deduAmt = _this._list.deduAmt;
                        _this._list.data = data.data;
                    }
                    if (data.detailData != null) {
                        if (!(data.detailData instanceof Array)) {
                            var m = [];
                            m[0] = data.detailData;
                            data.detailData = m;
                        }
                    }
                    _this._list.detailData = data.detailData;
                }
            }
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmmerchantreportComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    // routerCanDeactivate(next: ComponentInstruction, prev: ComponentInstruction) {
    //   return true;
    // }
    FrmmerchantreportComponent.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    FrmmerchantreportComponent.prototype.goDownload = function () {
        var templateData = "";
        var formatJSP = "";
        if (this._obj.aMerchantID != this._checkmerchant) {
            this._obj.msgstatus = "Please select merchant!";
            this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: this._obj.msgstatus });
        }
        else {
            if (this._mObject.processingCode == '01' || this._mObject.processingCode == '03') {
                for (var k = 0; k < this._obj.templatedata.length; k++) {
                    if (k == 0) {
                        templateData = "fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                    }
                    else {
                        templateData = templateData + "@@fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                    }
                }
            }
            if (this._mObject.detailOrder == 'lov') {
                formatJSP = "merchantdetailsexcel.jsp";
            }
            else {
                formatJSP = "merchantexcel.jsp";
            }
            console.log("templateData " + templateData);
            this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
            var url = this.ics._rpturl + formatJSP + "?aMerchantID=" + this._obj.aMerchantID + "&aFeature=" + this._obj.aFeature + "&debitaccount=" + this._obj.debitaccount
                + "&collectionaccount=" + this._obj.collectionaccount + "&" +
                "aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&" + "did=" + this._obj.did
                + "&initiatedby=" + this._obj.initiatedby + "&transrefno=" + this._obj.transrefno + "&processingCode=" + this._obj.processingCode
                + "&proNotiCode=" + this._obj.proNotiCode + "&proDisCode=" + this._obj.proDisCode + "&templatedata=" + templateData + "&status=" + this._obj.status;
            console.log("url " + url);
            this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
        }
    };
    FrmmerchantreportComponent.prototype.goDownloadDetail = function () {
        var templateData = "";
        if (this._mObject.processingCode == '01' || this._mObject.processingCode == '03') {
            for (var k = 0; k < this._obj.templatedata.length; k++) {
                if (k == 0) {
                    templateData = "fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                }
                else {
                    templateData = templateData + "@@fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                }
            }
        }
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        var url = this.ics._rpturl + "merchantdetailsexcel.jsp?aMerchantID=" + this._obj.aMerchantID + "&debitaccount=" + this._obj.debitaccount
            + "&collectionaccount=" + this._obj.collectionaccount + "&" +
            "aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&" + "did=" + this._obj.did
            + "&initiatedby=" + this._obj.initiatedby + "&transrefno=" + this._obj.transrefno + "&processingCode=" + this._obj.processingCode
            + "&proNotiCode=" + this._obj.proNotiCode + "&proDisCode=" + this._obj.proDisCode + "&templatedata=" + templateData;
        console.log("url " + url);
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmmerchantreportComponent.prototype.goClear = function () {
        this._entryhide = true;
        var tmptemplateObj = this._obj.templatedata;
        var tmpprocessingCode = this._obj.processingCode;
        var tmpproNotiCode = this._obj.proNotiCode;
        var tmpproDisCode = this._obj.proDisCode;
        //  this._obj={"aMerchantID":this._obj.aMerchantID,"aFromDate":"","aToDate":"","select":"EXCEL","totalCount":0,"currentPage":1,"pageSize":10,"msgstatus":"","debitaccount":"","collectionaccount":"","did":"","customername":"","description":"", "initiatedby":"ALL", "transrefno":"" };
        this._obj = {
            "sessionID": "", "aFeature": "",
            "aMerchantID": this._obj.aMerchantID, "aCustomerID": "", "aFromDate": "", "aToDate": "", "status": "ALL", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "debitaccount": "", "collectionaccount": "", "did": "", "customername": "", "description": "", "initiatedby": "ALL", "transrefno": "", "processingCode": "", "proNotiCode": "", "proDisCode": "",
            "templatedata": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }]
        };
        this._obj.processingCode = tmpprocessingCode;
        this._obj.proNotiCode = tmpproNotiCode;
        this._obj.proDisCode = tmpproDisCode;
        this._obj.templatedata = tmptemplateObj;
        this._obj.aFromDate = this._util.getTodayDate();
        this._obj.aToDate = this._util.getTodayDate();
    };
    FrmmerchantreportComponent.prototype.Validation = function () {
        if (this._obj.aMerchantID == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please Type MerchantID!" });
        }
        else if (this._obj.aFromDate.length == 0) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date should not blank!" });
        }
        else if (this._obj.aToDate.length == 0) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "To Date should not blank!" });
        }
        else if (this._obj.aFromDate > this._obj.aToDate) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
            this.goClear();
        }
        else {
            this.goDownload();
        }
    };
    FrmmerchantreportComponent.prototype.ValidationDetails = function () {
        if (this._obj.aMerchantID == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please Type MerchantID!" });
        }
        else if (this._obj.aFromDate > this._obj.aToDate) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
            this.goClear();
        }
        else {
            this.goDownloadDetail();
        }
    };
    FrmmerchantreportComponent.prototype.changeInitiatedVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.initiatedby = this.selectedOptions[0];
    };
    FrmmerchantreportComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmmerchantreportComponent.prototype.getPortalFormControl = function (merchantId) {
        var _this = this;
        this._mObject.merchantID = merchantId;
        console.log(" this._mObject.merchantID" + JSON.stringify(this._mObject.merchantID));
        var url = this.ics._apiurl + 'service001/getPortalFormControlByID';
        var json = this._mObject;
        this.http.doPost(url, json).subscribe(function (data) {
            console.log("getPortalFormControl dataObj " + JSON.stringify(data));
            if (data != null) {
                _this._mObject = data;
                _this._obj.processingCode = _this._mObject.processingCode;
                _this._obj.proNotiCode = _this._mObject.proNotiCode;
                _this._obj.proDisCode = _this._mObject.proDisCode;
                if (_this._mObject.detailOrder == "text" || _this._mObject.detailOrder == "lov") {
                    console.log("this._mObject.detailOrder" + _this._mObject.detailOrder);
                    _this._hideDetailTran = false;
                    _this._hideDetailDownload = false;
                }
                //this._obj.templatedata = this._mObject.data;
                if (data.data != null) {
                    if (!(data.data instanceof Array)) {
                        var m = [];
                        m[0] = data.data;
                        _this._mObject.data = m;
                    }
                    else {
                        _this._mObject.data = data.data;
                    }
                    _this._obj.templatedata = [];
                    for (var i = 0; i < _this._mObject.data.length; i++) {
                        if (_this._mObject.data[i].dataType == "lov") {
                            if (!(_this._mObject.data[i].lovData instanceof Array)) {
                                var n = [{ "srno": "1", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }];
                                n[0] = data.data[i].lovData;
                                _this._mObject.data[i].lovData = n;
                            }
                            else {
                                _this._mObject.data[i].lovData = data.data[i].lovData;
                            }
                            if (_this._mObject.processingCode == '01' || _this._mObject.processingCode == '03') {
                                //01 is Telenor 03 is Myanmar Beer
                                //Add ---ALL--- in Lov and item selected that '---ALL---' when initailize load form 
                                if (_this._mObject.data[i].fieldID == 'R4' || _this._mObject.data[i].fieldID == 'R5') {
                                    var lovObj = [{ "srno": "1", "lovCde": "000", "lovDesc1": "ALL", "lovDesc2": "" }];
                                    for (var j = 0; j < _this._mObject.data[i].lovData.length; j++) {
                                        lovObj.push({ "srno": j + 1 + "", "lovCde": _this._mObject.data[i].lovData[j].lovCde, "lovDesc1": _this._mObject.data[i].lovData[j].lovDesc1, "lovDesc2": "" });
                                    }
                                    _this._mObject.data[i].lovData = lovObj;
                                    _this._mObject.data[i].fieldvalue = _this._mObject.data[i].lovData[0].lovCde;
                                    _this._obj.templatedata.push(_this._mObject.data[i]);
                                }
                            }
                        }
                    }
                }
                console.log("Template data " + JSON.stringify(_this._obj.templatedata));
                if (_this._mObject.processingCode == '01') {
                    _this._customerTypeHidden = true;
                }
            }
        });
    };
    FrmmerchantreportComponent.prototype.changePaymentType = function (options, index) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        console.log("lov change " + this.selectedOptions[0]);
        this._obj.templatedata[index].fieldvalue = this.selectedOptions[0];
        if (this._obj.templatedata[index].lovData != null) {
            if (this.selectedOptions[0] == this._obj.templatedata[index].lovData[2].lovCde) {
                //Is PostPaid
                this._customerTypeHidden = false;
            }
            else {
                //Other
                for (var i = 0; i < this._obj.templatedata.length; i++) {
                    if (this._obj.templatedata[i].fieldID == 'R5') {
                        this._obj.templatedata[i].fieldvalue = this._obj.templatedata[i].lovData[0].lovCde;
                    }
                }
                this._customerTypeHidden = true;
            }
        }
    };
    FrmmerchantreportComponent.prototype.changeCustomerType = function (options, index) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        console.log("this.selectedOptions[0] " + this.selectedOptions[0]);
        this._obj.templatedata[index].fieldvalue = this.selectedOptions[0];
    };
    FrmmerchantreportComponent.prototype.changeMMBearyPaymentType = function (options, index) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        console.log("this.selectedOptions[0] " + this.selectedOptions[0]);
        this._obj.templatedata[index].fieldvalue = this.selectedOptions[0];
    };
    FrmmerchantreportComponent.prototype.getmerchantidlist = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            // let merchant = [{ "value": "", "caption": ""}];
            var merchant = [];
            if (_this.ref._lov3.ref015 != null) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                    merchant.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.ref015.length; j++) {
                    merchant.push(_this.ref._lov3.ref015[j]);
                }
            }
            _this.ref._lov3.ref015 = merchant;
            _this._obj.aMerchantID = _this.ref._lov3.ref015[0].value;
            _this._checkmerchant = _this.ref._lov3.ref015[0].value;
            _this._obj.processingCode = _this.ref._lov3.ref015[0].processingCode;
            _this.getAllFeature(_this._obj.aMerchantID);
            console.log("combo data is: " + _this.ref._lov3.ref015[0].caption);
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmmerchantreportComponent.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.status = value;
        for (var i = 1; i < this.ref._lov3.ref013.length; i++) {
            if (this.ref._lov3.ref013[i].value == value) {
                this._obj.status = this.ref._lov3.ref013[i].value;
                break;
            }
        }
    };
    FrmmerchantreportComponent.prototype.getStatus = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getStatus?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref013 = data.ref013;
            // let merchant = [{ "value": "", "caption": ""}];
            var merchant = [];
            if (_this.ref._lov3.ref013 != null) {
                if (!(_this.ref._lov3.ref013 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref013;
                    merchant.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.ref013.length; j++) {
                    merchant.push(_this.ref._lov3.ref013[j]);
                }
            }
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmmerchantreportComponent.prototype.changeInitiated = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.initiatedby = value;
        for (var i = 1; i < this.ref._lov3.ref012.length; i++) {
            if (this.ref._lov3.ref012[i].value == value) {
                this._obj.initiatedby = this.ref._lov3.ref012[i].value;
                break;
            }
        }
    };
    FrmmerchantreportComponent.prototype.getInitiatedBy = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getInitiatedBy?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref012 = data.ref012;
            // let merchant = [{ "value": "", "caption": ""}];
            var merchant = [];
            if (_this.ref._lov3.ref012 != null) {
                if (!(_this.ref._lov3.ref012 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref012;
                    merchant.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.ref012.length; j++) {
                    merchant.push(_this.ref._lov3.ref012[j]);
                }
            }
            _this.ref._lov3.ref012 = merchant;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmmerchantreportComponent.prototype.changeMerchant = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.aMerchantID = value;
        this._checkmerchant = value;
        for (var i = 0; i < this.ref._lov3.ref015.length; i++) {
            if (this.ref._lov3.ref015[i].value == value) {
                this._obj.aMerchantID = this.ref._lov3.ref015[i].value;
                this._obj.processingCode = this.ref._lov3.ref015[i].processingCode;
                break;
            }
        }
        this.getAllFeature(this._obj.aMerchantID);
    };
    FrmmerchantreportComponent.prototype.getAllFeature = function (mId) {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllFeatures?Code=' + mId).subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.refFeature instanceof Array)) {
                        var m = [];
                        m[0] = data.refFeature;
                        _this.ref._lov3.refFeature = m;
                    }
                    else {
                        _this.ref._lov3.refFeature = data.refFeature;
                    }
                }
                else {
                    _this.ref._lov3.refFeature = [{ "value": "", "caption": "-" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmmerchantreportComponent = __decorate([
        core_1.Component({
            selector: 'frmmerchantreport',
            template: " \n<!-- Here -->\n<div class=\"container\">\n       <div class=\"row clearfix\"> \n         <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n          <form class=\"form-horizontal\" (ngSubmit)=\"goList();\"> \n            <fieldset>  \n            <legend>Merchant Transaction Report</legend>\n            \n            <div class=\"form-group\">       \n              <div class=\"col-md-4\">\n                    <button class=\"btn btn-primary\" type=\"button\"  (click)=\"goClear();\" >Clear</button>   \n                    <button class=\"btn btn-primary\" type=\"submit\">List</button>\n                    <button class=\"btn btn-primary\" type=\"button\"  (click)=\" Validation();\" >Download</button>                 \n                </div>         \n            </div>\n      \n              <div class=\"col-md-5\">           \n              <div class=\"form-group\">\n              <label class=\"control-label col-md-4\" style=\"text-align: left;\">Merchant&nbsp; <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                <div class=\"col-md-8\" > \n                    <select [(ngModel)]=\"_obj.aMerchantID\" (change)=\"changeMerchant($event)\"  class=\"form-control col-md-0\" required [ngModelOptions]=\"{standalone: true}\">\n                      <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                    </select> \n                </div>             \n               </div>\n             \n            <div class=\"form-group\">            \n              <label class=\"col-md-4 control-label\" style=\"text-align: left;\"> From Date <font size=\"4\" color=\"#FF0000\" >*</font></label>\n              <div class=\"col-md-8\">\n                     <input  class=\"form-control\" type = \"date\" [(ngModel)]=\"_obj.aFromDate\" required=\"true\" autofocus [ngModelOptions]=\"{standalone: true}\">                              \n              </div>\n            </div>            \n            \n            <div class=\"form-group\">\n              <label class=\"col-md-4 control-label\" style=\"text-align: left;\"> To Date <font size=\"4\" color=\"#FF0000\" >*</font></label>\n              <div class=\"col-md-8\">\n                  <input  class=\"form-control\" type = \"date\" [(ngModel)]=\"_obj.aToDate\" required=\"true\" autofocus [ngModelOptions]=\"{standalone: true}\">                              \n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label class=\"control-label col-md-4\"  style=\"text-align: left;\">Initiated By&nbsp;<font size=\"4\" color=\"#FF0000\" >*</font></label>\n                <div class=\"col-md-8\" > \n                    <select [(ngModel)]=\"_obj.initiatedby\" (change)=\"changeInitiated($event)\"  class=\"form-control col-md-0\" required [ngModelOptions]=\"{standalone: true}\">\n                      <option *ngFor=\"let item of ref._lov3.ref012\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                    </select> \n                </div>             \n               </div>\n               \n                <div class=\"form-group\">\n              <label class=\"control-label col-md-4\"style=\"text-align: left;\">Status &nbsp;<font size=\"4\" color=\"#FF0000\" >*</font></label>\n                <div class=\"col-md-8\" > \n                    <select [(ngModel)]=\"_obj.status\" (change)=\"changeStatus($event)\"  class=\"form-control col-md-0\" required [ngModelOptions]=\"{standalone: true}\">\n                      <option *ngFor=\"let item of ref._lov3.ref013\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                    </select> \n                </div>             \n               </div>               \n                           \n            <div class=\"form-group\">\n              <label class=\"control-label col-md-4\" style=\"text-align: left;\">Feature&nbsp; <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                <div class=\"col-md-8\" > \n                    <select [(ngModel)]=\"_obj.aFeature\"  class=\"form-control col-md-0\" required [ngModelOptions]=\"{standalone: true}\">\n                      <option *ngFor=\"let item of ref._lov3.refFeature\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                    </select> \n                </div>             \n               </div>\n               \n           <div *ngIf=\"_mObject.processingCode == '01'\" >\n           <span *ngFor=\"let item of _obj.templatedata;let i=index;\" > \n            <div *ngIf=\"_obj.templatedata[i].fieldID =='R4'\" class=\"form-group\">                   \n              <label class=\"control-label col-md-2\" align=\"right\">{{_obj.templatedata[i].caption}}</label>\n                <div class=\"col-md-2\" > \n                    <select [(ngModel)]=\"_obj.templatedata[i].fieldvalue\" (change)=\"changePaymentType($event.target.options,i);\"  class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                      <option *ngFor=\"let item2 of item.lovData\" value=\"{{item2.lovCde}}\" >{{item2.lovDesc1}}</option>\n                    </select> \n                </div> \n              </div>  \n              <div *ngIf=\"_obj.templatedata[i].fieldID =='R5'  \" class=\"form-group\" [hidden]=\"_customerTypeHidden\">\n              \n                <label  class=\"control-label col-md-2\" align=\"right\" >{{_obj.templatedata[i].caption}}</label>\n                  <div class=\"col-md-2\" > \n                      <select [(ngModel)]=\"_obj.templatedata[i].fieldvalue\" (change)=\"changeCustomerType($event.target.options,i);\"  class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                        <option *ngFor=\"let item2 of item.lovData\" value=\"{{item2.lovCde}}\" >{{item2.lovDesc1}}</option>\n                      </select>             \n                  </div> \n                </div> \n               </span>\n              </div>\n              \n          <div *ngIf=\"_mObject.processingCode == '03' \" >\n            <span *ngFor=\"let item of _obj.templatedata;let i=index;\" > \n\n            <div *ngIf=\"_obj.templatedata[i].fieldID =='R4'\" class=\"form-group\">\n              <label class=\"control-label col-md-2\" align=\"right\">{{_obj.templatedata[i].caption}}</label>\n                <div class=\"col-md-2\" > \n                    <select [(ngModel)]=\"_obj.templatedata[i].fieldvalue\" (change)=\"changeMMBearyPaymentType($event.target.options,i);\"  class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                      <option *ngFor=\"let item2 of item.lovData\" value=\"{{item2.lovCde}}\" >{{item2.lovDesc1}}</option>\n                    </select> \n                </div> \n            </div>    \n            </span>                   \n        </div> \n      </div>\n      \n             <div class=\"col-md-5\">             \n             <div class=\"form-group\">            \n              \n              <label class=\"col-md-4 control-label\" style=\"text-align: left;\">Debit Account</label>\n              <div class=\"col-md-8\">\n                  <input  class=\"form-control\" type = \"txt\" [(ngModel)]=\"_obj.debitaccount\" autofocus [ngModelOptions]=\"{standalone: true}\">                              \n              </div>\n             </div>\n\n            <div class=\"form-group\">               \n             \n             <label class=\"col-md-4 control-label\" style=\"text-align: left;\">Collection Account</label>\n              <div class=\"col-md-8\">\n                  <input  class=\"form-control\" type = \"txt\" [(ngModel)]=\"_obj.collectionaccount\" autofocus [ngModelOptions]=\"{standalone: true}\">                              \n              </div>\n            \n            </div>\n            \n            <div class=\"form-group\">               \n              \n              <label class=\"col-md-4 control-label\" style=\"text-align: left;\">Customer Code</label>\n              <div class=\"col-md-8\">\n                  <input  class=\"form-control\" type = \"txt\" [(ngModel)]=\"_obj.did\" autofocus [ngModelOptions]=\"{standalone: true}\">                              \n              </div>             \n            </div>\n            \n            <div class=\"form-group\">              \n              \n               <label class=\"col-md-4 control-label\" style=\"text-align: left;\">Transaction Ref No.</label>\n              <div class=\"col-md-8\">\n                  <input  class=\"form-control\" type = \"txt\" [(ngModel)]=\"_obj.transrefno\"  autofocus [ngModelOptions]=\"{standalone: true}\">                              \n              </div>                \n            </div>\n\n            <div class=\"form-group\">\n              <label class=\"control-label col-md-4\" style=\"text-align: left;\">Format<font size=\"4\" color=\"#FF0000\" >*</font></label>\n                <div class=\"col-md-8\" > \n                    <select [(ngModel)]=\"_obj.select\"  class=\"form-control col-md-0\"  [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeSelectVal($event.target.options)\">\n                      <option *ngFor=\"let item of ref._lov3.ref010\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                    </select> \n                </div>             \n               </div>\n               \n            <div class=\"form-group\">\n              <rp-input rpClass=\"col-md-4\" rpLabelClass =\"col-md-3 control-label\"  rpType=\"ref010\"  [(rpModel)]=\"_obj.select\" (change)=\"changeSelectVal($event.target.options)\"></rp-input>\n            </div>            \n             </div> \n             \n                          \n           </fieldset>\n          </form> \n         </div>\n       </div>\n     </div>\n    \n   \n    <div [hidden]=\"_entryhide\">\n      <div class=\"container\">\n      <div class=\"row clearfix\">\n       <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n        <form  class=\"form-horizontal\"> \n            <div class=\"form-group\">\n             <legend>Transaction List</legend>    \n            </div>                       \n                <div class=\"form-group\">\n                   <div class=\"form-group\">\n                      <div class=\"col-md-9\">  \n                        <pager id=\"pgsupplier\" rpPageSizeMax=\"100\" [(rpModel)]=\"_totalcount\" (rpChanged)=\"changedPager($event)\"></pager>\n                      </div>                    \n                        <table style=\"align:right;font-size:14px\" cellpadding=\"10px\" cellspacing=\"10px\">                        \n                            <tbody>\n                                  <colgroup>\n                                  <col span=\"1\" style=\"width: 50%;\">\n                                  <col span=\"1\" style=\"width: 50%;\">                                  \n                                  </colgroup>                           \n                        \n                              <tr>\n                              <td style=\"color:blue ; text-align:left\">\n                              <label >Grand Total Amount &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</label>\n                              </td>\n                              <td style=\"color:blue;display:block; text-align:right; \">\n                                <label >{{_grandAmtTotal}}</label>\n                            </td>\n                            </tr>\n                            \n                             <tr>\n                   <td style=\"color:blue \">\n                    <label >Grand Total Service Charges &nbsp;:&nbsp;</label>  \n                     \n                   </td>\n                   <td  style=\"color:blue;display:block; text-align:right; \">\n                      <label >{{_serviceAmt}}</label>\n                   </td>\n                   </tr>\n               \n                    <tr>\n                    <td style=\"color:blue ; text-align:left\">\n                    <label >Grand Total Charges &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</label>\n                    </td>\n                    <td style=\"color:blue;display:block; text-align:right; \">\n                      <label >{{_incomeAmt}}</label>\n                   </td>\n                   </tr>\n                   </tbody>\n                 </table>\n               </div>\n               \n                <!--    <div  style=\"overflow-x:auto;\">\n                    <table class=\"table table-striped\">\n                        <thead>\n                        <tr></tr>\n                          <tr>\n                           <th align=\"center\"></th>\n                            <th align=\"center\">Transaction Date</th>\n                            <th align=\"center\">Transaction Reference No.</th>\n                            <th align=\"center\" *ngIf = \"_obj.processingCode == '080400' \" >Transaction Tax Reference No.</th>\n                            <th align=\"center\">Debit Account</th>\n                            <th align=\"center\">Collection Account</th>\n                            <th align=\"center\">Customer Code</th>\n                            <th align=\"center\">Customer Name</th>\n                            <th align=\"center\">Initiated By</th>\n                            <th align=\"center\">Amount</th>\n                            <th align=\"center\">Service Charges</th>\n                            \n                            <th align=\"center\" *ngIf = \"_obj.processingCode == '080400' \" >Commercial Tax</th>\n                            <th align=\"center\" *ngIf = \"_obj.processingCode != '080400' \" >Deduction Amount</th>\n                             \n                            <th align=\"center\">TPS Message Code</th>\n                            <th align=\"center\">TPS Message Description</th> \n                            <th *ngIf=\"_mObject.proDisCode == '01' \">Distributor Name</th>\n                            <th *ngIf=\"_mObject.proDisCode == '01' \">Distributor Address</th>\n                          </tr>\n                        </thead>\n                        <tbody>\n                          <tr *ngFor=\"let obj of _list.data\">\n                              <td>\n                              <div  [hidden] =\"_hideDetailTran\">\n                            <button class=\"btn btn-primary\" type=\"submit\"  (click)=\"showdetailspopup(obj.syskey);\" >Show Details</button>\n                            </div>\n                          </td>\n                            \n                            <td align=\"center\">{{obj.transactiondate}}</td>\n                            <td align=\"center\">{{obj.tranrefnumber}}</td> \n                            <td align=\"center\"  *ngIf = \"_obj.processingCode == '080400' \" >{{obj.trantaxrefnumber}}</td> \n                            \n                            <td align=\"center\">{{obj.debitaccount}}</td>\n                            <td align=\"center\">{{obj.collectionaccount}}</td>\n                            <td align=\"left\">{{obj.did}}</td>\n                            <td align=\"left\">{{obj.customername}}</td>   \n                             <td align=\"center\" >{{obj.branch}}</td>                                       \n                            <td align = \"right\">{{obj.amountst}}</td>\n                            <td align = \"right\">{{obj.commchargest}}</td>\n                            <td align = \"right\">{{obj.commuchargest}}</td>\n                            <td align = \"left\">{{obj.messageCode}}</td>\n                            <td align = \"left\">{{obj.messageDesc}}</td>\n                             \n                            <td *ngIf=\"_mObject.proDisCode == '01' \" align = \"left\">{{obj.distributorName}}</td>\n                            <td *ngIf=\"_mObject.proDisCode == '01' \" align = \"left\">{{obj.distributorAddress}}</td>\n                          </tr> \n                        </tbody>                     \n                      </table> \n                      </div>   -->\n                      \n                      <div  style=\"overflow-x:auto;\">\n                      <!-- <table class=\"table table-striped\"> -->\n                      <table  class=\"table table-striped table-condense table-hover tblborder\" style=\"font-size: 16px;\" >\n                         <thead>\n                             <tr>\n                               <!-- <th align=\"center\">Transaction Date</th> \n                               <th align=\"center\">XRef</th>\n                               <th align=\"center\">Transaction Reference No. 1</th>\n                               <th align=\"center\">Transaction Reference No. 2</th>\n                               <th align=\"center\">Debit Account</th>\n                               <th align=\"center\">Collection Account</th>\n                               <th align=\"center\">Customer Code</th>\n                               <th align=\"center\">Customer Name</th>\n                               <th align=\"center\">Initiated By</th>\n                               <th align=\"right\">Amount</th>\n                               <th align=\"right\">Service Charges</th>\n                               <th align=\"right\" *ngIf = \"_obj.processingCode == '080400' \" >Commercial Tax</th>\n                               <th align=\"right\" *ngIf = \"_obj.processingCode != '080400' \" >Deduction Amount</th>\n                               <th align=\"center\">TPS Message Code</th>\n                               <th align=\"center\">TPS Message Description</th> \n                               <th align=\"center\">Feature</th> -->\n   \n                               <th align=\"center\"><div style=\"width:150\">Transaction Date</div></th>\n                               <th align=\"center\"><div style=\"width:100\">XRef</div></th>\n                               \n                               <th align=\"center\"  *ngIf = \"_obj.processingCode != '080400'\"><div style=\"width:220\">Transaction Reference No. </div></th>\n                               \n                               <th align=\"center\"  *ngIf = \"_obj.processingCode == '080400'\"><div style=\"width:220\">Transaction Reference No. 1</div></th>\n                               <th align=\"center\"  *ngIf = \"_obj.processingCode == '080400'\"><div style=\"width:220\" >Transaction Reference No. 2</div></th>\n   \n                               <th align=\"center\"><div style=\"width:140\">Debit Account</div></th>\n                               <th align=\"center\"><div style=\"width:150\">Collection Account</div></th>\n                               <th align=\"center\"><div style=\"width:150\">Customer Code</div></th>\n                               <th align=\"center\"><div style=\"width:150\">Customer Name</div></th>\n                               <th align=\"center\"><div style=\"width:100\">Initiated By</div></th>\n                               <th align=\"right\"><div style=\"width:100\">Amount</div></th>\n                               <th align=\"right\"><div style=\"width:150\">Service Charges</div></th>\n                               <th align=\"right\" *ngIf = \"_obj.processingCode == '080400' \" ><div style=\"width:150\">Commercial Tax</div></th>\n                               <th align=\"right\" *ngIf = \"_obj.processingCode != '080400' \" ><div style=\"width:150\">Deduction Amount</div></th>\n                               <th align=\"center\"><div style=\"width:200\">TPS Message</div></th>\n                               <th align=\"center\"><div style=\"width:150\">Feature</div></th>\n   \n                             </tr>\n                           </thead>\n                           <tbody>\n                             <tr *ngFor=\"let obj of _list.data\">\n                               <td align=\"left\">{{obj.transactiondate}}</td>\n                               <td align=\"left\">{{obj.xref}}</td>\n                               \n                               <td align=\"left\" >{{obj.tranrefnumber}}</td> \n                               <td align=\"left\" *ngIf = \"_obj.processingCode == '080400' \" >{{obj.trantaxrefnumber}}</td> \n                               \n                               <td align=\"left\">{{obj.debitaccount}}</td>\n                               <td align=\"left\">{{obj.collectionaccount}}</td>\n                               <td align=\"left\">{{obj.did}}</td>\n                               <td align=\"left\">{{obj.customername}}</td>   \n                               <td align=\"left\" >{{obj.branch}}</td>                                       \n                               <td align = \"right\">{{obj.amountst}}</td>\n                               <td align = \"right\">{{obj.commchargest}}</td>\n                               <td align = \"right\">{{obj.commuchargest}}</td>\n                               <td align = \"left\">{{obj.messageCode}} : {{obj.messageDesc}} </td>                             \n                               <td  align=\"left\" >{{obj.feature}}</td>\n                             </tr> \n                           </tbody>                     \n                         </table> \n                         </div> \n                   </div>\n            <!--     </fieldset>  -->\n                </form>\n               </div>\n              </div>\n            </div>\n\t\t\t</div>\n \n <!-- End -->\n \n <!-- Start Popup -->\n \n <div id=\"lu001popup\" class=\"modal fade\" role=\"dialog\">\n      <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 id=\"lu001popuptitle\" class=\"modal-title\">Details of Myanmar Brewery</h4>\n          </div> \n          <div id=\"lu001popupbody\" class=\"modal-body\"> \n        <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th>SrNo</th>\n                <th>Items</th> \n                <th>Price</th>\n                <th>Quantity</th>   \n                <th>Amount</th>                \n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let obj of _listpopup.arr \">\n                <td> {{obj.srno}}</td>\n                <td>{{obj.description}}</td>\n                <td> {{obj.price}}</td>\n                <td>{{obj.quantity}}</td>\n                <td> {{obj.amount}}</td>               \n              </tr>\n            </tbody>\n          </table>  \n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n          </div>\n        </div>\n      </div>\n    </div> \n \n  <!-- End -->\n\n<div id=\"sessionalert\" class=\"modal fade\">\n  <div class=\"modal-dialog\">\n  <div class=\"modal-content\">\n    <div class=\"modal-body\">\n    <p>{{_sessionMsg}}</p>\n    </div>\n    <div class=\"modal-footer\">\n    </div>\n  </div>\n  </div>\n  </div>\n   ",
            // directives: [RpInputComponent, Pager],
            providers: [rp_http_service_1.RpHttpService]
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmmerchantreportComponent);
    return FrmmerchantreportComponent;
}());
exports.FrmmerchantreportComponent = FrmmerchantreportComponent;
//# sourceMappingURL=frmmerchantreport.component.js.map