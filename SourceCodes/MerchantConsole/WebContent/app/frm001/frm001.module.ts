import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';
import { PagerModule } from '../util/pager.module';
import { AdvancedSearchModule } from '../util/advancedsearch.module';

import {FrmTransCodeComponent} from './frmTransCode.component';
import {FrmTransCodeListComponent} from './frmTransList.component';
import {FrmStockComponent} from './frmStock.component';
import {FrmStockListComponent} from './frmStockList.component'

import { frm001Routing }     from './frm001.routing';
import { RpHttpService } from '../framework/rp-http.service';
import { ClientUtil } from '../util/rp-client.util';
import {KeyValuePipe} from '../util/pipe';
import { CustomBrowse} from '../util/custom-browse.component';
import {FrmmerchantreportComponent} from './frmmerchantreport.component';
import {FrmChangePwd} from './frmchangepwd.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,
    AdvancedSearchModule,
    PagerModule,
    frm001Routing
  ],
  exports : [],
  declarations: [
     FrmTransCodeComponent,
     FrmTransCodeListComponent,
     FrmStockComponent,
     FrmStockListComponent,  
     FrmmerchantreportComponent, 
     FrmChangePwd, 
     KeyValuePipe,
     CustomBrowse
  ],
  providers: [
    RpHttpService,
    ClientUtil
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Frm001Module {}