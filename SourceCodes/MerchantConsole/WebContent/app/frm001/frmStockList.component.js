"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmStockListComponent = (function () {
    function FrmStockListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._adsearch = []; // advance search list
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._util = new rp_client_util_1.ClientUtil();
        this._array = []; // datalist to show on grid
        this._srchobj = { "pager": this.getDefaultPager(), "search": [] };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._srchobj.pager = this.getDefaultPager();
            this.search(this._srchobj);
        }
    }
    FrmStockListComponent.prototype.search = function (p) {
        var _this = this;
        if (p.pager.end == 0) {
            p.pager.end = this.ics._profile.n1;
        }
        if (p.pager.size == 0) {
            p.pager.size = this.ics._profile.n1;
        }
        var url = this.ics._apiurl + 'serviceStock/search?searchVal=' + this._searchVal + "&sort=" + this._sorting._sort_type + "&type=" + this._sorting._sort_col;
        var json = p;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null && data != undefined && data.state) {
                _this._srchobj.pager.totalcount = data.totalCount;
                _this._array = data.dataList;
            }
            else {
                _this._array = [];
                _this._srchobj.pager.totalcount = 1;
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
            }
        }, function (error) {
            return function () { };
        });
    };
    FrmStockListComponent.prototype.goto = function (p) {
        this._router.navigate(['/stock', 'read', p]);
    };
    FrmStockListComponent.prototype.goNew = function () {
        this._router.navigate(['/stock', 'new']);
    };
    FrmStockListComponent.prototype.changeStringtoDate = function (k) {
        return this._util.changeStringtoDateDDMMYYYY(k);
    };
    FrmStockListComponent.prototype.formatNumber = function (n) {
        return this._util.formatMoney(n);
    };
    FrmStockListComponent.prototype.getDefaultPager = function () {
        return { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    };
    FrmStockListComponent.prototype.searchVal = function () {
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = [];
        this.search(this._srchobj);
    };
    FrmStockListComponent.prototype.changeAS = function (event) {
        this._adsearch = event;
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = this._adsearch;
        this.search(this._srchobj);
    };
    FrmStockListComponent.prototype.hideShowAS = function (event) {
        this._flagas = event;
    };
    FrmStockListComponent.prototype.changedPager = function (event) {
        var k = event.flag;
        this._srchobj.pager = event.obj;
        this._srchobj.search = this._adsearch;
        if (k) {
            this.search(this._srchobj);
        }
    };
    // list sorting part
    FrmStockListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmStockListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                this.search(this._srchobj);
                break;
            }
        }
    };
    FrmStockListComponent = __decorate([
        core_1.Component({
            selector: 'pr-stocklist',
            template: " \n  <div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n    \n    <!-- Form Name -->\n    <legend>Stock List</legend>\n\n      <div *ngIf=\"_flagas\" class=\"input-group\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t      </span>  \n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)= \"searchVal()\"  class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searchVal()\" >\n\t          <span class=\"glyphicon glyphicon-search\"></span>Search\n\t        </button>\n\t      </span>        \n\t    </div>      \n    </form>\n    <div style = \"margin-top : 10px\">\n   \n   <ad-search-stocklist [(rpModel)]=\"_adsearch\" (rpHidden)=\"hideShowAS($event)\" (rpChanged)=\"changeAS($event)\"></ad-search-stocklist>\n\n   <pager id=\"pgcarrier\" [(rpModel)]=\"_srchobj.pager.totalcount\" [(rpCurrent)]=\"_srchobj.pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n    \n  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n     <tr>\n      <th>Code  <img src=\"image/sortAsc.png\" [hidden]=\"_col_list[0].flag\" alt=\"sortAsc.png\" height=\"12\" width=\"15\" (click)=\"addSort(1)\"/>\n                        <img src=\"image/sortDesc.png\" [hidden]=\"!_col_list[0].flag\" alt=\"sortDesc.png\" height=\"12\" width=\"15\" (click)=\"addSort(1)\"/></th>\n      <th>Description  <img src=\"image/sortAsc.png\" [hidden]=\"_col_list[1].flag\" alt=\"sortAsc.png\" height=\"12\" width=\"15\" (click)=\"addSort(2)\"/>\n                        <img src=\"image/sortDesc.png\" [hidden]=\"!_col_list[1].flag\" alt=\"sortDesc.png\" height=\"12\" width=\"15\" (click)=\"addSort(2)\"/></th>      \n      <th>Price</th>\n      <th>Effective Date</th>\n      <th>Trans. Code</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let obj of _array\">\n      <td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n      <td>{{obj.t2}}</td>\n      <td style=\"text-align:right;\">{{formatNumber(obj.n2)}}</td>\n      <td>{{changeStringtoDate(obj.t3)}} - {{changeStringtoDate(obj.t4)}}</td>\n      <td>{{obj.transCode}}</td>       \n    </tr> \n    </tbody>\n  </table>\n  </div> \n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmStockListComponent);
    return FrmStockListComponent;
}());
exports.FrmStockListComponent = FrmStockListComponent;
//# sourceMappingURL=frmStockList.component.js.map