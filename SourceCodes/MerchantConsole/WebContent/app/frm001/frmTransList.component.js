"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmTransCodeListComponent = (function () {
    function FrmTransCodeListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._adsearch = []; // advance search list
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._util = new rp_client_util_1.ClientUtil();
        this._array = []; // datalist to show on grid
        this._srchobj = { "pager": this.getDefaultPager(), "search": [] };
        this._new_pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this._existing_pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this._error_pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this._newList = [];
        this._existingList = [];
        this._errorList = [];
        this._showGrid = true;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._srchobj.pager = this.getDefaultPager();
            this.search(this._srchobj);
        }
    }
    FrmTransCodeListComponent.prototype.search = function (p) {
        var _this = this;
        if (p.pager.end == 0) {
            p.pager.end = this.ics._profile.n1;
        }
        if (p.pager.size == 0) {
            p.pager.size = this.ics._profile.n1;
        }
        var url = this.ics._apiurl + 'serviceTransCode/search?searchVal=' + this._searchVal + "&sort=" + this._sorting._sort_type + "&type=" + this._sorting._sort_col;
        var json = p;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null && data != undefined && data.state) {
                _this._srchobj.pager.totalcount = data.totalCount;
                _this._array = data.dataList;
            }
            else {
                _this._array = [];
                _this._srchobj.pager.totalcount = 1;
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
            }
        }, function (error) {
            return function () { };
        });
    };
    FrmTransCodeListComponent.prototype.goto = function (p) {
        this._router.navigate(['/transcode', 'read', p]);
    };
    FrmTransCodeListComponent.prototype.goNew = function () {
        this._router.navigate(['/transcode', 'new']);
    };
    FrmTransCodeListComponent.prototype.alterShowGrid = function () {
        this._showGrid = !this._showGrid;
    };
    FrmTransCodeListComponent.prototype.transUploadEvent = function (event) {
        var _this = this;
        this.clearpreviewData();
        this._sheetNo = -1;
        this._sheets = "";
        var url;
        var json;
        this._fileName = event.fileName;
        if (this._fileName.endsWith("xlsx") || this._fileName.endsWith('xls')) {
            url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName;
            this.http.upload(url, event.file).subscribe(function (data) {
                if (data.code == "SUCCESS") {
                    _this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": "Upload Successfully!" });
                    url = _this.ics._apiurl + 'file/getSheets';
                    json = { "fn": _this._fileName, "fp": "upload" };
                    _this.http.doPost(url, json).subscribe(function (data) {
                        _this._sheets = data;
                    }, function (error) {
                    });
                }
                else {
                    _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Upload Unsuccessfully!" });
                }
            }, function (error) {
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Transaction" });
            });
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Invalid File" });
            this.clearpreviewData();
        }
    };
    FrmTransCodeListComponent.prototype.clearpreviewData = function () {
        this._newList = [];
        this._errorList = [];
        this._existingList = [];
    };
    FrmTransCodeListComponent.prototype.previewData = function () {
        var _this = this;
        if (this._sheetNo >= 0) {
            var url_1;
            var json_1;
            url_1 = this.ics._apiurl + 'serviceTransCode/isCorrectExcel';
            json_1 = { "fn": this._fileName, "fp": "upload", "sn": this._sheetNo };
            this.http.doPost(url_1, json_1).subscribe(function (data) {
                if (data.state == true) {
                    url_1 = _this.ics._apiurl + 'serviceTransCode/getExcelData';
                    _this.http.doPost(url_1, json_1).subscribe(function (data) {
                        if (_this._new_pager.end == 0) {
                            _this._new_pager.end = _this.ics._profile.n1;
                        }
                        if (_this._new_pager.size == 0) {
                            _this._new_pager.size = _this.ics._profile.n1;
                        }
                        if (_this._existing_pager.end == 0) {
                            _this._existing_pager.end = _this.ics._profile.n1;
                        }
                        if (_this._existing_pager.size == 0) {
                            _this._existing_pager.size = _this.ics._profile.n1;
                        }
                        if (_this._error_pager.end == 0) {
                            _this._error_pager.end = _this.ics._profile.n1;
                        }
                        if (_this._error_pager.size == 0) {
                            _this._error_pager.size = _this.ics._profile.n1;
                        }
                        _this._newList = data.newd;
                        _this._existingList = data.existing;
                        _this._errorList = data.error;
                    });
                }
                if (data.state == false) {
                    _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Invalid Sheet" });
                    _this.clearpreviewData();
                }
            }, function (error) {
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Sheets" });
            });
        }
    };
    FrmTransCodeListComponent.prototype.import = function () {
        var _this = this;
        var url;
        var json;
        if (this._newList.length > 0 || this._existingList.length > 0) {
            url = this.ics._apiurl + 'serviceTransCode/import';
            json = { "newd": this._newList, "existing": this._existingList };
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.state == true) {
                    _this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": "Imported Successfully." });
                }
                else {
                    _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Import Fail." });
                }
            }, function (error) {
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Import Fail." });
            });
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "No data to import." });
        }
    };
    FrmTransCodeListComponent.prototype.export = function () {
        var _this = this;
        var url;
        url = this.ics._apiurl + 'serviceTransCode/checkData';
        this.http.doGet(url).subscribe(function (data) {
            if (data.state == true) {
                _this.iframe.nativeElement.src = _this.ics._apiurl + 'serviceTransCode/exportData';
            }
            else {
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "No data." });
            }
        }, function (error) {
        });
    };
    FrmTransCodeListComponent.prototype.changedNewPager = function (event) {
        var k = event.flag;
        if (k) {
            this._new_pager = event.obj;
        }
    };
    FrmTransCodeListComponent.prototype.changedExistingPager = function (event) {
        var k = event.flag;
        if (k) {
            this._existing_pager = event.obj;
        }
    };
    FrmTransCodeListComponent.prototype.changedErrorPager = function (event) {
        var k = event.flag;
        if (k) {
            this._error_pager = event.obj;
        }
    };
    FrmTransCodeListComponent.prototype.getDefaultPager = function () {
        return { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    };
    FrmTransCodeListComponent.prototype.searchVal = function () {
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = [];
        this.search(this._srchobj);
    };
    FrmTransCodeListComponent.prototype.changeAS = function (event) {
        this._adsearch = event;
        this._srchobj.pager = this.getDefaultPager();
        this._srchobj.search = this._adsearch;
        this.search(this._srchobj);
    };
    FrmTransCodeListComponent.prototype.hideShowAS = function (event) {
        this._flagas = event;
    };
    FrmTransCodeListComponent.prototype.changedPager = function (event) {
        var k = event.flag;
        this._srchobj.pager = event.obj;
        this._srchobj.search = this._adsearch;
        if (k) {
            this.search(this._srchobj);
        }
    };
    // list sorting part
    FrmTransCodeListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmTransCodeListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                this.search(this._srchobj);
                break;
            }
        }
    };
    __decorate([
        core_1.ViewChild('iframe'), 
        __metadata('design:type', core_1.ElementRef)
    ], FrmTransCodeListComponent.prototype, "iframe", void 0);
    FrmTransCodeListComponent = __decorate([
        core_1.Component({
            selector: 'pr-transcodelist',
            template: " \n    <div *ngIf=\"_showGrid\">\n        <div class=\"container col-md-12\">\n            <form class=\"form-inline\">\n                <legend>Trans. Code List</legend>\n\n                <div *ngIf=\"_flagas\" class=\"input-group\">\n                    <span class=\"input-group-btn input-md\">\n                        <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                    </span>  \n                    <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)= \"searchVal()\"  class=\"form-control input-md\">\n                    <span class=\"input-group-btn input-md\">\n                        <button class=\"btn btn-primary input-md\" type=\"button\" style=\"border-top-left-radius: 0px;border-bottom-left-radius: 0px;\" (click)=\"searchVal()\" >\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                        </button>\n                    </span>  \n\n                    <div style=\"float: left; padding:4px;\">\n                        <a (click)=\"alterShowGrid()\"> Import/Export</a>\n                    </div> \n                </div> \n               \n            </form>\n            <div style = \"margin-top : 10px\">\n                <ad-search-transcode [(rpModel)]=\"_adsearch\" (rpHidden)=\"hideShowAS($event)\" (rpChanged)=\"changeAS($event)\"></ad-search-transcode>\n            </div>\n            <pager id=\"pgcarrier\" [(rpModel)]=\"_srchobj.pager.totalcount\" [(rpCurrent)]=\"_srchobj.pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n        \n            <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n                <thead>\n                    <tr>\n                        <th>Code  <img src=\"image/sortAsc.png\" [hidden]=\"_col_list[0].flag\" alt=\"sortAsc.png\" height=\"12\" width=\"15\" (click)=\"addSort(1)\"/>\n                                    <img src=\"image/sortDesc.png\" [hidden]=\"!_col_list[0].flag\" alt=\"sortDesc.png\" height=\"12\" width=\"15\" (click)=\"addSort(1)\"/></th>\n                        <th>Description  <img src=\"image/sortAsc.png\" [hidden]=\"_col_list[1].flag\" alt=\"sortAsc.png\" height=\"12\" width=\"15\" (click)=\"addSort(2)\"/>\n                                    <img src=\"image/sortDesc.png\" [hidden]=\"!_col_list[1].flag\" alt=\"sortDesc.png\" height=\"12\" width=\"15\" (click)=\"addSort(2)\"/></th>      \n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let obj of _array\">\n                    <td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n                    <td>{{obj.t2}}</td>       \n                    </tr> \n                </tbody>\n            </table>\n        </div> \n    </div>\n\n    <!-- Import/Export -->\n    <div *ngIf=\"!(_showGrid)\" style=\"padding:4px;margin:5px;\">\n            <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"alterShowGrid()\" >Back</button>\n            <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"export()\" >Export</button>\n            <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"import()\" >Import</button>\n            <custom-browse (cuBrowse)=\"transUploadEvent($event)\"></custom-browse>\n        \n            <div class=\"input-group stylish-input-group\" style=\"width: 300px;margin-top : 10px\">\n                <select class=\"form-control input-sm\" [(ngModel)]=\"_sheetNo\" style=\"width: 230px;height:34px\">\n                    <option *ngFor=\"let sheet of _sheets | keyValuePipe\" value=\"{{sheet.key}}\">{{sheet.value}}</option>\n                </select> \n                <span class=\"input-group-btn input-md\"> \n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"previewData()\">Preview </button>\n                </span>\n            </div>\n            \n            <div>\n                <!-- Tab -->\n                <ul class=\"nav nav-tabs\" style = \"margin-top : 10px\">\n                <li class=\"active\"><a data-toggle=\"tab\" href=\"#tab1\"><i class=\"glyphicon glyphicon-list\"></i> New Data</a></li>\n                <li><a data-toggle=\"tab\" href=\"#tab2\"><i class=\"glyphicon glyphicon-list\"></i> Existing Data</a></li> \n                <li><a data-toggle=\"tab\" href=\"#tab3\"><i class=\"glyphicon glyphicon-list\"></i> Error Data</a></li> \n                </ul>\n\n                <div class=\"tab-content\">\n                    <div id=\"tab1\" class=\"tab-pane fade in active\">\n                        <div>\n                            <pager id=\"pgcarrier\" [(rpModel)]=\"_newList.length\" [(rpCurrent)]=\"_new_pager.current\" (rpChanged)=\"changedNewPager($event)\"></pager> \t\n                            <table class=\"table table-striped table-condensed table-hover table-bordered\"\n                                style=\"width: 70%\">\n                                <thead>\n                                    <tr>\n                                        <th style=\"width: 20%\">Code</th>\n                                        <th style=\"width: 80%\">Description</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let newData of _newList | slice:_new_pager.start-1:_new_pager.end \">\n                                        <td>{{newData.t1}}</td>\n                                        <td>{{newData.t2}}</td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                    <div id=\"tab2\" class=\"tab-pane fade\">  \n                        <div>\n                            <pager id=\"pgcarrier\" [(rpModel)]=\"_existingList.length\" [(rpCurrent)]=\"_existing_pager.current\" (rpChanged)=\"changedExistingPager($event)\"></pager> \t\n                            <table class=\"table table-striped table-condensed table-hover table-bordered\"\n                                style=\"width: 70%\">\n                                <thead>\n                                    <tr>\n                                        <th style=\"width: 20%\">Code</th>\n                                        <th style=\"width: 80%\">Description</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let exData of _existingList | slice:_existing_pager.start-1:_existing_pager.end \" >\n                                        <td>{{exData.t1}}</td>\n                                        <td>{{exData.t2}}</td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div> \n                    <div id=\"tab3\" class=\"tab-pane fade\">  \n                        <div>\t\t\n                            <pager id=\"pgcarrier\" [(rpModel)]=\"_errorList.length\" [(rpCurrent)]=\"_error_pager.current\" (rpChanged)=\"changedErrorPager($event)\"></pager> \t\n                            \n                            <table class=\"table table-striped table-condensed table-hover table-bordered\"\n                                style=\"width: 70%\">\n                                <thead>\n                                    <tr>\n                                        <th style=\"width: 20%\">Code</th>\n                                        <th style=\"width: 80%\">Description</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let erData of _errorList | slice:_error_pager.start-1:_error_pager.end \">\n                                        <td>{{erData.t1}}</td>\n                                        <td>{{erData.t2}}</td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div> \n                </div>\n            </div>\n\n    \t<div style=\"display: none;width: 0px;height: 0px;\">\n            <iframe #iframe ></iframe>\n        </div>\n\t</div>    \n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmTransCodeListComponent);
    return FrmTransCodeListComponent;
}());
exports.FrmTransCodeListComponent = FrmTransCodeListComponent;
//# sourceMappingURL=frmTransList.component.js.map