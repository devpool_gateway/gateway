import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription'; 
import {RpIntercomService} from '../framework/rp-intercom.service';
import {RpInputComponent} from '../framework/rp-input.component';
import {RpHttpService} from '../framework/rp-http.service';
import {RpBean} from '../framework/rp-bean';
import {RpReferences} from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';  

enableProdMode(); 
@Component({
  selector: 'pr-stock',
  template: `
    <div class="container">
    <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
    <form class= "form-horizontal"> 
    <!-- Form Name -->
    <legend>Stock</legend>
    <div class="row  col-md-12"> 
      <button class="btn btn-primary" *ngIf="_rights._new" id="new" type="button" (click)="goNew()">New</button>      
      <button class="btn btn-primary" *ngIf="_rights._save" id="save" type="button" (click) = "goSave();" >Save</button> 
      <button class="btn btn-primary" *ngIf="_rights._delete" [disabled]="_btn_flag._delete" id="delete" type="button" (click)="goDelete();" >Delete</button> 
      <button class="btn btn-primary" *ngIf="_rights._list" type="button"   (click)="goList()">List</button>  
    </div>
    <div class="row col-md-12">&nbsp;</div>
    <div class="form-group">
        <rp-input [(rpModel)]="_obj.t1" rpRequired ="true" rpType="text" rpLabel="Code" autofocus></rp-input>
    </div>

    <div class="form-group">
        <rp-input [(rpModel)]="_obj.t2" rpRequired ="true" rpType="text" rpLabel="Description" autofocus></rp-input>
    </div>

    <div class="form-group">
        <rp-input [(rpModel)]="_obj.n2" rpRequired ="true" rpType="number" rpLabel="Price" autofocus></rp-input>
    </div>

    <div class="form-group">
        <label class="col-md-2">Efective Date</label>
        <div class="col-md-2"> 
            <input [(ngModel)]="_dates.date1" required type="date" autofocus class="form-control input-md" [ngModelOptions]="{standalone: true}"/>
        </div>
        <div class="col-md-2"> 
            <input [(ngModel)]="_dates.date2" required type="date" autofocus class="form-control input-md" [ngModelOptions]="{standalone: true}"/>
        </div>
    </div>

    <div class="form-group">
        <rp-input [(rpModel)]="_obj.n1" rpRequired ="true" rpType="ref003" rpLabel="Trans. Code" autofocus></rp-input>
    </div>
    </form>
    </div>
    </div>
    </div>
  `
})
export class FrmStockComponent {
    // RP Framework 
    subscription : Subscription;
    sub : any;
    _obj = this.getDefaultObj();
    _btn_flag = {"_delete" : false }
    _rights = {"_new":false,"_save":false,"_delete":false,"_list":false};
    _dates = {"date1":"", "date2":""};
    _util: ClientUtil = new ClientUtil();
    constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0){
            this._router.navigate(['/login']);
        } else {
            this._obj = this.getDefaultObj();
            this._btn_flag._delete = true;
            this.setBtns();
            this.getTransCode();
        }
    }
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                this.goNew();
            } else if (cmd != null && cmd != "" && cmd == "read") {
                let id = params['id'];
                this.goReadBySyskey(id);
            }
        });
    }
    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    getDefaultObj(){
        this. _dates = {"date1":"", "date2":""};
        return {"syskey":0,"autokey":0,"createdDate":"","modifiedDate":"","userId":"","userName":"","recordStatus":0,"syncStatus":0,"syncBatch":0,"userSyskey":0,"t1":"","t2":"","t3":"","t4":"","n1":0,"n2":0.0,"transCode":""};
    }

    getTransCode(){
        this.http.doGet(this.ics._apiurl + 'serviceTransCode/getAllData').subscribe(
            response => {
                if (response != null && response != undefined) {
                    this.ref._lov3.ref003 = response.data;
                } else {
                    this.ref._lov3.ref003 = [{ "value": "", "caption": "" }];
                }
            },
            error => { },
            () => { }
        );
    }
    goReadBySyskey(p) {
        this.http.doGet(this.ics._apiurl + 'serviceStock/readBySyskey?key=' + p).subscribe(
            data => {
                this. _dates.date1 = this._util.changeStringtoDateTime(data.t3);
                this. _dates.date2 = this._util.changeStringtoDateTime(data.t4);
                this._obj = data;
                this._btn_flag._delete = false;
            },
            error => { },
            () => { }
        );
    }
    goNew() {
        this._btn_flag._delete = true;
        this._obj = this.getDefaultObj();
    }
    goSave() {
        let url: string = this.ics._apiurl + 'serviceStock/save';
        this._obj.t3 = this._util.changeDatetoString(this._dates.date1);
        this._obj.t4 = this._util.changeDatetoString(this._dates.date2);

        let json: any = this._obj;
        this.http.doPost(url, json).subscribe(
            data => {
               this._obj.syskey = data.longResult[0];
               this._obj.createdDate = data.stringResult[0]; 
               this._btn_flag._delete = false;
               this.showMessage(data.msgDesc, data.state);
            },
            error => {
                this.showMessage("Can't Saved This Record!!!", undefined);
            },
            () => { }
        );
    }
    goDelete() {
        if (this._obj.syskey != 0) {
            let url: string = this.ics._apiurl + 'serviceStock/delete';
            let json: any = this._obj;
            this.http.doPost(url, json).subscribe(
                data => {
                    this.showMessage(data.msgDesc, data.state);
                    if (data.state) {
                        this.goNew();
                    }
                },
                error => { },
                () => { }
            );
        } else {
            this.showMessage("No Record to Delete!", undefined);
        }
    }
    goList() {
        this._router.navigate(['/stocklist']);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
    }
    setBtns(){
        let k = this.ics.getBtns("/transcode");
        if (k != "" && k != undefined) {
            let strs = k.split(",");
            for (let i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this._rights._new = true;
                } else if (strs[i] == "2") {
                    this._rights._save = true;
                } else if (strs[i] == "3") {
                    this._rights._delete = true;
                } else if (strs[i] == "4") {
                    this._rights._list = true;
                }
            }
        }
    }
}