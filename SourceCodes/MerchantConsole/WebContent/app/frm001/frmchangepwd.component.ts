import {Component, Input, OnDestroy} from '@angular/core'; 
import {Router,CanDeactivate} from '@angular/router'; 
import {RpIntercomService} from '../framework/rp-intercom.service';  
import {RpInputComponent} from '../framework/rp-input.component';  
import {RpHttpService}from '../framework/rp-http.service';
import {RpReferences} from '../framework/rp-references';  
import {Subscription}   from 'rxjs/Subscription';
import {RpBean} from '../framework/rp-bean'; 
import {Observable} from 'rxjs/Rx';


declare var jQuery:any;
declare var $:any;
@Component({
    selector: 'frmpwd',
    template:`

<div class="container">
            <div class="row clearfix"> 
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
                <form class="form-horizontal" (ngSubmit)="goChange()">
                <legend>Change Password</legend>

                 <div class="form-group">
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary"> Change </button> 
                        <button type="button" class="btn btn-primary" (click)="goCancel()">Cancel</button> 
                    </div> 
                 </div>

            <!--    <div class="form-group">
                    <div class="col-md-10">
                        <p>* Passwords are case-sensitive and must be at least 6 characters and no more than 20 characters.</p>
                        <p>* A good password must contain a mix of capital and lower-case letters,numbers and special characters.</p>
                    </div>
                </div> -->                  
               
                 <div class="form-group">
                     <rp-input [(rpModel)]="_pwdobj.password" rpRequired ="true" rpLabelRequired="true" rpType="password" rpLabel="Current Password" autofocus ></rp-input>
                </div>

             
                      <div class="form-group">
                     <rp-input [(rpModel)]="_pwdobj.newpassword" rpRequired ="true" rpLabelRequired="true" rpType="password" rpLabel="New Password" autofocus ></rp-input>
                </div>
        
                      <div class="form-group">
                     <rp-input [(rpModel)]="_pwdobj.confirmnewpassword" rpRequired ="true" rpLabelRequired="true" rpType="password" rpLabel="Confirm Password" autofocus ></rp-input>
                </div>
                </form>
             </div>
          </div>
      </div>
      
 <div id="confirm" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<p>&nbsp;<br></p>
<p>Update successfully.Please login again.</p>
</div>
<div class="modal-footer">

</div>
</div>
</div>
</div>

 
 <!-- End -->
` ,
//directives: [RpInputComponent],
 providers: [RpHttpService]
 
})

export class FrmChangePwd {

_pwdobj={"password":"","newpassword":"","confirmnewpassword":""}
_resultobj={"msgCode":"","msgDesc":"","keyst":"",state:false};
_passPattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&()_+/':;?,.<>[]|\{}]).{6,20}$/;
subscription:Subscription;

_changePwdReq = {"userID":"","sessionID":"","oldPassword":"","newPassword":""};
_changePwdRes = {"code":"","desc":""};

constructor(private ics: RpIntercomService,private _router: Router, private http: RpHttpService, private ref: RpReferences) { 
 // RP Framework
  this.subscription = ics.rpbean$.subscribe(   x => { })
        if  (!ics.getRole() || ics.getRole()==0 ) this._router.navigate(['Login',, { p1: '*' }]);

}
     
goChange(){    
      if(this._pwdobj.newpassword ==this._pwdobj.confirmnewpassword){
             let url:string =this.ics._apiurl + 'service001/forceChangePassword';
              this._changePwdReq = {"userID":this.ics._profile.userID,"sessionID":"","oldPassword":this._pwdobj.password,"newPassword":this._pwdobj.newpassword};
              let json:any  = this._changePwdReq;
       
              this.http.doPost(url,json).subscribe(
              result => { 
                        this._changePwdRes=result;
                          this.popupMessage(this._changePwdRes.desc);
                         if(this._changePwdRes.code=='0000'){
                             this._router.navigate(['/login']);                                                    
                        }                       
                      },
              error => alert(error),() => {});
        
      }else {
            this.popupMessage("New and confirm passwords must be same");
      
           }
     }
    
 goCancel(){
        
        this._pwdobj={"password":"","newpassword":"","confirmnewpassword":""}
 }
    
 popupMessage(msg){      
      this.ics.sendBean({"t1":"rp-msg",t2:"Password Information",t3:msg});           
 }
    
 passwordValidation(newPass){
    if(newPass.match(this._passPattern)){
            return true;    
    }
    else{       
            return false;
    }
 }
 
 confirmLogout()
  {
    jQuery("#confirm").modal();
    Observable.timer(3000).subscribe(x => {
    this.goLogin();
  });
  }

 
 goLogin()
{
    jQuery("#confirm").modal('hide');
    this._router.navigate(['Login',, { p1: '*' }]);
}
  
}