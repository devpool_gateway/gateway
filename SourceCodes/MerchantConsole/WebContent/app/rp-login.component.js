"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_http_service_1 = require('./framework/rp-http.service');
var rp_intercom_service_1 = require('./framework/rp-intercom.service');
var rp_bean_1 = require('./framework/rp-bean');
core_2.enableProdMode();
var RpLoginComponent = (function () {
    function RpLoginComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.url = "url";
        this.cname = "username";
        this.pname = "password";
        this.remembercheck = true;
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        this.ics._profile.role = 0;
        this.checkCookie();
        this.ics.sendBean(new rp_bean_1.RpBean());
        ics.confirmUpload(false);
    }
    RpLoginComponent.prototype.checkCookie = function () {
        var value = this.readCookieValue('username');
        //let urls = this.readUrl('url');
        if (value != "" && value != null) {
            this._user = value;
        }
        else {
            this._user = "";
            this.remembercheck = false;
        }
    };
    RpLoginComponent.prototype.goPost = function () {
        var _this = this;
        if (this.apiUrl == null && this.apiUrl == undefined) {
            this.ics.sendBean({ t1: "rp-wait", t2: "Signing in ..." });
        }
        var url = this.ics._apiurl + 'service001/signin';
        var profile = { "userID": this._user, "password": this._pw, "commandCenter": this._remember };
        this.http.doPost(url, profile).subscribe(function (data) {
            if (_this.apiUrl == null && _this.apiUrl == undefined) {
                _this.ics.sendBean({ t1: "rp-msg-off" });
            }
            if (data.role > 0) {
                _this.authorize(data);
            }
            else {
                _this._result = "Invalid User ID or Password";
            }
        }, function (error) {
            _this.ics.sendBean({ t1: "rp-error", t2: "HTTP Error Type " + error.type });
        }, function () { });
    };
    RpLoginComponent.prototype.authorize = function (data) {
        this.ics._profile = data;
        this.ics._profile.userid = this._user;
        var value = this.readCookieValue('pager');
        if (value != "" && value != null) {
            this.ics._profile.n1 = Number(value);
        }
        else {
            this.ics._profile.n1 = 10;
        }
        this.ics.sendBean(new rp_bean_1.RpBean());
        this._router.navigate(['/mdashboard']);
    };
    RpLoginComponent.prototype.anonymous = function () {
        this.ics._profile.role = 9;
        this.ics._profile = {
            "userID": ".",
            "userName": ".",
            "role": 9,
            "logoText": "AG2",
            "logoLink": "/frm000",
            "commandCenter": "true",
            "btndata": [],
            "userid": "",
            "t1": "",
            "n1": 10,
            "menus": [{
                    "menuItem": "", "caption": "Form",
                    "menuItems": [
                        { "menuItem": "/merchant", "caption": "Transaction Report" },
                        { "menuItem": "/frm000", "caption": "Dash Board" },
                        { "menuItem": "/transcode", "caption": "Transaction Code" },
                        { "menuItem": "/stock", "caption": "Stock" },
                        { "menuItem": "/changepwd", "caption": "Change Password" }
                    ]
                }
            ],
            "rightMenus": [
                { "menuItem": "/login", "caption": "Sign In" }
            ]
        };
        ;
        this.ics.sendBean(new rp_bean_1.RpBean());
        this._router.navigate(['/mdashboard']);
    };
    RpLoginComponent.prototype.setremember = function (cname, _user, pname, _pw, event) {
        if (event.target.checked) {
            if (cname == "" && _pw == "") {
                document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            }
            else {
                var d = new Date();
                d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toDateString();
                document.cookie = cname + "=" + _user + ";" + expires;
                document.cookie = pname + "=" + _pw + ";" + expires;
                document.cookie = this.url + "=" + this.ics._apiurl + ";" + expires;
            }
        }
        else {
            document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        }
    };
    RpLoginComponent.prototype.readCookieValue = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    };
    RpLoginComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    RpLoginComponent = __decorate([
        core_1.Component({
            selector: 'rp-login',
            template: "\n    <div class=\"container\">\n  <div class=\"row\" style=\"height:25px\">\n  </div>\n </div>\n<div class=\"container\"> \n<div class=\"col-lg-4 col-sm-3 merchants-block\">\n<div>\n<!-- Here -->\n      <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-sm-4 col-sm-offset-4 form-box\" style=\"background:#d3d3d3;border-style:solid;border-color:#E0E6F8;border-radius: 10px; padding: 20px;  \" >\n                  <div class=\"form-top\">\n                    <div id=\"over\" style=\"width:100%\" align=\"center\">\n                    <img src=\"image/applogo.png\" style=\"width:130px;\">\n                    </div>\n                    <div class=\"form-top-left\">\n                       <h1 style=\"color:#67310B;text-align:center;font-family:serif;margin-bottom: 30px;\">{{ics._appname}}</h1>\n                    </div>\n                    \n                    <div class=\"form-top-right\">\n                      <i class=\"fa fa-key\"></i>\n                    </div>\n                    </div>\n                    <div class=\"form-bottom\">\n                  \n                  <form class=\"login-form\" (ngSubmit)=\"goPost()\" >\n                      <div style=\"margin-bottom: 30px\" class=\"input-group\">\n                          <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></span>\n                          <input type=\"text\" id=\"inputUserID\" class=\"form-control\" placeholder=\"User ID\" required autofocus [(ngModel)]=_user [ngModelOptions]=\"{standalone: true}\" >\n                      </div> \n                      <div style=\"margin-bottom: 30px\" class=\"input-group\">\n                        <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-lock\"></i></span>\n                         <input type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Password\" required [(ngModel)]=_pw [ngModelOptions]=\"{standalone: true}\" >\n                      </div> \n                     <div class=\"col-md-offset-0 col-md-10\"> \n                      <div class=\"checkbox\">\n                        <label style=\"font-size: 15px;\">\n                        <input type=\"checkbox\" value=\"remember-me\" [(ngModel)]=\"remembercheck\"  (change)=\"setremember(cname,_user,pname,_pw,$event)\" [ngModelOptions]=\"{standalone: true}\" > Remember me\n                   </label>\n                   </div>\n                  </div>\n                      <div align=\"center\">                            \n                        <button class=\"btn btn-md btn-primary col-sm-12\" type=\"submit\">Sign in</button>\n                       </div>\n                       <br/><br/><br/>\n                    <div>\n                    <br/>\n                    <p style=\"color:red;\">{{_result}} </p> \n                  </div> \n                  </form>\n                                 \n                </div>\n                </div>\n            </div>\n\n        </div>\n\n<!-- End -->\n</div>\n\n</div>\n</div> \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], RpLoginComponent);
    return RpLoginComponent;
}());
exports.RpLoginComponent = RpLoginComponent;
//# sourceMappingURL=rp-login.component.js.map