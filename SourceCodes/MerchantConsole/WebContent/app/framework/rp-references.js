"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var RpReferences = (function () {
    function RpReferences() {
        this._lov1 = {
            "stock": [{ "value": "1", "caption": "STOCK" }],
            "currency": [{ "value": "1", "caption": "USD" }],
            "gender": [{ "value": "", "caption": "" }, { "value": "m", "caption": "male" }, { "value": "f", "caption": "female" }, { "value": "0", "caption": "others" }],
            "prefix": [{ "value": "", "caption": "" }, { "value": "5", "caption": "Dr" }, { "value": "3", "caption": "Miss" }, { "value": "1", "caption": "Mr" }, { "value": "2", "caption": "Mrs" }, { "value": "4", "caption": "Ms" }],
            "string": [{ "value": "eq", "caption": "Equals" }, { "value": "c", "caption": "Contains" }, { "value": "bw", "caption": "Begins with" }, { "value": "ew", "caption": "End with" }],
            "numeric": [{ "value": "eq", "caption": "Equals" }, { "value": "gt", "caption": "Greater than" }, { "value": "lt", "caption": "Less than" }, { "value": "geq", "caption": "Greater than or Equal" },
                { "value": "leq", "caption": "Less than or Equal" }, { "value": "bt", "caption": "Between" }],
            "date": [{ "value": "eq", "caption": "Equals" }, { "value": "gt", "caption": "Greater than" }, { "value": "lt", "caption": "Less than" }, { "value": "geq", "caption": "Greater than or Equal" },
                { "value": "leq", "caption": "Less than or Equal" }, { "value": "bt", "caption": "Between" }],
            "yesno": [{ "value": "1", "caption": "NO" }, { "value": "2", "caption": "YES" }]
        };
        this._lov2 = {
            "ref001": [{ "value": "", "caption": "Empty" }]
        };
        this._lov3 = {
            "ref001": [{ "value": "", "caption": "Empty" }],
            //Excel / CSV
            "ref010": [{ "value": "EXCEL", "caption": "EXCEL" }, { "value": "CSV", "caption": "CSV" }],
            //CMSMerchantID
            "ref011": [{ "value": "", "caption": "Empty" }],
            //MUJunction MerchantID    
            "ref015": [{ "value": "", "caption": "Empty", "processingCode": "" }],
            //Account Type
            "ref016": [{ "value": "ALL", "caption": "ALL" }, { "value": "CA", "caption": "CA" }, { "value": "SA", "caption": "SA" }],
            //Initiated By
            "ref012": [{ "value": "", "caption": "" }],
            // "ref012" : [{"value":"ALL","caption":"ALL"},{"value":"Online","caption":"Online"}],
            //Status
            "ref013": [{ "value": "", "caption": "" }],
            // "ref013" : [{"value":"ALL","caption":"ALL"},{"value":"Success","caption":"SUCCESS"},{"value":"FAIL","caption":"FAIL"},{"value":"Reverse","caption":"REVERSE"}],
            //Transaction Status
            "ref014": [{ "value": "reg", "caption": "Registered" }, { "value": "liq", "caption": "Liquidated" }, { "value": "refund", "caption": "Refunded" }]
        };
    }
    RpReferences = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], RpReferences);
    return RpReferences;
}());
exports.RpReferences = RpReferences;
//# sourceMappingURL=rp-references.js.map