"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var dashboard_service_1 = require('../framework/dashboard-service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
core_1.enableProdMode();
var Frm000Component = (function () {
    function Frm000Component(ics, _router, dashboard, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.dashboard = dashboard;
        this.http = http;
        this.ref = ref;
        this.loginUser = "";
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!this.ics.getRole() || this.ics.getRole() == 0)
            this._router.navigate(['/login']);
        else
            this.ics.confirmUpload(true);
        this.getmerchantidlist();
    }
    Frm000Component.prototype.ngAfterViewInit = function () {
        this.showPieChart1();
        this.showLineChart();
        this.showBarChart();
    };
    Frm000Component.prototype.showPieChart = function () {
        this.dashboard.generatePieChart('pie', [{
                name: 'IE',
                y: 56.33
            }, {
                name: 'Chrome',
                y: 24.03
            }, {
                name: 'Firefox',
                y: 10.38
            }, {
                name: 'Safari',
                y: 4.77
            }, {
                name: 'Opera',
                y: 0.91
            }]);
    };
    Frm000Component.prototype.showLineChart = function () {
        this.dashboard.generateLineChart('line', [{
                name: 'Installation',
                data: [43934, 52503, 57177, 69658]
            }, {
                name: 'Manufacturing',
                data: [24916, 24064, 29742, 29851]
            }, {
                name: 'Sales & Distribution',
                data: [11744, 17722, 16005, 19771]
            }, {
                name: 'Project Development',
                data: [8332, 43990, 7988, 12169]
            }, {
                name: 'Other',
                data: [12908, 5948, 8105, 11248]
            }], ["Jan", "Feb", "Mar", "Apr"], 'Amount');
    };
    Frm000Component.prototype.showBarChart = function () {
        this.dashboard.generateBarChart('bar', [{
                name: 'Tokyo',
                data: [49.9, 71.5, 106.4, 129.2, 10.99, 80.00, 49.9, 71.5, 106.4, 129.2, 10.99, 80.00]
            }, {
                name: 'New York',
                data: [83.6, 78.8, 98.5, 93.4, 4.9, 68.3, 83.6, 78.8, 98.5, 93.4, 4.9, 68.3]
            }, {
                name: 'London',
                data: [48.9, 38.8, 39.3, 41.4, 34.88, 23.89, 48.9, 38.8, 39.3, 41.4, 34.88, 23.89]
            }, {
                name: 'Berlin',
                data: [42.4, 33.2, 34.5, 39.7, 23.99, 59.88, 48.9, 38.8, 39.3, 41.4, 34.88, 23.89]
            }], ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], 'Y Title');
    };
    Frm000Component.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    Frm000Component.prototype.getmerchantidlist = function () {
        var _this = this;
        this.loginUser = this.ics._profile.userID;
        this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            // let merchant = [{ "value": "", "caption": ""}];
            var merchant = [];
            if (_this.ref._lov3.ref015 != null) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                    merchant.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.ref015.length; j++) {
                    merchant.push(_this.ref._lov3.ref015[j]);
                }
            }
            _this.ref._lov3.ref015 = merchant;
            _this.getDashBoardGeneral();
            //this.getDashBoardBarMonthlyCount();
            _this.getDashBoardBarMonthlyAmount();
            _this.getBarTxnStatusofCurrentMonth();
            console.log("combo data is: " + _this.ref._lov3.ref015[0].caption);
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    Frm000Component.prototype.getDashBoardGeneral = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getGeneralDashboard';
        var json = { "merchantID": this.ref._lov3.ref015[0].value, "merchantName": this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode };
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('pie', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    Frm000Component.prototype.getDashBoardBarMonthlyCount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnCount';
        var json = { "merchantID": this.ref._lov3.ref015[0].value, "merchantName": this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode };
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('bar', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    Frm000Component.prototype.getDashBoardBarMonthlyAmount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnAmount';
        var json = { "merchantID": this.ref._lov3.ref015[0].value, "merchantName": this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode };
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('line', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    Frm000Component.prototype.getBarTxnStatusofCurrentMonth = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarTxnStatusofCurrentMonth';
        var json = { "merchantID": this.ref._lov3.ref015[0].value, "merchantName": this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode };
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('table', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    Frm000Component.prototype.showPieChart1 = function () {
        this.dashboard.generatePieChart('pie', [{
                name: 'MPT',
                y: 56.33
            }, {
                name: 'YCDC',
                y: 24.03
            }, {
                name: 'YESC',
                y: 20.38
            }]);
    };
    Frm000Component = __decorate([
        core_1.Component({
            template: "\n<div class=\"container col-md-12\" style='margin-top:-8px'>\n    <div class=\"row col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Sample Pie Chart </div>\n          <div class=\"body\" id=\"pie\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Sample Bar Chart </div>\n          <div class=\"body\" id=\"bar\"></div>\n        </div></div>\n      </div>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Sample Line Chart </div>\n          <div class=\"body\" id=\"line\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Table Goes Here </div>\n          <div class=\"body\" id=\"table\"></div>\n        </div></div>\n      </div>\n    </div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, dashboard_service_1.DashboardService, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], Frm000Component);
    return Frm000Component;
}());
exports.Frm000Component = Frm000Component;
//# sourceMappingURL=frm000.component.js.map