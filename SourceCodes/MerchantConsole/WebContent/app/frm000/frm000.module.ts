import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';

import { Frm000Component }       from './frm000.component';
import { FrmSearchComponent }       from './frmsearch.component';
import { frm000Routing }     from './frm000.routing';

import { RpHttpService } from '../framework/rp-http.service';
import { DashboardService } from '../framework/dashboard-service';
import {FrmDashBoardComponent} from './frmdashboard.component'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,
    frm000Routing
  ],
  exports : [],
  declarations: [
     Frm000Component,
     FrmSearchComponent ,
     FrmDashBoardComponent
  ],
  providers: [
    RpHttpService,
    DashboardService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Frm000Module {}