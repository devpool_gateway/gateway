import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { DashboardService } from '../framework/dashboard-service';
import { RpHttpService } from '../framework/rp-http.service';
import { RpReferences } from '../framework/rp-references';
enableProdMode();
@Component({
  template: `
<div class="container col-md-12" style='margin-top:-8px'>
    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div>
        <div class="col-md-6"><div class="dbox">
          <div class="title"> Sample Pie Chart </div>
          <div class="body" id="pie"></div>
        </div></div>
        <div class="col-md-6"><div class="dbox">
          <div class="title"> Sample Bar Chart </div>
          <div class="body" id="bar"></div>
        </div></div>
      </div>
      <div>
        <div class="col-md-6"><div class="dbox">
          <div class="title"> Sample Line Chart </div>
          <div class="body" id="line"></div>
        </div></div>
        <div class="col-md-6"><div class="dbox">
          <div class="title"> Table Goes Here </div>
          <div class="body" id="table"></div>
        </div></div>
      </div>
    </div>
</div>
  `
})
export class Frm000Component {
  subscription: Subscription;
  loginUser = "";
  constructor(private ics: RpIntercomService, private _router: Router, private dashboard: DashboardService, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!this.ics.getRole() || this.ics.getRole() == 0) this._router.navigate(['/login']);
    else this.ics.confirmUpload(true);
    
    this.getmerchantidlist();
  }

  ngAfterViewInit() {
    this.showPieChart1();
    this.showLineChart();
    this.showBarChart();   
  }

  showPieChart() {
    this.dashboard.generatePieChart('pie', [{
      name: 'IE',
      y: 56.33
    }, {
      name: 'Chrome',
      y: 24.03
    }, {
      name: 'Firefox',
      y: 10.38
    }, {
      name: 'Safari',
      y: 4.77
    }, {
      name: 'Opera',
      y: 0.91
    }]);
  }

  showLineChart() {
    this.dashboard.generateLineChart('line', [{
      name: 'Installation',
      data: [43934, 52503, 57177, 69658]
    }, {
      name: 'Manufacturing',
      data: [24916, 24064, 29742, 29851]
    }, {
      name: 'Sales & Distribution',
      data: [11744, 17722, 16005, 19771]
    }, {
      name: 'Project Development',
      data: [8332, 43990, 7988, 12169]
    }, {
      name: 'Other',
      data: [12908, 5948, 8105, 11248]
    }], ["Jan", "Feb", "Mar", "Apr"], 'Amount');
  }

  showBarChart() {
    this.dashboard.generateBarChart('bar', [{
      name: 'Tokyo',
      data: [49.9, 71.5, 106.4, 129.2,10.99,80.00,49.9, 71.5, 106.4, 129.2,10.99,80.00]
    }, {
      name: 'New York',
      data: [83.6, 78.8, 98.5, 93.4,4.9,68.3,83.6, 78.8, 98.5, 93.4,4.9,68.3]
    }, {
      name: 'London',
      data: [48.9, 38.8, 39.3, 41.4,34.88,23.89,48.9, 38.8, 39.3, 41.4,34.88,23.89]
    }, {
      name: 'Berlin',
      data: [42.4, 33.2, 34.5, 39.7,23.99,59.88,48.9, 38.8, 39.3, 41.4,34.88,23.89]
    }], ['Jan', 'Feb', 'Mar', 'Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'], 'Y Title');
  }

  showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
    }
  getmerchantidlist() {
    this.loginUser = this.ics._profile.userID;
    this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(
      data => {
        this.ref._lov3.ref015 = data.ref015;
        // let merchant = [{ "value": "", "caption": ""}];
        let merchant = [];
        if (this.ref._lov3.ref015 != null) {
          if (!(this.ref._lov3.ref015 instanceof Array)) {
            let m = [];
            m[1] = this.ref._lov3.ref015;
            merchant.push(m[1]);
          }
          for (let j = 0; j < this.ref._lov3.ref015.length; j++) {

            merchant.push(this.ref._lov3.ref015[j]);
          }
        }
        this.ref._lov3.ref015 = merchant;
        this.getDashBoardGeneral();
        //this.getDashBoardBarMonthlyCount();
        this.getDashBoardBarMonthlyAmount();
        this.getBarTxnStatusofCurrentMonth();
        console.log("combo data is: " + this.ref._lov3.ref015[0].caption);
      },
      error => {
        if (error._body.type == 'error') {
          alert("Connection Timed Out!");
        }
        else {

        }
      }, () => { }
    );
  }

  getDashBoardGeneral() {
    let url: string = this.ics._apiurl + 'service001/getGeneralDashboard';
    let json: any ={ "merchantID": this.ref._lov3.ref015[0].value, "merchantName":  this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode};
    let dbArray = [{name: '', y:0.00}];
    this.http.doPost(url, json).subscribe(
      data => {
        if(data !=null){
         dbArray = data.data;
         this.dashboard.generatePieChart('pie',dbArray);
        }
      },
      error => {
       this.showMessage("Can't Get Data!!", undefined);
      },
      () => { }
    );
  }
  getDashBoardBarMonthlyCount(){
     let url: string = this.ics._apiurl + 'service001/getBarMonthlyTxnCount';
    let json: any ={ "merchantID": this.ref._lov3.ref015[0].value, "merchantName":  this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode};

    this.http.doPost(url, json).subscribe(
      data => {
        if(data !=null){
         this.dashboard.generateBarChart('bar',data.detaildata,data.month,'Y Title');
        }
      },
      error => {
        this.showMessage("Can't Get Data!!", undefined);
      },
      () => { }
    );
  }
    getDashBoardBarMonthlyAmount(){
     let url: string = this.ics._apiurl + 'service001/getBarMonthlyTxnAmount';
    let json: any ={ "merchantID": this.ref._lov3.ref015[0].value, "merchantName":  this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode};

    this.http.doPost(url, json).subscribe(
      data => {
        if(data !=null){
         this.dashboard.generateBarChart('line',data.detaildata,data.month,'Y Title');
        }
      },
      error => {
        this.showMessage("Can't Get Data!!", undefined);
      },
      () => { }
    );
  }
   getBarTxnStatusofCurrentMonth(){
     let url: string = this.ics._apiurl + 'service001/getBarTxnStatusofCurrentMonth';
    let json: any ={ "merchantID": this.ref._lov3.ref015[0].value, "merchantName":  this.ref._lov3.ref015[0].caption, "processingCode": this.ref._lov3.ref015[0].processingCode};

    this.http.doPost(url, json).subscribe(
      data => {
        if(data !=null){
         this.dashboard.generateBarChart('table',data.detaildata,data.month,'Y Title');
        }
      },
      error => {
       this.showMessage("Can't Get Data!!", undefined);
      },
      () => { }
    );
  }
  showPieChart1() {
    
    this.dashboard.generatePieChart('pie', [{
      name: 'MPT',
      y: 56.33
    }, {
      name: 'YCDC',
      y: 24.03
    }, {
      name: 'YESC',
      y: 20.38
    }]);
  }

}
