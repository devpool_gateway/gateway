import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import { Frm000Component }       from './frm000.component';
import { FrmSearchComponent }       from './frmsearch.component';
import {FrmDashBoardComponent} from './frmdashboard.component';

const frm000Routes: Routes = [
  { path: 'frm000',  component: Frm000Component },
  { path: 'command',  component: FrmSearchComponent },
  { path: 'command/:cmd',  component: FrmSearchComponent } ,
  { path: 'mdashboard',component:FrmDashBoardComponent} 
];

export const frm000Routing: ModuleWithProviders = RouterModule.forChild(frm000Routes);