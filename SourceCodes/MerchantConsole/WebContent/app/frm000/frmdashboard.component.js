"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var dashboard_service_1 = require('../framework/dashboard-service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
core_1.enableProdMode();
var FrmDashBoardComponent = (function () {
    function FrmDashBoardComponent(ics, _router, dashboard, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.dashboard = dashboard;
        this.http = http;
        this.ref = ref;
        this.loginUser = "";
        this._hideCNP = true;
        this._hideSkypay = true;
        this._request = { "merchantID": "", "merchantName": "", "processingCode": "", "feature": 0 };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!this.ics.getRole() || this.ics.getRole() == 0)
            this._router.navigate(['/login']);
        else
            this.ics.confirmUpload(true);
        this.getmerchantidlist();
    }
    FrmDashBoardComponent.prototype.ngAfterViewInit = function () {
        // this.showPieChart1();
        // this.showLineChart();
        //this.showBarChart();   
    };
    // showPieChart() {
    //   this.dashboard.generatePieChart('pie', [{
    //     name: 'IE',
    //     y: 56.33
    //   }, {
    //     name: 'Chrome',
    //     y: 24.03
    //   }, {
    //     name: 'Firefox',
    //     y: 10.38
    //   }, {
    //     name: 'Safari',
    //     y: 4.77
    //   }, {
    //     name: 'Opera',
    //     y: 0.91
    //   }]);
    // }
    // showLineChart() {
    //   this.dashboard.generateLineChart('line', [{
    //     name: 'Installation',
    //     data: [43934, 52503, 57177, 69658]
    //   }, {
    //     name: 'Manufacturing',
    //     data: [24916, 24064, 29742, 29851]
    //   }, {
    //     name: 'Sales & Distribution',
    //     data: [11744, 17722, 16005, 19771]
    //   }, {
    //     name: 'Project Development',
    //     data: [8332, 43990, 7988, 12169]
    //   }, {
    //     name: 'Other',
    //     data: [12908, 5948, 8105, 11248]
    //   }], ["Jan", "Feb", "Mar", "Apr"], 'Amount');
    // }
    // showBarChart() {
    //   this.dashboard.generateBarChart('bar', [{
    //     name: 'Tokyo',
    //     data: [49.9, 71.5, 106.4, 129.2,10.99,80.00,49.9, 71.5, 106.4, 129.2,10.99,80.00]
    //   }, {
    //     name: 'New York',
    //     data: [83.6, 78.8, 98.5, 93.4,4.9,68.3,83.6, 78.8, 98.5, 93.4,4.9,68.3]
    //   }, {
    //     name: 'London',
    //     data: [48.9, 38.8, 39.3, 41.4,34.88,23.89,48.9, 38.8, 39.3, 41.4,34.88,23.89]
    //   }, {
    //     name: 'Berlin',
    //     data: [42.4, 33.2, 34.5, 39.7,23.99,59.88,48.9, 38.8, 39.3, 41.4,34.88,23.89]
    //   }], ['Jan', 'Feb', 'Mar', 'Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'], 'Y Title');
    // }
    FrmDashBoardComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmDashBoardComponent.prototype.getmerchantidlist = function () {
        var _this = this;
        this.loginUser = this.ics._profile.userID;
        this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            // let merchant = [{ "value": "", "caption": ""}];
            var merchant = [];
            if (_this.ref._lov3.ref015 != null) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                    merchant.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.ref015.length; j++) {
                    merchant.push(_this.ref._lov3.ref015[j]);
                }
            }
            _this.ref._lov3.ref015 = merchant;
            _this._request = { "merchantID": _this.ref._lov3.ref015[0].value, "merchantName": _this.ref._lov3.ref015[0].caption,
                "processingCode": _this.ref._lov3.ref015[0].processingCode, "feature": 0 };
            if (_this.ref._lov3.ref015[0].processingCode == '050200') {
                _this._hideCNP = false;
                _this.getDashBoardGeneralCNPBill();
                _this.getDashBoardGeneralCNPTopup();
                _this.getDashBoardBarMonthlyCountBill();
                _this.getDashBoardBarMonthlyCountTopup();
                _this.getDashBoardBarMonthlyAmountBill();
                _this.getDashBoardBarMonthlyAmountTopup();
                _this.getBarTxnStatusofCurrentMonthBill();
                _this.getBarTxnStatusofCurrentMonthTopup();
            }
            else if (_this.ref._lov3.ref015[0].processingCode == '040300') {
                _this._hideSkypay = false;
                _this.getDashBoardGeneralSky();
                _this.getDashBoardBarMonthlyCount();
                _this.getDashBoardBarMonthlyAmount();
                _this.getBarTxnStatusofCurrentMonth();
            }
            console.log("combo data is: " + _this.ref._lov3.ref015[0].caption);
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardGeneralCNPBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getGeneralDashboard';
        this._request.feature = 1;
        var json = this._request;
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('billtotal', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardGeneralCNPTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getGeneralDashboard';
        this._request.feature = 2;
        var json = this._request;
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('topuptotal', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardGeneralSky = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getGeneralDashboard';
        var json = this._request;
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('skytotal', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardBarMonthlyCountBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnCount';
        this._request.feature = 1;
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('billmlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardBarMonthlyCountTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnCount';
        this._request.feature = 2;
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('topupmlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardBarMonthlyCount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnCount';
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skymlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardBarMonthlyAmountBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnAmount';
        this._request.feature = 1;
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('billmlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardBarMonthlyAmountTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnAmount';
        this._request.feature = 2;
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('topupmlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getDashBoardBarMonthlyAmount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarMonthlyTxnAmount';
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skymlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getBarTxnStatusofCurrentMonthBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarTxnStatusofCurrentMonth';
        this._request.feature = 1;
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('billstatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getBarTxnStatusofCurrentMonthTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarTxnStatusofCurrentMonth';
        this._request.feature = 2;
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('topupstatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent.prototype.getBarTxnStatusofCurrentMonth = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getBarTxnStatusofCurrentMonth';
        var json = this._request;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skystatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmDashBoardComponent = __decorate([
        core_1.Component({
            template: "\n<div class=\"container col-md-12\" style='margin-top:-8px'>\n    <div class=\"row col-xs-12 col-sm-12 col-md-12 col-lg-12\" [hidden]=_hideSkypay>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction  </div>\n          <div class=\"body\" id=\"skytotal\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Count</div>\n          <div class=\"body\" id=\"skymlycount\"></div>\n        </div></div>\n      </div>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Amount </div>\n          <div class=\"body\" id=\"skymlyamount\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction Count By Status </div>\n          <div class=\"body\" id=\"skystatus\"></div>\n        </div></div>\n      </div>\n    </div>\n     <div class=\"row col-xs-12 col-sm-12 col-md-12 col-lg-12\" [hidden]=_hideCNP>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Bill Payment Transaction  </div>\n          <div class=\"body\" id=\"billtotal\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Topup Transaction  </div>\n          <div class=\"body\" id=\"topuptotal\"></div>\n        </div></div>\n      </div>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Bill Payment Monthly Transaction Count</div>\n          <div class=\"body\" id=\"billmlycount\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Bill Payment Monthly Transaction Amount</div>\n          <div class=\"body\" id=\"billmlyamount\"></div>\n        </div></div>\n      </div>\n       <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Bill Payment Transaction Count By Status</div>\n          <div class=\"body\" id=\"billstatus\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Count </div>\n          <div class=\"body\" id=\"topupmlycount\"></div>\n        </div></div>\n      </div>\n       <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Amount</div>\n          <div class=\"body\" id=\"topupmlyamount\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction Count By Status</div>\n          <div class=\"body\" id=\"topupstatus\"></div>\n        </div></div>\n      </div>\n    </div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, dashboard_service_1.DashboardService, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmDashBoardComponent);
    return FrmDashBoardComponent;
}());
exports.FrmDashBoardComponent = FrmDashBoardComponent;
//# sourceMappingURL=frmdashboard.component.js.map