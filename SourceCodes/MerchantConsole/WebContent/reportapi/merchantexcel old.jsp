<%@page import="java.util.concurrent.ExecutionException"%>
<%@page import="com.csvreader.CsvWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.nio.file.Files"%>
<%@page import="com.nirvasoft.rp.framework.ConnAdmin"%>
<%@page import="jxl.write.Label"%>
<%@page import="jxl.format.Alignment"%>
<%@page import="jxl.format.CellFormat"%>
<%@page import="jxl.format.VerticalAlignment"%>
<%@page import="jxl.write.WritableCellFormat"%>
<%@page import="jxl.write.WritableFont"%>
<%@page import="jxl.write.WritableSheet"%>
<%@page import="jxl.write.WritableWorkbook"%>
<%@page import="java.awt.Window"%>
<%@page import="com.nirvasoft.rp.util.FileUtil"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="jxl.Cell"%>
<%@page import="org.apache.poi.ss.usermodel.Row"%>
<%@page import="jxl.Sheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="jxl.Workbook"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.InputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.export.*"%>
<%@page import="com.nirvasoft.rp.shared.MerchantData"%>
<%@page import="com.nirvasoft.rp.shared.DispaymentTransactionData"%>
<%@page import="com.nirvasoft.rp.shared.PayTemplateDetail"%>
<%@page import="com.nirvasoft.rp.shared.ServerGlobal"%>
<%@page import="com.nirvasoft.rp.mgr.MerchantReportMgr"%>
<%@page import="com.nirvasoft.rp.shared.DispaymentTransactionData"%>
<%@page import="com.nirvasoft.rp.dao.DispaymentTransactionDao"%>
<%@page import="com.nirvasoft.rp.shared.PayTemplateDetailArr"%>
<%@page import="com.nirvasoft.rp.util.GeneralUtil"%>
<%@page import="com.nirvasoft.rp.dao.MerchantReportDao"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="jxl.write.WritableImage"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@page import="java.io.ByteArrayOutputStream"%>

<%@page import="org.apache.poi.hssf.usermodel.*"%><%@page
	import="java.io.*"%>

<html>
<head>
<script type="text/javascript">

	
<%Connection conn = null;
			PreparedStatement st = null , pst = null;//
			ResultSet rs = null , res = null;

			JRHtmlExporter l_htmlExporter = new JRHtmlExporter();
			StringBuffer l_stringBuffer = new StringBuffer();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			//String date2 = dateFormat.format(date);
			String param = (dateFormat.format(date));
			//String select = request.getParameter("aSelect");
			String select2 = (request.getParameter("aSelect"));
			try {
				String l_aMerchantID = "";
				if (request.getParameter("aMerchantID") != null) {
					l_aMerchantID = request.getParameter("aMerchantID");
				}
				
				String abspath = session.getServletContext().getRealPath("/");
				System.out.println(abspath);
				conn = ConnAdmin.getConn("001", abspath);
				GeneralUtil.readDataConfig();
				String templateMerchant = "";
				
				String pCode = "";
				String query = "Select ProcessingCode From PayTemplateHeader Where MerchantID = '" + l_aMerchantID + "'";
				PreparedStatement pstmt = conn.prepareStatement(query);
				ResultSet ret = pstmt.executeQuery();
				while (ret.next()) {
					pCode = ret.getString("ProcessingCode"); // 080400
				}
				System.out.print("PCode : " + pCode);
				
				if (pCode.equalsIgnoreCase("080400")) {
					
					double service = 0.0, tax = 0.0 , amount = 0.0;
					double amountst = 0.0 , incomest = 0.0 , servicest = 0.0;
					double grandtotal = 0.0 , servicetotal = 0.0 , taxtotal = 0.0;
					
					l_aMerchantID = "";
					String l_aFeature = "";
					String l_aFromDate = "";
					String l_aToDate = "";
					String l_aSelect = "";
					String l_FromAcc = "";
					String l_ToAcc = "";
					String did = "";
					String initiatedby = "";
					String transrefno = "";
					String templateData = "";
					String processingCode= "";
					String proDisCode = "";
					String status = "";
					
					if (request.getParameter("aMerchantID") != null) {
						l_aMerchantID = request.getParameter("aMerchantID");
					}

					if (request.getParameter("aFeature") != null) {
						l_aFeature = request.getParameter("aFeature");
					}
					
					if (request.getParameter("aFromDate") != null) {
						l_aFromDate = request.getParameter("aFromDate").toString().replaceAll("[-+.^:,]", "");
					}
					if (request.getParameter("aToDate") != null) {
						l_aToDate = request.getParameter("aToDate").toString().replaceAll("[-+.^:,]", "");
					}
					if (request.getParameter("aSelect") != null) {
						l_aSelect = request.getParameter("aSelect");
					}
					if (request.getParameter("debitaccount") != null) {
						l_FromAcc = request.getParameter("debitaccount");
					}
					if (request.getParameter("collectionaccount") != null) {
						l_ToAcc = request.getParameter("collectionaccount");
					}
					if (request.getParameter("did") != null) {
						did = request.getParameter("did");
					}
					if (request.getParameter("initiatedby") != null) {
						initiatedby = request.getParameter("initiatedby");
					}
					if (request.getParameter("transrefno") != null) {
						transrefno = request.getParameter("transrefno");
					}
					if(request.getParameter("templatedata") !=null && !request.getParameter("templatedata").equals("")){
						templateData = request.getParameter("templatedata");
					}//processingCode
					if(request.getParameter("processingCode") !=null){
						processingCode = request.getParameter("processingCode");
					}
					if(request.getParameter("status") != null) {
						status = request.getParameter("status");
					}
					if(request.getParameter("proDisCode") !=null){//proDisCode ==01 is Use Distributor
						proDisCode = request.getParameter("proDisCode");
					}
				
					
					if(processingCode.equalsIgnoreCase("02")){
						//Just Fortune Ygn merchant is mapped in Paytemplate Setup 
						templateMerchant = ServerGlobal.getFortuneYgnMerchant();
					}else{
						templateMerchant = l_aMerchantID;
					}
					
					
					String merchantName = "" , s_query = "";
					s_query = "select UserName from CMSMerchant where recordStatus <> 4 and  UserId = '" + l_aMerchantID + "' ";
					//Preare Criteria for Qurey
					System.out.println(s_query);
					pst = conn.prepareStatement(s_query);
					res = pst.executeQuery();
					while(res.next()){
						merchantName = res.getString("UserName");
					}
					String aCriteria = "";
					if(!l_aFromDate.trim().equals("")){
						aCriteria += " and d.TransDate >= '" + l_aFromDate.trim() + "'";
					}
					
					if(!l_aToDate.trim().equals("")){
						aCriteria += " and d.TransDate <= '" + l_aToDate.trim() + "'";
					}
					
					if (!l_FromAcc.trim().equals("")) {
						aCriteria += " and FromAccount = '" + l_FromAcc.trim() + "'";
					}

					if (!l_ToAcc.trim().equals("")) {
						aCriteria += " and ToAccount = '" + l_ToAcc.trim() + "'";
					}

					if (!did.trim().equals("")) {
						aCriteria += " and CustomerCode = '" + did.trim() + "'";
					}

					if (!status.trim().equalsIgnoreCase("ALL")) {
						if (status.trim().equalsIgnoreCase("SUCCESS")) {
							aCriteria += " and d.t14 = '1'" ;//SUCCESS
						}else if (status.trim().equalsIgnoreCase("REVERSED")) {
							aCriteria += " and d.t14 <> '1' and d.n2 = 4" ;//REVERSED
						}else if (status.trim().equalsIgnoreCase("FAIL")){
							aCriteria += " and d.t14 <> '1'" ;//FAIL
						}
					}
					
					if (!transrefno.trim().equals("")) {
						aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
					}
					
					if (!l_aFeature.trim().equals("")) {
						if (l_aFeature.trim().equalsIgnoreCase("All")) {
							aCriteria += " " ;
						}else {
							aCriteria += " and d.n20= "+ l_aFeature +" " ;
						}
					}
					
					String countSQL = "";
					countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  where 1=1 and d.t20 = '"
							+ l_aMerchantID + "' ";
					
					countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";
					
					PreparedStatement stat = conn.prepareStatement(countSQL);
					System.out.println("Count.." + countSQL);
					ResultSet result = stat.executeQuery();
					result.next();
					int totCount = result.getInt("recCount");
					
					
					String sql = "";
					if(proDisCode.equalsIgnoreCase("01")){// Use Distributor 
						sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,d.CustomerCode AS DID ,dl.DistributorName,"+
								" dl.DistributorAddress from DispaymentTransaction d ,DistributorList dl "+
								 " where d.CustomerCode=dl.DistributorID and d.t20 = '" + l_aMerchantID + "' And d.N2=9 And d.n3=4  " + aCriteria + " )"+
								 " As r WHERE 1=1 ";
					}else{		
						//Please check
						sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,d.CustomerCode AS DID "+
								" from DispaymentTransaction d where 1=1 and d.t20 = '" + l_aMerchantID + "' And d.n3=4  " + aCriteria + ") "
								+" As r WHERE 1=1 ";
					}
			

					System.out.println(sql);
					st = conn.prepareStatement(sql);
					rs = st.executeQuery();
					

					if (l_aSelect == "Excel" || l_aSelect.equalsIgnoreCase("Excel")) {

						ResultSetMetaData rsmd = rs.getMetaData();
						int col = rsmd.getColumnCount();

						File outputStream = new File(session.getServletContext().getRealPath("/")
								+ "ExcelExport/MerchantList" + param + ".xls");
						WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
						WritableSheet sheet = workBook.createSheet("MerchantList", 0);
						sheet.getSettings().setDefaultColumnWidth(15);
						sheet.getSettings().setDefaultRowHeight(24 * 20);

						/* Generates Headers Cells */
						WritableFont headerFont = new WritableFont(WritableFont.TAHOMA, 16, WritableFont.BOLD);
						WritableFont headerFont1 = new WritableFont(WritableFont.TAHOMA, 14, WritableFont.BOLD);
						WritableFont headerFont2 = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD);
						WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
						WritableCellFormat headerCellFormat1 = new WritableCellFormat(headerFont1);
						WritableCellFormat headerCellFormat2 = new WritableCellFormat(headerFont2);
						headerCellFormat1.setAlignment(Alignment.CENTRE);
						headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
						headerCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
						/* Generates Data Cells */
						WritableFont dataFont = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
						WritableFont dataFont1 = new WritableFont(WritableFont.TAHOMA, 9);
						WritableFont dataFont2 = new WritableFont(WritableFont.TAHOMA, 9);
						WritableCellFormat dataCellFormat = new WritableCellFormat(dataFont);
						WritableCellFormat dataCellFormat1 = new WritableCellFormat(dataFont1);
						WritableCellFormat dataCellFormat2 = new WritableCellFormat(dataFont2);
						WritableCellFormat dataCellFormat3 = new WritableCellFormat(dataFont1);
						dataCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
						dataCellFormat.setAlignment(Alignment.CENTRE);
						dataCellFormat1.setAlignment(Alignment.CENTRE);
						dataCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
						dataCellFormat2.setAlignment(Alignment.RIGHT);
						dataCellFormat2.setVerticalAlignment(VerticalAlignment.CENTRE);
						dataCellFormat3.setAlignment(Alignment.LEFT);
						dataCellFormat3.setVerticalAlignment(VerticalAlignment.CENTRE);
					
						String p_fromDate = "";
						String p_toDate = "";
						if(!l_aFromDate.trim().equals("")){
							try{
								p_fromDate = l_aFromDate.substring(6) + "/" + l_aFromDate.substring(4, 6) + "/"
										+ l_aFromDate.substring(0, 4);
							}catch(Exception e){
								p_fromDate = "";
							}
							
						}
						
						if(!l_aToDate.trim().equals("")){
							try{
								p_toDate = l_aToDate.substring(6) + "/" + l_aToDate.substring(4, 6) + "/"
										+ l_aToDate.substring(0, 4);
							}catch(Exception e){
								p_toDate = "";
							}
							
						}

						int row = 0; //For Excel Format
						int heightInPoints = 62 * 20;
						sheet.setRowView(row, heightInPoints);
						String imgName = ConnAdmin.readExternalUrl("ExcelImageName");
						File imageFile = new File(abspath + "/image/"+imgName);
						sheet.addImage(new WritableImage(0, 0, 1, 1, imageFile));
						//sheet.mergeCells(0, 0, 4, 0);
						//sheet.addCell(new Label(0, row, "", headerCellFormat));
						sheet.mergeCells(1, 0, 10, 0); // (start col:,row,last col; row)
						sheet.addCell(new Label(1, row, "Bill Payment System", headerCellFormat1));
						int row1 = 1;
						int heightInPoints1 = 30 * 20;
						sheet.setRowView(row1, heightInPoints);
						sheet.mergeCells(0, 1, 10, 1);
						sheet.addCell(new Label(0, row1, "Daily Transaction Report between '"+p_fromDate+"' and '"+p_toDate+"'", headerCellFormat1));

							
						sheet.addCell(new Label(0, 2, "Total Count", headerCellFormat2));
						sheet.addCell(new Label(1, 2, ":", headerCellFormat2));
						sheet.addCell(new Label(2, 2, String.valueOf(totCount), headerCellFormat2));
						
						sheet.addCell(new Label(0, 3, "Merchant ID", headerCellFormat2));
						sheet.addCell(new Label(1, 3, ":", headerCellFormat2));
						sheet.addCell(new Label(2, 3, l_aMerchantID, headerCellFormat2));
						
						sheet.addCell(new Label(0, 4, "Merchant Name", headerCellFormat2));
						sheet.addCell(new Label(1, 4, ":", headerCellFormat2));
						sheet.addCell(new Label(2, 4, merchantName , headerCellFormat2));

						int row5 = 5; // 3 4
						
						int heightInPoints5 = 25 * 20;
						int col0 = 0;
						int widthInChars = 16;
						sheet.setColumnView(col0, widthInChars);   
						sheet.setRowView(row5, heightInPoints5);
						sheet.addCell(new Label(col0, row5, "Transaction Date", dataCellFormat));
						
						int col1 = 1;
						int widthInChars1 = 25;
						sheet.setColumnView(col1, widthInChars1);
						sheet.addCell(new Label(1, row5, "Transaction Reference No. ", dataCellFormat));

						int col2 = 2;
						int widthInChars2 = 16;
						sheet.setColumnView(col2, widthInChars2);
						sheet.addCell(new Label(col2, row5, "Transaction Tax Reference No. ", dataCellFormat));

						int col3 = 3;
						int widthInChars3 = 20;
						sheet.setColumnView(col3, widthInChars3);
						sheet.addCell(new Label(col3, row5, "Debit Account", dataCellFormat));

						int col4 = 4;
						int widthInChars4 = 20;
						sheet.setColumnView(col4, widthInChars4);
						sheet.addCell(new Label(col4, row5, "Collection Account", dataCellFormat));

						int col5 = 5;
						int widthInChars5 = 25;
						sheet.setColumnView(col5, widthInChars5);
						sheet.addCell(new Label(col5, row5, "Customer Code", dataCellFormat));

						int col6 = 6;
						int widthInChars6 = 20;
						sheet.setColumnView(col6, widthInChars6);
						sheet.addCell(new Label(col6, row5, "Customer Name", dataCellFormat));

						/*int col7 = 7;
						int widthInChars7 = 16;
						sheet.setColumnView(col7, widthInChars7);
						sheet.addCell(new Label(col7, row5, "Description", dataCellFormat));*/

						int col7 = 7;
						int widthInChars8 = 16;
						sheet.setColumnView(col7, widthInChars8);
						sheet.addCell(new Label(col7, row5, "Initiated By", dataCellFormat));

						int col8 = 8;
						int widthInChars9 = 20;
						sheet.setColumnView(col8, widthInChars9);
						sheet.addCell(new Label(col8, row5, "Amount", dataCellFormat));

						int col9 = 9;
						int widthInChars10 = 20;
						sheet.setColumnView(col9, widthInChars10);
						sheet.addCell(new Label(col9, row5, "Service Charges", dataCellFormat));

						int col10 = 10;
						int widthInChars11 = 20;
						sheet.setColumnView(col10, widthInChars11);
						sheet.addCell(new Label(col10, row5, "Commercial Tax", dataCellFormat));

						int col11 = 11;
						int widthInChars12 = 20;
						sheet.setColumnView(col11, widthInChars12);
						sheet.addCell(new Label(col11, row5, "TPS Message Code", dataCellFormat));
						
						int col12 = 12;
						int widthInChars13 = 20;
						sheet.setColumnView(col12, widthInChars13);
						sheet.addCell(new Label(col12, row5, "TPS Message Description", dataCellFormat));
						
						int coldtl = 0;
						int rowtotal = 0;
						int heightInPointsg = 0;
						while (rs.next()) {
							int currentCol = 0;
							int grow = rs.getRow() + 5; // 3 4
							System.out.print("Row for title : " + rs.getRow() + " : grow : " + grow);
							rowtotal = rs.getRow();
							heightInPointsg = 25 * 20;
							sheet.setRowView(grow, heightInPointsg);
							String aTransDate = "";
							aTransDate = rs.getString("TransDate");
							String transDate = "";
							if(!aTransDate.trim().equals("")){
								try{
									transDate = aTransDate.substring(6) + "/"
											+ aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4);
								}catch(Exception e){
									transDate = "";
								}
								
							}
							
						
							sheet.addCell(new Label(currentCol++, grow,transDate, dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("FlexRefNo"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("t22"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("FromAccount"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("ToAccount"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("DID"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("CustomerName"), dataCellFormat3));
							//sheet.addCell(new Label(currentCol++, grow, rs.getString("t16"), dataCellFormat3));						
							sheet.addCell(new Label(currentCol++, grow, "Online", dataCellFormat1));

							amount = rs.getDouble("Amount");						
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", amount), dataCellFormat2));
							
							String servicet15 = "0.00",chargest18 = "0.00";
							servicet15 = rs.getString("T15");
							if(servicet15.equals("")){
								servicet15 = "0.00";
							}
							service = Double.valueOf(servicet15); 
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", service), dataCellFormat2));
							
							
							chargest18 = rs.getString("T18");
							if(chargest18.equals("")){
								chargest18 = "0.00";
							}
							tax = Double.valueOf(chargest18);
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", tax), dataCellFormat2));


							sheet.addCell(new Label(currentCol++, grow, rs.getString("t14"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("t12"), dataCellFormat1));
							grandtotal += amount;
							servicetotal += service;
							taxtotal += tax;
						} 

						rowtotal += 6; // 6 7
						sheet.setRowView(rowtotal, heightInPointsg);
						sheet.addCell(new Label(7, rowtotal, "Grand Total: ", dataCellFormat2));
						sheet.addCell(new Label(8, rowtotal, String.format("%,.2f", grandtotal), dataCellFormat2));
						sheet.addCell(new Label(9, rowtotal, String.format("%,.2f", servicetotal), dataCellFormat2));
						sheet.addCell(new Label(10, rowtotal, String.format("%,.2f", taxtotal), dataCellFormat2));
						/* Write & Close Excel WorkBook */
						workBook.write();
						workBook.close();

					} else {
						File outputStream = new File(session.getServletContext().getRealPath("/") + "CSV/MerchantList"
								+ param + ".csv");
						boolean alreadyExists = new File("MerchantList.csv").exists();
						try {

							CsvWriter csvOutput = new CsvWriter(new FileWriter(outputStream, true), ',');

							csvOutput.write("Transaction Date");
							csvOutput.write("Transaction Reference No.");
							csvOutput.write("Transaction Tax Reference No.");
							csvOutput.write("Debit Account");
							csvOutput.write("Collection Account");
							csvOutput.write("Customer Code");
							csvOutput.write("Customer Name");
							csvOutput.write("Description ");
							csvOutput.write("Initiated By");
							csvOutput.write("Amount");
							csvOutput.write("Service Charges");
							csvOutput.write("Commercial Tax");
							csvOutput.write("TPS Message Code");
							csvOutput.write("TPS Message Description");

							if(proDisCode.equalsIgnoreCase("01")){
								csvOutput.write("Distributor Name");
								csvOutput.write("Distributor Address");
							}
							csvOutput.endRecord();

							while (rs.next()) {
								String aTransDate = "";
								aTransDate = rs.getString("TransDate");
								double T15 = 0.0, T18 = 0.0;
								csvOutput.write(aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
										+ aTransDate.substring(0, 4));
								csvOutput.write(rs.getString("FlexRefNo"));
								csvOutput.write(rs.getString("t22"));
								csvOutput.write(rs.getString("FromAccount"));
								csvOutput.write(rs.getString("ToAccount"));
								//csvOutput.write(rs.getString("DID"));
								csvOutput.write(rs.getString("CustomerCode"));
								csvOutput.write(rs.getString("CustomerName"));
								csvOutput.write(rs.getString("t16"));
								csvOutput.write("Online");

								double amt = rs.getDouble("Amount");
								csvOutput.write(String.format("%,.2f", amt));
								
								String servicet15 = "0.00",chargest18 = "0.00";
								servicet15 = rs.getString("T15");
								if(servicet15.equals("")){
									servicet15 = "0.00";
								}
								service = Double.valueOf(servicet15); 
								csvOutput.write(String.format("%,.2f", service));
								
								
								chargest18 = rs.getString("T18");
								if(chargest18.equals("")){
									chargest18 = "0.00";
								}
								tax = Double.valueOf(chargest18); 
								csvOutput.write(String.format("%,.2f", tax));

								amount=0.0;
							
								csvOutput.write(rs.getString("t14"));
								csvOutput.write(rs.getString("t12"));
								
								 
								if("01".equalsIgnoreCase(proDisCode)){
									csvOutput.write(rs.getString("DistributorName"));
									csvOutput.write(rs.getString("DistributorAddress"));
								}
								csvOutput.endRecord();
								
								
							}
							csvOutput.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				
				} else {

					
					String l_aFromDate = "";
					String l_aToDate = "";
					String l_aSelect = "";
					String l_FromAcc = "";
					String l_ToAcc = "";
					String did = "";
					String initiatedby = "";
					String transrefno = "";
					String templateData = "";
					String status = "";
					String l_aFeature = "";
					String processingCode= "";
					String proDisCode = "";
					if (request.getParameter("aMerchantID") != null) {
						l_aMerchantID = request.getParameter("aMerchantID");
					}
					
					if (request.getParameter("aFeature") != null) {
						l_aFeature = request.getParameter("aFeature");
					}

					if (request.getParameter("aFromDate") != null) {
						l_aFromDate = request.getParameter("aFromDate").toString().replaceAll("[-+.^:,]", "");
					}
					if (request.getParameter("aToDate") != null) {
						l_aToDate = request.getParameter("aToDate").toString().replaceAll("[-+.^:,]", "");
					}
					if (request.getParameter("aSelect") != null) {
						l_aSelect = request.getParameter("aSelect");
					}
					if (request.getParameter("debitaccount") != null) {
						l_FromAcc = request.getParameter("debitaccount");
					}
					if (request.getParameter("collectionaccount") != null) {
						l_ToAcc = request.getParameter("collectionaccount");
					}
					if (request.getParameter("did") != null) {
						did = request.getParameter("did");
					}
					if (request.getParameter("initiatedby") != null) {
						initiatedby = request.getParameter("initiatedby");
					}
					if (request.getParameter("transrefno") != null) {
						transrefno = request.getParameter("transrefno");
					}
					if(request.getParameter("templatedata") !=null && !request.getParameter("templatedata").equals("")){
						templateData = request.getParameter("templatedata");
					}
					//status
					if(request.getParameter("status") !=null){
						status = request.getParameter("status");
					}
					//processingCode
					if(request.getParameter("processingCode") !=null){
						processingCode = request.getParameter("processingCode");
					}
					if(request.getParameter("proDisCode") !=null){//proDisCode ==01 is Use Distributor
						proDisCode = request.getParameter("proDisCode");
					}
				
					abspath = session.getServletContext().getRealPath("/");
					System.out.println(abspath);
					conn = ConnAdmin.getConn("001", abspath);
					GeneralUtil.readDataConfig();
					templateMerchant = "";
					if(processingCode.equalsIgnoreCase("02")){
						//Just Fortune Ygn merchant is mapped in Paytemplate Setup 
						templateMerchant = ServerGlobal.getFortuneYgnMerchant();
					}else{
						templateMerchant = l_aMerchantID;
					}
				 	long key = 0;
					String Query="SELECT SysKey from PayTemplateHeader WHERE MerchantID = ?";
					pstmt= conn.prepareStatement(Query);
					pstmt.setString(1, templateMerchant);
					ResultSet rs1 = pstmt.executeQuery();
					if(rs1.next()){
						key = rs1.getLong("SysKey");
					}
					
					
					pstmt.close();
					rs1.close();
					
					
					//4,5,6
					ArrayList<PayTemplateDetailArr> DetailList=new ArrayList<PayTemplateDetailArr>();
					query="select Caption,DataType,DisPayField from PayTemplateDetail "
							+ " where HKey=? and FieldShow in (4,5,6)"
							+ " and DisPayField not in('TransDate','Amount','CustomerCode','T16')";
					
					pstmt= conn.prepareStatement(query);
					pstmt.setLong(1, key);
					ResultSet rs2 = pstmt.executeQuery();
					while (rs2.next()) {
						PayTemplateDetailArr Detail= new PayTemplateDetailArr();
						
						Detail.setPayCaption(rs2.getString("Caption"));
						Detail.setPaydatatype(rs2.getString("DataType"));
						Detail.setPaydispayfield(rs2.getString("DisPayField"));
						DetailList.add(Detail);
					}
					
					//Preare Criteria for Qurey
					
					String merchantName = "" , s_query = "";
					s_query = "select UserName from CMSMerchant where recordStatus <> 4 and  UserId = '" + l_aMerchantID + "' ";
					//Preare Criteria for Qurey
					System.out.println(s_query);
					pst = conn.prepareStatement(s_query);
					res = pst.executeQuery();
					while(res.next()){
						merchantName = res.getString("UserName");
					}
					
					String aCriteria = "" ;
					
					if(!l_aFromDate.trim().equals("")){
						aCriteria += " and c.TransDate >= '" + l_aFromDate.trim() + "'";
					}
					
					if(!l_aToDate.trim().equals("")){
						aCriteria += " and c.TransDate <= '" + l_aToDate.trim() + "'";
					}
					if (!l_FromAcc.trim().equals("")) {
						aCriteria += " and FromAccount = '" + l_FromAcc.trim() + "'";
					}
					if (!l_ToAcc.trim().equals("")) {
						aCriteria += " and ToAccount = '" + l_ToAcc.trim() + "'";
					}
					if (!did.trim().equals("")) {
						aCriteria += " and CustomerCode = '" + did.trim() + "'";
					}
					if (!initiatedby.trim().equals("ALL")) {
						if (initiatedby.trim().equals("Online")) {
							aCriteria += " and d.MobileUserID = 'Online'";
						} else if (initiatedby.trim().equals("OTC")) {
							aCriteria += " and d.MobileUserID <> 'Online'";
						}
					}

					if (!transrefno.trim().equals("")) {
						aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
					}
					
					if (!l_aFeature.trim().equals("")) {
						if (l_aFeature.trim().equalsIgnoreCase("All")) {
							aCriteria += " " ;
						}else {
							aCriteria += " and d.n20= "+ l_aFeature +" " ;
						}
					}
				
					String countSQL = "";

					if (pCode.equalsIgnoreCase("050200")) {

						if (status.equalsIgnoreCase("SUCCESS")) {
							countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' ";
							countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 2 and c.Status = 'Success'"
									+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
									+ " ) As r ";
						} else if (status.equalsIgnoreCase("FAIL")) {

							countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' ";
							countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 2 and c.Status = 'Failed'"
									+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1 and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed'))"
									+ " ) As r ";

						} else if (status.equalsIgnoreCase("PENDING")) {

							countSQL = "select COUNT(*) as recCount from ( select * from ( SELECT ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num, d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' ";
							countSQL += "  And d.n3=4  " + aCriteria
									+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
									+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
									+ " ) As r ";

						} else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED")
								|| status.equalsIgnoreCase("REVERSED")) {

							countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' ";
							countSQL += "  And d.n3=4  " + aCriteria + "and c.ApiStep = 3 and c.Status in ('" + status
									+ "')" + " ) As r ";

						} else if (status.equalsIgnoreCase("ALL")) {

							countSQL = "select COUNT(*) as recCount from ( SELECT  c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 "
									+ " and d.t20 = (select merchantid from PayTemplateHeader where ProcessingCode = '050200') "
									+ "  And d.n3=4  " + aCriteria + " ) As r ";

						}

					} else if (pCode.equalsIgnoreCase("040300")){
						countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  , CNPMerchant c where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
								+ l_aMerchantID + "' ";
						if (status.equalsIgnoreCase("FAIL")) {
							countSQL += "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '" + 216 + "'"
									+ " And c.status='" + status + "'";
						} else if (status.equalsIgnoreCase("SUCCESS")) {
							countSQL += "And c.status='" + status + "'";
						} else if (status.equalsIgnoreCase("PENDING")) {
							countSQL += "And c.MessageCode='" + 217 + "'";
						}
						countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";
					}

				
					
					PreparedStatement stat = conn.prepareStatement(countSQL);
					System.out.println("Count.." + countSQL);
					ResultSet result = stat.executeQuery();
					result.next();
					int totCount = result.getInt("recCount");
					
					System.out.println("templateData "+templateData);
					if(templateData !=""){
						String data = "";
						System.out.println("data "+data);
						String[] fieldArr = templateData.split("@@");
						ArrayList<PayTemplateDetail> lstTemplate = new ArrayList<PayTemplateDetail>();
						for(int i=0; i<fieldArr.length; i++){
							System.out.println("fieldArr "+i+" "+fieldArr[i]);
							String[] subFieldArr = fieldArr[i].split("!!");
							PayTemplateDetail temData = new PayTemplateDetail();
							for(int k=0; k<subFieldArr.length; k++){// fix length is 3
								
							System.out.println("subFieldArr "+k +" "+subFieldArr[k]);
							String[] keyValue = subFieldArr[k].split("!");
							System.out.println("keyValue.length "+keyValue.length);
							System.out.println("keyValue.0 "+keyValue[0]);
								if(k==0){
									//FieldID
									temData.setFieldID(keyValue[1]);
									
								}else if(k==1){
									//Fieldvalue
									temData.setFieldvalue(keyValue[1]);
								}else if(k==2){
									//DisPayField
									temData.setDisPayField(keyValue[1]);
								}else if(k==3){
									//DisPayField
									if(keyValue.length >1){
									temData.setLovKey(Long.parseLong(keyValue[1]));
									}
								}
							}
							lstTemplate.add(temData);
							
						}
						DispaymentTransactionDao disDao = new DispaymentTransactionDao();
						
						 if("01".equalsIgnoreCase(processingCode)||"03".equalsIgnoreCase(processingCode)){
								for(int i = 0; i< lstTemplate.size(); i++){
									//000 is ALL
									String fValue = "";
									if(!lstTemplate.get(i).getFieldvalue().equals("000")&& lstTemplate.get(i).getFieldID().equals("R4")){
										fValue = disDao.getLovDescriptionByLovKeyandLovCode(lstTemplate.get(i).getLovKey(), lstTemplate.get(i).getFieldvalue(), conn);	
										System.out.println("Lov Desc of "+ lstTemplate.get(i).getFieldvalue()+" "+fValue);									
										 aCriteria += " and d."+lstTemplate.get(i).getDisPayField()+" = '"+fValue+"' ";
									}else if(!lstTemplate.get(i).getFieldvalue().trim().equals("ALL")&& lstTemplate.get(i).getFieldID().equals("R5")){
										
										fValue = disDao.getLovDescriptionByLovKeyandLovCode(lstTemplate.get(i).getLovKey(), lstTemplate.get(i).getFieldvalue(), conn);
										System.out.println("Lov Desc of "+ lstTemplate.get(i).getFieldvalue()+" "+fValue);	
										 aCriteria += " and d."+lstTemplate.get(i).getDisPayField()+" = '"+fValue+"' ";
									}
									
								}
						}
						
					}
					
			String sql = "";
				if(proDisCode.equalsIgnoreCase("01")){// Use Distributor 
					sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,d.CustomerCode AS DID ,dl.DistributorName,"+
							" dl.DistributorAddress from DispaymentTransaction d ,DistributorList dl "+
							 " where d.CustomerCode=dl.DistributorID and d.t20 = '" + l_aMerchantID + "' And d.N2=9 And d.n3=4  " + aCriteria + " )"+
							 " As r WHERE 1=1 order by autokey desc";
				}else{
					if (pCode.equalsIgnoreCase("050200")) {//CNP
						if (status.equalsIgnoreCase("SUCCESS")) {
							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + aCriteria + " And d.n3=4 " 
									+ "and c.ApiStep = 2 and c.Status = 'Success'"
									+ "and c.CustRefNo not in ( select c.CustRefNo from CNPMerchant c where 1=1  " + aCriteria + " and c.ApiStep = 3 and c.Status in ('Approved', 'Rejected','Reversed'))"
									+ ") As r  WHERE 1=1 order by autokey desc";
							
						}else if (status.equalsIgnoreCase("FAIL")) {
							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + aCriteria + " And d.n3=4 " 
									+ "and c.ApiStep = 2 and c.Status = 'Failed'"
									+ ") As r  WHERE 1=1 order by autokey desc";
						}else if (status.equalsIgnoreCase("PENDING")) {
							sql = "select * from ( select ROW_NUMBER() OVER (ORDER BY tbl.autokey desc) AS RowNum,* from (select ROW_NUMBER() OVER (PARTITION BY custrefno ORDER BY c.autokey) as num,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + aCriteria + " And d.n3=4 " 
									+ "and c.ApiStep = 3 and c.Status not in ('Approved', 'Rejected','Reversed')"
									+ "and c.CustRefNo not in (select custrefno from CNPMerchant c where 1=1 and c.TransStatus <> 0 )) tbl WHERE num = 1"
									+ ") As r  WHERE 1=1 order by autokey desc";
						}else if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("REJECTED") || status.equalsIgnoreCase("Reversed")) {
							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + aCriteria  + " And d.n3=4 " 
									+ "and c.ApiStep = 3 and c.Status in ('" + status + "')"
									+ ") As r  WHERE 1=1 order by autokey desc";
						}else{//ALL
							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY t.autokey desc) AS RowNum,*"
									+ " from (SELECT c.messagecode, c.messagedescription, d.*,d.CustomerCode AS DID"
									+ " FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 " + aCriteria +" and d.t20 = "
									+ " (select merchantid from PayTemplateHeader where ProcessingCode = '050200') And d.n3=4 " 
									+ " ) as t"
									+ ") As r  WHERE 1=1 order by autokey desc";
						}
					}else {
						if (status.equalsIgnoreCase("ALL")) {
							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + " And d.n3=4 " + aCriteria
									+ ") As r  WHERE 1=1 order by autokey desc";
						}else if (status.equalsIgnoreCase("PENDING")) {

							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + "And c.MessageCode='" + 217 + "'" + " And d.n3=4 " + aCriteria
									+ ") As r  WHERE 1=1 order by autokey desc";
							

						}else if (status.equalsIgnoreCase("FAIL")) {

							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + "And c.MessageCode <> '" + 217 + "'" + " And c.MessageCode <> '" + 216 + "'" + " And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
									+ ") As r  WHERE 1=1 order by autokey desc";
							

						}  else {

							sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription, d.*,"
									+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and 1=1 and d.t20 = '"
									+ l_aMerchantID + "' " + "And c.status='" + status + "'" + " And d.n3=4 " + aCriteria
									+ ") As r  WHERE 1=1 order by autokey desc";
							

						}
					}			
				}
					System.out.println(sql);
					st = conn.prepareStatement(sql);
					rs = st.executeQuery();

					if (l_aSelect == "Excel" || l_aSelect.equalsIgnoreCase("Excel")) {

						ResultSetMetaData rsmd = rs.getMetaData();
						int col = rsmd.getColumnCount();

						File outputStream = new File(session.getServletContext().getRealPath("/")
								+ "ExcelExport/MerchantList" + param + ".xls");
						WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
						WritableSheet sheet = workBook.createSheet("MerchantList", 0);
						sheet.getSettings().setDefaultColumnWidth(15);
						sheet.getSettings().setDefaultRowHeight(24 * 20);

						/* Generates Headers Cells */
						WritableFont headerFont = new WritableFont(WritableFont.TAHOMA, 16, WritableFont.BOLD);
						WritableFont headerFont1 = new WritableFont(WritableFont.TAHOMA, 14, WritableFont.BOLD);
						WritableFont headerFont2 = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD);
						WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
						WritableCellFormat headerCellFormat1 = new WritableCellFormat(headerFont1);
						WritableCellFormat headerCellFormat2 = new WritableCellFormat(headerFont2);
						headerCellFormat1.setAlignment(Alignment.CENTRE);
						headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
						headerCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
						/* Generates Data Cells */
						WritableFont dataFont = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
						WritableFont dataFont1 = new WritableFont(WritableFont.TAHOMA, 9);
						WritableFont dataFont2 = new WritableFont(WritableFont.TAHOMA, 9);
						WritableCellFormat dataCellFormat = new WritableCellFormat(dataFont);
						WritableCellFormat dataCellFormat1 = new WritableCellFormat(dataFont1);
						WritableCellFormat dataCellFormat2 = new WritableCellFormat(dataFont2);
						WritableCellFormat dataCellFormat3 = new WritableCellFormat(dataFont1);
						dataCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
						dataCellFormat.setAlignment(Alignment.CENTRE);
						dataCellFormat1.setAlignment(Alignment.CENTRE);
						dataCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
						dataCellFormat2.setAlignment(Alignment.RIGHT);
						dataCellFormat2.setVerticalAlignment(VerticalAlignment.CENTRE);
						dataCellFormat3.setAlignment(Alignment.LEFT);
						dataCellFormat3.setVerticalAlignment(VerticalAlignment.CENTRE);
					
						String p_fromDate = "";
						String p_toDate = "";
						if(!l_aFromDate.trim().equals("")){
							try{
								p_fromDate = l_aFromDate.substring(6) + "/" + l_aFromDate.substring(4, 6) + "/"
										+ l_aFromDate.substring(0, 4);
							}catch(Exception e){
								p_fromDate = "";
							}
						}
						if(!l_aToDate.trim().equals("")){
							try{
								p_toDate = l_aToDate.substring(6) + "/" + l_aToDate.substring(4, 6) + "/"
										+ l_aToDate.substring(0, 4);
							}catch(Exception e){
								p_toDate = "";
							}
							
						}

						/*int row = 0; //For Excel Format
						int heightInPoints = 62 * 20;
						sheet.setRowView(row, heightInPoints);
						
						sheet.mergeCells(0, 0, 11, 0); // (start col:,row,last col; row)
						sheet.addCell(new Label(0, row, "Cash Management  Service", headerCellFormat1));
						int row1 = 1;
						int heightInPoints1 = 30 * 20;
						sheet.setRowView(row1, heightInPoints);
						sheet.mergeCells(0, 1, 11, 1);
						sheet.addCell(new Label(0, row1, "CMS Daily Closing Report", headerCellFormat1));

						sheet.mergeCells(0, 2, 8, 4);//Col from , Row From, Col To, Row To
						sheet.addCell(new Label(0, 2, "", headerCellFormat2));//col , row
						sheet.addCell(new Label(11, 2, "From Date", headerCellFormat2));

						int cols = 10;
						int widthInCharss = 2;
						sheet.setColumnView(cols, widthInCharss);
						sheet.addCell(new Label(9, 2, "From Date", headerCellFormat2)); //(start col,row , , )
						sheet.addCell(new Label(cols, 2, ":", headerCellFormat2));

						sheet.addCell(new Label(11, 2, p_fromDate, headerCellFormat2)); 
						sheet.addCell(new Label(9, 3, "To Date", headerCellFormat2));
						sheet.addCell(new Label(10, 3, ":", headerCellFormat2));
						
						sheet.addCell(new Label(11, 3, p_toDate, headerCellFormat2)); 
						sheet.addCell(new Label(9, 4, "Merchant ID", headerCellFormat2));
						sheet.addCell(new Label(10, 4, ":", headerCellFormat2));
						sheet.addCell(new Label(11, 4, l_aMerchantID, headerCellFormat2));*/
						
						int row = 0; //For Excel Format
						int heightInPoints = 62 * 20;
						sheet.setRowView(row, heightInPoints);
						String imgName = ConnAdmin.readExternalUrl("ExcelImageName");
						File imageFile = new File(abspath + "/image/"+imgName);						
						sheet.addImage(new WritableImage(0, 0, 1, 1, imageFile));
						//sheet.mergeCells(0, 0, 4, 0);
						//sheet.addCell(new Label(0, row, "", headerCellFormat));
						sheet.mergeCells(1, 0, 10, 0); // (start col:,row,last col; row)
						sheet.addCell(new Label(1, row, "Bill Payment System", headerCellFormat1));
						int row1 = 1;
						int heightInPoints1 = 30 * 20;
						sheet.setRowView(row1, heightInPoints);
						sheet.mergeCells(0, 1, 10, 1);
						sheet.addCell(new Label(0, row1, "Daily Transaction Report between '"+p_fromDate+"' and '"+p_toDate+"'", headerCellFormat1));

							
						sheet.addCell(new Label(0, 2, "Total Count", headerCellFormat2));
						sheet.addCell(new Label(1, 2, ":", headerCellFormat2));
						sheet.addCell(new Label(2, 2, String.valueOf(totCount), headerCellFormat2));
						
						sheet.addCell(new Label(0, 3, "Merchant ID", headerCellFormat2));
						sheet.addCell(new Label(1, 3, ":", headerCellFormat2));
						sheet.addCell(new Label(2, 3, l_aMerchantID, headerCellFormat2));
						
						sheet.addCell(new Label(0, 4, "Merchant Name", headerCellFormat2));
						sheet.addCell(new Label(1, 4, ":", headerCellFormat2));
						sheet.addCell(new Label(2, 4, merchantName , headerCellFormat2));

						int row5 = 5;
						int heightInPoints5 = 25 * 20;
						int col0 = 0;
						int widthInChars = 16;
						sheet.setColumnView(col0, widthInChars);
						sheet.setRowView(row5, heightInPoints5);
						sheet.addCell(new Label(col0, row5, "Transaction Date", dataCellFormat));

						int col1 = 1;
						int widthInChars1 = 25;
						sheet.setColumnView(col1, widthInChars1);
						sheet.addCell(new Label(1, row5, "Transaction Reference No. ", dataCellFormat));

						int col2 = 2;
						int widthInChars2 = 16;
						sheet.setColumnView(col2, widthInChars2);
						sheet.addCell(new Label(col2, row5, "Debit Account", dataCellFormat));

						int col3 = 3;
						int widthInChars3 = 20;
						sheet.setColumnView(col3, widthInChars3);
						sheet.addCell(new Label(col3, row5, "Collection Account", dataCellFormat));

						int col4 = 4;
						int widthInChars4 = 20;
						sheet.setColumnView(col4, widthInChars4);
						sheet.addCell(new Label(col4, row5, "Customer Code", dataCellFormat));
						
						int col5 = 5;
						int widthInChars5 = 25;
						sheet.setColumnView(col5, widthInChars5);
						sheet.addCell(new Label(col5, row5, "Customer Name", dataCellFormat));
						
						int col6 = 6;
						int widthInChars6 = 20;
						sheet.setColumnView(col6, widthInChars6);
						sheet.addCell(new Label(col6, row5, "Initiated By", dataCellFormat));

						int col7 = 7;
						int widthInChars7 = 16;
						sheet.setColumnView(col7, widthInChars7);
						sheet.addCell(new Label(col7, row5, "Amount", dataCellFormat));

						int col8 = 8;
						int widthInChars8 = 16;
						sheet.setColumnView(col8, widthInChars8);
						sheet.addCell(new Label(col8, row5, "Service Charges", dataCellFormat));

						int col9 = 9;
						int widthInChars9 = 20;
						sheet.setColumnView(col9, widthInChars9);
						sheet.addCell(new Label(col9, row5, "Deduction Amount", dataCellFormat));

						int col10 = 10;
						int widthInChars10 = 25;
						sheet.setColumnView(col10, widthInChars10);
						sheet.addCell(new Label(col10, row5, "TPS Message Code", dataCellFormat));

						int col11 = 11;
						int widthInChars11 = 16;
						sheet.setColumnView(col11, widthInChars11);
						sheet.addCell(new Label(col11, row5, "TPS Message Description", dataCellFormat));

						/*int col12 = 12;
						int widthInChars12 = 16;
						sheet.setColumnView(col12, widthInChars12);
						sheet.addCell(new Label(col12, row5, "", dataCellFormat));
						
						
						 for(int i=0;i<DetailList.size();i++){
							coldtl = i+12;
							sheet.setColumnView(coldtl, 20);
							sheet.addCell(new Label(coldtl, row5, DetailList.get(i).getPayCaption(), dataCellFormat));						
						} */
						
						int coldtl = 0;
						if(proDisCode.equalsIgnoreCase("01")){
							int coldName = coldtl+1;
							sheet.setColumnView(coldName, 25);
							sheet.addCell(new Label(coldName, row5, "Distributor Name", dataCellFormat));
							
							int coldAdd = coldtl+2;
							sheet.setColumnView(coldAdd, 25);
							sheet.addCell(new Label(coldAdd, row5, "Distributor Address", dataCellFormat));
						}
						double grandtotal = 0.0;
						double incomeAmtT = 0.0;
						double deduAmtT = 0.0;
						int rowtotal = 0;
						int heightInPointsg = 0;
						while (rs.next()) {
							int currentCol = 0;
							int grow = rs.getRow() + 5;
							rowtotal = rs.getRow();
							heightInPointsg = 25 * 20;
							sheet.setRowView(grow, heightInPointsg);
							String aTransDate = "";
							aTransDate = rs.getString("TransDate");
							String transDate = "";
							if(!aTransDate.trim().equals("")){
								try{
									transDate = aTransDate.substring(6) + "/"
											+ aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4);
								}catch(Exception e){
									transDate = "";
								}
								
							}
							sheet.addCell(new Label(currentCol++, grow,transDate, dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("FlexRefNo"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("FromAccount"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("ToAccount"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("DID"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("CustomerName"), dataCellFormat3));
							//sheet.addCell(new Label(currentCol++, grow, rs.getString("t16"), dataCellFormat3));
							
							if (rs.getString("MobileUserID").equals("Online")) {
								sheet.addCell(new Label(currentCol++, grow, "Online", dataCellFormat1));
							} else {
								sheet.addCell(new Label(currentCol++, grow, "OTC", dataCellFormat1));
							}
							double T15 = 0.0, T18 = 0.0, amount=0.0;
							 amount = rs.getDouble("Amount");
							
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", amount), dataCellFormat2));
							
							if (!(rs.getString("t15").equals(""))) {
								T15 = Double.valueOf(rs.getString("t15"));
							}
							if (!(rs.getString("t18").equals(""))) {
								T18 = Double.valueOf(rs.getString("t18"));
							}
							
							
							 sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f",T15),dataCellFormat2));
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", T18), dataCellFormat2)); 
							/* double commcharges = 0.0;
							commcharges = T15 - T18;
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", commcharges),
									dataCellFormat2));
							sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", T18), dataCellFormat2)); */
						 	 double totalamt = 0.0;
							totalamt +=  amount;
							grandtotal += totalamt;
							
							
							double incomeAmt = 0.0;
							incomeAmt += T15;
							incomeAmtT += incomeAmt;
						
							
							double deduAmt = 0.0;
							deduAmt += T18;
							deduAmtT += deduAmt; 
							
							//sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", totalamt), dataCellFormat2));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("messagecode"), dataCellFormat1));
							sheet.addCell(new Label(currentCol++, grow, rs.getString("messagedescription"), dataCellFormat1));
							
							if(proDisCode.equalsIgnoreCase("01")){
								sheet.addCell(new Label(currentCol++, grow, rs.getString("DistributorName"), dataCellFormat3));
								sheet.addCell(new Label(currentCol++, grow, rs.getString("DistributorAddress"), dataCellFormat3));
							}
						}

						rowtotal += 6; 
						sheet.setRowView(rowtotal, heightInPointsg);
						sheet.addCell(new Label(6, rowtotal, "Grand Total", dataCellFormat2));
						sheet.addCell(new Label(7, rowtotal, String.format("%,.2f", grandtotal), dataCellFormat2));
						sheet.addCell(new Label(8, rowtotal, String.format("%,.2f", incomeAmtT), dataCellFormat2));
						sheet.addCell(new Label(9, rowtotal, String.format("%,.2f", deduAmtT), dataCellFormat2)); 
						/* Write & Close Excel WorkBook */
						workBook.write();
						workBook.close();

					} else {
						File outputStream = new File(session.getServletContext().getRealPath("/") + "CSV/MerchantList"
								+ param + ".csv");
						boolean alreadyExists = new File("MerchantList.csv").exists();
						try {

							CsvWriter csvOutput = new CsvWriter(new FileWriter(outputStream, true), ',');

							csvOutput.write("Transaction Date");
							csvOutput.write("Transaction Reference No.");
							csvOutput.write("Debit Account");
							csvOutput.write("Collection Account");
							csvOutput.write("Customer Code");
							csvOutput.write("Customer Name");
							//csvOutput.write("Description ");
							csvOutput.write("Initiated By");
							csvOutput.write("Amount");
							csvOutput.write("Income Amount");
							csvOutput.write("Deduction Amount");
						
							csvOutput.write("TPS Message Code");
							csvOutput.write("TPS Message Description");

							for(int i=0;i<DetailList.size();i++){
								csvOutput.write( DetailList.get(i).getPayCaption());					
							}
							if(proDisCode.equalsIgnoreCase("01")){
								csvOutput.write("Distributor Name");
								csvOutput.write("Distributor Address");
							}
							csvOutput.endRecord();

							while (rs.next()) {
								String aTransDate = "";
								aTransDate = rs.getString("TransDate");
								double T15 = 0.0, T18 = 0.0;
								csvOutput.write(aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
										+ aTransDate.substring(0, 4));
								csvOutput.write(rs.getString("FlexRefNo"));
								csvOutput.write(rs.getString("FromAccount"));
								csvOutput.write(rs.getString("ToAccount"));
								//csvOutput.write(rs.getString("DID"));
								csvOutput.write(rs.getString("CustomerCode"));
								csvOutput.write(rs.getString("CustomerName"));
								//csvOutput.write(rs.getString("t16"));
								if (rs.getString("MobileUserID").equals("Online")) {
									csvOutput.write("Online");
								} else {
									csvOutput.write("OTC");
								}

								double amt = rs.getDouble("Amount");

								csvOutput.write(String.format("%,.2f", amt));

								if (!(rs.getString("t15").equals(""))) {
									T15 = Double.valueOf(rs.getString("t15"));
								}
								if (!(rs.getString("t18").equals(""))) {
									T18 = Double.valueOf(rs.getString("t18"));

								}

								double commcharges = 0.0;
								commcharges = T15 - T18;
								csvOutput.write(String.format("%,.2f", commcharges));
								csvOutput.write(String.format("%,.2f", T18));
								double tamount = 0.0;
								tamount = T15 + amt;
								csvOutput.write(String.format("%,.2f", tamount));
								csvOutput.write(rs.getString("messagecode"));
								csvOutput.write(rs.getString("messagedescription"));
								
								 
								if("01".equalsIgnoreCase(proDisCode)){
									csvOutput.write(rs.getString("DistributorName"));
									csvOutput.write(rs.getString("DistributorAddress"));
								}
								csvOutput.endRecord();
								
								
							}
							csvOutput.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				
				
				}
			} catch (Exception e) {
				out.println(e);
				System.out.println("Here Here " + e.getMessage());
				
			}%>	

foo();
function foo()  
{  
  var slt = "<%=select2%>";
		var value =<%=param%>;
		if (slt == "EXCEL") {
			location.href = "./ExcelExport/MerchantList" + value + ".xls";
		} else {
			location.href = "./CSV/MerchantList" + value + ".csv";
		}
	}
</script>

</head>

<body>
	<label>Exported Successfully.</label>

</body>
</html>


