<%@page import="com.csvreader.CsvWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.nio.file.Files"%>
<%@page import="com.nirvasoft.rp.framework.ConnAdmin"%>
<%@page import="jxl.write.Label"%>
<%@page import="jxl.format.Alignment"%>
<%@page import="jxl.format.CellFormat"%>
<%@page import="jxl.format.VerticalAlignment"%>
<%@page import="jxl.write.WritableCellFormat"%>
<%@page import="jxl.write.WritableFont"%>
<%@page import="jxl.write.WritableSheet"%>
<%@page import="jxl.write.WritableWorkbook"%>
<%@page import="java.awt.Window"%>
<%@page import="com.nirvasoft.rp.util.FileUtil"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="jxl.Cell"%>
<%@page import="org.apache.poi.ss.usermodel.Row"%>
<%@page import="jxl.Sheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="jxl.Workbook"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.InputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.export.*"%>
<%@page import="com.nirvasoft.rp.shared.MerchantData"%>
<%@page import="com.nirvasoft.rp.shared.DispaymentTransactionData"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.*"%><%@page
	import="java.io.*"%>

<html>
<head>
<script type="text/javascript">
	
<%Connection conn = null;
			PreparedStatement st = null;
			ResultSet rs = null;

			JRHtmlExporter l_htmlExporter = new JRHtmlExporter();
			StringBuffer l_stringBuffer = new StringBuffer();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			String param = (dateFormat.format(date));
			String select2 = (request.getParameter("aSelect"));
			try {

				String l_aFromDate = "";
				String l_aToDate = "";
				String l_status = "";
				String l_tStatus = "";
				String l_kbzTransID = "";
				String l_transID = "";
				String l_aSelect = "";
				String aSValue = "";

				if (request.getParameter("aFromDate") != null) {
					l_aFromDate = request.getParameter("aFromDate").toString().replaceAll("[-+.^:,]", "");

				}
				if (request.getParameter("aToDate") != null) {
					l_aToDate = request.getParameter("aToDate").toString().replaceAll("[-+.^:,]", "");

				}
				if (request.getParameter("tStatus") != null) {
					l_tStatus = request.getParameter("tStatus");

				}
				if (request.getParameter("status") != null) {
					l_status = request.getParameter("status");

				}
				if (request.getParameter("kbzTransID") != null) {
					l_kbzTransID = request.getParameter("kbzTransID");

				}
				if (request.getParameter("transID") != null) {
					l_transID = request.getParameter("transID");

				}
				if (request.getParameter("aSelect") != null) {
					l_aSelect = request.getParameter("aSelect");

				}

				System.out.println(l_status);
				System.out.println(l_tStatus);

				String aCriteria = "";
				String rspk = "";
				if (!l_kbzTransID.trim().equals("")) {
					aCriteria += " and re.TransRef = '" + l_kbzTransID.trim() + "'";

				}

				if (!l_transID.trim().equals("")) {
					if (l_tStatus.trim().equals("reg")) {
						aCriteria += " and re.RSPInfoKey = '" + l_transID.trim() + "'";
					}
					if (l_tStatus.trim().equals("liq") || l_tStatus.trim().equals("refund")) {
						rspk = l_transID.substring(0, l_transID.length() -3);
						aCriteria += " and re.t4 = '" + rspk.trim() + "'";
					}
				}

				if (l_tStatus.trim().equals("reg")) {
					aSValue = "1";
				}
				if (l_tStatus.trim().equals("liq")) {
					aSValue = "2";
				}
				if (l_tStatus.trim().equals("refund")) {
					aSValue = "3";
				}
				aCriteria += " and re.Status = " + aSValue;
				if (l_status.trim().equals("Success")) {
					aCriteria += " and re.t1 ='" + aSValue + "'";
				} else if (l_status.trim().equals("Fail")) {
					aCriteria += " and re.t1<> '" + aSValue + "'";
				}

				

				String abspath = session.getServletContext().getRealPath("/");

				conn = ConnAdmin.getConn("001", abspath);

				String sql = "SELECT ROW_NUMBER() OVER (ORDER BY r.RSPDate DESC,r.RSPTime DESC) AS RowNum, ";
				if (l_tStatus.trim().equals("reg")) {
					sql += "r.RSPDate,r.RSPTime";
				}
				if (l_tStatus.trim().equals("liq") || l_tStatus.trim().equals("refund")) {
					sql += "re.SysDate,r.SysTime";
				}
				sql += ",re.TransRef,re.RSPInfoKey,r.Status,r.ToCCY,r.Amount,re.t2 "
						+ "from RemitTransfer r join RemitTransferExt re on r.t4 = re.t4 " + "Where ";
				if (l_tStatus.trim().equals("reg")) {
					sql += " r.RSPDate >='" + l_aFromDate + "' AND r.RSPDate <= '" + l_aToDate + "' " + aCriteria;
				}
				if (l_tStatus.trim().equals("liq") || l_tStatus.trim().equals("refund")) {
					sql += " re.SysDate >='" + l_aFromDate + "' AND re.SysDate <= '" + l_aToDate + "' " + aCriteria;
				}

				st = conn.prepareStatement(sql);
				rs = st.executeQuery();

				if (l_aSelect == "Excel" || l_aSelect.equalsIgnoreCase("Excel")) {

					ResultSetMetaData rsmd = rs.getMetaData();
					int col = rsmd.getColumnCount();

					File outputStream = new File(session.getServletContext().getRealPath("/")
							+ "ExcelExport/ReconciliationList" + param + ".xls");
					WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
					WritableSheet sheet = workBook.createSheet("ReconciliationList", 0);
					sheet.getSettings().setDefaultColumnWidth(15);
					sheet.getSettings().setDefaultRowHeight(24 * 20);

					/* Generates Headers Cells */
					WritableFont headerFont = new WritableFont(WritableFont.TAHOMA, 16, WritableFont.BOLD);
					WritableFont headerFont1 = new WritableFont(WritableFont.TAHOMA, 14, WritableFont.BOLD);
					WritableFont headerFont2 = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD);
					WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
					WritableCellFormat headerCellFormat1 = new WritableCellFormat(headerFont1);
					WritableCellFormat headerCellFormat2 = new WritableCellFormat(headerFont2);
					headerCellFormat1.setAlignment(Alignment.CENTRE);
					headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
					headerCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
					/* Generates Data Cells */
					WritableFont dataFont = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
					WritableFont dataFont1 = new WritableFont(WritableFont.TAHOMA, 9);
					WritableFont dataFont2 = new WritableFont(WritableFont.TAHOMA, 9);
					WritableCellFormat dataCellFormat = new WritableCellFormat(dataFont);
					WritableCellFormat dataCellFormat1 = new WritableCellFormat(dataFont1);
					WritableCellFormat dataCellFormat2 = new WritableCellFormat(dataFont2);
					WritableCellFormat dataCellFormat3 = new WritableCellFormat(dataFont1);
					dataCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat.setAlignment(Alignment.CENTRE);
					dataCellFormat1.setAlignment(Alignment.CENTRE);
					dataCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat2.setAlignment(Alignment.RIGHT);
					dataCellFormat2.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat3.setAlignment(Alignment.LEFT);
					dataCellFormat3.setVerticalAlignment(VerticalAlignment.CENTRE);

					if (request.getParameter("aFromDate") != null) {
						l_aFromDate = request.getParameter("aFromDate").toString().replaceAll("[-+.^:,]", "");
					}

					if (request.getParameter("aToDate") != null) {
						l_aToDate = request.getParameter("aToDate").toString().replaceAll("[-+.^:,]", "");
					}

					int row = 0; //For Excel Format
					int heightInPoints = 62 * 20;
					sheet.setRowView(row, heightInPoints);
					sheet.mergeCells(0, 0, 6, 0); // (start col:,row,last col; row)
					sheet.addCell(new Label(0, row, "Reconciliation List", headerCellFormat1));
					int row1 = 1;
					int heightInPoints1 = 30 * 20;
					sheet.setRowView(row1, heightInPoints);
					sheet.mergeCells(0, 1, 6, 1);
					sheet.addCell(new Label(0, row1, "Reconciliation Transaction List", headerCellFormat1));

					int row5 = 2;
					int heightInPoints5 = 25 * 20;

					int col0 = 0;
					int widthInChars2 = 10;
					sheet.setColumnView(col0, widthInChars2);
					sheet.addCell(new Label(col0, row5, "Sr No.", dataCellFormat));

					int col1 = 1;
					int widthInChars = 25;
					sheet.setColumnView(col1, widthInChars);
					sheet.setRowView(row5, heightInPoints5);
					sheet.addCell(new Label(col1, row5, "Transaction Date Time", dataCellFormat));

					int col2 = 2;
					int widthInChars1 = 25;
					sheet.setColumnView(col2, widthInChars1);
					sheet.addCell(new Label(col2, row5, "KBZ Transaction ID ", dataCellFormat));

					int col3 = 3;
					int widthInChars3 = 25;
					sheet.setColumnView(col3, widthInChars3);
					sheet.addCell(new Label(col3, row5, "Tranglo Transaction ID", dataCellFormat));

					int col4 = 4;
					int widthInChars4 = 25;
					sheet.setColumnView(col4, widthInChars4);
					sheet.addCell(new Label(col4, row5, "Transaction Status", dataCellFormat));

					int col5 = 5;
					int widthInChars5 = 20;
					sheet.setColumnView(col5, widthInChars5);
					sheet.addCell(new Label(col5, row5, "Receiving amount", dataCellFormat));

					int col6 = 6;
					int widthInChars6 = 50;
					sheet.setColumnView(col6, widthInChars6);
					sheet.addCell(new Label(col6, row5, "Remarks", dataCellFormat));

					while (rs.next()) {
						int currentCol = 0;
						int grow = rs.getRow() + 2;
						int heightInPointsg = 25 * 20;
						sheet.setRowView(grow, heightInPointsg);
						double amount = rs.getDouble("Amount");
						String aTransDate = "";
						String trTime = "";
						if (l_tStatus.trim().equals("reg")) {
							aTransDate = rs.getString("RSPDate");
							trTime = rs.getString("RSPTime");
						}
						if (l_tStatus.trim().equals("liq") || l_tStatus.trim().equals("refund")) {
							aTransDate = rs.getString("SysDate");
							trTime = rs.getString("SysTime");
						}

						String timedate = aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
								+ aTransDate.substring(0, 4);
						sheet.addCell(new Label(currentCol++, grow, Integer.toString(rs.getInt("RowNum")),
								dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, timedate.concat(" " + trTime), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("TransRef"), dataCellFormat1));

						sheet.addCell(new Label(currentCol++, grow, rs.getString("RSPInfoKey"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, l_status, dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("ToCCY").concat(
								String.format("%,.2f", amount)), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("t2"), dataCellFormat1));
					}
					/* Write & Close Excel WorkBook */
					workBook.write();
					workBook.close();

				} else {
					File outputStream = new File(session.getServletContext().getRealPath("/")
							+ "CSV/ReconciliationList" + param + ".csv");
					boolean alreadyExists = new File("ReconciliationList.csv").exists();
					try {

						CsvWriter csvOutput = new CsvWriter(new FileWriter(outputStream, true), ',');

						csvOutput.write("Sr No.");
						csvOutput.write("Transaction Date Time");
						csvOutput.write("KBZ Transaction ID");
						csvOutput.write("Tranglo Transaction ID");
						csvOutput.write("Transaction Status");
						csvOutput.write("Receiving amount");
						csvOutput.write("Remarks");
						csvOutput.endRecord();

						while (rs.next()) {
							double amount = rs.getDouble("Amount");
							String aTransDate = "";
							String trTime = "";
							if (l_tStatus.trim().equals("reg")) {
								aTransDate = rs.getString("RSPDate");
								trTime = rs.getString("RSPTime");
							}
							if (l_tStatus.trim().equals("liq") || l_tStatus.trim().equals("refund")) {
								aTransDate = rs.getString("SysDate");
								trTime = rs.getString("SysTime");
							}
							String timedate = aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
									+ aTransDate.substring(0, 4);
							csvOutput.write(Integer.toString(rs.getInt("RowNum")));
							csvOutput.write(timedate + " " + trTime);
							csvOutput.write(rs.getString("TransRef"));
							csvOutput.write(rs.getString("RSPInfoKey"));
							csvOutput.write(l_status);
							csvOutput.write(rs.getString("ToCCY").concat(String.format("%,.2f", amount)));
							csvOutput.write(rs.getString("t2"));

							csvOutput.write("");
							csvOutput.endRecord();
						}
						csvOutput.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				out.println(e);
			}%>	

foo();
function foo()  
{  
  var slt = "<%=select2%>";
		var value =
<%=param%>
	;
		if (slt == "EXCEL") {
			location.href = "./ExcelExport/ReconciliationList" + value + ".xls";
		} else {
			location.href = "./CSV/ReconciliationList" + value + ".csv";
		}
	}
</script>

</head>

<body>
	<label>Exported Successfully.</label>

</body>
</html>


