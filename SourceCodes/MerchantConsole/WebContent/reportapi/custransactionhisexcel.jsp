<%@page import="java.util.concurrent.ExecutionException"%>
<%@page import="com.csvreader.CsvWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.nio.file.Files"%>
<%@page import="com.nirvasoft.rp.framework.ConnAdmin"%>
<%@page import="jxl.write.Label"%>
<%@page import="jxl.format.Alignment"%>
<%@page import="jxl.format.CellFormat"%>
<%@page import="jxl.format.VerticalAlignment"%>
<%@page import="jxl.write.WritableCellFormat"%>
<%@page import="jxl.write.WritableFont"%>
<%@page import="jxl.write.WritableSheet"%>
<%@page import="jxl.write.WritableWorkbook"%>
<%@page import="java.awt.Window"%>
<%@page import="com.nirvasoft.rp.util.FileUtil"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="jxl.Cell"%>
<%@page import="org.apache.poi.ss.usermodel.Row"%>
<%@page import="jxl.Sheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="jxl.Workbook"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.InputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.export.*"%>
<%@page import="com.nirvasoft.rp.shared.MerchantData"%>
<%@page import="com.nirvasoft.rp.shared.DispaymentTransactionData"%>
<%@page import="com.nirvasoft.rp.shared.PayTemplateDetail"%>
<%@page import="com.nirvasoft.rp.shared.ServerGlobal"%>
<%@page import="com.nirvasoft.rp.bl.DBDispaymentMgr"%>
<%@page import="com.nirvasoft.rp.shared.PayTemplateDetailArr"%>
<%@page import="com.nirvasoft.rp.util.GeneralUtil"%>
<%@page import="com.nirvasoft.rp.dao.DispaymentTransactionDao"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.*"%><%@page
	import="java.io.*"%>

<html>
<head>
<script type="text/javascript">

	
<%Connection conn = null;
			PreparedStatement st = null;//
			ResultSet rs = null;
			JRHtmlExporter l_htmlExporter = new JRHtmlExporter();
			StringBuffer l_stringBuffer = new StringBuffer();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			//String date2 = dateFormat.format(date);
			String param = (dateFormat.format(date));
			//String select = request.getParameter("aSelect");
			String select2 = (request.getParameter("aSelect"));
			System.out.println("asleect " + select2);
			try {

				String l_aMerchantID = "";
				String l_aCustomerID = "";
				String l_aFromDate = "";
				String l_aToDate = "";
				String l_aSelect = "";
				String l_FromAcc = "";
				String l_ToAcc = "";
				String did = "";
				String initiatedby = "";
				String transrefno = "";
				String templateData = "";
				String processingCode = "";
				String proDisCode = "";
				if (request.getParameter("aMerchantID") != null) {
					l_aMerchantID = request.getParameter("aMerchantID");
				}
				if (request.getParameter("aCustomerID") != null) {
					l_aCustomerID = request.getParameter("aCustomerID");
				}
				if (request.getParameter("aFromDate") != null) {
					l_aFromDate = request.getParameter("aFromDate").toString().replaceAll("[-+.^:,]", "");
				}
				if (request.getParameter("aToDate") != null) {
					l_aToDate = request.getParameter("aToDate").toString().replaceAll("[-+.^:,]", "");
				}
				if (request.getParameter("aSelect") != null) {
					l_aSelect = request.getParameter("aSelect");
				}
				if (request.getParameter("debitaccount") != null) {
					l_FromAcc = request.getParameter("debitaccount");
				}
				if (request.getParameter("did") != null) {
					did = request.getParameter("did");
				}
				if (request.getParameter("initiatedby") != null) {
					initiatedby = request.getParameter("initiatedby");
				}
				if (request.getParameter("transrefno") != null) {
					transrefno = request.getParameter("transrefno");
				}
				if (request.getParameter("templatedata") != null && !request.getParameter("templatedata").equals("")) {
					templateData = request.getParameter("templatedata");
				} //processingCode
				if (request.getParameter("processingCode") != null) {
					processingCode = request.getParameter("processingCode");
				}
				if (request.getParameter("proDisCode") != null) {//proDisCode ==01 is Use Distributor
					proDisCode = request.getParameter("proDisCode");
				}
				String abspath = session.getServletContext().getRealPath("/");
				System.out.println(abspath);
				conn = ConnAdmin.getConn("001", abspath);
				GeneralUtil.readDataConfig();
				String templateMerchant = "";
				if (processingCode.equalsIgnoreCase("02")) {//Just Fortune Ygn merchant is mapped in Paytemplate Setup 
					templateMerchant = ServerGlobal.getFortuneYgnMerchant();
				} else {
					templateMerchant = l_aMerchantID;
				}
				long key = 0;
				String Query = "SELECT SysKey from PayTemplateHeader WHERE MerchantID = ?";
				PreparedStatement pstmt = conn.prepareStatement(Query);
				pstmt.setString(1, templateMerchant);
				ResultSet rs1 = pstmt.executeQuery();
				if (rs1.next()) {
					key = rs1.getLong("SysKey");
				}
				pstmt.close();
				rs1.close();
				//4,5,6
				ArrayList<PayTemplateDetailArr> DetailList = new ArrayList<PayTemplateDetailArr>();
				String query = "select Caption,DataType,DisPayField from PayTemplateDetail "
						+ " where HKey=? and FieldShow in (4,5,6)"
						+ " and DisPayField not in('TransDate','Amount','CustomerCode','T16')";
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, key);
				ResultSet rs2 = pstmt.executeQuery();
				while (rs2.next()) {
					PayTemplateDetailArr Detail = new PayTemplateDetailArr();
					Detail.setPayCaption(rs2.getString("Caption"));
					Detail.setPaydatatype(rs2.getString("DataType"));
					Detail.setPaydispayfield(rs2.getString("DisPayField"));
					DetailList.add(Detail);
				}
				//Preare Criteria for Qurey
				String aCriteria = "";
				if (!l_aFromDate.trim().equals("")) {
					aCriteria += " and d.TransDate >= '" + l_aFromDate.trim() + "'";
				}
				if (!l_aToDate.trim().equals("")) {
					aCriteria += " and d.TransDate <= '" + l_aToDate.trim() + "'";
				}
				if (!l_FromAcc.trim().equals("")) {
					aCriteria += " and FromAccount = '" + l_FromAcc.trim() + "'";
				}
				if (!l_ToAcc.trim().equals("")) {
					aCriteria += " and ToAccount = '" + l_ToAcc.trim() + "'";
				}
				if (!did.trim().equals("")) {
					aCriteria += " and CustomerCode = '" + did.trim() + "'";
				}
				if (!initiatedby.trim().equals("ALL")) {
					if (initiatedby.trim().equals("Online")) {
						aCriteria += " and d.MobileUserID = 'Online'";
					} else if (initiatedby.trim().equals("OTC")) {
						aCriteria += " and d.MobileUserID <> 'Online'";
					}
				}
				if (!transrefno.trim().equals("")) {
					aCriteria += " and d.FlexRefNo = '" + transrefno.trim() + "'";
				}
				System.out.println("templateData " + templateData);
				if (templateData != "") {
					String data = "";
					System.out.println("data " + data);
					String[] fieldArr = templateData.split("@@");
					ArrayList<PayTemplateDetail> lstTemplate = new ArrayList<PayTemplateDetail>();
					for (int i = 0; i < fieldArr.length; i++) {
						System.out.println("fieldArr " + i + " " + fieldArr[i]);
						String[] subFieldArr = fieldArr[i].split("!!");
						PayTemplateDetail temData = new PayTemplateDetail();
						for (int k = 0; k < subFieldArr.length; k++) {// fix length is 3
							System.out.println("subFieldArr " + k + " " + subFieldArr[k]);
							String[] keyValue = subFieldArr[k].split("!");
							System.out.println("keyValue.length " + keyValue.length);
							System.out.println("keyValue.0 " + keyValue[0]);
							if (k == 0) {
								//FieldID
								temData.setFieldID(keyValue[1]);
							} else if (k == 1) {
								//Fieldvalue
								temData.setFieldvalue(keyValue[1]);
							} else if (k == 2) {
								//DisPayField
								temData.setDisPayField(keyValue[1]);
							} else if (k == 3) {
								//DisPayField
								if (keyValue.length > 1) {
									temData.setLovKey(Long.parseLong(keyValue[1]));
								}
							}
						}
						lstTemplate.add(temData);
					}
					DispaymentTransactionDao disDao = new DispaymentTransactionDao();
					if ("01".equalsIgnoreCase(processingCode) || "03".equalsIgnoreCase(processingCode)) {
						for (int i = 0; i < lstTemplate.size(); i++) {
							//000 is ALL
							String fValue = "";
							if (!lstTemplate.get(i).getFieldvalue().equals("000")
									&& lstTemplate.get(i).getFieldID().equals("R4")) {
								fValue = disDao.getLovDescriptionByLovKeyandLovCode(lstTemplate.get(i).getLovKey(),
										lstTemplate.get(i).getFieldvalue(), conn);
								System.out.println("Lov Desc of " + lstTemplate.get(i).getFieldvalue() + " " + fValue);
								aCriteria += " and d." + lstTemplate.get(i).getDisPayField() + " = '" + fValue + "' ";
							} else if (!lstTemplate.get(i).getFieldvalue().trim().equals("ALL")
									&& lstTemplate.get(i).getFieldID().equals("R5")) {

								fValue = disDao.getLovDescriptionByLovKeyandLovCode(lstTemplate.get(i).getLovKey(),
										lstTemplate.get(i).getFieldvalue(), conn);
								System.out.println("Lov Desc of " + lstTemplate.get(i).getFieldvalue() + " " + fValue);
								aCriteria += " and d." + lstTemplate.get(i).getDisPayField() + " = '" + fValue + "' ";
							}

						}
					}

				}
				String sql = "";
				if (proDisCode.equalsIgnoreCase("01")) {// Use Distributor 			
					sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,d.CustomerCode AS DID ,dl.DistributorName,"
							+ " dl.DistributorAddress from DispaymentTransaction d ,DistributorList dl "
							+ " where d.CustomerCode=dl.DistributorID and d.t20 = '" + l_aMerchantID
							+ "' And d.n3=4  " + aCriteria + " )" + " As r WHERE 1=1 order by autokey desc";
				} else {
					sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,c.messagecode, c.messagedescription , d.*,d.CustomerCode AS DID "
							+ " from DispaymentTransaction d  , CNPMerchant c  where d.FlexRefNo = c.TransferRefNo2 and  1=1 ";
							if(!(l_aMerchantID.equalsIgnoreCase("ALL"))){
								sql += " and d.t20 = '" + l_aMerchantID + "' ";
							} 
							// filter customer
							sql += " and d.t24 = '" + l_aCustomerID + "' ";
							sql += " And d.n3=4  " + aCriteria + ") " + " As r WHERE 1=1 order by autokey desc";
				}
				System.out.println(sql);
				st = conn.prepareStatement(sql);
				rs = st.executeQuery();
				if (l_aSelect == "Excel" || l_aSelect.equalsIgnoreCase("Excel")) {
					ResultSetMetaData rsmd = rs.getMetaData();
					int col = rsmd.getColumnCount();
					File outputStream = new File(
							session.getServletContext().getRealPath("/") + "ExcelExport/CusTransactionHis" + param + ".xls");
					WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
					WritableSheet sheet = workBook.createSheet("CusTransactionHis", 0);
					sheet.getSettings().setDefaultColumnWidth(15);
					sheet.getSettings().setDefaultRowHeight(24 * 20);
					/* Generates Headers Cells */
					WritableFont headerFont = new WritableFont(WritableFont.TAHOMA, 16, WritableFont.BOLD);
					WritableFont headerFont1 = new WritableFont(WritableFont.TAHOMA, 14, WritableFont.BOLD);
					WritableFont headerFont2 = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD);
					WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
					WritableCellFormat headerCellFormat1 = new WritableCellFormat(headerFont1);
					WritableCellFormat headerCellFormat2 = new WritableCellFormat(headerFont2);
					headerCellFormat1.setAlignment(Alignment.CENTRE);
					headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
					headerCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
					/* Generates Data Cells */
					WritableFont dataFont = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
					WritableFont dataFont1 = new WritableFont(WritableFont.TAHOMA, 9);
					WritableFont dataFont2 = new WritableFont(WritableFont.TAHOMA, 9);
					WritableCellFormat dataCellFormat = new WritableCellFormat(dataFont);
					WritableCellFormat dataCellFormat1 = new WritableCellFormat(dataFont1);
					WritableCellFormat dataCellFormat2 = new WritableCellFormat(dataFont2);
					WritableCellFormat dataCellFormat3 = new WritableCellFormat(dataFont1);
					dataCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat.setAlignment(Alignment.CENTRE);
					dataCellFormat1.setAlignment(Alignment.CENTRE);
					dataCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat2.setAlignment(Alignment.RIGHT);
					dataCellFormat2.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat3.setAlignment(Alignment.LEFT);
					dataCellFormat3.setVerticalAlignment(VerticalAlignment.CENTRE);
					String p_fromDate = "";
					String p_toDate = "";
					if (!l_aFromDate.trim().equals("")) {
						try {
							p_fromDate = l_aFromDate.substring(6) + "/" + l_aFromDate.substring(4, 6) + "/"
									+ l_aFromDate.substring(0, 4);
						} catch (Exception e) {
							p_fromDate = "";
						}
					}
					if (!l_aToDate.trim().equals("")) {
						try {
							p_toDate = l_aToDate.substring(6) + "/" + l_aToDate.substring(4, 6) + "/"
									+ l_aToDate.substring(0, 4);
						} catch (Exception e) {
							p_toDate = "";
						}
					}
					int row = 0; //For Excel Format
					int heightInPoints = 62 * 20;
					sheet.setRowView(row, heightInPoints);
					//sheet.mergeCells(0, 0, 4, 0);
					//sheet.addCell(new Label(0, row, "", headerCellFormat));
					sheet.mergeCells(0, 0, 11, 0); // (start col:,row,last col; row)
					sheet.addCell(new Label(0, row, "Cash Management  Service", headerCellFormat1));
					int row1 = 1;
					int heightInPoints1 = 30 * 20;
					sheet.setRowView(row1, heightInPoints);
					sheet.mergeCells(0, 1, 11, 1);
					sheet.addCell(new Label(0, row1, "Transaction History Report", headerCellFormat1));

					sheet.mergeCells(0, 2, 8, 4);//Col from , Row From, Col To, Row To
					sheet.addCell(new Label(0, 2, "", headerCellFormat2));//col , row
					sheet.addCell(new Label(11, 2, "From Date", headerCellFormat2));

					int cols = 10;
					int widthInCharss = 2;
					sheet.setColumnView(cols, widthInCharss);
					sheet.addCell(new Label(9, 2, "From Date", headerCellFormat2)); //(start col,row , , )
					sheet.addCell(new Label(cols, 2, ":", headerCellFormat2));

					sheet.addCell(new Label(11, 2, p_fromDate, headerCellFormat2));
					sheet.addCell(new Label(9, 3, "To Date", headerCellFormat2));
					sheet.addCell(new Label(10, 3, ":", headerCellFormat2));
					sheet.addCell(new Label(11, 3, p_toDate, headerCellFormat2));
					sheet.addCell(new Label(9, 4, "Merchant ID", headerCellFormat2));
					sheet.addCell(new Label(10, 4, ":", headerCellFormat2));
					sheet.addCell(new Label(11, 4, l_aMerchantID, headerCellFormat2));

					int row5 = 5;
					int heightInPoints5 = 25 * 20;
					int col0 = 0;
					int widthInChars = 16;
					sheet.setColumnView(col0, widthInChars);
					sheet.setRowView(row5, heightInPoints5);
					sheet.addCell(new Label(col0, row5, "Transaction Date", dataCellFormat));

					int col1 = 1;
					int widthInChars1 = 25;
					sheet.setColumnView(col1, widthInChars1);
					sheet.addCell(new Label(1, row5, "Transaction Reference No. ", dataCellFormat));

					int col2 = 2;
					int widthInChars2 = 16;
					sheet.setColumnView(col2, widthInChars2);
					sheet.addCell(new Label(col2, row5, "Debit Account", dataCellFormat));

					int col3 = 3;
					int widthInChars3 = 20;
					sheet.setColumnView(col3, widthInChars3);
					sheet.addCell(new Label(col3, row5, "Collection Account", dataCellFormat));

					int col4 = 4;
					int widthInChars4 = 20;
					sheet.setColumnView(col4, widthInChars4);
					sheet.addCell(new Label(col4, row5, "Customer Code", dataCellFormat));

					int col5 = 5;
					int widthInChars5 = 25;
					sheet.setColumnView(col5, widthInChars5);
					sheet.addCell(new Label(col5, row5, "Customer Name", dataCellFormat));

					int col6 = 6;
					int widthInChars6 = 20;
					sheet.setColumnView(col6, widthInChars6);
					sheet.addCell(new Label(col6, row5, "Description", dataCellFormat));

					int col7 = 7;
					int widthInChars7 = 16;
					sheet.setColumnView(col7, widthInChars7);
					sheet.addCell(new Label(col7, row5, "Initiated By", dataCellFormat));

					int col8 = 8;
					int widthInChars8 = 16;
					sheet.setColumnView(col8, widthInChars8);
					sheet.addCell(new Label(col8, row5, "Amount", dataCellFormat));
					
					int col9 = 9;
					int widthInChars9 = 16;
					sheet.setColumnView(col9, widthInChars9);
					sheet.addCell(new Label(col9, row5, "TPS Message Code", dataCellFormat));
					
					int col10 = 10;
					int widthInChars10 = 10;
					sheet.setColumnView(col10, widthInChars10);
					sheet.addCell(new Label(col10, row5, "TPS Message Description", dataCellFormat));
					
					int coldtl = 0;
					for (int i = 0; i < DetailList.size(); i++) {
						coldtl = i + 12;
						sheet.setColumnView(coldtl, 20);
						sheet.addCell(new Label(coldtl, row5, DetailList.get(i).getPayCaption(), dataCellFormat));
					}
					if (proDisCode.equalsIgnoreCase("01")) {
						int coldName = coldtl + 1;
						sheet.setColumnView(coldName, 25);
						sheet.addCell(new Label(coldName, row5, "Distributor Name", dataCellFormat));

						int coldAdd = coldtl + 2;
						sheet.setColumnView(coldAdd, 25);
						sheet.addCell(new Label(coldAdd, row5, "Distributor Address", dataCellFormat));
					}
					while (rs.next()) {
						int currentCol = 0;
						int grow = rs.getRow() + 5;
						int heightInPointsg = 25 * 20;
						sheet.setRowView(grow, heightInPointsg);
						String aTransDate = "";
						aTransDate = rs.getString("TransDate");
						String transDate = "";
						if (!aTransDate.trim().equals("")) {
							try {
								transDate = aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
										+ aTransDate.substring(0, 4);
							} catch (Exception e) {
								transDate = "";
							}
						}
						double T15 = 0.0, T18 = 0.0;
						sheet.addCell(new Label(currentCol++, grow, transDate, dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("FlexRefNo"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("FromAccount"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("ToAccount"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("DID"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("CustomerName"), dataCellFormat3));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("t16"), dataCellFormat3));
						if (rs.getString("MobileUserID").equals("Online")) {
							sheet.addCell(new Label(currentCol++, grow, "Online", dataCellFormat1));
						} else {
							sheet.addCell(new Label(currentCol++, grow, "OTC", dataCellFormat1));
						}

						double amount = rs.getDouble("Amount");
						sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", amount), dataCellFormat2));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("messagecode"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("messagedescription"), dataCellFormat1));
						
						if (proDisCode.equalsIgnoreCase("01")) {
							sheet.addCell(
									new Label(currentCol++, grow, rs.getString("DistributorName"), dataCellFormat3));
							sheet.addCell(
									new Label(currentCol++, grow, rs.getString("DistributorAddress"), dataCellFormat3));
						}
					}
					/* Write & Close Excel WorkBook */
					workBook.write();
					workBook.close();
				} else {
					File outputStream = new File(
							session.getServletContext().getRealPath("/") + "CSV/CusTransactionHis" + param + ".csv");
					boolean alreadyExists = new File("CusTransactionHis.csv").exists();
					try {

						CsvWriter csvOutput = new CsvWriter(new FileWriter(outputStream, true), ',');
						csvOutput.write("Transaction Date");
						csvOutput.write("Transaction Reference No.");
						csvOutput.write("Debit Account");
						csvOutput.write("Collection Account");
						csvOutput.write("Customer Code");
						csvOutput.write("Customer Name");
						csvOutput.write("Description ");
						csvOutput.write("Initiated By");
						csvOutput.write("Amount");						
						csvOutput.write("TPS Message Code");
						csvOutput.write("TPS Message Description");
						
						for (int i = 0; i < DetailList.size(); i++) {
							csvOutput.write(DetailList.get(i).getPayCaption());
						}
						if (proDisCode.equalsIgnoreCase("01")) {
							csvOutput.write("Distributor Name");
							csvOutput.write("Distributor Address");
						}
						csvOutput.endRecord();

						while (rs.next()) {
							String aTransDate = "";
							aTransDate = rs.getString("TransDate");
							double T15 = 0.0, T18 = 0.0;
							csvOutput.write(aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/"
									+ aTransDate.substring(0, 4));
							csvOutput.write(rs.getString("FlexRefNo"));
							csvOutput.write(rs.getString("FromAccount"));
							csvOutput.write(rs.getString("ToAccount"));
							//csvOutput.write(rs.getString("DID"));
							csvOutput.write(rs.getString("CustomerCode"));
							csvOutput.write(rs.getString("CustomerName"));
							csvOutput.write(rs.getString("t16"));
							if (rs.getString("MobileUserID").equals("Online")) {
								csvOutput.write("Online");
							} else {
								csvOutput.write("OTC");
							}
							double amt = rs.getDouble("Amount");
							csvOutput.write(String.format("%,.2f", amt));							
							csvOutput.write(rs.getString("messagecode"));
							csvOutput.write(rs.getString("messagedescription"));
							/* for (int i = 0; i < DetailList.size(); i++) {

								if (DetailList.get(i).getPaydatatype().equalsIgnoreCase("lov")
										|| DetailList.get(i).getPaydatatype().equalsIgnoreCase("date")
										|| DetailList.get(i).getPaydatatype().equalsIgnoreCase("text")) {

									switch (DetailList.get(i).getPaydispayfield()) {
										case "FileUploadDateTime" :
											csvOutput.write(rs.getString("FileUploadDateTime"));
											break;
										case "CustomerNumber" :
											csvOutput.write(rs.getString("CustomerNumber"));
											break;
										case "T7" :
											csvOutput.write(rs.getString("T7"));
											break;
										case "T8" :
											csvOutput.write(rs.getString("T8"));
											break;
										case "T10" :
											csvOutput.write(rs.getString("T10"));
											break;
										case "T11" :
											csvOutput.write(rs.getString("T11"));
											break;
										case "T12" :
											csvOutput.write(rs.getString("T12"));
											break;
										case "T13" :
											csvOutput.write(rs.getString("T13"));
											break;
										case "T15" :
											csvOutput.write(rs.getString("T15"));
											break;
										case "T16" :
											csvOutput.write(rs.getString("T16"));
											break;
										case "T17" :
											csvOutput.write(rs.getString("T17"));
											break;
										case "T19" :
											csvOutput.write(rs.getString("T19"));
											break;
										case "T20" :
											csvOutput.write(rs.getString("T20"));
											break;
										case "T21" :
											csvOutput.write(rs.getString("T21"));
											break;
										case "T22" :
											csvOutput.write(rs.getString("T22"));
											break;
										case "T23" :
											csvOutput.write(rs.getString("T23"));
											break;
										case "T24" :
											csvOutput.write(rs.getString("T24"));
											break;
										case "T25" :
											csvOutput.write(rs.getString("T25"));
											break;
									}
								}
								if (DetailList.get(i).getPaydatatype().equalsIgnoreCase("number")
										|| (!(DetailList.get(i).getPaydispayfield().equals("")))) {
									switch (DetailList.get(i).getPaydispayfield()) {
										case "N1" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N1")));
											break;
										case "N7" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N7")));
											break;
										case "N8" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N8")));
											break;
										case "N9" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N9")));
											break;
										case "N17" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N17")));
											break;
										case "N18" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N18")));
											break;
										case "N19" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N19")));
											break;
										case "N20" :
											csvOutput.write(String.format("%,.2f", rs.getInt("N20")));
											break;
										//	case "T18"	: csvOutput.write(String.format("%,.2f", Double.parseDouble(rs.getString("T18"))));break;
									}
								}
							} */

							if ("01".equalsIgnoreCase(proDisCode)) {
								csvOutput.write(rs.getString("DistributorName"));
								csvOutput.write(rs.getString("DistributorAddress"));
							}
							csvOutput.endRecord();
						}
						csvOutput.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				out.println(e);
				System.out.println(e.getMessage());

			}%>	

foo();
function foo()  
{  
  var slt = "<%=select2%>";
		var value = <%=param%>;
		if (slt == "EXCEL") {
			location.href = "./ExcelExport/CusTransactionHis" + value + ".xls";
		} else {
			location.href = "./CSV/CusTransactionHis" + value + ".csv";
		}
	}
</script>
</head>
<body>
	<label>Exported Successfully.</label>
</body>
</html>


