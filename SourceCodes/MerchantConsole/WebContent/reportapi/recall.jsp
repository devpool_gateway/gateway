<%@ page contentType="text/html;charset=windows-1252"%>
 
<%@ page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.Map" %>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.InputStream"%> 
<%@page import="net.sf.jasperreports.engine.JasperRunManager" %>  
<%@page import="net.sf.jasperreports.engine.JasperReport" %>
<%@page import="net.sf.jasperreports.engine.JasperPrint" %>
<%@page import="net.sf.jasperreports.engine.export.*" %>

<%

JRHtmlExporter l_htmlExporter = new JRHtmlExporter();
StringBuffer l_stringBuffer = new StringBuffer();  
String sid="s001";
JasperReport jr;
JasperPrint jp;
try{ 
sid =  request.getParameter("sid");
String abspath=session.getServletContext().getRealPath("/"); 
System.out.println(abspath);
JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource((ResultSet) application.getAttribute(sid));
application.setAttribute(sid, null);
String jrxmlFile = session.getServletContext().getRealPath("/rpt/report1.jrxml");
InputStream input = new FileInputStream(new File(jrxmlFile));
jr = JasperCompileManager.compileReport(input);
HashMap<String, Object> params = new HashMap<String, Object>();
params.put("p1", "This is Parameter Passing");
jp =JasperFillManager.fillReport(jr, params,resultSetDataSource);
net.sf.jasperreports.engine.JasperExportManager.exportReportToPdfFile(jp, abspath +"\\pdf\\"+ sid + ".pdf");

l_htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
l_htmlExporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
l_htmlExporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);	//To Hide Jasper Default Image
l_htmlExporter.setParameter(JRHtmlExporterParameter.OUTPUT_STRING_BUFFER, l_stringBuffer);
l_htmlExporter.exportReport();



}catch(Exception e){
out.println(e);
}
%>
 
	<table>
	<tr><td>
	<%= l_stringBuffer.toString() %>
	</td></tr>
	</table> 