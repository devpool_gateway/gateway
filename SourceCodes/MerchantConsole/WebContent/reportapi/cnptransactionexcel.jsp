<%@page import="java.util.concurrent.ExecutionException"%>
<%@page import="com.csvreader.CsvWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.nio.file.Files"%>
<%@page import="com.nirvasoft.rp.framework.ConnAdmin"%>
<%@page import="jxl.write.Label"%>
<%@page import="jxl.format.Alignment"%>
<%@page import="jxl.format.CellFormat"%>
<%@page import="jxl.format.VerticalAlignment"%>
<%@page import="jxl.write.WritableCellFormat"%>
<%@page import="jxl.write.WritableFont"%>
<%@page import="jxl.write.WritableSheet"%>
<%@page import="jxl.write.WritableWorkbook"%>
<%@page import="java.awt.Window"%>
<%@page import="com.nirvasoft.rp.util.FileUtil"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="jxl.Cell"%>
<%@page import="org.apache.poi.ss.usermodel.Row"%>
<%@page import="jxl.Sheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="jxl.Workbook"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.InputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.export.*"%>
<%@page import="com.nirvasoft.rp.shared.MerchantData"%>
<%@page import="com.nirvasoft.rp.shared.DispaymentTransactionData"%>
<%@page import="com.nirvasoft.rp.shared.PayTemplateDetail"%>
<%@page import="com.nirvasoft.rp.shared.ServerGlobal"%>
<%@page import="com.nirvasoft.rp.bl.DBDispaymentMgr"%>
<%@page import="com.nirvasoft.rp.shared.PayTemplateDetailArr"%>
<%@page import="com.nirvasoft.rp.util.GeneralUtil"%>
<%@page import="com.nirvasoft.rp.dao.DispaymentTransactionDao"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.*"%><%@page
	import="java.io.*"%>

<html>
<head>
<script type="text/javascript">

	
<%Connection conn = null;
			PreparedStatement st = null;//
			ResultSet rs = null;

			JRHtmlExporter l_htmlExporter = new JRHtmlExporter();
			StringBuffer l_stringBuffer = new StringBuffer();
			DBDispaymentMgr dbMgr = new DBDispaymentMgr();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			String param = (dateFormat.format(date));
			String select2 = (request.getParameter("aSelect"));
			System.out.println("asleect " + select2);
			try {

				String accType = "";
				String l_aFromDate = "";
				String l_aToDate = "";
				DecimalFormat df = new DecimalFormat("###,000.00");

				if (request.getParameter("accType") != null) {
					accType = request.getParameter("accType");
				}

				if (request.getParameter("aFromDate") != null) {
					l_aFromDate = request.getParameter("aFromDate").toString().replaceAll("[-+.^:,]", "");
				}
				if (request.getParameter("aToDate") != null) {
					l_aToDate = request.getParameter("aToDate").toString().replaceAll("[-+.^:,]", "");
				}
				
				String abspath = session.getServletContext().getRealPath("/");
				System.out.println(abspath);
				conn = ConnAdmin.getConn("001", abspath);
				GeneralUtil.readDataConfig();
				
			 	long key = 0;
				String aCriteria = "" , sql = "";
				
				if (accType.equalsIgnoreCase("CA") || accType.equalsIgnoreCase("SA") ) {
					aCriteria += " and AccType = '" + accType.trim() + "'"; 			
				}else{
					aCriteria += " "; 
				}

			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey DESC) AS RowNum, "
	                + " drAcc,crAcc,amount,commission,flexcubeId,xref,merchantId,AccType,CreatedDate,t1 from CNPTransaction Where 1=1 " + aCriteria + " And CreatedDate <= '" + l_aToDate.trim() + "' And CreatedDate >= '" + l_aFromDate.trim() + "') AS RowConstrainedResult "
	                + " WHERE ( 1= 1)";
				System.out.println(sql);
				
				st = conn.prepareStatement(sql);
				rs = st.executeQuery();

				if (select2 == "Excel" || select2.equalsIgnoreCase("EXCEL")) {

					ResultSetMetaData rsmd = rs.getMetaData();
					int col = rsmd.getColumnCount();

					File outputStream = new File(session.getServletContext().getRealPath("/")
							+ "ExcelExport/CNPTransactionList" + param + ".xls");
					System.out.println("Excel file name " + outputStream); 
					WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
					WritableSheet sheet = workBook.createSheet("CNPTransactionList", 0);
					sheet.getSettings().setDefaultColumnWidth(15);
					sheet.getSettings().setDefaultRowHeight(24 * 20);

					/* Generates Headers Cells */
					WritableFont headerFont = new WritableFont(WritableFont.TAHOMA, 16, WritableFont.BOLD);
					WritableFont headerFont1 = new WritableFont(WritableFont.TAHOMA, 14, WritableFont.BOLD);
					WritableFont headerFont2 = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD);
					WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
					WritableCellFormat headerCellFormat1 = new WritableCellFormat(headerFont1);
					WritableCellFormat headerCellFormat2 = new WritableCellFormat(headerFont2);
					headerCellFormat1.setAlignment(Alignment.CENTRE);
					headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
					headerCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
					/* Generates Data Cells */
					WritableFont dataFont = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
					WritableFont dataFont1 = new WritableFont(WritableFont.TAHOMA, 9);
					WritableFont dataFont2 = new WritableFont(WritableFont.TAHOMA, 9);
					WritableCellFormat dataCellFormat = new WritableCellFormat(dataFont);
					WritableCellFormat dataCellFormat1 = new WritableCellFormat(dataFont1);
					WritableCellFormat dataCellFormat2 = new WritableCellFormat(dataFont2);
					WritableCellFormat dataCellFormat3 = new WritableCellFormat(dataFont1);
					dataCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat.setAlignment(Alignment.CENTRE);
					dataCellFormat1.setAlignment(Alignment.CENTRE);
					dataCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat2.setAlignment(Alignment.RIGHT);
					dataCellFormat2.setVerticalAlignment(VerticalAlignment.CENTRE);
					dataCellFormat3.setAlignment(Alignment.LEFT);
					dataCellFormat3.setVerticalAlignment(VerticalAlignment.CENTRE);
				
					String p_fromDate = "";
					String p_toDate = "";
					if(!l_aFromDate.trim().equals("")){
						try{
							p_fromDate = l_aFromDate.substring(6) + "/" + l_aFromDate.substring(4, 6) + "/"
									+ l_aFromDate.substring(0, 4);
						}catch(Exception e){
							p_fromDate = "";
						}					
					}
					
					if(!l_aToDate.trim().equals("")){
						try{
							p_toDate = l_aToDate.substring(6) + "/" + l_aToDate.substring(4, 6) + "/"
									+ l_aToDate.substring(0, 4);
						}catch(Exception e){
							p_toDate = "";
						}
						
					}

					int row = 0; //For Excel Format
					int heightInPoints = 62 * 20;
					sheet.setRowView(row, heightInPoints);
					
					sheet.mergeCells(0, 0, 7, 0); // (start col:,row,last col; row)
					sheet.addCell(new Label(0, row, "Bill Payment System", headerCellFormat1));
					int row1 = 1;
					int heightInPoints1 = 30 * 20;
					sheet.setRowView(row1, heightInPoints);
					sheet.mergeCells(0, 1, 7, 1);
					sheet.addCell(new Label(0, row1, "CNP Transaction Report", headerCellFormat1));

					/*sheet.mergeCells(0, 2, 2, 4);//Col from , Row From, Col To, Row To
					sheet.addCell(new Label(0, 2, "", headerCellFormat2));//col , row
					sheet.addCell(new Label(6, 2, "From Date", headerCellFormat2));*/

					int cols = 5; // 10 
					int widthInCharss = 2;
					sheet.setColumnView(cols, widthInCharss);
					
					sheet.addCell(new Label(0, 2, "From Date", headerCellFormat2)); //(start col,row , , )
					sheet.addCell(new Label(1, 2, ":", headerCellFormat2));
					sheet.addCell(new Label(2, 2, p_fromDate, headerCellFormat2));
					
					sheet.addCell(new Label(0, 3, "To Date", headerCellFormat2));
					sheet.addCell(new Label(1, 3, ":", headerCellFormat2));					
					sheet.addCell(new Label(2, 3, p_toDate, headerCellFormat2));
					
					sheet.addCell(new Label(0, 4, "Account Type", headerCellFormat2));
					sheet.addCell(new Label(1, 4, ":", headerCellFormat2));
					sheet.addCell(new Label(2, 4, accType, headerCellFormat2));
					
					/*sheet.addCell(new Label(4, 2, "From Date", headerCellFormat2)); //(start col,row , , )
					sheet.addCell(new Label(cols, 2, ":", headerCellFormat2));
					sheet.addCell(new Label(6, 2, p_fromDate, headerCellFormat2));
					
					sheet.addCell(new Label(4, 3, "To Date", headerCellFormat2));
					sheet.addCell(new Label(cols, 3, ":", headerCellFormat2));					
					sheet.addCell(new Label(6, 3, p_toDate, headerCellFormat2));
					
					sheet.addCell(new Label(4, 4, "Account Type", headerCellFormat2));
					sheet.addCell(new Label(cols, 4, ":", headerCellFormat2));
					sheet.addCell(new Label(6, 4, accType, headerCellFormat2));*/
					
					int row5 = 5;
					int heightInPoints5 = 25 * 20;
					int col0 = 0;
					int widthInChars = 16;
					sheet.setColumnView(col0, widthInChars);
					sheet.setRowView(row5, heightInPoints5);
					sheet.addCell(new Label(col0, row5, "Sr No.", dataCellFormat));
					
					int col1 = 1;
					int widthInChars1 = 25;
					sheet.setColumnView(col1, widthInChars1);
					sheet.addCell(new Label(1, row5, "Batch No.", dataCellFormat));

					int col2 = 2;
					int widthInChars2 = 16;
					sheet.setColumnView(col2, widthInChars2);
					sheet.addCell(new Label(col2, row5, "Customer Ref No.", dataCellFormat));

					int col3 = 3;
					int widthInChars3 = 20;
					sheet.setColumnView(col3, widthInChars3);
					sheet.addCell(new Label(col3, row5, "Debit Account", dataCellFormat));
					
					int col4 = 4;
					int widthInChars4 = 20;
					sheet.setColumnView(col4, widthInChars3);
					sheet.addCell(new Label(col4, row5, "Credit Account", dataCellFormat));
					
					
					sheet.setColumnView(5, widthInChars2);
					sheet.addCell(new Label(5, row5, "Amount", dataCellFormat));

					sheet.setColumnView(6, widthInChars3);
					sheet.addCell(new Label(6, row5, "Account Type ", dataCellFormat));

					sheet.setColumnView(7, widthInChars3);
					sheet.addCell(new Label(7, row5, "Bank Ref No.", dataCellFormat));
					
					double grandtotal = 0.0;
					int rowtotal = 0;
					int heightInPointsg = 0;
					int srno = 0 ;
					
					String custRefNo = "";
					while (rs.next()) {
						int currentCol = 0;
						int grow = rs.getRow() + 5;
						rowtotal = rs.getRow();
						heightInPointsg = 25 * 20;
						sheet.setRowView(grow, heightInPointsg);

						sheet.addCell(new Label(currentCol++, grow, String.valueOf(rowtotal) , dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("t1"), dataCellFormat1));
						custRefNo = dbMgr.getAllCustRefNo(l_aFromDate, l_aToDate, rs.getString("t1"));
						sheet.addCell(new Label(currentCol++, grow, custRefNo, dataCellFormat3));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("drAcc"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("crAcc"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, String.format("%,.2f", rs.getDouble("Amount")), dataCellFormat2));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("accType"), dataCellFormat1));
						sheet.addCell(new Label(currentCol++, grow, rs.getString("flexcubeId"), dataCellFormat1));
						

						double amount=0.0 , totalamt = 0.0;
						amount = rs.getDouble("Amount");
						totalamt +=  amount;
						grandtotal += totalamt;
												
					}

					rowtotal += 6;
					sheet.setRowView(rowtotal, heightInPointsg);
					sheet.addCell(new Label(4, rowtotal, "Grand Total ", dataCellFormat2));
					sheet.addCell(new Label(5, rowtotal, String.format("%,.2f", grandtotal), dataCellFormat2));
					/* Write & Close Excel WorkBook */
					workBook.write();
					workBook.close();

				} else {
					File outputStream = new File(session.getServletContext().getRealPath("/") + "CSV/CNPTransactionList"
							+ param + ".csv");
					boolean alreadyExists = new File("CNPTransactionList.csv").exists();
					try {

						CsvWriter csvOutput = new CsvWriter(new FileWriter(outputStream, true), ',');
						csvOutput.write("Batch No.");
						csvOutput.write(" Customer Ref No.");
						csvOutput.write("Debit Account");
						csvOutput.write("Credit Account");
						csvOutput.write("Amount");
						csvOutput.write("Account Type");
						csvOutput.write("Bank Ref No.");
						csvOutput.endRecord();

						while (rs.next()) {
							String aTransDate = "";
							String custRefNo = dbMgr.getAllCustRefNo(l_aFromDate, l_aToDate, rs.getString("t1"));
							double amt = rs.getDouble("Amount");
							aTransDate = rs.getString("crAcc");
							
							csvOutput.write(rs.getString("t1"));
							csvOutput.write(custRefNo);
							
							csvOutput.write(rs.getString("drAcc"));
							csvOutput.write(rs.getString("crAcc"));
							csvOutput.write(String.format("%,.2f", amt));
							csvOutput.write(rs.getString("accType"));
							csvOutput.write(rs.getString("flexcubeId"));
							csvOutput.endRecord();
						}
						csvOutput.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				out.println(e);
				System.out.println(e.getMessage());
				
			}%>	

foo();
function foo()  
{  
  var slt = "<%=select2%>";
		var value =<%=param%>;
		if (slt == "EXCEL") {
			location.href = "./ExcelExport/CNPTransactionList" + value + ".xls";
		} else {
			location.href = "./CSV/CNPTransactionList" + value + ".csv";
		}
	}
</script>

</head>

<body>
	<label>Exported Successfully.</label>

</body>
</html>


